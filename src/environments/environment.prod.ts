export const environment = {
  production: true,
  fileServer: 'host',
  apiUrl: window["env"]["apiUrl"] || "default",
  fileUrl: window["env"]["fileUrl"] || "default",
  crawlerUrl: window["env"]["crawlerUrl"] || "default",
  aiUrl: window["env"]["aiUrl"] || "default",
  host: window["env"]["host"] || "default",
  catUrl: window["env"]["catUrl"] || "default",
  catApiUrl: window["env"]["catApiUrl"] || "default",
  easyLogin: window["env"]["easyLogin"] || false,
  showAPK: window["env"]["showAPK"] || false,
};