(function(window) {
    window.env = window.env || {};
  
    // Environment variables
    window["env"]["apiUrl"] = "https://crown8899.com/gin-pokergame-api";
    window["env"]["fileUrl"] = "https://crown8899.com/gin-pokergame-file";
    window["env"]["crawlerUrl"] = "https://crown8899.com/flask-crawler-api/flask-crawler-api";
    window["env"]["aiUrl"] = "https://crown8899.com/flask-detect-api/api/v1/pic-catch-all";
    window["env"]["host"] = "crown8899.com/gin-pokergame-file";
    window["env"]["easyLogin"] = false;
    window["env"]["showAPK"] = false;
  })(this);