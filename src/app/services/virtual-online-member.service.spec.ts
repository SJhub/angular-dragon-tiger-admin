import { TestBed } from '@angular/core/testing';

import { VirtualOnlineMemberService } from './virtual-online-member.service';

describe('VirtualOnlineMemberService', () => {
  let service: VirtualOnlineMemberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VirtualOnlineMemberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
