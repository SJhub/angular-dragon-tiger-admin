import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { PlayGame } from '../models/play-game';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class PlayGameService {

  private apiURL = `${environment.apiUrl}/api/v1/play-games`;

  constructor(private http: HttpClient) { }

  getPlayGames() {
    return this.http.get<ApiResponse>(`${this.apiURL}`);
  }

  getPlayGame(id): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}/${id}`);
  }

  getPlayGameByGameNumber(type: number, number: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}-by-game-type-and-number/type/${type}/number/${number}`);
  }

  getPlayGameLastRowByGameTypesId(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}-last-row-by-game-type/${id}`);
  }

  addPlayGame(game: PlayGame): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.apiURL}`, game);
  }

  getPlayGameBetweenDate(type: number, startDate: string, endDate: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}-by-type-between-date/type/${type}/start/${startDate}/end/${endDate}`);
  }

}
