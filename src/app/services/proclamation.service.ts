import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { Proclamation } from '../models/proclamation';

@Injectable({
  providedIn: 'root'
})
export class ProclamationService {

  private baseUrl = `${environment.apiUrl}/api/v1/proclamations`;

  constructor(private http: HttpClient) { }

  getProclamations(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getProclamation(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  getProclamationsByTypeLargerThenDate(id: number, date: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-type-larger-then-date/${id}/date/${date}`);
  }

  addProclamation(proclamation: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, proclamation);
  }

  updateProclamation(proclamation: Proclamation): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${proclamation.id}`, proclamation);
  }

  deleteProclamation(id: number) {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
