import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class ChangeLogService {
  private baseUrl = `${environment.apiUrl}/api/v1/change-logs`;

  constructor(
    private http: HttpClient,
  ) { }

  getChangeLogs(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getChangeLog(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  getChangeLogsByRoleID(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-roles-id/${id}`);
  }

  getChangeLogsByRoleIDAndRole(id: number, role: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-role-and-role-id/role/${role}/roleID/${id}`);
  }

  getChangeLogsBetweenDate(startDate: string, endDate: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-between-date/start/${startDate}/end/${endDate}`);
  }
}
