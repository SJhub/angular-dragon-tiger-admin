import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SystemSetting } from '../models/system-setting';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class SystemService {

  constructor(private http: HttpClient) { }

  private apiURL = `${environment.apiUrl}/api/v1/system-settings`;

  getSystemSettings(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}`);
  }

  getSystemSetting(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}/${id}`);
  }

  updateSystemSetting(systemSetting: SystemSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${systemSetting.id}`, systemSetting);
  }

  updateWashCodeAmount(id: number, wash_code_amount: number, default_member_credit: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/wash-code-amount`, { wash_code_amount, default_member_credit });
  }

  updateDrawResult(id: number, draw_result: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/draw-result`, { draw_result });
  }

  updateResultRenew(renewDrawResult: RenewDrawResult): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${renewDrawResult.id}/draw-result-renew`, renewDrawResult);
  }

  updateOpenRegistration(id: number, open_registration: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/open-registration`, { id, open_registration });
  }

  updateDrawSeconds(id: number, draw_seconds: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/draw-seconds`, { id, draw_seconds });
  }

  updateMemberLoginShowStatus(id: number, member_login_show_status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/member-login-show-status`, { id, member_login_show_status });
  }

  updateAppleLoginShowStatus(id: number, apple_login_show_status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/apple-login-show-status`, { id, apple_login_show_status });
  }

}

export interface RenewDrawResult {
  id: number;
  users_id: number;
  game_types_id: number;
  game_number: number;
  draw_result: string;
}