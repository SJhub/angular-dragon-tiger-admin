import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { User } from '../models/user';
import { AuthenticationService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = `${environment.apiUrl}/api/v1/users`;
  private loginUserID: number;

  constructor(
    private http: HttpClient
  ) {

  }

  /**
   * getUsers - get all user
   */
  getUsers(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  /**
   * getUserById - get user by id
   * @param id - user id
   */
  getUser(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  userSayHello(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-say-hello/${id}`);
  }

  /**
   * 取得代理底下所有分身
   */
  getUserByCloneId(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-clone-id/${id}`);
  }

  /**
   * 取得用戶資料 by parent id
   */
  getUserByParentID(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-parent/${id}`);
  }

  getUserLogin(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-login/${id}`);
  }

  userLogout(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-logout/${id}`);
  }


  /**
   * addUser - add user
   * @param user
   */
  addUser(user: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, user);
  }

  /**
   * updateUser - update user
   * @param user
   */
  updateUser(user: User): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${user.id}`, user);
  }

  updateUserStatus(user: User): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${user.id}/status`, user);
  }

  /**
   * updateUser - update password
   * @param password
   */

  updateUserSelf(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/self`, data);
  }

  updateUserPassword(id: number, old_password: string, password: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/password`, { old_password, password });
  }

  updateUserDecrementCredit(data: any): Observable<ApiResponse> {
    const body = { ...data, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/decrement-credit`, body);
  }

  updateUserIncrementCredit(data: any): Observable<ApiResponse> {
    const body = { ...data, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/increment-credit`, body);
  }

  updateUserDecrementWallet(data: any): Observable<ApiResponse> {
    const body = { ...data, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/decrement-wallet`, body);
  }

  updateUserIncrementWallet(data: any): Observable<ApiResponse> {
    const body = { ...data, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/increment-wallet`, body);
  }

  incrementCreditAndWallet(userID: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${userID}/increment-credit-and-wallet`, { amount, operator_id });
  }

  decrementCreditAndWallet(userID: number, amount: number, operator_id: number): Observable<ApiResponse> {
    const body = { amount, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}/${userID}/decrement-credit-and-wallet`, { amount, operator_id });
  }

  /**
   * deleteUser - delete user
   * @param id
   */
  deleteUser(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
