import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Betamount } from '../models/bet-amount';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class BetAmountService {

  Betamount: Betamount[];
  private baseUrl = `${environment.apiUrl}/api/v1/bet-amounts`;

  constructor(private http: HttpClient) { }

  getBetAmountList(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getBetAmount(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  updateBetAmount(data: Betamount): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}`, data);
  }

  updateBetAmountStatus(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/status`, data);
  }

  updateBetAmountIsAutoBet(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/is-auto-bet`, data);
  }

  addBetAmount(data: Betamount): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, data);
  }
}
