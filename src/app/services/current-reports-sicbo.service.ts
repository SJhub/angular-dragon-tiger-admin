import { Observable, TimeoutError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response';
import { tap, timeout } from 'rxjs/operators';
import { API_TIMEOUT } from '../common/global';
import { HttpInterceptorService } from './http-interceptor.service';
import { TimeOutAlertService } from './time-out-alert.service';

@Injectable({
  providedIn: 'root'
})
export class CurrentReportsSicboService {

  private baseUrl = `${environment.apiUrl}/api/v1/current-reports`;

  constructor(
    private http: HttpClient,
    private timeoutAlertServ: TimeOutAlertService
  ) { }

  getcurrentReportsDragonTiger(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-dragon-tiger`).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }

  getcurrentReportsSicbo(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-sicbo`).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }

  getcurrentReportsBaccarat(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-baccarat`).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }
}
