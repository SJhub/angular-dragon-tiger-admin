import { TestBed } from '@angular/core/testing';

import { ProclamationService } from './proclamation.service';

describe('ProclamationService', () => {
  let service: ProclamationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProclamationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
