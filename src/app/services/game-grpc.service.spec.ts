import { TestBed } from '@angular/core/testing';

import { GameGrpcService } from './game-grpc.service';

describe('GameGrpcService', () => {
  let service: GameGrpcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameGrpcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
