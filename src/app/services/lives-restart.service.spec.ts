import { TestBed } from '@angular/core/testing';

import { LivesRestartService } from './lives-restart.service';

describe('LivesRestartService', () => {
  let service: LivesRestartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LivesRestartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
