import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { BoardMessage } from '../models/board-message';

@Injectable({
  providedIn: 'root'
})
export class BoardMessageService {

  private baseUrl = `${environment.apiUrl}/api/v1/board-messages`;

  constructor(private http: HttpClient) { }

  getBoardMessages(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getBoardMessage(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

}
