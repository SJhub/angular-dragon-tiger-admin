import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { BotMembers } from '../models/bot-member';

@Injectable({
    providedIn: 'root'
})
export class BotMembersService {

    private baseUrl = `${environment.apiUrl}/api/v1/bot-members`;

    constructor(
        private http: HttpClient,
    ) { }

    getBotMembers(): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(`${this.baseUrl}`);
    }

    getBotMembersBySettingID(id: number): Observable<ApiResponse> {
        return this.http.get<ApiResponse>(`${this.baseUrl}-by-auto-donate-settings/${id}`);
    }

    addBotMembersRand(game_types_id: number, auto_donate_settings_id: number, number: number): Observable<ApiResponse> {
        return this.http.post<ApiResponse>(`${this.baseUrl}-rand`, { game_types_id, auto_donate_settings_id, number });
    }

    editBotMember(body: BotMembers) {
        return this.http.put<ApiResponse>(`${this.baseUrl}/${body.id}`, body);
    }

    deleteBotMember(id: number): Observable<ApiResponse> {
        return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
    }

    deleteAllBotMemberBySettingID(id: number): Observable<ApiResponse> {
        return this.http.delete<ApiResponse>(`${this.baseUrl}-by-auto-donate-settings/${id}`);
    }
}
