import { Injectable } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';

const USER_PREFERRED_LANGUAGE = 'user_preferred_language';
@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  language$ = new ReplaySubject<LangChangeEvent>(1);
  translate = this.translateService;

  constructor(private translateService: TranslateService) { }

  setInitState() {
    this.translateService.addLangs(['en', 'zh-tw', 'zh-cn']);
    // 根據使用者的瀏覽器語言設定，如果是中文就顯示中文，否則都顯示英文
    // 繁體/簡體中文代碼都是zh
    let userLang = (this.translate.getBrowserLang().includes('zh')) ? 'zh-tw' : 'en';
    const lastLang = localStorage.getItem(USER_PREFERRED_LANGUAGE);
    lastLang && (userLang = lastLang);
    this.setLang(userLang);
  }

  setLang(lang: string) {
    this.translateService.onLangChange.pipe(take(1)).subscribe(result => {
      this.language$.next(result);
      localStorage.setItem(USER_PREFERRED_LANGUAGE, lang);
    });
    this.translateService.use(lang);
  }

  storeLang(lang: string) {
    localStorage.setItem(USER_PREFERRED_LANGUAGE, lang);
  }
}
