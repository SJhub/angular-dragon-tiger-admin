import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Member } from '../models/member.model';
import { Observable, TimeoutError } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { GameTester } from '../models/game-tester';
import { TimeOutAlertService } from './time-out-alert.service';
import { timeout, tap } from 'rxjs/operators';
import { API_TIMEOUT } from '../common/global';
import { AuthenticationService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  roles: string;
  loginUserID: number;

  private apiURL = `${environment.apiUrl}/api/v1/members`;

  private baseUrl = `${environment.apiUrl}/api/v1/member`;

  private gameUrl = `${environment.apiUrl}/api/v1/game-testers`;

  constructor(
    private http: HttpClient,
    private timeoutAlertServ: TimeOutAlertService,
    private authServ: AuthenticationService
  ) {
    this.roles = localStorage.getItem('authenticatedRoles');
    this.loginUserID = Number(authServ.getUserId());
  }

  /**
   * 取得會員總數
   */
  getMemberTotal(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}-total`);
  }

  getMemberList(): Observable<ApiResponse> {

    // if (!this.roles.includes('ADMIN') || !this.roles.includes('MANAGEMENT')) {
    //   return this.http.get<any>(`${this.apiURL}-by-agent/${this.loginUserID}`);
    // }
    return this.http.get<any>(`${this.apiURL}`);
  }

  getMember(id: number): Observable<ApiResponse> {
    return this.http.get<any>(`${this.apiURL}/${id}`);
  }

  getMemberRamdomPassword(id: number): Observable<ApiResponse> {
    return this.http.get<any>(`${this.apiURL}-random-password-by-id/${id}`);
  }

  setMemberRamdomPassword(id: number): Observable<ApiResponse> {
    return this.http.put<any>(`${this.apiURL}/${id}/random-password`, {});
  }

  getMemberSingleBetTotalByAgentID(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}-single-bet-total-by-agents-id/${id}`);
  }

  /**
   * 取得會員資料 by Agent ID
   */
  getMemberByAgent(id: number) {
    return this.http.get<any>(`${this.apiURL}-by-agent/${id}`);
  }

  /**
   * 以代理ID及開始及結束日期搜尋會員日報表
   * @param data 
   */
  getMemberDailyReportsSearchByTypeAndAgentBetweenDate(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-daily-reports-search-by-type-and-agent-between-date`, data).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }

  /**
   * 以開始及結束日期搜尋會員日報表
   * @param member 
   */
  getMemberDailyReportsSearchByTypeAndMemberBetweenDate(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-daily-reports-search-by-type-and-member-between-date`, data).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }

  addMember(member: Member) {
    return this.http.post<any>(`${this.apiURL}`, member);
  }

  updateMember(member: Member): Observable<ApiResponse> {
    return this.http.put<any>(`${this.apiURL}/${member.id}`, member);
  }

  updateMemberStatus(data: any): Observable<ApiResponse> {
    return this.http.put<any>(`${this.apiURL}/${data.id}/status`, data);
  }

  updateMemberIsAllowPlay(data: any): Observable<ApiResponse> {
    return this.http.put<any>(`${this.apiURL}/${data.id}/is-allow-play`, data);
  }

  updateMemberByAdmin(member: Member) {
    const body = { ...member, operator_id: this.loginUserID };
    return this.http.put<any>(`${this.apiURL}-by-agent/${member.id}`, body);
  }

  updateMemberWashCodeCommissionPercent(washCode: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${washCode.id}/wash-code-commission-percent-and-rebate-percent`, washCode);
  }

  deleteMember(id) {
    return this.http.delete<any>(`${this.apiURL}/${id}`);
  }

  deleteGarbageMember(id) {
    return this.http.delete<any>(`${this.apiURL}/${id}/garbage`);
  }

  getLevelNodeAll(id) {
    return this.http.get<any>(`${this.apiURL}/${id}/level-node-all`);
  }

  cencelByAdmin(member: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${member.id}/cancel-by-admin`, member);
  }

  updateStatus(member: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${member.id}/status`, member);
  }

  kickMember(id: number, game_types_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/kick`, { game_types_id });
  }

  clearGameResultAmount(memberID: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/clear-game-result-amount`, {});
  }

  incrementCredit(memberID: number, amount: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/increment-credit`, { amount });
  }

  decrementCredit(memberID: number, amount: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/decrement-credit`, { amount });
  }

  incrementWallet(memberID: number, amount: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/increment-wallet`, { amount });
  }

  decrementWallet(memberID: number, amount: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/decrement-wallet`, { amount });
  }

  incrementCreditAndWallet(memberID: number, amount: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/increment-credit-and-wallet`, { amount });
  }

  decrementCreditAndWallet(memberID: number, amount: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/decrement-credit-and-wallet`, { amount });
  }

  settlementWallet(memberID: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/settlement`, {});
  }

  clearWallet(memberID: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/clear-wallet`, {});
  }

  clearCredit(memberID: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/clear-credit`, {});
  }

  clearCreditAndWallet(memberID: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${memberID}/clear-credit-and-wallet`, {});
  }
  /**
   * 遊戲測試
   */
  getGameTesters(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.gameUrl}`);
  }

  getGameTester(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.gameUrl}/${id}`);
  }

  addGameTester(body: GameTester): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.gameUrl}`, body);
  }

  editGameTester(body: GameTester): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.gameUrl}/${body.id}`, body);
  }

  // 更新自動下注狀態
  updateMemberAutoBet(id: number, operator_id: number, auto_bet: number, auto_bet_result: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/auto-bet`, { id, auto_bet, operator_id, auto_bet_result });
  }

  settingChangeNotify(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}/${id}/setting-change-notify`);
  }
}
