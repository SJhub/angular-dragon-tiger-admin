import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class ItemTypesService {

  private baseUrl = `${environment.apiUrl}/api/v1/item-types`;

  constructor(private http: HttpClient) { }

  getItemTypesList(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getItemTypes(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  updateItemTypesOdds(item_type_odds: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-all-odds`, { item_type_odds });
  }
}
