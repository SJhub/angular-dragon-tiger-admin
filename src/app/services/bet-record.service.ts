import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class BetRecordService {

  private baseUrl = `${environment.apiUrl}/api/v1/bet-records`;

  constructor(private http: HttpClient) { }

  getBetRecords(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getBetRecord(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  /**
   * 取得押注記錄資料 by 代理 ID
   */
  getBetRecordByGameTypeAndAgent(gameTypeId: number, agentsId: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-game-type-and-agent/type/${gameTypeId}/agent/${agentsId}`);
  }

  /**
   * 取得押注記錄資料 by 會員 ID
   */
  getBetRecordByGameTypeAndMember(gameTypeID: number, memberID: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-game-type-and-member/type/${gameTypeID}/member/${memberID}`);
  }

  /**
   * 以開始及結束日期搜尋押注記錄資料
   */
  getBetRecordsSearchByDate(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-search-by-date`, data);
  }

  /**
   * 以代理ID及開始及結束日期搜尋押注記錄資料
   */
  getBetRecordsSeachByAgentAndDate(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-search-by-agent-and-date`, data);
  }

  getBetRecordByItemTypeAndGameNumber(gameTypeID: number, gameNumber: number, date: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-game-type-and-number-larger-then-date/type/${gameTypeID}/number/${gameNumber}/date/${date}`);
  }

  getBetRecordByGameTypeAndNumber(gameTypeID: number, gameNumber: number, membersID: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-game-type-and-number-with-member/type/${gameTypeID}/number/${gameNumber}/member/${membersID}`);
  }

  getMemberBetRecordByAgentID(gameTypeID: number, gameNumber: number, agentID: number, membersID: number, date: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-game-type-number-agent-member-larger-then-date/type/${gameTypeID}/number/${gameNumber}/agent/${agentID}/member/${membersID}/date/${date}`);
  }

  getResultTotalAmountByDateAndMember(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-result-total-amount-search-by-date-and-member`, data);
  }

  getTotalAmountByDateAndMember(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-total-amount-search-by-date-and-member`, data);
  }

  updateBetRecordStatus(id: number, status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/status`, { status });
  }

  setMembersToUnusual(game_types_id: number, game_number: number, members_id: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-members-to-unusual`, {
      game_types_id, game_number, members_id
    });
  }
}
