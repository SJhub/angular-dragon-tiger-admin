import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { AutoDonateMember } from '../models/auto-donate-member';

@Injectable({
  providedIn: 'root'
})
export class AutoDonateMembersService {
  private baseUrl = `${environment.apiUrl}/api/v1/auto-donate-members`;

  constructor(
    private http: HttpClient,
  ) { }

  getAutoDonateMembers(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getAutoDonateMember(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  addAutoDonateMember(body: AutoDonateMember): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, body);
  }

  editAutoDonateMember(body: AutoDonateMember) {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${body.id}`, body);
  }

  deleteAutoDonateMember(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }

}
