import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { AuthenticationService } from '../services/auth.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    expired$ = new Subject();
    ExpiredStatus = 'expired';

    constructor(
        private injector: Injector,
        private http: HttpClient,
        private authService: AuthenticationService,
    ) {
        this.refreshToken();
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // let authService = this.injector.get(AuthenticationService)
        let tokenizedReq = req.clone({
            setHeaders: {
                // 'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });

        return next.handle(tokenizedReq).pipe(
            tap(
                (event: HttpEvent<any>) => { }, (error: any) => {
                    if (error instanceof HttpErrorResponse) {
                        if (error.status === 401) {
                            if (error.error.msg === '驗證Token失敗') {
                                this.expired$.next(this.ExpiredStatus);
                            }
                        }
                    }
                }
            )
        );
    }

    refreshToken() {
        this.expired$.pipe(
            debounceTime(1200),
            // switchMap(() => {
            //     const authRefreshToken = this.authService.getRefreshToken();
            //     return this.http.post(`${environment.apiUrl}/api/v1/auth/refresh`, { refresh_token: authRefreshToken });
            // })
        ).subscribe(
            // (res: ApiResponse) => {
            //     alert(`${res.code} - 登入逾時，系統將自動更新頁面!`);
            //     this.authService.setTokenRefreshed(res.data.access_token, res.data.refresh_token);
            //     window.location.reload();
            // },
            () => {
                // alert(`登入超時，系統將自動登出頁面!`);
                this.authService.logoutUser(false);
            }
        );
    }
}
