import { TestBed } from '@angular/core/testing';

import { EnumTranslateService } from './enum-translate.service';

describe('EnumTranslateService', () => {
  let service: EnumTranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnumTranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
