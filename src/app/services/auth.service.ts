import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, TimeoutError } from 'rxjs';
import { tap, timeout } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { RouteManagement } from '../models/RouteManagement';
import { ApiResponse } from '../models/api-response';
import { Authority } from '../models/authority';
import { UserRole } from '../enum/user-role.enum';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from './language.service';
import { UserService } from './user.service';
import { UserLogin } from '../models/user-login';
import { API_TIMEOUT } from '../common/global';
import { HttpInterceptorService } from './http-interceptor.service';
import { TimeOutAlertService } from './time-out-alert.service';
import { AgentAuthority } from '../enum/agent-authority.enum';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
    USER_NAME_SESSION_ACCESS_TOKEN = 'authUserAccessToken';
    USER_NAME_SESSION_REFRESH_TOKEN = 'authUserRefreshToken';
    USER_NAME_SESSION_ATTRIBUTE_ID = 'authenticatedID';
    USER_NAME_SESSION_ATTRIBUTE_CLONE_ID = 'authenticatedCloneID';
    USER_NAME_SESSION_ATTRIBUTE_ROLES = 'authenticatedRoles';
    USER_NAME_SESSION_ATTRIBUTE_IDENTITY = 'loginUserIdentity';
    USER_NAME_SESSION_USER_LOGIN = 'userlogindata';
    USER_NAME_SESSION_DEVICE_LOGIN_AT = 'deviceLoginAt';
    user: {};

    gotoPath = '/appadmin';

    cloneID: number;

    constructor(
        private languageServ: LanguageService,
        private translateService: TranslateService,
        private timeoutAlertServ: TimeOutAlertService,
        private userServ: UserService,
        private http: HttpClient,
        private _router: Router
    ) { }

    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    registerSuccess(user) {
        sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, user.name)
    }

    getServerInfo() {
        this.http.get<any>(`${environment.apiUrl}/api/v1/file-server-info/1`).subscribe(
            res => {
                let host = res["data"]["host"];
                let localhost = res["data"]["localhost"];
                let pic_path = res["data"]["pic_path"];
                localStorage.setItem("serverinfoHost", host);
                localStorage.setItem("serverinfoLocalhost", localhost);
                localStorage.setItem("serverinfoPath", pic_path);
            });
    }

    async saveUserData(id: string, account: string, accessToken: string, refreshToken: string) {
        localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_ID, id);
        localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, account);
        localStorage.setItem(this.USER_NAME_SESSION_ACCESS_TOKEN, accessToken);
        localStorage.setItem(this.USER_NAME_SESSION_REFRESH_TOKEN, refreshToken);


        await this.userServ.getUserLogin(Number(id)).toPromise().then(r => {
            const userLogin: UserLogin = r.data;
            localStorage.setItem(this.USER_NAME_SESSION_USER_LOGIN, JSON.stringify(userLogin));
            localStorage.setItem('last_login_at', userLogin.last_login_at);
        });

        await this.http.get<any>(`${environment.apiUrl}/api/v1/users/${id}`).toPromise().then(
            res => {
                const data = res.data as User;

                localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_ID, String(data.id));
                localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_IDENTITY, String(data.identity));

                if (data.clone_id) {
                    localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_CLONE_ID, String(data.clone_id));
                }

                localStorage.setItem(this.USER_NAME_SESSION_DEVICE_LOGIN_AT, data.last_login_at);
                // console.log(data);
                if (data.identity === UserRole.admin) {
                    localStorage.setItem('operationitemsid', null);
                    this.getRouteID();
                    return;
                }

                if (data.identity === UserRole.dealers) {
                    return this._router.navigate(['dealers/operation']);
                }

                if (data.identity === UserRole.staff) {
                    return this._router.navigate(['system/timely-bet']);
                }

                if (data.identity === UserRole.topAgent || data.identity === UserRole.agent) {
                    return this._router.navigate(['/appadmin']);
                }

                const operation_itemID = JSON.parse(data.operation_items) as Array<string>;
                operation_itemID.push('0');
                localStorage.setItem('operationitemsid', JSON.stringify(operation_itemID));


                this.getRouteID();
            }, () => alert('登入失敗(直播主無法登入)')
        );
    }

    getRouteID() {
        const operation_itemID = localStorage.getItem('operationitemsid');
        // console.log(operation_itemID)
        // console.log('-----')

        if (operation_itemID !== 'null') {
            let operationID: number[] = JSON.parse(operation_itemID);
            operationID = operationID.map(e => Number(e));
            operationID = operationID.filter(e => e != AgentAuthority.OperationItemAuthority).sort((a, b) => a - b);
            if (operationID.length > 0) {
                let tmp = operationID.filter(e => e != 0)[0];
                this.gotoPath = '/' + Authority.authorityManagement(tmp);
            }
        }
        this._router.navigate([this.gotoPath]);
    }

    registerUser(user) {
        return this.http.post<any>(`${environment.apiUrl}/api/v1/login`, user, { responseType: 'text' as 'json' });
    }

    loginUser(user, password) {
        let body = {
            'name': user,
            'password': password
        };
        return this.http.post<any>(`${environment.apiUrl}/api/v1/auth`, body)
            .pipe(
                timeout(API_TIMEOUT),
                tap(
                    res => {
                        const data = res['data'];
                        const token = data['token'];
                        this.saveUserData(
                            data['id'],
                            user,
                            token['access_token'],
                            // token['token'],
                            token['refresh_token']
                        );
                        this.getServerInfo();
                        // console.log(res)
                    },
                    err => {
                        if (err instanceof TimeoutError) {
                            this.timeoutAlertServ.timeout$.next('timeout');
                        }
                    })
                // catchError(this.handleError(`loginUser: ${user}`))
            );
    }


    getUserName(id) {
        return this.http.get<any>(`${environment.apiUrl}/api/v1/users/${id}`)
            .pipe(
                tap(
                    res => {
                        const real_name = res['data']['real_name'];
                        const user_role = res['data']['roles'];
                        localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, real_name);
                        localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_ROLES, user_role);
                    })
            );
    }

    loggedIn() {
        return !!localStorage.getItem(this.USER_NAME_SESSION_ACCESS_TOKEN);
    }

    getToken() {
        return localStorage.getItem(this.USER_NAME_SESSION_ACCESS_TOKEN);
    }

    logoutUser(confirmCheck: boolean = true) {
        if (window.innerWidth > 991 && confirmCheck) {
            if (confirm("確定要登出?")) {
                localStorage.clear();
                this._router.navigate(['login']);
            }
        } else {
            localStorage.clear();
            window.location.reload();
            // this._router.navigate(['login']);
        }
    }

    logout() {
        const id = Number(this.getUserId());
        this.userServ.userLogout(id).subscribe();
        localStorage.clear();
        this.user = {
            name: '',
            password: ''
        };
        this.languageServ.storeLang(this.translateService.currentLang);
    }

    getRefreshToken() {
        return localStorage.getItem(this.USER_NAME_SESSION_REFRESH_TOKEN);
    }

    setTokenRefreshed(token: string, refresh_token: string) {
        localStorage.setItem(this.USER_NAME_SESSION_ACCESS_TOKEN, token);
        localStorage.setItem(this.USER_NAME_SESSION_REFRESH_TOKEN, refresh_token);
    }

    isUserLoggedIn() {
        let user = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
        return user === null ? false : true;
    }

    isRoleAdmin(): boolean {
        let roles = this.getUserRoles();
        if (roles.indexOf("ROLE_ADMIN") != -1) return true;
        else return false;
    }

    getLoggedInUserName() {
        let user = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
        return user === null ? '' : user;
    }

    getLoggedInToken() {
        let token = localStorage.getItem(this.USER_NAME_SESSION_ACCESS_TOKEN);
        return token === null ? '' : token;
    }

    getUserId() {
        let id = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_ID);
        return id === null ? '' : id;
    }

    getUserCloneId() {
        let id = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_CLONE_ID);
        return id === null ? '' : id;
    }

    getUserRoles() {
        let roles = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_ROLES);
        return roles === null ? '' : roles;
    }

    getUserIdentity() {
        let identity = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_IDENTITY);
        return identity === null ? '' : identity;
    }

    getUserLogin() {
        let data = localStorage.getItem(this.USER_NAME_SESSION_USER_LOGIN);
        return data;
    }

    getDeviceLoginAt() {
        let data = localStorage.getItem(this.USER_NAME_SESSION_DEVICE_LOGIN_AT);
        return data;
    }
}
