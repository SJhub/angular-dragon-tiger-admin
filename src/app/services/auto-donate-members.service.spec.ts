import { TestBed } from '@angular/core/testing';

import { AutoDonateMembersService } from './auto-donate-members.service';

describe('AutoDonateMembersService', () => {
  let service: AutoDonateMembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutoDonateMembersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
