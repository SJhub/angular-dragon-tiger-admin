import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, TimeoutError } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { Agent } from '../models/agent';
import { TimeOutAlertService } from './time-out-alert.service';
import { timeout, tap } from 'rxjs/operators';
import { API_TIMEOUT } from '../common/global';
import { AuthenticationService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  private baseUrl = `${environment.apiUrl}/api/v1/agents`;
  private apiUrl = `${environment.apiUrl}/api/v1/agent`;

  private loginUserID: number;

  constructor(
    private http: HttpClient,
    private authServ: AuthenticationService,
    private timeoutAlertServ: TimeOutAlertService
  ) {
    this.loginUserID = Number(this.authServ.getUserId());
  }

  /**
   * 取得代理總數
   */
  getAgentTotal(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-total`);
  }

  /**
   * 取得代理底下的代理總數
   */
  getAgentChildsIdByAgentsId(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-child-ids-by-agents-id/${id}`);
  }

  /**
   * 取得代理底下所有id
   */
  getAgentTotalByAgentId(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-total-count-by-agents-id/${id}`);
  }

  /**
 * 取得代理上線所有id
 */
  getAgentParentsIdByAgentsId(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-parent-ids-by-agents-id/${id}`);
  }

  /**
   * 以上級代理ID 及開始及結束日期搜尋代理日報表
   */
  getAgentDailyReportsSearchByTypeAndParentBetweenDate(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.apiUrl}-daily-reports-search-by-type-and-parent-between-date`, data).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }

  /**
   * 以開始及結束日期搜尋代理日報表
   */
  getAgentDailyReportsSearchByTypeAndAgentBetweenDate(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.apiUrl}-daily-reports-search-by-type-and-agent-between-date`, data).pipe(
      timeout(API_TIMEOUT),
      tap({
        error: err => {
          if (err instanceof TimeoutError) {
            this.timeoutAlertServ.timeout$.next('timeout');
          }
        }
      })
    );
  }

  getAgentRamdomPassword(id: number): Observable<ApiResponse> {
    return this.http.get<any>(`${this.baseUrl}-random-password-by-id/${id}`);
  }

  getAgentSingleBetTotalByParentID(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-single-bet-total-by-parents-id/${id}`);
  }

  /**
   * addAgent - add agent
   * @param agent
   */
  addAgent(agent: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, agent);
  }

  /**
   * addTopAgent - add top agent
   * @param agent
   */
  addTopAgent(agent: Agent): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-top`, agent);
  }

  IncrementAgentRevenueShare(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/increment-revenue-share`, data);
  }

  /**
   * updateTopAgent - update top agent
   * @param agent
   */
  updateTopAgent(agent: Agent): Observable<ApiResponse> {
    const body = { ...agent, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}-top/${agent.id}`, body);
  }

  /**
   * updateAgent - update agent
   * @param agent
   */
  updateAgent(agent: Agent): Observable<ApiResponse> {
    const body = { ...agent, operator_id: this.loginUserID };
    return this.http.put<ApiResponse>(`${this.baseUrl}/${agent.id}`, body);
  }

  updateAgentIsAllowPlay(agent: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${agent.id}/is-allow-play`, agent);
  }

  updateAgentStatus(agent: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${agent.id}/status`, agent);
  }

  updateAgentWashCodeCommissionPercent(washCode: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${washCode.id}/wash-code-commission-percent`, washCode);
  }

  deleteAgent(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  clearAgentWallet(id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/clear-wallet-and-its-childs`, { id, operator_id: this.loginUserID });
  }

  clearAgentCredit(id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/clear-credit-and-its-childs`, { id, operator_id: this.loginUserID });
  }

  clearAgentCreditAndWallet(id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/clear-credit-and-wallet-and-its-childs`, { id, operator_id: this.loginUserID });
  }

  updateAgentPasswordFirst(id: number, new_password: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/password-first`, { new_password });
  }

  // 檢查邀請碼是否存在
  getByInvitationCode(code: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-invitation-code/${code}`);
  }
}
