import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response';
import { StreamersMember } from '../models/streamers-member';

@Injectable({
  providedIn: 'root'
})
export class StreamersMemberService {

  private baseUrlStreamMember = `${environment.apiUrl}/api/v1/streamers-member`;

  private baseUrlStreamUser = `${environment.apiUrl}/api/v1/streamers-user`;

  private baseUrlStream = `${environment.apiUrl}/api/v1/streamers`;

  constructor(private http: HttpClient) { }

  addStreamersMember(data: StreamersMember): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrlStreamMember}`, data);
  }

  addStreamersUser(data: StreamersMember): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrlStreamUser}`, data);
  }

  updateStreamersMember(data: StreamersMember): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrlStreamMember}-by-account/${data.account}`, data);
  }

  updateStreamersUser(data: StreamersMember): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrlStreamUser}/${data.id}`, data);
  }

  deleteStreamer(account: string): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrlStream}-by-account/${account}`);
  }
}
