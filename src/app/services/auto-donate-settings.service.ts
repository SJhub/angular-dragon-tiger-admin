import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { AutoDonateSetting } from '../models/auto-donate-setting';

@Injectable({
  providedIn: 'root'
})
export class AutoDonateSettingsService {
  private baseUrl = `${environment.apiUrl}/api/v1/auto-donate-settings`;

  constructor(private http: HttpClient) { }

  getAutoDonateSettings(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getAutoDonateSetting(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  addAutoDonateSetting(body: AutoDonateSetting): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, body);
  }

  editAutoDonateSetting(body: AutoDonateSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${body.id}`, body);
  }

  deleteAutoDonateSetting(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }

}
