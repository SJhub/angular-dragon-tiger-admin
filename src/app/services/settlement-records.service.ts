import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class SettlementRecordsService {
  private baseUrl = `${environment.apiUrl}/api/v1/settlement-records`;

  constructor(private http: HttpClient) { }

  getSettlementRecords(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getSettlementRecord(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  getSettlementByFromAgent(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-from-agent/${id}`);
  }

  getSettlementByToAgent(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-to-agent/${id}`);
  }
}
