import { TestBed } from '@angular/core/testing';

import { MemberWalletService } from './member-wallet.service';

describe('MemberWalletService', () => {
  let service: MemberWalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemberWalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
