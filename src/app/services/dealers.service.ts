import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Dealers } from '../models/dealers';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class DealersService {

  private baseUrl = `${environment.apiUrl}/api/v1/dealers`;
  private staffUrl = `${environment.apiUrl}/api/v1/staffs`;

  constructor(private http: HttpClient) { }

  addDealers(data: Dealers): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, data);
  }

  updateDealers(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}`, data);
  }

  addStaff(data: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.staffUrl}`, data);
  }

  updateStaff(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.staffUrl}/${data.id}`, data);
  }
}
