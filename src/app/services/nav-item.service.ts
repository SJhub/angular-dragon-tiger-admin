import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { defaultAgentOperation } from '../common/global';
import { AgentAuthority } from '../enum/agent-authority.enum';
import { OperationItems } from '../enum/operation-items.enum';
import { UserRole } from '../enum/user-role.enum';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class NavItemService {

  constructor(
    private translateService: TranslateService
  ) {
  }

  getNavItemsByManageIDs(user: User) {
    let defaultNavItems = this.getNavItems();
    let operationItems = [];

    // 非代理&超級代理 移除項目
    if (!(user.identity === UserRole.agent || user.identity === UserRole.topAgent)) {
      defaultNavItems = defaultNavItems.filter(e => !e.attributes?.onlyAgent);
    }

    // 最高權限且非clone返回預設
    if (user.identity === UserRole.admin && !user.clone_id) {
      // 帳戶管理改變名稱
      const index = defaultNavItems.findIndex(e => e.url === '/system/clone-agent');
      index != -1 && (defaultNavItems[index].name = this.translateService.instant('nav.cloneAgentAdmin'));
      return defaultNavItems;
    }

    // 過濾帳戶權限列表
    defaultNavItems = defaultNavItems.filter(e => e.attributes?.manageID !== AgentAuthority.OperationItemAuthority)

    try {
      operationItems = JSON.parse(user.operation_items);
      if (user.identity != UserRole.admin && user.identity != UserRole.management) {
        operationItems = operationItems.filter(e => Number(e) !== AgentAuthority.OperationItemUser);
      }
    } catch (error) {
      return defaultNavItems;
    }

    // 代理&超級代理 預設代理管理&代理分身
    if ((user.identity === UserRole.agent || user.identity === UserRole.topAgent)) {
      if (user.clone_id) return this.getCloneNavItems();
      operationItems = operationItems.filter(e => defaultAgentOperation.includes(e));
      operationItems.push(AgentAuthority.OperationItemMyAgents);
      operationItems.push(AgentAuthority.OperationItemMyClones);

      // 總代不可新增會員
      if (user.identity === UserRole.agent) operationItems.push(AgentAuthority.OperationItemMyMembers);

    }

    // 工作人員 預設功能
    if (user.identity === UserRole.staff) {
      operationItems = this.getStaffOperations();
    }

    // 過濾可用功能
    let navItems = defaultNavItems.filter(nav => {
      if (nav.attributes && nav.attributes.manageID) {
        const manageID = Number(nav.attributes.manageID);
        return operationItems.some(e => Number(e) === manageID);
      }
      return true;
    });


    // 總代理隱藏會員報表
    if (user.identity === UserRole.topAgent) navItems = navItems.filter(e => e.url !== '/system/report-record-member');

    // 工作人員&管理者DEMO去掉代理公告
    if (user.identity === UserRole.staff || (user.identity === UserRole.admin && user.clone_id)) {
      navItems = navItems.filter(e => e.url !== '/system/insite-proclamations');
    }

    // 過濾群組標題
    navItems = navItems.filter((nav, index) => {
      if (nav.title) {
        if ((index + 1) < navItems.length) {
          return !navItems[index + 1].title;
        }
        return false;
      }
      return true;
    });


    return navItems;
  }

  getStaffOperations() {
    const operations = [
      AgentAuthority.OperationItemTimelyBet,
      AgentAuthority.OperationItemProclamations,
      AgentAuthority.OperationItemNormalProclamations
    ];
    return operations;
  }

  getNavItems() {
    return [
      {
        name: '個人資料',
        url: '/my-account',
        // icon: 'fa fa-user'
      },
      {
        name: this.translateService.instant('nav.manageAgent'),
        url: '/users',
        icon: 'fa fa-user-circle-o',
        attributes: {
          manageID: AgentAuthority.OperationItemUser
        }
      },
      // {
      //   name: this.translateService.instant('nav.manageMember'),
      //   url: '/members',
      //   icon: 'fa fa-user-o',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemRegistrationMembers
      //   }
      // },
      {
        name: '帳號管理',
        url: '/my-agents',
        // icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemMyAgents,
          onlyAgent: true,
        }
      },
      {
        name: '報表記錄',
        url: '/system/report-record-agent',
        // icon: 'icon-menu',
        attributes: {
          onlyAgent: true,
        }
      },
      {
        name: this.translateService.instant('nav.drawSettings'),
        url: '/system/draw-settings',
        // icon: 'icon-menu',
        attributes: {
          onlyAgent: true,
        }
      },
      {
        name: '子帳號',
        url: '/my-clones',
        // icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemMyClones,
          onlyAgent: true,
        }
      },
      {
        title: true,
        name: this.translateService.instant('nav.recordSearching')
      },
      {
        name: this.translateService.instant('nav.reportRecordList'),
        url: '/system/report-record-list',
        icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemReportRecordList
        }
      },
      // {
      //   name: this.translateService.instant('nav.advancedSearching'),
      //   url: '/system/search-result',
      //   icon: 'icon-menu',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemSearchResultList
      //   }
      // },
      // {
      //   name: this.translateService.instant('nav.donateRecordList'),
      //   url: '/system/donate-record-list',
      //   icon: 'icon-menu',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemDonateList
      //   }
      // },
      {
        name: this.translateService.instant('nav.gameRecordList'),
        url: '/system/calc-item-result-record-list',
        icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemcalcItemResultList
        }
      },
      {
        name: this.translateService.instant('nav.renewDrawRecord'),
        url: '/system/renew-draw-result-record',
        icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemRenewDrawResultRecord
        }
      },
      {
        name: this.translateService.instant('nav.lotteryStatistics'),
        url: '/system/lottery-statistics',
        icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemLotteryStatistics
        }
      },
      {
        name: 'VIP記錄',
        url: '/system/vip-records',
        icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemVipRecords
        }
      },
      //   {
      //     name: this.translateService.instant('nav.betRecordList'),
      //     url: '/system/bet-record-list',
      //     icon: 'icon-menu',
      //     attributes: {
      //       manageID: AgentAuthority.OperationItemBetRecordList
      //     }
      //   },
      // {
      //   name: '報表校正',
      //   url: '/system/re-calc-report',
      //   icon: 'icon-menu',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemReCalcReport
      //   }
      // },
      {
        title: true,
        name: this.translateService.instant('nav.systemSetting')
      },
      // {
      //   name: this.translateService.instant('nav.croupierOperation'),
      //   url: '/dealers/operation',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemCroupierOperation
      //   }
      // },
      {
        name: this.translateService.instant('nav.drawSettings'),
        url: '/system/draw-settings',
        icon: 'icon-menu',
        attributes: {
          manageID: AgentAuthority.OperationItemDrawSettings
        }
      },
      {
        name: this.translateService.instant('nav.timelyBet'),
        url: '/system/timely-bet',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemTimelyBet
        }
      },
      // {
      //   name: this.translateService.instant('nav.singleItems'),
      //   url: '/system/single-items',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemSingleItems
      //   }
      // },
      {
        name: this.translateService.instant('nav.betAmounts'),
        url: '/system/bet-amounts',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemBetAmounts
        }
      },
      {
        name: this.translateService.instant('nav.otherSettings'),
        url: '/system/other-settings',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemOtherSettings
        }
      },
      // {
      //   name: this.translateService.instant('nav.virtualOnlineMembers'),
      //   url: '/system/virtual-online-members',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemVirtualOnlineMembers
      //   }
      // },
      // {
      //   name: this.translateService.instant('nav.botSettings'),
      //   url: '/system/bot-settings',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemBotSettings
      //   }
      // },
      // {
      //   name: this.translateService.instant('nav.botAccounts'),
      //   url: '/system/bot-accounts',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemBotAccounts
      //   }
      // },
      // {
      //   name: this.translateService.instant('nav.messagBlackList'),
      //   url: '/system/message-blacklists',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemMessageBlacklists
      //   }
      // },
      {
        title: true,
        name: this.translateService.instant('nav.messages')
      },
      {
        name: this.translateService.instant('nav.proclamations'),
        url: '/system/proclamations',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemProclamations
        }
      },
      // {
      //   name: this.translateService.instant('nav.normalProclamations'),
      //   url: '/system/normal-proclamations',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemNormalProclamations
      //   }
      // },
      // {
      //   name: this.translateService.instant('nav.insiteProclamations'),
      //   url: '/system/insite-proclamations',
      //   icon: 'icon-settings',
      // },
      {
        name: this.translateService.instant('nav.adProclamations'),
        url: '/system/ad-proclamations',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemAdProclamationsSetting
        }
      },
      {
        title: true,
        name: this.translateService.instant('nav.authority')
      },
      {
        name: this.translateService.instant('nav.authority'),
        url: '/system/agent-authority',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemAuthority
        }
      },
      // {
      //   name: this.translateService.instant('nav.cloneAgent'),
      //   url: '/system/clone-agent',
      //   icon: 'icon-settings',
      //   attributes: {
      //     manageID: AgentAuthority.OperationItemCloneAgents,
      //     onlyAgent: true,
      //   }
      // },
      {
        name: '線上付款',
        url: '/online-payment',
        icon: 'icon-settings',
        attributes: {
          manageID: AgentAuthority.OperationItemOnlinePayment,
        }
      },
    ];
  }

  getCloneNavItems() {
    return [
      {
        name: '個人資料',
        url: '/my-account',
        // icon: 'fa fa-user'
      },
      {
        name: '報表記錄',
        url: '/system/report-record-agent',
        // icon: 'icon-menu',
        attributes: {
          onlyAgent: true,
        }
      }
    ];
  }
}
