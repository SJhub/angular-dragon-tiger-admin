import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UploadFileService {

    private apiURL = `${environment.apiUrl}/api/v1/upload`;
    private aiURL = `${environment.aiUrl}/api/v1/pic-catch`;

    constructor(private http: HttpClient) { }

    uploadFile(file: any) {
        const formData: FormData = new FormData();
        let fileName = `${Math.floor(Date.now() / 100)}-${file.name}`;
        formData.append('file', file, fileName);
        return this.http.post<any>(`${this.apiURL}`, formData);
    }

    uploadSingleFile(file: File) {
        const formData: FormData = new FormData();
        let fileName = `screenshot_baccarat_${file.name}.jpg`;
        formData.append('file', file, fileName);
        return this.http.post<ApiResponse>(`${this.apiURL}-single-file`, formData)
            .pipe(switchMap(() => this.http.post<ApiResponse>(`${this.aiURL}`, formData)));
    }

    getRandomFileName(fileName: string) {
        return `${Math.floor(Date.now() / 100)}-${fileName}`;
    }

    base64toFile(base64: any, fileName: string) {
        let blob = this.dataURItoBlob(base64);
        let file = new File([blob], fileName);
        return file;
    }

    private dataURItoBlob(dataURI: any) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], { type: mimeString });
    }
}
