import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { MenuDetail } from '../models/menu-detail.model';
import { MessageService } from './message.service';

@Injectable({
    providedIn: 'root'
})
export class MenuDetailService {

    private menuDetailBaseUrl = `${environment.apiUrl}/api/v1/menu-detail`;
    constructor(
        private http: HttpClient,
        private messageService: MessageService) { }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * getMenuDetails - get all menuDetail
     */
    getMenuDetails(): Observable<MenuDetail[]> {
        return this.http.get<MenuDetail[]>(`${this.menuDetailBaseUrl}s`)
            .pipe(
                tap(_ => this.log('fetched menuDetail')),
                catchError(this.handleError<MenuDetail[]>('getMenuDetails', []))
            );
    }

    /**
     * getMenuDetailsByMenuId - get menuDetail by menuId
     */
    getMenuDetailsByMenuId(menuId: number): Observable<MenuDetail[]> {
        return this.http.get<MenuDetail[]>(`${this.menuDetailBaseUrl}s/menu/${menuId}`)
            .pipe(
                tap(_ => this.log('fetched menuDetail')),
                catchError(this.handleError<MenuDetail[]>('getMenuDetailsByMenuId', []))
            );
    }

    /**
     * getMenuDetailById - get menuDetail by id
     */
    getMenuDetailById(id: number): Observable<MenuDetail> {
        return this.http.get<MenuDetail>(`${this.menuDetailBaseUrl}/${id}`)
            .pipe(
                tap(_ => this.log('fetched menuDetail')),
                catchError(this.handleError<MenuDetail>('getMenuDetailById'))
            );
    }

    /**
     * addMenuDetail - add a menuDetail
     * @param menuDetail - new menuDetail
     */
    addMenuDetail(menuDetail: MenuDetail): Observable<MenuDetail> {
        return this.http.post<MenuDetail>(`${this.menuDetailBaseUrl}`, menuDetail)
            .pipe(
                tap(_ => this.log('add menuDetail')),
                catchError(this.handleError<MenuDetail>('addMenuDetail'))
            );
    }

    /**
     * update a menuDetail
     * @param menuDetail - new menuDetail
     */
    updateMenuDetail(menuDetail: MenuDetail): Observable<MenuDetail> {
        return this.http.put<MenuDetail>(this.menuDetailBaseUrl, menuDetail)
        // .pipe(
        //     tap(_ => this.log('update menuDetail')),
        //     catchError(this.handleError<MenuDetail>('updateMenuDetail'))
        // );
    }

    /**
     * delete a menuDetail
     * @param menuDetail - menuDetail or id of menuDetail
     */
    deleteMenuDetail(menuDetail: number | MenuDetail): Observable<any> {
        const id = typeof menuDetail === 'number' ? menuDetail : menuDetail.id;
        const url = `${this.menuDetailBaseUrl}/${id}`;
        return this.http.delete<MenuDetail>(url)
            .pipe(
                tap(_ => this.log(`deleted menuDetail = ${menuDetail}`)),
                catchError(this.handleError<MenuDetail>('deleteMenuDetail'))
            );
    }

    /** Log a MenuDetailService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`MenuDetailService: ${message}`);
    }
}
