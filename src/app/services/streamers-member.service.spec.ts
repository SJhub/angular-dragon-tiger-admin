import { TestBed } from '@angular/core/testing';

import { StreamersMemberService } from './streamers-member.service';

describe('StreamersMemberService', () => {
  let service: StreamersMemberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StreamersMemberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
