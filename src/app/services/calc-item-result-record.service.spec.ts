import { TestBed } from '@angular/core/testing';

import { CalcItemResultRecordService } from './calc-item-result-record.service';

describe('CalcItemResultRecordService', () => {
  let service: CalcItemResultRecordService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalcItemResultRecordService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
