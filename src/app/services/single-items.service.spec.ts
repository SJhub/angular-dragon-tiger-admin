import { TestBed } from '@angular/core/testing';

import { SingleItemsService } from './single-items.service';

describe('SingleItemsService', () => {
  let service: SingleItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SingleItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
