import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response';
import { SingleItems } from '../models/single-items';

@Injectable({
  providedIn: 'root'
})
export class SingleItemsService {

  private baseUrl = `${environment.apiUrl}/api/v1/single-item-limits`;

  constructor(private http: HttpClient) { }

  getSingleItemLimits(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getSingleItemLimit(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  // 以代理ID 取得單項限制資料
  getSingleItemLimitsByAgnet(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-agent/${id}`);
  }

  //  以上級 ID 取得單項限制資料 - 取得到自己下面的資料
  getSingleItemLimitsByParent(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-parent/${id}`);
  }

  updateSingleItemLimits(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}`, data);
  }

  updateSingleItemStatus(data: any): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}/status`, data);
  }

  addSingleItemLimits(data: SingleItems): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, data);
  }

  deleteSingleItemLimits(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
