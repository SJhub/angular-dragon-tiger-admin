import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Token } from '../models/token.model';
import { MessageService } from './message.service';

@Injectable({
    providedIn: 'root'
})
export class TokenService {

    private tokenBaseUrl = `${environment.apiUrl}/api/v1/token`;
    constructor(
        private http: HttpClient,
        private messageService: MessageService) { }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * getTokens - get all token
     */
    getTokens(): Observable<Token[]> {
        return this.http.get<Token[]>(`${this.tokenBaseUrl}s`)
            .pipe(
                tap(_ => this.log('fetched token')),
                catchError(this.handleError<Token[]>('getTokens', []))
            );
    }

    /**
     * getTokenById - get token by id
     */
    getTokenById(id: number): Observable<Token> {
        return this.http.get<Token>(`${this.tokenBaseUrl}/${id}`)
            .pipe(
                tap(_ => this.log('fetched token')),
                catchError(this.handleError<Token>('getTokenById'))
            );
    }

    /**
     * getTokenByMemberName - get token by member name
     */
    getTokenByMemberName(name: string): Observable<Token> {
        return this.http.get<Token>(`${this.tokenBaseUrl}/member/${name}`)
            .pipe(
                tap(_ => this.log('fetched token')),
                catchError(this.handleError<Token>('getTokenByMemberName'))
            );
    }

    /** Log a TokenService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`TokenService: ${message}`);
    }
}
