import { TestBed } from '@angular/core/testing';

import { SettlementRecordsService } from './settlement-records.service';

describe('SettlementRecordsService', () => {
  let service: SettlementRecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SettlementRecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
