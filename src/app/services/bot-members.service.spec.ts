import { TestBed } from '@angular/core/testing';

import { BotMembersService } from './bot-members.service';

describe('BotMembersService', () => {
  let service: BotMembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BotMembersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
