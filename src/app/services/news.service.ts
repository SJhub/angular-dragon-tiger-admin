import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { News } from '../models/news';


@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private baseUrl = `${environment.apiUrl}/api/v1/news`;
  addNews: any;

  constructor(private http: HttpClient) { }


  getAllNews(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getNews(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  createNews(news: News): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, news);
  }

  updateNews(news: News): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${news.id}`, news);
  }

  deleteNews(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
