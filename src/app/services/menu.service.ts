import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Menu } from '../models/menu.model';
import { MessageService } from './message.service';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    private menuBaseUrl = `${environment.apiUrl}/api/v1/menu`;
    constructor(
        private http: HttpClient,
        private messageService: MessageService) { }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * getMenus - get all menu
     */
    getMenus(): Observable<Menu[]> {
        return this.http.get<Menu[]>(`${this.menuBaseUrl}s`)
        // .pipe(
        //     tap(_ => this.log('fetched menu')),
        //     catchError(this.handleError<Menu[]>('getMenus', []))
        // );
    }

    /**
     * getMenuById - get menu by id
     */
    getMenuById(id: number): Observable<Menu> {
        return this.http.get<Menu>(`${this.menuBaseUrl}/${id}`)
            .pipe(
                tap(_ => this.log('fetched menu')),
                catchError(this.handleError<Menu>('getMenuById'))
            );
    }

    /**
     * add a menu
     * @param menu - new menu
     */
    addMenu(menu: Menu): Observable<Menu> {
        return this.http.post<Menu>(`${this.menuBaseUrl}`, menu)
            // .pipe(
            //     tap(_ => this.log('add menu')),
            //     catchError(this.handleError<Menu>('addMenu'))
            // );
    }

    /**
     * update a menu
     * @param menu - new menu
     */
    updateMenu(menu: Menu): Observable<Menu> {
        return this.http.put<Menu>(this.menuBaseUrl, menu)
        // .pipe(
        //     tap(_ => this.log('update menu')),
        //     catchError(this.handleError<Menu>('updateMenu'))
        // );
    }

    /**
     * delete a menu
     * @param menu - menu or id of menu
     */
    deleteMenu(menu: number | Menu): Observable<any> {
        const id = typeof menu === 'number' ? menu : menu.id;
        const url = `${this.menuBaseUrl}/${id}`;
        return this.http.delete<Menu>(url)
            .pipe(
                tap(_ => this.log(`deleted menu = ${menu}`)),
                catchError(this.handleError<Menu>('deleteMenu'))
            );
    }

    /** Log a MenuService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`MenuService: ${message}`);
    }
}
