import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Admin } from '../models/admin';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private apiURL = `${environment.apiUrl}/api/v1/users`;

  constructor(private http: HttpClient) { }

  listUser() {
    return this.http.get<ApiResponse>(`${this.apiURL}`);
  }

  getUser(id) {
    return this.http.get<ApiResponse>(`${this.apiURL}/${id}`);
  }

  addUser(body: Admin) {
    return this.http.post<ApiResponse>(`${this.apiURL}`, body);
  }

  editUser(body: Admin) {
    return this.http.put<ApiResponse>(`${this.apiURL}/${body.id}`, body);
  }

  deleteUser(id) {
    return this.http.delete<ApiResponse>(`${this.apiURL}/${id}`);
  }
}
