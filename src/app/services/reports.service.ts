import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private baseUrl = `${environment.apiUrl}/api/v1/reports`;

  constructor(private http: HttpClient) { }

  reportsSicboReCalc(usersID: number, identity: number, year: number, month: number, day: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-sicbo-re-calc`,
      {
        users_id: usersID,
        identity,
        year,
        month,
        day
      });
  }
}
