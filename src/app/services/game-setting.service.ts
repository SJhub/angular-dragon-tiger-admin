import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { GameSetting } from '../models/game-setting';

@Injectable({
  providedIn: 'root'
})
export class GameSettingService {

  private baseUrl = `${environment.apiUrl}/api/v1/game-settings`;

  constructor(private http: HttpClient) { }

  getGameSettings(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getGameSetting(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  updateGameSetting(setting: GameSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${setting.id}`, setting);
  }

}
