import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class CalcItemResultRecordService {

  private baseUrl = `${environment.apiUrl}/api/v1/calc-item-result-records`;

  private activeUrl = `${environment.apiUrl}/api/v1/calc-item-result-active`;

  constructor(private http: HttpClient) { }

  getCalcItemResultRecords(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getCalcItemResultRecordsIsBet(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-is-bet`);
  }

  getCalcItemResultRecord(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  getCalcItemResultRecordByTypeBetweenDate(id: number, startDate: string, endDate: string) {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-type-between-date/type/${id}/start/${startDate}/end/${endDate}`);
  }

  getCalcItemResultRecordByGameTypeAndNumber(gameTypeID: number, gameNumber: number) {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-type/${gameTypeID}/number/${gameNumber}`);
  }

  clearCalcItemResultRecordsStatus(): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-clear-status`, {});
  }

  getCalcItemResultActiveRecords(id: number, startDate: string, endDate: string) {
    return this.http.get<ApiResponse>(`${this.activeUrl}-records-by-type-between-date/type/${id}/start/${startDate}/end/${endDate}`);
  }
}
