import { TestBed } from '@angular/core/testing';

import { BetRecordService } from './bet-record.service';

describe('BetRecordService', () => {
  let service: BetRecordService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BetRecordService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
