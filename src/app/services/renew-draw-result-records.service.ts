import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class RenewDrawResultRecordsService {
  private baseUrl = `${environment.apiUrl}/api/v1/renew-draw-result-records`;

  constructor(private http: HttpClient) { }

  getRenewDrawResultRecords(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getRenewDrawResultRecord(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
