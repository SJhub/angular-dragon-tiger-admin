import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { System } from '../models/system-log';

@Injectable({
  providedIn: 'root'
})
export class SystemService {

  private api_url = `${environment.apiUrl}/api/v1/system-logs`;

  constructor(private http: HttpClient) { }

  getSystemLogs() {
    return this.http.get<ApiResponse>(`${this.api_url}`);
  }

}
