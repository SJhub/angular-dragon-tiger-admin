import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { MessageBlacklist } from '../models/message-blacklist';

@Injectable({
  providedIn: 'root'
})
export class MessageBlacklistService {

  private baseUrl = `${environment.apiUrl}/api/v1/message-blacklists`;

  constructor(private http: HttpClient) { }

  getMessageBlacklists(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getMessageBlacklist(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  addMessageBlacklist(blacklist: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, blacklist);
  }

  updateMessageBlacklist(blacklist: MessageBlacklist): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${blacklist.id}`, blacklist);
  }

  deleteMessageBlacklist(id: number) {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
