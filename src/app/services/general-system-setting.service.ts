import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { GeneralSystemSetting } from '../models/general-system-setting';

@Injectable({
  providedIn: 'root'
})
export class GeneralSystemSettingService {
  constructor(private http: HttpClient) { }

  private apiURL = `${environment.apiUrl}/api/v1/general-system-settings`;

  getGeneralSystemSettings(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}`);
  }

  getAppReleaseVersion(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}/${id}/app-release-version`);
  }

  getGeneralSystemSetting(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.apiURL}/${id}`);
  }

  editGeneralSystemSetting(body: GeneralSystemSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${body.id}`, body);
  }

  editBankAccounts(body: GeneralSystemSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${body.id}/bank-accounts`, body);
  }

  editBankAccountStatus(body: GeneralSystemSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${body.id}/bank-account-status`, body);
  }

  editAppReleaseVersion(id: number, app_release_version: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/app-release-version`, { id, app_release_version });
  }

  editBaccaratSingleGameResultLimit(id: number, baccarat_single_game_result_limit: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/baccarat-single-game-result-limit`, { id, baccarat_single_game_result_limit });
  }

  editBaccaratDonateSetting(id: number, auto_donate_start_sec: number, auto_donate_end_sec: number, auto_donate_status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/baccarat-donate-setting`, { id, auto_donate_start_sec, auto_donate_end_sec, auto_donate_status });
  }

  editBaccaratAutoMemberOnlineSetting(id: number, auto_member_online_start_sec: number, auto_member_online_end_sec: number, auto_member_online_cnt: number, auto_member_online_status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/baccarat-auto-member-online-setting`, { id, auto_member_online_start_sec, auto_member_online_end_sec, auto_member_online_cnt, auto_member_online_status });
  }

  editGameBoardShowStatus(body: GeneralSystemSetting, operator_id: number): Observable<ApiResponse> {
    body['operator_id'] = operator_id;
    return this.http.put<ApiResponse>(`${this.apiURL}/${body.id}/game-board-show-status`, body);
  }

  editGameBoardStatus(id: number, game_board_status: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${id}/game-board-status`, { id, game_board_status, operator_id });
  }

  editCheckReadyToStartBeforeMin(body: GeneralSystemSetting): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.apiURL}/${body.id}/check-ready-to-start-before-min`, body);
  }

}