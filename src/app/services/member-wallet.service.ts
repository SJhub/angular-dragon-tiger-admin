import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class MemberWalletService {
  private baseUrl = `${environment.apiUrl}/api/v1/member-wallets`;

  constructor(private http: HttpClient) { }

  getMemberWalletByMemberID(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-members-id/${id}`).pipe(catchError(error => of(error)));
  }

  // 龍虎增加錢包與額度
  incrementDragonTigerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-dragon-tiger-credit-and-wallet`, { id, amount, operator_id });
  }


  // 骰寶增加錢包與額度
  incrementSicboCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-sicbo-credit-and-wallet`, { id, amount, operator_id });
  }

  // 對戰遊戲增加錢包與額度
  incrementPokerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-poker-credit-and-wallet`, { id, amount, operator_id });
  }

  // 百家增加錢包與額度
  incrementOtherCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-baccarat-credit-and-wallet`, { id, amount, operator_id });
  }

  // 體育增加錢包與額度
  incrementSportCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-world-football-credit-and-wallet`, { id, amount, operator_id });
  }


  // 龍虎減少錢包與額度
  decrementDragonTigerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-dragon-tiger-credit-and-wallet`, { id, amount, operator_id });
  }

  // 骰寶減少錢包與額度
  decrementSicboCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-sicbo-credit-and-wallet`, { id, amount, operator_id });
  }

  // 對戰遊戲減少錢包與額度
  decrementPokerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-poker-credit-and-wallet`, { id, amount, operator_id });
  }

  // 百家減少錢包與額度
  decrementOtherCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-other-credit-and-wallet`, { id, amount, operator_id });
  }


  // 體育增加錢包與額度
  decrementSportCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-world-football-credit-and-wallet`, { id, amount, operator_id });
  }


  // 龍虎清空錢包與額度
  clearDragonTigerCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/clear-dragon-tiger-credit-and-wallet`, { id, operator_id });
  }

  // 骰寶清空錢包與額度
  clearSicboCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/clear-sicbo-credit-and-wallet`, { id, operator_id });
  }

  // 對戰遊戲清空錢包與額度
  clearPokerCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/clear-poker-credit-and-wallet`, { id, operator_id });
  }

  // 百家清空錢包與額度
  clearOtherCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/clear-other-credit-and-wallet`, { id, operator_id });
  }

  // 體育清空錢包與額度
  clearSportCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/clear-world-football-credit-and-wallet`, { id, operator_id });
  }

  // 骰寶結算
  settlementSicboWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/settlement-sicbo`, { id, operator_id });
  }

  // 對戰遊戲結算
  settlementPokerWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/settlement-poker`, { id, operator_id });
  }

  // 百家結算
  settlementOtherWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/settlement-baccarat`, { id, operator_id });
  }

  // 體育結算
  settlementSportWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/settlement-world-football`, { id, operator_id });
  }

  // 更新限贏金額
  editWinTotalAmountLimit(body: WinTotalAmountLimitBody): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${body.id}/win-total-amount-limit`, body);
  }

  // 重置當前輸贏金額
  resetWinTotalAmountLimit(body: WinTotalAmountLimitBody): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${body.id}/reset-game-result-amount`, body);
  }

  // 更新結算類型
  updateSettlementType(body: UpdateSettlementTypeBody): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${body.id}/settlement-type`, body);
  }
}

export interface WinTotalAmountLimitBody {
  id: number;
  game_types_id: number;
  operator_id: number;
  amount: number;
}

export interface UpdateSettlementTypeBody {
  id: number;
  wallet_settlement_type_baccarat: number;
  wallet_settlement_type_poker: number;
  wallet_settlement_type_world_football: number;
}
