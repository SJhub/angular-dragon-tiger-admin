import { DatePipe, formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

const LIMIT_TIME_MIN = 2; // 限制2分鐘

export interface RestartTime {
  id: number;
  time: number;
}

@Injectable({
  providedIn: 'root'
})
export class LivesRestartService {
  private baseUrl = `${environment.apiUrl}/api/v1/lives-restart`;

  private LOCAL_STORAGE = 'liveRestartTime';

  constructor(private http: HttpClient) { }

  restartByGameLiveNumber(game_live_number: number, users_id: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-by-game-live-number`, { game_live_number, users_id })
      .pipe(tap(() => this.logRestartTime(game_live_number)));
  }

  getRemainingTimeByGameLiveNumber(game_live_number: number) {
    const restartTime = this.getRestartTimeByID(game_live_number);
    if (restartTime) {
      const limitSec = LIMIT_TIME_MIN * 1000 * 60;
      const count = Math.round((restartTime.time + limitSec - new Date().getTime()) / 1000);
      if (count > 0) {
        return timer(0, 1000).pipe(take(count), map(s => this.secToMin(count - s - 1)));
      }
    }
  }

  checkRestartTimeIsValid(game_live_number: number) {
    const restartTime = this.getRestartTimeByID(game_live_number);
    if (restartTime) {
      const now = new Date().getTime();
      return now - restartTime.time >= LIMIT_TIME_MIN * 1000 * 60;
    }
    return true;
  }

  logRestartTime(game_live_number: number) {
    const restartTimes: RestartTime[] = this.getRestartTime();
    const restartTime: RestartTime = {
      id: game_live_number,
      time: new Date().getTime()
    }
    const old = restartTimes.find(e => e.id === game_live_number);
    if (old) {
      old.time = restartTime.time;
    } else {
      restartTimes.push(restartTime);
    }
    localStorage.setItem(this.LOCAL_STORAGE, JSON.stringify(restartTimes));
  }

  private getRestartTime() {
    const str = localStorage.getItem(this.LOCAL_STORAGE);
    if (str) {
      try {
        const restartTimes = JSON.parse(str);
        return restartTimes;
      } catch (error) {
        return [];
      }
    }
    return [];
  }

  private getRestartTimeByID(id: number) {
    const restartTimes: RestartTime[] = this.getRestartTime();
    const restartTime = restartTimes.find(e => e.id === id);
    return restartTime;
  }

  private secToMin(value: number) {
    const minutes: number = Math.floor(value / 60);
    return minutes.toString().padStart(2, '0') + ':' +
      (value - minutes * 60).toString().padStart(2, '0');
  }
}
