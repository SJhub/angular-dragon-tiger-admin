import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { GameType } from '../models/game-type';
import { GameTypeEnum } from '../enum/game-type.enum';
import { map } from 'rxjs/operators';

const AVAILABLE_GAME_IDS = [
  GameTypeEnum.GameTypeDragonTiger
];
@Injectable({
  providedIn: 'root'
})
export class GameTypeService {

  private baseUrl = `${environment.apiUrl}/api/v1/game-types`;

  constructor(private http: HttpClient) { }

  getGameTypes(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`).pipe(map(r => {
      let types: GameType[] = r.data;
      r.data = types.filter(e => AVAILABLE_GAME_IDS.includes(e.id));
      return r;
    }));
  }

  getGameType(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  updateGameType(data: GameType): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${data.id}`, data);
  }

  updateGameTypeMode(id, mode): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/mode`, { mode });
  }

  updateFameTypeShowStatus(id: number, show_status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/show-status`, { show_status });
  }

  updateGameTypeStatus(id: number, status: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/status`, { status });
  }

  updateRestGameStatus(id: number, rest_status: number, time_to_rest: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/rest-status`, { id, rest_status, time_to_rest });
  }

  updateBreakGameStatus(id: number, break_status: number, time_to_rest: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/break-status`, { id, break_status, time_to_rest });
  }

  updateTimeToOpenWeek(id: number, time_to_open_week: string): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/time-to-open-week`, { id, time_to_open_week });
  }
}
