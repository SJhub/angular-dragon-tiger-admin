import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { FileServerInfo } from '../models/file-server-info.model';

import { MessageService } from './message.service';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class FileServerInfoService {

  private fileServerInfoBaseUrl = `${environment.apiUrl}/api/v1/file-server-info`;
  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * getFileServerInfoById - get fileServerInfo by id
   */
  getFileServerInfoById(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.fileServerInfoBaseUrl}/${id}`)
    // .pipe(
    //   tap(_ => this.log('fetched fileServerInfo')),
    //   catchError(this.handleError<FileServerInfo>('getFileServerInfoById'))
    // );
  }

  /**
   * update fileServerInfo
   * @param fileServerInfo - new fileServerInfo
   */
  updateFileServerInfo(fileServerInfo: FileServerInfo): Observable<FileServerInfo> {
    return this.http.put<FileServerInfo>(this.fileServerInfoBaseUrl, fileServerInfo)
  }

  /** Log a FileServerInfoService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`FileServerInfoService: ${message}`);
  }

}
