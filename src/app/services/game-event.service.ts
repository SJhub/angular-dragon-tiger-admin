import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api-response';
import { Event } from '../models/event';
import { GRPCEvent } from '../models/grpc-event';

@Injectable({
  providedIn: 'root'
})
export class GameEventService {

  private baseUrl = `${environment.apiUrl}/api/v1/events`;

  constructor(private http: HttpClient) { }

  getEvents(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getEvent(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  addEvent(event: Event): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, event);
  }

  addGRPCEvent(event: GRPCEvent): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-grpc`, event);
  }

  updateEvent(event: Event): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${event.id}`, event);
  }

  deleteEvent(id: number) {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
