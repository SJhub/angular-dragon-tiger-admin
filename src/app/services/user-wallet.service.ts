import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class UserWalletService {
  private baseUrl = `${environment.apiUrl}/api/v1/user-wallets`;

  constructor(private http: HttpClient) { }

  getUserWalletByUserID(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-users-id/${id}`).pipe(catchError(error => of(error)));
  }

  // 龍虎增加錢包與額度
  incrementDragonTigerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-dragon-tiger-credit-and-wallet`, { id, amount, operator_id });
  }

  // 骰寶增加錢包與額度
  incrementSicboCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-sicbo-credit-and-wallet`, { id, amount, operator_id });
  }

  // 對戰遊戲增加錢包與額度
  incrementPokerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-poker-credit-and-wallet`, { id, amount, operator_id });
  }

  // 百家增加錢包與額度
  incrementOtherCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-baccarat-credit-and-wallet`, { id, amount, operator_id });
  }

  // 體育增加錢包與額度
  incrementSportCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/increment-world-football-credit-and-wallet`, { id, amount, operator_id });
  }

  // 龍虎減少錢包與額度
  decrementDragonTigerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-dragon-tiger-credit-and-wallet`, { id, amount, operator_id });
  }


  // 骰寶減少錢包與額度
  decrementSicboCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-sicbo-credit-and-wallet`, { id, amount, operator_id });
  }

  // 對戰遊戲減少錢包與額度
  decrementPokerCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-poker-credit-and-wallet`, { id, amount, operator_id });
  }

  // 百家減少錢包與額度
  decrementOtherCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-other-credit-and-wallet`, { id, amount, operator_id });
  }

  // 體育減少錢包與額度
  decrementSportCreditAndWallet(id: number, amount: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${id}/decrement-world-football-credit-and-wallet`, { id, amount, operator_id });
  }

  // 龍虎清空錢包與額度
  clearDragonTigerCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-members-id/${id}/clear-dragon-tiger-credit-and-wallet`, { id, operator_id });
  }

  // 骰寶遊戲清空錢包與額度
  clearSicboCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-users-id/${id}/clear-sicbo-credit-and-wallet-and-its-childs`, { id, operator_id });
  }

  // 對戰遊戲清空錢包與額度
  clearPokerCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-users-id/${id}/clear-poker-credit-and-wallet-and-its-childs`, { id, operator_id });
  }

  // 百家清空錢包與額度
  clearOtherCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-users-id/${id}/clear-other-credit-and-wallet-and-its-childs`, { id, operator_id });
  }

  // 體育清空錢包與額度
  clearSportCreditAndWallet(id: number, operator_id: number): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}-by-users-id/${id}/clear-world-football-credit-and-wallet-and-its-childs`, { id, operator_id });
  }

  // 現金版代理提現
  withdrawUserWallet(users_id: number, amount: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}-withdraw`, { users_id, amount });
  }

}
