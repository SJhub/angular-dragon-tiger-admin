import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { VirtualOnline } from '../models/virtual-online';

@Injectable({
  providedIn: 'root'
})
export class VirtualOnlineMemberService {
  private baseUrl = `${environment.apiUrl}/api/v1/virtual-online-members`;

  constructor(private http: HttpClient) { }

  getVirtualOnlines(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getVirtualOnline(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  addVirtualOnline(body: VirtualOnline): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, body);
  }

  editVirtualOnline(body: VirtualOnline): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${body.id}`, body);
  }

  deleteVirtualOnline(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
