import { TestBed } from '@angular/core/testing';

import { TimeOutAlertService } from './time-out-alert.service';

describe('TimeOutAlertService', () => {
  let service: TimeOutAlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimeOutAlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
