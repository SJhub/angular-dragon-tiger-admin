import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { ContactUs } from '../models/contact-us';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {
  private baseUrl = `${environment.apiUrl}/api/v1/contact-us`;

  constructor(private http: HttpClient) { }

  getAllContactUs(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getAllContactUsGuest(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-guest`);
  }

  getContactUs(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}/${id}`);
  }

  addContactUs(body: ContactUs): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, body);
  }

  editContactUs(body: ContactUs) {
    return this.http.put<ApiResponse>(`${this.baseUrl}/${body.id}`, body);
  }

  deleteContactUs(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${this.baseUrl}/${id}`);
  }
}
