import { TestBed } from '@angular/core/testing';

import { RenewDrawResultRecordsService } from './renew-draw-result-records.service';

describe('RenewDrawResultRecordsService', () => {
  let service: RenewDrawResultRecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RenewDrawResultRecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
