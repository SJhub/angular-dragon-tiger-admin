import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TimeOutAlertService {

  timeout$ = new Subject();

  constructor() {
    this.timeoutAlert();
  }

  timeoutAlert() {
    this.timeout$.pipe(throttleTime(5000)).subscribe(
      () => {
        alert('您的網路品質不佳，請稍待網路連線穩定後重試!')
      }
    );
  }
}
