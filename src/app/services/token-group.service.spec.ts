import { TestBed } from '@angular/core/testing';

import { TokenGroupService } from './token-group.service';

describe('TokenGroupService', () => {
  let service: TokenGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TokenGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
