import { TestBed } from '@angular/core/testing';

import { AutoDonateSettingsService } from './auto-donate-settings.service';

describe('AutoDonateSettingsService', () => {
  let service: AutoDonateSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutoDonateSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
