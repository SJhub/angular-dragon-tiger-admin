import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TokenGroup } from '../models/token-group.model';
import { MessageService } from './message.service';

@Injectable({
    providedIn: 'root'
})
export class TokenGroupService {

    private tokenGroupBaseUrl = `${environment.apiUrl}/api/v1/token-group`;
    constructor(
        private http: HttpClient,
        private messageService: MessageService) { }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * getTokenGroups - get all tokenGroup
     */
    getTokenGroups(): Observable<TokenGroup[]> {
        return this.http.get<TokenGroup[]>(`${this.tokenGroupBaseUrl}s`);
    }

    /**
     * getTokenGroupById - get tokenGroup by id
     */
    getTokenGroupById(id: number): Observable<TokenGroup> {
        return this.http.get<TokenGroup>(`${this.tokenGroupBaseUrl}/${id}`);
    }

    /**
     * add a tokenGroup
     * @param tokenGroup - new tokenGroup
     */
    addTokenGroup(tokenGroup: TokenGroup): Observable<TokenGroup> {
        return this.http.post<TokenGroup>(`${this.tokenGroupBaseUrl}`, tokenGroup);
    }

    /**
     * update a tokenGroup
     * @param tokenGroup - new tokenGroup
     */
    updateTokenGroup(tokenGroup: TokenGroup): Observable<TokenGroup> {
        return this.http.put<TokenGroup>(this.tokenGroupBaseUrl, tokenGroup)
    }

    /**
     * delete a tokenGroup
     * @param id - tokenGroup id
     */
    deleteTokenGroup(id: number): Observable<any> {
        return this.http.delete<any>(`${this.tokenGroupBaseUrl}/${id}`)
    }

    /** Log a TokenGroupService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`TokenGroupService: ${message}`);
    }
}
