import { TestBed } from '@angular/core/testing';

import { MessageBlacklistService } from './message-blacklist.service';

describe('MessageBlacklistService', () => {
  let service: MessageBlacklistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageBlacklistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
