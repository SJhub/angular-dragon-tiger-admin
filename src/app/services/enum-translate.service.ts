import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Action } from '../enum/action.enum';
import { BetRecordStatusEnum } from '../enum/bet-record-status.enum';
import { GameResultTypeEnum } from '../enum/game-result-type.enum';
import { GameTypeEnum } from '../enum/game-type.enum';
import { UserRole } from '../enum/user-role.enum';

@Injectable({
  providedIn: 'root'
})
export class EnumTranslateService {

  constructor(private translateService: TranslateService) { }

  getTranslatedGameTypeByID(type: number) {
    switch (Number(type)) {
      case GameTypeEnum.GameTypeSicBo:
        return this.translateService.instant('game.sicBo');
      case GameTypeEnum.GameTypeBaccarat:
        return this.translateService.instant('game.baccarat');
      case GameTypeEnum.GameTypeRoulette:
        return this.translateService.instant('game.roulette');
      case GameTypeEnum.GameTypeDragonTiger:
        return this.translateService.instant('game.dragonTiger');
      default:
        return this.translateService.instant('common.noData');
    }
  }

  getTranslatedReceiptType(type: number) {
    switch (Number(type)) {
      case UserRole.admin:
        return this.translateService.instant('system.admin');
      case UserRole.management:
        return this.translateService.instant('system.management');
      case UserRole.topAgent:
        return this.translateService.instant('system.topAgent');
      case UserRole.agent:
        return this.translateService.instant('system.agent');
      case UserRole.dealers:
        return this.translateService.instant('system.dealers');
      case UserRole.streamer:
        return this.translateService.instant('system.streamer');
      case UserRole.gametester:
        return this.translateService.instant('system.gametester');
      case UserRole.staff:
        return this.translateService.instant('system.staff');
      default:
        return this.translateService.instant('bet.None');
    }
  }

  getTranslatedWeek(type: number) {
    switch (Number(type)) {
      case 0:
        return this.translateService.instant('week.sunday');
      case 1:
        return this.translateService.instant('week.monday');
      case 2:
        return this.translateService.instant('week.tuesday');
      case 3:
        return this.translateService.instant('week.wednesday');
      case 4:
        return this.translateService.instant('week.thursday');
      case 5:
        return this.translateService.instant('week.friday');
      case 6:
        return this.translateService.instant('week.saturday');
      default:
        return this.translateService.instant('common.noData');
    }
  }

  getTranslatedAction(value: number) {
    switch (Number(value)) {
      case Action['所有行為']:
        return this.translateService.instant('searching.allActions');
      case Action['押注']:
        return this.translateService.instant('searching.bet');
      case Action['取消押注']:
        return this.translateService.instant('searching.cancelBet');
      case Action['打賞']:
        return this.translateService.instant('searching.donate');
      default:
        return this.translateService.instant('common.noData');
    }
  }

  getTranslatedGameResultType(value: number) {
    switch (Number(value)) {
      case GameResultTypeEnum.Win:
        return this.translateService.instant('searching.win');
      case GameResultTypeEnum.Loss:
        return this.translateService.instant('searching.lose');
      case GameResultTypeEnum.NoLottery:
        return this.translateService.instant('searching.noLottery');
      default:
        return this.translateService.instant('bet.None');
    }
  }

  getTranslatedGameStatusType(value: number) {
    switch (Number(value)) {
      case BetRecordStatusEnum.Deleted:
        return this.translateService.instant('common.delete');
      case BetRecordStatusEnum.Finish:
        return this.translateService.instant('betRecord.normal');
      case BetRecordStatusEnum.Process:
        return this.translateService.instant('betRecord.process');
      case BetRecordStatusEnum.Unusual:
        return this.translateService.instant('betRecord.abnormal');
      case BetRecordStatusEnum.Voided:
        return this.translateService.instant('betRecord.voided');
      default:
        return this.translateService.instant('bet.None');
    }
  }

  getTranslatedGameName(name: string) {
    if (typeof name !== 'string') return '';
    name = name.charAt(0).toLowerCase() + name.slice(1);
    return this.translateService.instant(`game.${name}`);
  }

  getTranslatedBetItem(name: string) {
    return this.translateService.instant(`bet.${name}`);
  }
}
