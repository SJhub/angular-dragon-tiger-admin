/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FileServerInfoService } from './file-server-info.service';

describe('Service: FileServerInfo', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileServerInfoService]
    });
  });

  it('should ...', inject([FileServerInfoService], (service: FileServerInfoService) => {
    expect(service).toBeTruthy();
  }));
});
