import { TestBed } from '@angular/core/testing';

import { CurrentReportsSicboService } from './current-reports-sicbo.service';

describe('CurrentReportsSicboService', () => {
  let service: CurrentReportsSicboService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrentReportsSicboService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
