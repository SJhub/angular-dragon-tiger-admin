import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';
import { OperationLog } from '../models/operation-log';

@Injectable({
  providedIn: 'root'
})
export class OperationLogService {
  private baseUrl = `${environment.apiUrl}/api/v1/operation-logs`;

  constructor(private http: HttpClient) { }

  getOperationLogs(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}`);
  }

  getOperationLogsByActionType(type: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.baseUrl}-by-action-type/${type}`);
  }

  addOperationLog(body: OperationLog): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.baseUrl}`, body);
  }
}
