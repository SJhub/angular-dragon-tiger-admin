import { TestBed } from '@angular/core/testing';

import { GeneralSystemSettingService } from './general-system-setting.service';

describe('GeneralSystemSettingService', () => {
  let service: GeneralSystemSettingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeneralSystemSettingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
