import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Notification } from '../models/notification.model';

@Injectable({
    providedIn: 'root'
})
export class PushNotificationService {

    private pushNotificationBaseUrl = `${environment.apiUrl}/api/v1/fcm`;

    constructor(
        private http: HttpClient) { }

    /**
     * add a notification
     * @param notification - new notification
     */
    addNotification(notification: Notification): Observable<Notification> {
        return this.http.post<Notification>(`${this.pushNotificationBaseUrl}/send`, notification);
    }
}
