import { GameType } from './../models/game-type';
import { Injectable } from '@angular/core';
const { GameGreeterClient } = require('../proto/pb/ts/game_grpc_web_pb');
const { GameTypeRequest } = require('../proto/pb/ts/game_pb');

@Injectable({
  providedIn: 'root'
})
export class GameGrpcService {

  client: any;

  constructor() {
    this.client = new GameGreeterClient('gameproxy_game-proxy_1');
  }

  getSecondsLeftOfGameType() {
    const request = new GameTypeRequest();

    request.GameTypesID = 1;

    const metadata = { 'test': '123' };
    this.client.getSecondsLeftOfGameType(request, metadata, (err, response) => {
      console.log(response);
      console.log(err);
    });
  }
}
