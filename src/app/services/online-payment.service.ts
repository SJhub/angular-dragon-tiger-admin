import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class OnlinePaymentService {
  private apiURL = `${environment.catUrl}/api/v1/payment-log`;

  constructor(private http: HttpClient) { }

  getPaymentLogs() {
    return this.http.get<ApiResponse>(`${this.apiURL}`);
  }

  addPaymentLog(body: PaymentLog) {
    return this.http.post<ApiResponse>(`${this.apiURL}`, body);
  }
}

export interface PaymentLog {
  order_number: string;
  pay_amount: number;
  remark: string;
  status: number;
}
