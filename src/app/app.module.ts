import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// import { FileSelectDirective, FileDropDirective, FileUploadModule } from 'ng2-file-upload';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { ChartsModule } from 'ng2-charts';
import { HttpInterceptorService } from './services/http-interceptor.service';

// import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';

// ngx-translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { CommentListComponent } from './views/comment-list/comment-list.component';
import { SystemModule } from './views/system/system.module';
import { MyAccountComponent } from './views/my-admin/my-account/my-account.component';
import { AdminComponent } from './views/admin/admin/admin.component';
import { MemberComponent } from './views/member/member/member.component';
import { SharedModule } from './shared/shared.module';
import { AdminFormComponent } from './views/admin/admin-form/admin-form.component';
import { MemberFormComponent } from './views/member/member-form/member-form.component';
import { NewsComponent } from './views/news/news/news.component';
import { NewsFormComponent } from './views/news/news-form/news-form.component';

import { NewsListComponent } from './views/news-list/news-list.component';
import { SystemLogComponent } from './views/system-log/system-log.component';
import { MatButtonModule } from '@angular/material/button';
import { AdminMemberComponent } from './views/admin/admin-member/admin-member.component';
import { AdminAgentComponent } from './views/admin/admin-agent/admin-agent.component';
import { AddMemberFormComponent } from './views/member/add-member-form/add-member-form.component';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { getDutchPaginatorIntl } from './common/paginator-intl';
import { RegistrationMemberComponent } from './pages/registration-member/registration-member.component';
import { SingleBetPanelComponent } from './component/single-bet-panel/single-bet-panel.component';
import { AgentOnlineComponent } from './component/agent-online/agent-online.component';
import { TimelyBetModalComponent } from './component/timely-bet-modal/timely-bet-modal.component';
import { MemberTimelyBetComponent } from './component/member-timely-bet/member-timely-bet.component';
import { MyMemberComponent } from './pages/my-member/my-member.component';
import { MyAgentComponent } from './pages/my-agent/my-agent.component';
import { MyCloneComponent } from './pages/my-clone/my-clone.component';
import { MyCloneFormComponent } from './pages/my-clone/my-clone-form/my-clone-form.component';
import { OnlinePaymentComponent } from './pages/online-payment/online-payment.component';



export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    SortableModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    // Ng2SmartTableModule,
    AlertModule.forRoot(),
    SystemModule,
    ModalModule.forRoot(),
    MatButtonModule,
    // FileUploadModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    SharedModule,

  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    MemberComponent,
    CommentListComponent,
    AdminComponent,
    MyAccountComponent,
    AdminFormComponent,
    MemberFormComponent,
    NewsComponent,
    NewsFormComponent,
    NewsListComponent,
    SystemLogComponent,
    AdminMemberComponent,
    AdminAgentComponent,
    AddMemberFormComponent,
    RegistrationMemberComponent,
    SingleBetPanelComponent,
    AgentOnlineComponent,
    TimelyBetModalComponent,
    MemberTimelyBetComponent,
    MyMemberComponent,
    MyAgentComponent,
    MyCloneComponent,
    MyCloneFormComponent,
    OnlinePaymentComponent,
  ],
  entryComponents: [
    MemberTimelyBetComponent
  ],
  providers: [
    { provide: MatPaginatorIntl, useValue: getDutchPaginatorIntl() },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
