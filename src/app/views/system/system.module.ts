import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemRoutingModule } from './system.routing.module';
import { AdListComponent } from './ad-list/ad-list.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { MetaComponent } from './meta/meta.component';
import { OtherComponent } from './other/other.component';
import { ContactComponent } from './contact/contact.component';
import { SharedModule } from '../../shared/shared.module';
import { GameSettingsComponent } from '../../pages/game-settings/game-settings.component';
import { GameSettingsListComponent } from '../../pages/game-settings-list/game-settings-list.component';
import { DrawSettingsComponent } from '../../pages/draw-settings/draw-settings.component';
import { OtherSettingsComponent } from '../../pages/other-settings/other-settings.component';
import { BetRecordListComponent } from '../../pages/bet-record-list/bet-record-list.component';
import { DonateRecordListComponent } from '../../pages/donate-record-list/donate-record-list.component';
import { CalcItemResultRecordListComponent } from '../../pages/calc-item-result-record-list/calc-item-result-record-list.component';
import { BoardMessageComponent } from '../../pages/board-message/board-message.component';
import { MessageBlacklistComponent } from '../../pages/message-blacklist/message-blacklist.component';
import { MessageBlacklistEditComponent } from '../../pages/message-blacklist-edit/message-blacklist-edit.component';
import { ProclamationListComponent } from '../../pages/proclamation-list/proclamation-list.component';
import { ProclamationEditComponent } from '../../pages/proclamation-edit/proclamation-edit.component';
import { BetRecordEditComponent } from '../../pages/bet-record-edit/bet-record-edit.component';
import { NormalProclamationListComponent } from '../../pages/normal-proclamation-list/normal-proclamation-list.component';
import { SearchResultComponent } from '../../pages/search-result/search-result.component';
import { BetAmountsComponent } from '../../pages/bet-amounts/bet-amounts.component';
import { BetAmountEditComponent } from '../../pages/bet-amount-edit/bet-amount-edit.component';
import { ReportRecordListComponent } from '../../pages/report-record-list/report-record-list.component';
import { ReportRecordMemberListComponent } from '../../pages/report-record-member-list/report-record-member-list.component';
import { MatTableComponent } from '../../component/mat-table/mat-table.component';
import { InsiteProclamationsComponent } from '../../pages/insite-proclamations/insite-proclamations.component';
import { CroupierOperationComponent } from '../../pages/croupier-operation/croupier-operation.component';
import { InstantBetComponent } from '../../component/instant-bet/instant-bet.component';
import { TotalBetComponent } from '../../component/total-bet/total-bet.component';
import { AgentAuthorityComponent } from '../../pages/agent-authority/agent-authority.component';
import { AgentAuthorityEditComponent } from '../../pages/agent-authority-edit/agent-authority-edit.component';
import { SingleItemsComponent } from '../../pages/single-items/single-items.component';
import { MemberInGameComponent } from '../../component/member-in-game/member-in-game.component';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LanguageService } from '../../services/language.service';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CroupierOperationPanelComponent } from '../../component/croupier-operation-panel/croupier-operation-panel.component';
import { TimelyBetComponent } from '../../pages/timely-bet/timely-bet.component';
import { BotSettingComponent } from '../../pages/bot-setting/bot-setting.component';
import { LoadingComponent } from '../../component/loading/loading.component';
import { ReportByDateRangeComponent } from '../../component/report-by-date-range/report-by-date-range.component';
import { ReportByDateRangeMemberComponent } from '../../component/report-by-date-range-member/report-by-date-range-member.component';
import { BotSettingFormComponent } from '../../pages/bot-setting/bot-setting-form/bot-setting-form.component';
import { InsiteProclamationsFormComponent } from '../../pages/insite-proclamations-form/insite-proclamations-form.component';
import { GameTesterFormComponent } from '../../component/game-tester-form/game-tester-form.component';
import { OddsSettingComponent } from '../../component/odds-setting/odds-setting.component';
import { AdProclamationsFormComponent } from '../../pages/ad-proclamations-form/ad-proclamations-form.component';
import { AdProclamationsComponent } from '../../pages/ad-proclamations/ad-proclamations.component';
import { DonateRecordsComponent } from '../../component/donate-records/donate-records.component';
import { NormalProclamationEditComponent } from '../../pages/normal-proclamation-edit/normal-proclamation-edit.component';
import { BotAccountComponent } from '../../pages/bot-account/bot-account.component';
import { RenewDrawResultRecordComponent } from '../../component/renew-draw-result-record/renew-draw-result-record.component';
import { MemberBetRecordComponent } from '../../component/member-bet-record/member-bet-record.component';
import { CalcItemRecordPanelComponent } from '../../component/calc-item-record-panel/calc-item-record-panel.component';
import { VirtualOnlineFormComponent } from '../../pages/virtual-online-members/virtual-online-form/virtual-online-form.component';
import { VirtualOnlineMembersComponent } from '../../pages/virtual-online-members/virtual-online-members.component';
import { CloneAgentComponent } from '../../pages/clone-agent/clone-agent.component';
import { CloneAgentFormComponent } from '../../pages/clone-agent/clone-agent-form/clone-agent-form.component';
import { MarqueeComponent } from '../../component/marquee/marquee.component';
import { RenewDrawRecordComponent } from '../../pages/renew-draw-record/renew-draw-record.component';
import { ReCalcReportComponent } from '../../pages/re-calc-report/re-calc-report.component';
import { LotteryStatisticsComponent } from '../../pages/lottery-statistics/lottery-statistics.component';
import { BaccaratOddsSettingComponent } from '../../component/odds-setting/baccarat-odds-setting/baccarat-odds-setting.component';
import { BaccaratCroupierComponent } from '../../component/baccarat-croupier/baccarat-croupier.component';
import { GameTypeIntroComponent } from '../../pages/timely-bet/game-type-intro/game-type-intro.component';
import { GameTypeRenewComponent } from '../../pages/timely-bet/game-type-renew/game-type-renew.component';
import { BaccaratPokerResultComponent } from '../../component/baccarat-poker-result/baccarat-poker-result.component';
import { AiResultComponent } from '../../component/ai-result/ai-result.component';
import { ExternalWebcamComponent } from '../../pages/external-webcam/external-webcam.component';
import { NewReportRecordComponent } from '../../pages/new-report-record/new-report-record.component';
import { NewReportSicboComponent } from '../../pages/new-report-record/new-report-sicbo/new-report-sicbo.component';
import { NewReportMemberComponent } from '../../pages/new-report-agent/new-report-member/new-report-member.component';
import { NewReportSearchComponent } from '../../pages/new-report-agent/new-report-search/new-report-search.component';
import { NewReportSicboTableComponent } from '../../pages/new-report-agent/new-report-sicbo-table/new-report-sicbo-table.component';
import { NewReportAgentComponent } from '../../pages/new-report-agent/new-report-agent/new-report-agent.component';
import { VipRecordComponent } from '../../pages/vip-record/vip-record.component';
import { VipOperationRecordComponent } from '../../pages/vip-record/vip-operation-record/vip-operation-record.component';
import { DragonTigerCroupierComponent } from '../../component/dragon-tiger-croupier/dragon-tiger-croupier.component';
import { DragonTigerPokerResultComponent } from '../../component/dragon-tiger-poker-result/dragon-tiger-poker-result.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/system/', '.json');
}
@NgModule({
  declarations: [
    AdListComponent,
    TagListComponent,
    MetaComponent,
    OtherComponent,
    ContactComponent,
    GameSettingsComponent,
    GameSettingsListComponent,
    DrawSettingsComponent,
    OtherSettingsComponent,
    BetRecordListComponent,
    CalcItemResultRecordListComponent,
    BoardMessageComponent,
    MessageBlacklistComponent,
    MessageBlacklistEditComponent,
    DonateRecordListComponent,
    ProclamationListComponent,
    ProclamationEditComponent,
    BetRecordEditComponent,
    NormalProclamationListComponent,
    NormalProclamationEditComponent,
    SearchResultComponent,
    BetAmountsComponent,
    BetAmountEditComponent,
    ReportRecordListComponent,
    ReportRecordMemberListComponent,
    MatTableComponent,
    InsiteProclamationsComponent,
    CroupierOperationComponent,
    InstantBetComponent,
    TotalBetComponent,
    AgentAuthorityComponent,
    AgentAuthorityEditComponent,
    SingleItemsComponent,
    MemberInGameComponent,
    CroupierOperationPanelComponent,
    TimelyBetComponent,
    BotSettingComponent,
    BotSettingFormComponent,
    BotAccountComponent,
    LoadingComponent,
    ReportByDateRangeComponent,
    ReportByDateRangeMemberComponent,
    DonateRecordsComponent,
    InsiteProclamationsFormComponent,
    GameTesterFormComponent,
    OddsSettingComponent,
    BaccaratOddsSettingComponent,
    AdProclamationsComponent,
    AdProclamationsFormComponent,
    RenewDrawResultRecordComponent,
    MemberBetRecordComponent,
    VirtualOnlineMembersComponent,
    VirtualOnlineFormComponent,
    CloneAgentComponent,
    CloneAgentFormComponent,
    MarqueeComponent,
    RenewDrawRecordComponent,
    ReCalcReportComponent,
    LotteryStatisticsComponent,
    BaccaratCroupierComponent,
    DragonTigerCroupierComponent,
    GameTypeIntroComponent,
    GameTypeRenewComponent,
    BaccaratPokerResultComponent,
    DragonTigerPokerResultComponent,
    AiResultComponent,
    ExternalWebcamComponent,
    NewReportRecordComponent,
    NewReportSicboComponent,
    NewReportAgentComponent,
    NewReportMemberComponent,
    NewReportSearchComponent,
    NewReportSicboTableComponent,
    VipRecordComponent,
    VipOperationRecordComponent,
  ],
  entryComponents: [
    ReportByDateRangeComponent,
    ReportByDateRangeMemberComponent,
    DonateRecordsComponent,
    RenewDrawResultRecordComponent,
    CalcItemRecordPanelComponent,
    AiResultComponent,
  ],
  imports: [
    CommonModule,
    SystemRoutingModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
      extend: true
    })
  ]
})
export class SystemModule {
  language$ = this.languageService.language$;
  constructor(
    private translateService: TranslateService,
    private languageService: LanguageService,
  ) {
    this.language$.pipe(map(language => language.lang)).subscribe(lang => {
      this.translateService.currentLang = '';
      this.translateService.use(lang);
    });
  }
}
