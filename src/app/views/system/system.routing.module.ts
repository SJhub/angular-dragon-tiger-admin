import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameSettingsComponent } from '../../pages/game-settings/game-settings.component';
import { GameSettingsListComponent } from '../../pages/game-settings-list/game-settings-list.component';
import { DrawSettingsComponent } from '../../pages/draw-settings/draw-settings.component';
import { OtherSettingsComponent } from '../../pages/other-settings/other-settings.component';
import { BetRecordListComponent } from '../../pages/bet-record-list/bet-record-list.component';
import { DonateRecordListComponent } from '../../pages/donate-record-list/donate-record-list.component';
import { CalcItemResultRecordListComponent } from '../../pages/calc-item-result-record-list/calc-item-result-record-list.component';
import { MessageBlacklistComponent } from '../../pages/message-blacklist/message-blacklist.component';
import { MessageBlacklistEditComponent } from '../../pages/message-blacklist-edit/message-blacklist-edit.component';
import { ProclamationListComponent } from '../../pages/proclamation-list/proclamation-list.component';
import { ProclamationEditComponent } from '../../pages/proclamation-edit/proclamation-edit.component';
import { BetRecordEditComponent } from '../../pages/bet-record-edit/bet-record-edit.component';
import { NormalProclamationListComponent } from '../../pages/normal-proclamation-list/normal-proclamation-list.component';
import { SearchResultComponent } from '../../pages/search-result/search-result.component';
import { BetAmountsComponent } from '../../pages/bet-amounts/bet-amounts.component';
import { BetAmountEditComponent } from '../../pages/bet-amount-edit/bet-amount-edit.component';
import { ReportRecordListComponent } from '../../pages/report-record-list/report-record-list.component';
import { ReportRecordMemberListComponent } from '../../pages/report-record-member-list/report-record-member-list.component';
import { InsiteProclamationsComponent } from '../../pages/insite-proclamations/insite-proclamations.component';
import { CroupierOperationComponent } from '../../pages/croupier-operation/croupier-operation.component';
import { AgentAuthorityComponent } from '../../pages/agent-authority/agent-authority.component';
import { AgentAuthority } from '../../enum/agent-authority.enum';
import { AgentAuthorityEditComponent } from '../../pages/agent-authority-edit/agent-authority-edit.component';
import { SingleItemsComponent } from '../../pages/single-items/single-items.component';
import { CroupierOperationPanelComponent } from '../../component/croupier-operation-panel/croupier-operation-panel.component';
import { TimelyBetComponent } from '../../pages/timely-bet/timely-bet.component';
import { BotSettingComponent } from '../../pages/bot-setting/bot-setting.component';
import { BotSettingFormComponent } from '../../pages/bot-setting/bot-setting-form/bot-setting-form.component';
import { InsiteProclamationsFormComponent } from '../../pages/insite-proclamations-form/insite-proclamations-form.component';
import { GameTesterFormComponent } from '../../component/game-tester-form/game-tester-form.component';
import { AdProclamationsComponent } from '../../pages/ad-proclamations/ad-proclamations.component';
import { AdProclamationsFormComponent } from '../../pages/ad-proclamations-form/ad-proclamations-form.component';
import { NormalProclamationEditComponent } from '../../pages/normal-proclamation-edit/normal-proclamation-edit.component';
import { BotAccountComponent } from '../../pages/bot-account/bot-account.component';
import { VirtualOnlineMembersComponent } from '../../pages/virtual-online-members/virtual-online-members.component';
import { VirtualOnlineFormComponent } from '../../pages/virtual-online-members/virtual-online-form/virtual-online-form.component';
import { CloneAgentComponent } from '../../pages/clone-agent/clone-agent.component';
import { CloneAgentFormComponent } from '../../pages/clone-agent/clone-agent-form/clone-agent-form.component';
import { RenewDrawRecordComponent } from '../../pages/renew-draw-record/renew-draw-record.component';
import { ReCalcReportComponent } from '../../pages/re-calc-report/re-calc-report.component';
import { LotteryStatisticsComponent } from '../../pages/lottery-statistics/lottery-statistics.component';
import { ExternalWebcamComponent } from '../../pages/external-webcam/external-webcam.component';
import { NewReportRecordComponent } from '../../pages/new-report-record/new-report-record.component';
import { NewReportAgentComponent } from '../../pages/new-report-agent/new-report-agent/new-report-agent.component';
import { NewReportMemberComponent } from '../../pages/new-report-agent/new-report-member/new-report-member.component';
import { VipRecordComponent } from '../../pages/vip-record/vip-record.component';

export const routes: Routes = [
  {
    path: '',
    data: {
      title: '系統管理'
    },
    children: [
      {
        path: '',
        redirectTo: 'banners',
        pathMatch: 'full'
      },
      // {
      //   path: 'ad-list',
      //   component: AdListComponent,
      //   data: {
      //     title: '廣告管理'
      //   }
      // },
      // {
      //   path: 'tag-list',
      //   component: TagListComponent,
      //   data: {
      //     title: '標籤管理'
      //   }
      // },
      // {
      //   path: 'meta',
      //   component: MetaComponent,
      //   data: {
      //     title: '基本設置'
      //   }
      // },
      {
        path: 'external-webcam',
        component: ExternalWebcamComponent,
        data: {
          title: 'webcam'
        }
      },
      {
        path: 'operation',
        component: CroupierOperationPanelComponent,
        data: {
          title: '荷官操作'
        }
      },
      {
        path: 'bet-record-list',
        component: BetRecordListComponent,
        data: {
          title: '押注記錄',
          auth_manageID: AgentAuthority.OperationItemBetRecordList
        }
      },
      {
        path: 'bet-records/:id',
        component: BetRecordEditComponent,
        data: {
          title: '押注記錄設置',
          auth_manageID: AgentAuthority.OperationItemBetRecordSetting
        }
      },
      {
        path: 'donate-record-list',
        component: DonateRecordListComponent,
        data: {
          title: '打賞記錄',
          auth_manageID: AgentAuthority.OperationItemDonateList
        }
      },
      {
        path: 'calc-item-result-record-list',
        component: CalcItemResultRecordListComponent,
        data: {
          title: '開獎記錄',
          auth_manageID: AgentAuthority.OperationItemcalcItemResultList
        }
      },
      {
        path: 'renew-draw-result-record',
        component: RenewDrawRecordComponent,
        data: {
          title: '開獎修正記錄',
          auth_manageID: AgentAuthority.OperationItemRenewDrawResultRecord
        }
      },
      {
        path: 'lottery-statistics',
        component: LotteryStatisticsComponent,
        data: {
          title: '開獎統計',
          auth_manageID: AgentAuthority.OperationItemLotteryStatistics
        }
      },
      {
        path: 'search-result',
        component: SearchResultComponent,
        data: {
          title: '進階查詢',
          auth_manageID: AgentAuthority.OperationItemSearchResultList
        }
      },
      {
        path: 'report-record-list',
        component: NewReportRecordComponent,
        data: {
          title: '報表紀錄',
          auth_manageID: AgentAuthority.OperationItemReportRecordList
        }
      },
      {
        path: 'report-record-agent',
        component: NewReportAgentComponent,
        data: {
          title: '報表紀錄',
        }
      },
      {
        path: 'report-record-member',
        component: NewReportMemberComponent,
        data: {
          title: '報表紀錄',
        }
      },
      {
        path: 'report-record-member-list/:id/:startDate/:endDate',
        component: ReportRecordMemberListComponent,
        data: {
          title: '報表紀錄(會員)',
          auth_manageID: AgentAuthority.OperationItemReportRecordList
        }
      },
      {
        path: 'bet-amounts',
        component: BetAmountsComponent,
        data: {
          title: '籌碼設置',
          auth_manageID: AgentAuthority.OperationItemBetAmounts
        }
      },
      {
        path: 'bet-amounts-edit/:id',
        component: BetAmountEditComponent,
        data: {
          title: '修改',
          auth_manageID: AgentAuthority.OperationItemBetAmounts
        }
      },
      {
        path: 'game-settings-list',
        component: GameSettingsListComponent,
        data: {
          title: '項目設置',
          auth_manageID: AgentAuthority.OperationItemGameSettingsList
        }
      },
      {
        path: 'game-settings/:id',
        component: GameSettingsComponent,
        data: {
          title: '項目設置-修改',
          auth_manageID: AgentAuthority.OperationItemGameSettingsList
        }
      },
      {
        path: 'croupier-operation',
        component: CroupierOperationComponent,
        data: {
          title: '荷官設置',
          auth_manageID: AgentAuthority.OperationItemCroupierOperation
        }
      },
      {
        path: 'timely-bet',
        component: TimelyBetComponent,
        data: {
          title: '即時下注',
          auth_manageID: AgentAuthority.OperationItemDrawSettings
        }
      },
      {
        path: 'draw-settings',
        component: DrawSettingsComponent,
        data: {
          title: '遊戲設置',
          auth_manageID: AgentAuthority.OperationItemDrawSettings
        }
      },
      {
        path: 'single-items',
        component: SingleItemsComponent,
        data: {
          title: '單注設置',
          auth_manageID: AgentAuthority.OperationItemSingleItems
        }
      },
      {
        path: 'vip-records',
        component: VipRecordComponent,
        data: {
          title: 'VIP記錄',
          auth_manageID: AgentAuthority.OperationItemVipRecords
        }
      },
      {
        path: 'other-settings',
        component: OtherSettingsComponent,
        data: {
          title: '其他設置',
          auth_manageID: AgentAuthority.OperationItemOtherSettings
        }
      },
      {
        path: 'virtual-online-members',
        component: VirtualOnlineMembersComponent,
        data: {
          title: '虛擬人數',
          auth_manageID: AgentAuthority.OperationItemVirtualOnlineMembers
        }
      },
      {
        path: 'virtual-online-form',
        component: VirtualOnlineFormComponent,
        data: {
          title: '虛擬人數設置',
          auth_manageID: AgentAuthority.OperationItemVirtualOnlineMembers
        }
      },
      {
        path: 'virtual-online-form/:id',
        component: VirtualOnlineFormComponent,
        data: {
          title: '虛擬人數設置',
          auth_manageID: AgentAuthority.OperationItemVirtualOnlineMembers
        }
      },
      {
        path: 'bot-settings',
        component: BotSettingComponent,
        data: {
          title: '機器人設置',
          auth_manageID: AgentAuthority.OperationItemBotSettings
        }
      },
      {
        path: 'bot-settings-form',
        component: BotSettingFormComponent,
        data: {
          title: '機器人設置',
          auth_manageID: AgentAuthority.OperationItemBotSettings
        }
      },
      {
        path: 'bot-settings-form/:id',
        component: BotSettingFormComponent,
        data: {
          title: '機器人設置',
          auth_manageID: AgentAuthority.OperationItemBotSettings
        }
      },
      {
        path: 'bot-accounts',
        component: BotAccountComponent,
        data: {
          title: '打賞帳號',
          auth_manageID: AgentAuthority.OperationItemBotAccounts
        }
      },
      {
        path: 'message-blacklists',
        component: MessageBlacklistComponent,
        data: {
          title: '留言過濾列表',
          auth_manageID: AgentAuthority.OperationItemMessageBlacklists
        }
      },
      {
        path: 'message-blacklists/:id',
        component: MessageBlacklistEditComponent,
        data: {
          title: '留言過濾設置',
          auth_manageID: AgentAuthority.OperationItemMessageBlacklists
        }
      },
      {
        path: 'proclamations',
        component: ProclamationListComponent,
        data: {
          title: '即時公告列表',
          auth_manageID: AgentAuthority.OperationItemProclamations
        }
      },
      {
        path: 'normal-proclamations',
        component: NormalProclamationListComponent,
        data: {
          title: '一般公告列表',
          auth_manageID: AgentAuthority.OperationItemNormalProclamations
        }
      },
      {
        path: 'normal-proclamations-form-add',
        component: NormalProclamationEditComponent,
        data: {
          title: '一般公告列表',
          auth_manageID: AgentAuthority.OperationItemNormalProclamations
        }
      },
      {
        path: 'normal-proclamations-form-edit/:id',
        component: NormalProclamationEditComponent,
        data: {
          title: '一般公告列表',
          auth_manageID: AgentAuthority.OperationItemNormalProclamations
        }
      },
      {
        path: 'insite-proclamations',
        component: InsiteProclamationsComponent,
        data: {
          title: '站內訊息',
          auth_manageID: AgentAuthority.OperationItemInsiteProclamations
        }
      },
      {
        path: 'insite-proclamations-form-add',
        component: InsiteProclamationsFormComponent,
        data: {
          title: '新增站內訊息',
          auth_manageID: AgentAuthority.OperationItemInsiteProclamations
        }
      },
      {
        path: 'insite-proclamations-form-edit/:id',
        component: InsiteProclamationsFormComponent,
        data: {
          title: '修改站內訊息',
          auth_manageID: AgentAuthority.OperationItemInsiteProclamations
        }
      },
      {
        path: 'ad-proclamations',
        component: AdProclamationsComponent,
        data: {
          title: '廣告訊息',
          auth_manageID: AgentAuthority.OperationItemAdProclamationsSetting
        }
      },
      {
        path: 'ad-proclamations-form-add',
        component: AdProclamationsFormComponent,
        data: {
          title: '新增廣告訊息',
          auth_manageID: AgentAuthority.OperationItemAdProclamationsSetting
        }
      },
      {
        path: 'ad-proclamations-form-edit/:id',
        component: AdProclamationsFormComponent,
        data: {
          title: '修改廣告訊息',
          auth_manageID: AgentAuthority.OperationItemAdProclamationsSetting
        }
      },
      {
        path: 'proclamations/:id',
        component: ProclamationEditComponent,
        data: {
          title: '公告設置',
          auth_manageID: AgentAuthority.OperationItemProclamationsSetting,
        }
      },
      {
        path: 'agent-authority',
        component: AgentAuthorityComponent,
        data: {
          title: '帳戶權限',
          auth_manageID: AgentAuthority.OperationItemAuthority
        }
      },
      {
        path: 'agent-authority-add',
        component: AgentAuthorityEditComponent,
        data: {
          title: '新增',
          auth_manageID: AgentAuthority.OperationItemAuthority
        }
      },
      {
        path: 'agent-authority-edit/:id',
        component: AgentAuthorityEditComponent,
        data: {
          title: '編輯',
          auth_manageID: AgentAuthority.OperationItemAuthority
        }
      },
      {
        path: 'game-tester-edit/:id',
        component: GameTesterFormComponent,
        data: {
          title: '編輯',
          auth_manageID: AgentAuthority.OperationItemAuthority
        }
      },
      {
        path: 'clone-agent',
        component: CloneAgentComponent,
        data: {
          title: '代理分身',
          auth_manageID: AgentAuthority.OperationItemCloneAgents
        }
      },
      {
        path: 'clone-agent-add',
        component: CloneAgentFormComponent,
        data: {
          title: '代理分身',
          auth_manageID: AgentAuthority.OperationItemCloneAgents
        }
      },
      {
        path: 'clone-agent-edit/:id',
        component: CloneAgentFormComponent,
        data: {
          title: '代理分身',
          auth_manageID: AgentAuthority.OperationItemCloneAgents
        }
      },
      {
        path: 're-calc-report',
        component: ReCalcReportComponent,
        data: {
          title: '報表重算'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
