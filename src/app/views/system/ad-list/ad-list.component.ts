import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.css']
})
export class AdListComponent implements OnInit {

  data = [
    {
      'pic': '5ea01f4ceb1db.jpg',
      'pos': '首頁上方中',
      'enable': 'Y'
    },
    {
      'pic': '5e9fa9cb4e6b0.jpg',
      'pos': '首頁上方右',
      'enable': 'Y'
    },
    {
      'pic': '5e9fa9cb4e6b0.jpg',
      'pos': '首頁下方左',
      'enable': 'Y'
    },
    {
      'pic': '5e9fa9cb4e6b0.jpg',
      'pos': '首頁下方右',
      'enable': 'Y'
    },
    {
      'pic': '5e9fa9cb4e6b0.jpg',
      'pos': '首頁套餐組合	',
      'enable': 'Y'
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
