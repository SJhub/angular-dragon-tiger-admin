import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meta',
  templateUrl: './meta.component.html',
  styleUrls: ['./meta.component.css']
})
export class MetaComponent implements OnInit {
  data =
    {
      'title': '',
      'keywords': '',
      'description': '',
      'indexTitle': '',
      'indexPic': '',
      'faviconPic': ''
    };

  constructor() { }

  ngOnInit() {
  }

}
