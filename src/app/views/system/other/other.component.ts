import { Component, OnInit } from '@angular/core';
import { SystemService } from '../../../services/system.service';
import { SystemSetting } from '../../../models/system-setting';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css']
})
export class OtherComponent implements OnInit {

  systemsetting: SystemSetting;
  myForm: FormGroup;


  constructor(
    private systemService: SystemService,
    private fb: FormBuilder,
  ) {
    this.groupForm();
  }

  ngOnInit() {
    this.setForm(1);
  }

  groupForm() {
    this.myForm = this.fb.group({
      'wash_code_amount': [null],
      'draw_seconds': [null],
      'draw_result': [null]
    })
  }

  setForm(id) {
    this.systemService.getSystemSetting(id).subscribe(
      res => {
        let data = res.data;

        this.myForm.patchValue({
          wash_code_amount: data.wash_code_amount,
          draw_seconds: data.draw_seconds,
          draw_result: data.draw_result,
        })
      }
    )
  }

  submit(val) {
    if (this.myForm.valid) {
      val.wash_code_amount = parseInt(val.wash_code_amount);
      val.draw_seconds = parseInt(val.draw_seconds);
      val.id = 1;
      this.systemService.updateSystemSetting(val).subscribe(
        () => {
          this.setForm(1);
          alert('修改成功');
        },
        err => {
          alert(`操作失敗: ${err.error.message}`);
        }
      )
    }
  }

}
