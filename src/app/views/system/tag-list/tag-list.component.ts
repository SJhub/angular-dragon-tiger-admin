import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {

  data = [
    {
      'title': '評價',
      'url': 'https://google.com',
    },
    {
      'title': '測試',
      'url': '',
    },
    {
      'title': '標籤',
      'url': '',
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
