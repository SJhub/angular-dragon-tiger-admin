import { Component, OnInit } from '@angular/core';
import { MemberService } from '../../services/member.service';
import { News } from '../../models/news';
import { commonStatusEnum } from '../../enum/common.status.enum';
import { commonEnum } from '../../enum/common.enum';
import { newsEnum } from '../../enum/news.enum';
import { NewsService } from '../../services/news.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {
  newsHeads: Array<string> = [
    newsEnum.TITLE,
    newsEnum.PIC_PATH,
    newsEnum.BODY,
    commonEnum.AUDIENCE,
    commonStatusEnum.OPERATION
  ];
  news: News[];

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.getNews();
  }

  getAudienceName(audience: number): string {
    if (audience == parseInt(commonEnum.AUDIENCE_MEMBER)) {
      return commonEnum.MEMBER;
    } else if (audience == parseInt(commonEnum.AUDIENCE_BOTH)) {
      return commonEnum.BOTH_CHTNAME;
    }
  }

  getNews(): void {
    this.newsService.getAllNews()
      .subscribe(
        res => this.news = res.data,
        error => console.log(error)
      )
  }

  addNews(newsTitle: string): void {
    let newsObj = {
      title: newsTitle,
      pic: newsEnum.DEFAULT_NEWS_PIC,
      body: newsTitle,
      audience: parseInt(commonEnum.AUDIENCE_MEMBER),
    }
    this.newsService.addNews(newsObj)
      .subscribe(
        () => this.getNews(),
        error => alert(commonStatusEnum.ADD_FAILED)
      )
  }
}
