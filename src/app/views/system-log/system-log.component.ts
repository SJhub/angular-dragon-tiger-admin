import { Component, OnInit, ViewChild } from '@angular/core';
import { SystemService } from '../../services/system-log.service';
import { System } from '../../models/system-log';

import { MemberService } from '../../services/member.service';
import { Member } from '../../models/member';

import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Role } from '../../enum/role.enum';
import { Type } from '../../enum/type.enum';

@Component({
  selector: 'app-system-log',
  templateUrl: './system-log.component.html',
  styleUrls: ['./system-log.component.css']
})
export class SystemLogComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // @ViewChild(MatButtonModule, { static: true }) button: MatButtonModule;
  displayColumns: string[] = ['driverID', 'role', 'status', 'IP', 'created_at'];
  dataSource = new MatTableDataSource<System>();

  OptionRole = [
    { value: 'searchByMember()', viewValue: '會員' },
    { value: 'searchByUser()', viewValue: '成員' }
  ];

  system: System[];

  member: Member[];

  user: User[];

  selectRole: string;

  constructor(
    private systemService: SystemService,
    private memberService: MemberService,
    private userService: UserService
  ) { }

  getAllSystemLog() {
    this.systemService.getSystemLogs().subscribe(
      res => {
        this.system = res.data;
        this.dataSource.data = this.system;

        this.dataSource.data.map(item => {
          item.message = JSON.parse(item.message)
        })

        this.dataSource.data.find(item => {
          item.message['role'] = Role[item.message['role']];
          item.message['type'] = Type[item.message['type']];
        })
      },
      error => console.log(error)
    )
  }

  getDriverName(name: number): string {
    let roleName = this.member.find(item => item.id === name);
    if (roleName) {
      return roleName.name;
    }

    return '帳號已移除';
  }

  getRole(Role: string): any {
    switch (Role) {
      case '會員':
        return this.member;
      case '成員':
        return this.user;
      default:
        return '';
    }
  }

  getRoleName(Role: any[], role_id: number): string {
    if (Role === undefined) {
      return;
    }

    let roleName = Role.find(item => item.id === role_id);

    if (roleName) {
      return roleName.name;
    }

    return '帳號已移除';
  }

  getMember() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.member = res.data;
      },
      error => console.log(error)
    )
  }

  getUser() {
    this.userService.getUsers().subscribe(
      res => {
        this.user = res.data;
      },
      error => console.log(error)
    )
  }

  ngOnInit(): void {
    this.getAllSystemLog();
    this.getMember();
    this.getUser();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    //Material filter
    this.dataSource.filterPredicate = (data: System, filter: string): boolean => {

      let name = this.getRoleName(this.getRole(data.message['role']), data.message['role_id']);
      let type = data.message['type'].includes(filter);
      let role = data.message['role'].includes(filter);
      let IP = data.ip.includes(filter);
      let createtime = data.created_at.includes(filter);

      return name.includes(filter) || type || role || IP || createtime;
    };
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  searchByMember() {
    this.dataSource.data = this.system.filter(
      item => item.message['role'] === '會員'
    );
  }

  searchByUser() {
    this.dataSource.data = this.system.filter(
      item => item.message['role'] === '成員'
    );
  }

  clearAll() {
    this.dataSource.data = this.system;
  }
}
