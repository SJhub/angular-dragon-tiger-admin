import { Component, OnInit, ViewChild } from '@angular/core';
import { PaginateConfig } from '../../../ckeditor/paginateconfig';
import { NewsService } from '../../../services/news.service';
import { News } from '../../../models/news';
import { newsEnum } from '../../../enum/news.enum';
import { commonEnum } from '../../../enum/common.enum';
import { commonStatusEnum } from '../../../enum/common.status.enum';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['title', 'src', 'audience', 'edit'];
  dataSource = new MatTableDataSource<News>();

  newsHeads: Array<string> = [
    newsEnum.TITLE,
    newsEnum.PIC_PATH,
    newsEnum.BODY,
    commonEnum.AUDIENCE,
    commonStatusEnum.OPERATION
  ];
  public config = new PaginateConfig().config;

  news: News[];
  id: any;


  constructor(
    private newsServ: NewsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getAllNews();
    this.dataSource.data = this.news;
  }

  getAudienceName(audience: number): string {
    if (audience == parseInt(commonEnum.AUDIENCE_MEMBER)) {
      return commonEnum.MEMBER;
    } else if (audience == parseInt(commonEnum.AUDIENCE_BOTH)) {
      return commonEnum.BOTH_CHTNAME;
    }
  }

  getAllNews() {
    this.newsServ.getAllNews().subscribe(
      res => {
        this.news = res.data as News[];
        this.dataSource.data = this.news;
        // console.log(this.news);

      },
      error => console.log(error)
    )
  }

  delete(id: number) {
    if (confirm('確定刪除?')) {
      this.newsServ.deleteNews(id).subscribe(
        () => this.getAllNews(),
        err => alert(err.error.message)
      )
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
