import { Component, OnInit, TemplateRef } from '@angular/core';
import { Validators, FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Location } from '@angular/common';
import { NewsService } from '../../../services/news.service';
import { ActivatedRoute } from '@angular/router';
import { News } from '../../../models/news';
import { commonEnum } from '../../../enum/common.enum';
import { options } from '../../../ckeditor/config';
import { UploadFileService } from '../../../services/upload-file.service';
import { AuthenticationService } from '../../../services/auth.service';

@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.css']
})
export class NewsFormComponent implements OnInit {

  public myForm: FormGroup;
  modalRef: BsModalRef;
  newsId: any;
  avatarUrl: string;
  config = options;

  news: News;

  constructor(
    private newsServ: NewsService,
    private fb: FormBuilder,
    private modalService: BsModalService,
    private location: Location,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private uploadFileServ: UploadFileService,
  ) {
    this.groupMyForm();
  }

  ngOnInit(): void {
    this.setMyForm();
  }

  groupMyForm() {
    this.myForm = this.fb.group({
      'id': [null],
      'title': [null],
      'pic': [null],
      'body': [null],
      'audience': 3,
      'is_push_notify': 0
    });
  }


  setMyForm() {
    this.newsId = this.route.snapshot.paramMap.get('id');
    if (this.newsId) {
      this.newsServ.getNews(this.newsId).subscribe(res => {
        let data = res.data as News;
        this.avatarUrl = data.pic;

        this.myForm.patchValue({
          id: data.id,
          title: data.title,
          pic: data.pic,
          body: data.body,
          audience: data.audience,
          is_push_notify: data.is_push_notify
        })

      },
        err => {
          alert(`取得資料失敗 ${err.status} ${err.statusText}`);
        });
    }

  }

  submitForm(val) {
    if (this.myForm.valid) {
      val.pic = this.avatarUrl;
      val.is_push_notify = val.is_push_notify ? 1 : 0;
      // return;
      if (this.newsId) {
        this.newsServ.updateNews(val).subscribe(
          () => {
            this.location.back();
          },
          err => {
            alert(`操作失敗: ${err.error.message}`);
          }
        );
      }
      else {
        this.newsServ.createNews(val).subscribe(
          () => {
            this.location.back();
          },
          err => {
            alert(`操作失敗: ${err.error.message}`);
          }
        )
      }

    } else {
      alert('請正確填寫資料');
      this.markFormGroupTouched(this.myForm);
    }
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  cancel() {
    this.location.back();
  }

  openCropper(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
  }

  getImageCropperUrl(e) {
    this.avatarUrl = e;
    this.myForm.get('pic').setValue(e);
    this.modalRef.hide();
  }

  fileUploadRequest(e) {
    const file = e.data.requestData.upload.file as File;
    const fileName = this.uploadFileServ.getRandomFileName(file.name);
    const file$ = new File([file.slice()], fileName);
    e.data.requestData.upload;
    e.data.requestData.file = file$;
    e.data.requestData.token = this.authService.getToken();
    delete e.data.requestData.upload;
  }

}
