import { UserRole } from './../../enum/user-role.enum';
import { Identity } from './../../enum/identity.enum';
import { Location } from '@angular/common';
import { AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { MenuService } from '../../services/menu.service';
import { Menu } from '../../models/menu.model';
import { TokenService } from '../../services/token.service';

import { Member } from '../../models/member.model';
import { MemberService } from '../../services/member.service';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { AgentService } from '../../services/agent.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProclamationService } from '../../services/proclamation.service';
import { Proclamation } from '../../models/proclamation';
import { ProclamationTypesEnum } from '../../enum/proclamation-type.enum';
import { AuthenticationService } from '../../services/auth.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit, AfterViewInit {

  @ViewChild('template') public openModalWithClass: TemplateRef<any>;

  Proclamation: Proclamation[];
  member: Member[] = [];
  user: User[] = [];

  dataOfAgentLength: number;
  loginUserID: number;
  memberLengthByAgent: number;
  identityID: number;
  lastLoginAt: string;
  identity: number;
  countOfSystem = 0;
  countOfAgent = 0;
  countOfMember = 0;

  radioModel: string = 'Month';

  bsModalRef: BsModalRef;
  modalRef: BsModalRef;
  flag: any;

  // lineChart1
  public lineChart1Data: Array<any> = [
    {
      data: [65, 59, 84, 84, 51, 55, 40],
      label: 'Series A'
    }
  ];
  public lineChart1Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChart1Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: 40 - 5,
          max: 84 + 5,
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart1Colours: Array<any> = [
    {
      backgroundColor: getStyle('--primary'),
      borderColor: 'rgba(255,255,255,.55)'
    }
  ];
  public lineChart1Legend = false;
  public lineChart1Type = 'line';

  // lineChart2
  public lineChart2Data: Array<any> = [
    {
      data: [1, 18, 9, 17, 34, 22, 11],
      label: 'Series A'
    }
  ];
  public lineChart2Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChart2Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: 1 - 5,
          max: 34 + 5,
        }
      }],
    },
    elements: {
      line: {
        tension: 0.00001,
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart2Colours: Array<any> = [
    { // grey
      backgroundColor: getStyle('--info'),
      borderColor: 'rgba(255,255,255,.55)'
    }
  ];
  public lineChart2Legend = false;
  public lineChart2Type = 'line';


  // lineChart3
  public lineChart3Data: Array<any> = [
    {
      data: [78, 81, 80, 45, 34, 12, 40],
      label: 'Series A'
    }
  ];
  public lineChart3Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChart3Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false
      }],
      yAxes: [{
        display: false
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart3Colours: Array<any> = [
    {
      backgroundColor: 'rgba(255,255,255,.2)',
      borderColor: 'rgba(255,255,255,.55)',
    }
  ];
  public lineChart3Legend = false;
  public lineChart3Type = 'line';


  // barChart1
  public barChart1Data: Array<any> = [
    {
      data: [78, 81, 80, 45, 34, 12, 40, 78, 81, 80, 45, 34, 12, 40, 12, 40],
      label: 'Series A',
      barPercentage: 0.6,
    }
  ];
  public barChart1Labels: Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'];
  public barChart1Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false,
      }],
      yAxes: [{
        display: false
      }]
    },
    legend: {
      display: false
    }
  };
  public barChart1Colours: Array<any> = [
    {
      backgroundColor: 'rgba(255,255,255,.3)',
      borderWidth: 0
    }
  ];
  public barChart1Legend = false;
  public barChart1Type = 'bar';

  // mainChart

  public mainChartElements = 27;
  public mainChartData1: Array<number> = [];
  public mainChartData2: Array<number> = [];
  public mainChartData3: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Current'
    },
    {
      data: this.mainChartData2,
      label: 'Previous'
    },
    {
      data: this.mainChartData3,
      label: 'BEP'
    }
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Thursday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips,
      intersect: true,
      mode: 'index',
      position: 'nearest',
      callbacks: {
        labelColor: function (tooltipItem, chart) {
          return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
        }
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          callback: function (value: any) {
            return value.charAt(0);
          }
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 250
        }
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };
  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  // social box charts

  public brandBoxChartData1: Array<any> = [
    {
      data: [65, 59, 84, 84, 51, 55, 40],
      label: 'Facebook'
    }
  ];
  public brandBoxChartData2: Array<any> = [
    {
      data: [1, 13, 9, 17, 34, 41, 38],
      label: 'Twitter'
    }
  ];
  public brandBoxChartData3: Array<any> = [
    {
      data: [78, 81, 80, 45, 34, 12, 40],
      label: 'LinkedIn'
    }
  ];
  public brandBoxChartData4: Array<any> = [
    {
      data: [35, 23, 56, 22, 97, 23, 64],
      label: 'Google+'
    }
  ];

  public brandBoxChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public brandBoxChartOptions: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false,
      }],
      yAxes: [{
        display: false,
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };
  public brandBoxChartColours: Array<any> = [
    {
      backgroundColor: 'rgba(255,255,255,.1)',
      borderColor: 'rgba(255,255,255,.55)',
      pointHoverBackgroundColor: '#fff'
    }
  ];
  public brandBoxChartLegend = false;
  public brandBoxChartType = 'line';

  constructor(
    private authServ: AuthenticationService,
    private memberService: MemberService,
    private userService: UserService,
    private agentService: AgentService,
    private modalService: BsModalService,
    private proclamationService: ProclamationService,
    private location: Location,
  ) { }
  ngAfterViewInit(): void {

  }

  get UserRole() {
    return UserRole;
  }

  /**
   * 取得全部用戶 - 最高權限&管理者
   */
  getUsers() {
    this.userService.getUsers().subscribe(
      res => {
        this.user = res.data;
        const data = this.user.filter(e => e.identity !== UserRole.agent && e.identity !== UserRole.topAgent);
        this.countOfSystem = data.length;
      },
      error => console.log(error)
    );
  }

  /**
   * 
   * @param parentId
   */
  getAgentAndMemberTotalByAgentId(id) {
    if (this.identityID === UserRole.admin) {
      this.agentService.getAgentTotal().subscribe(r => this.countOfAgent = r.data);
      return this.memberService.getMemberTotal().subscribe(r => this.countOfMember = r.data);
    }

    this.agentService.getAgentTotalByAgentId(id).subscribe(
      res => {
        const data = res.data;
        this.countOfAgent = data.count_of_agent;
        this.countOfMember = data.count_of_member;
      }
    );
  }

  /**
   * 取得用戶資料 by parent id
   */
  getUserByParentID(parentId) {
    this.userService.getUserByParentID(parentId).subscribe(
      res => {
        const data = res.data;
        this.dataOfAgentLength = data.length;
        this.countOfSystem = data.filter(e => e.identity === UserRole.admin || e.identity === UserRole.management);
      }
    );
  }

  /**
   * 取得代理底下的會員資料
   */
  getMemberByAgent(id) {
    this.memberService.getMemberByAgent(id).subscribe(
      res => {
        const data = res.data;
        this.memberLengthByAgent = data.length;
        // console.log(data);
      }
    );
  }

  /**
   * 取得用戶自己的資料
   */
  getUser(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data;
        this.identityID = data.identity;

        if (this.identityID === UserRole.admin) {
          this.getUsers();
        }

        if (this.identityID === UserRole.agent || this.identityID === UserRole.topAgent) {
          this.getProclamationsByTypeLargerThenDate(2, this.lastLoginAt);
        }

        if (this.identityID === UserRole.topAgent || this.identityID === UserRole.agent) {
          this.getUserByParentID(this.loginUserID);
        }

        this.getAgentAndMemberTotalByAgentId(data.id);

      }
    );
  }

  /**
   *
   * 依照type, date 取得站內訊息
   */
  getProclamationsByTypeLargerThenDate(id: number, date: string) {
    this.proclamationService.getProclamationsByTypeLargerThenDate(id, date).subscribe(
      res => {
        const data = res.data;
        const createdAt = data.created_at;
        this.Proclamation = data;

        if (this.Proclamation.length > 0) {
          this.modalRef = this.modalService.show(
            this.openModalWithClass,
            Object.assign({}, { class: 'gray modal-l' })
          );
        }
      }
    );
  }

  cancel() {
    this.location.back();
  }

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  ngOnInit(): void {
    this.loginUserID = Number(localStorage.getItem('authenticatedID'));
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.lastLoginAt = localStorage.getItem('last_login_at');
    // generate random values for mainChart
    for (let i = 0; i <= this.mainChartElements; i++) {
      this.mainChartData1.push(this.random(50, 200));
      this.mainChartData2.push(this.random(80, 100));
      this.mainChartData3.push(65);
    }

    this.getUser(this.loginUserID);
  }
}
