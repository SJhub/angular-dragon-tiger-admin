import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { CommonModule } from '@angular/common';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../services/language.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/dashboard/', '.json');
}
@NgModule({
  imports: [
    FormsModule,
    DashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
      extend: true
    })
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule {
  language$ = this.languageService.language$;
  constructor(
    private translateService: TranslateService,
    private languageService: LanguageService,
  ) {
    this.language$.pipe(map(language => language.lang)).subscribe(lang => {
      this.translateService.currentLang = '';
      this.translateService.use(lang);
    });
  }
}
