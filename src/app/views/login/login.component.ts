import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/auth.service';
import CaptchaMini from 'captcha-mini';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';

declare var $: any;
@Component({
    selector: 'app-dashboard',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
    username: string = "";
    password: string = "";
    invalidLogin = true;
    loginSuccess = false;
    isLogging = false;
    CaptchaResponse: string;
    captcha1: any;
    captchaPass: any;
    inputValue: any;
    lastLoginAt: any;

    gotoPath = '/my-account';

    easyLogin = environment.easyLogin;

    constructor(
        private translateService: TranslateService,
        private authenticationService: AuthenticationService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.captcha1 = new CaptchaMini({
            fontSize: 35,
        });
        this.captcha1.draw(document.querySelector('#captcha1'), r => {
            this.captchaPass = r;
            // console.log(this.captchaPass, '验证码1');
        });

        this.setMetaViewport(true);
    }



    /**
     * login - member login
     */
    login(): void {
        // if (!this.inputValue) {
        //     alert(this.translateService.instant('login.verify.input'));
        //     return;
        // }
        // if (this.inputValue.toLowerCase() === this.captchaPass.toLowerCase()) {
        if (!this.username || !this.password) return alert('請輸入帳號密碼');
        this.isLogging = true;
        this.authenticationService.loginUser(this.username.trim(), this.password)
            .subscribe(
                result => {
                    const data = result.data;
                    if (this.authenticationService.isUserLoggedIn()) {
                        this.invalidLogin = false;
                        this.loginSuccess = true;
                    } else {
                        this.invalidLogin = true;
                        this.loginSuccess = false;
                        alert(this.translateService.instant('login.action.failed'));
                    }
                    this.setMetaViewport(false);
                },
                error => {
                    this.invalidLogin = true;
                    this.loginSuccess = false;
                    this.isLogging = false;
                    // alert(this.translateService.instant('login.action.failed'));
                    alert(`${error.error.message}`);
                }
            );
        // } else {
        //     alert(this.translateService.instant('login.verify.failed'));
        // }
    }

    resolved(captchaResponse: string) {
        this.CaptchaResponse = captchaResponse;
    }

    setMetaViewport(lock: boolean) {
        $('meta[name=viewport]').remove();
        if (lock) {
            $('head').append('<meta name="viewport" content="width=device-width, maximum-scale=1.0, user-scalable=0">');
        } else {
            $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">');
        }
    }
}