import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {

  data = [
    {
      'created_time': '2020-03-11',
      'comment': '很棒!',
      'product_name': '商品A',
      'user_name': '王小美',
      'audit': 'N'
    },
    {
      'created_time': '2020-03-11',
      'comment': '很棒!',
      'product_name': '商品A',
      'user_name': '王小美',
      'audit': 'N'
    },
    {
      'created_time': '2020-03-11',
      'comment': '很棒!',
      'product_name': '商品A',
      'user_name': '王小美',
      'audit': 'Y'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
