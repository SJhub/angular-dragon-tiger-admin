import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { AgentService } from '../../../services/agent.service';
import { Agent } from '../../../models/agent';
import { Location } from '@angular/common';
import { BetAmountService } from '../../../services/bet-amount.service';
import { Betamount } from '../../../models/bet-amount';
import { AgentAuthority } from '../../../enum/agent-authority.enum';
import { ItemTypesService } from '../../../services/item-types.service';
import { ItemTypes } from '../../../models/item-types';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../../../enum/user-role.enum';
import { SingleBetPanelComponent } from '../../../component/single-bet-panel/single-bet-panel.component';
import { defaultAgentOperation } from '../../../common/global';
import { AllowGameComponent } from '../../../component/allow-game/allow-game.component';
import { CommissionSettingComponent } from '../../../component/commission-setting/commission-setting.component';
import { UserWallet } from '../../../models/user-wallet';
import { genCode, MAX_WALLET_AMOUNT } from '../../../common/common';
import { getFormCtrlErrDesc } from '../../../common/form';

const POSITIVE_INTEGER = /^\d+$/;
const DEFAULT_AGENT_OPERATION = defaultAgentOperation;
@Component({
  selector: 'app-admin-agent',
  templateUrl: './admin-agent.component.html',
  styleUrls: ['./admin-agent.component.css']
})
export class AdminAgentComponent implements OnInit {
  @ViewChild('singlePanel') singlePanel: SingleBetPanelComponent;
  @ViewChild('allowGame') allowGame: AllowGameComponent;
  @ViewChild('commissionSetting') commissionSetting: CommissionSettingComponent;
  agent: Agent;
  myForm: FormGroup;
  id: number;
  user: User;
  betamount: Betamount[];
  itemTypes: ItemTypes[];

  parentWallet: number;
  parentRevenueShare = 100;
  parentWashCodePercent = 1;
  washCodeRange = [];
  revenueShareRange = [];
  parentSingleBetLimits: any;
  parentId: number;
  parentSingleBetTotal: number;
  usedSingleBetTotal: number;
  availableSingleBetTotal: number;

  parent: User;
  parentOperationItems: Array<string> = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private agentService: AgentService,
    private location: Location,
    private userService: UserService,
    private betAmountService: BetAmountService,
    private itemTypesService: ItemTypesService,
    private translateService: TranslateService,
  ) {
    this.groupMyForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.getBetAmountList();
    this.generateRange();
    // this.setRandomCode(6);
    this.genInvationCode();
    this.getItemTypesList();
  }

  groupMyForm() {
    this.myForm = this.fb.group({
      'name': [null, [Validators.pattern('^[A-Za-z0-9]+$'), Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      'password': [null, [Validators.required, Validators.pattern('^[A-Za-z0-9]+$'), Validators.minLength(6), Validators.maxLength(12)]],
      're_password': [null],
      'real_name': [null, [Validators.required, Validators.maxLength(10)]],
      'credit': [null],
      'wallet': [null, [Validators.min(0), Validators.max(MAX_WALLET_AMOUNT), Validators.pattern(POSITIVE_INTEGER)]],
      'rebate': [null],
      'single_bet_amount': [null],
      'status': [1],
      'wash_code_commission_percent': [0, [Validators.required, Validators.max(1)]],
      'revenue_share': [null, [Validators.required, Validators.min(0), Validators.max(100)]],
      'operation_items': new FormArray([]),
      'remark': [null],
      'bank_account': [null],
      'invitation_code': []
    });
  }

  submitForm(val) {
    val.parent_id = this.id;
    val.status = val.status ? 1 : 0;
    val.single_bet_amount = Number(val.single_bet_amount);
    val.revenue_share = Number(val.revenue_share);
    val.operation_items = JSON.stringify(DEFAULT_AGENT_OPERATION);
    val.wash_code_commission_percent = Number(val.wash_code_commission_percent);
    val.credit = val.wallet;

    let wallet = {
      dragon_tiger_credit: val.credit,
      dragon_tiger_wallet: val.wallet,
      sic_bo_credit: 0,
      sic_bo_wallet: 0,
      baccarat_wallet: 0,
      baccarat_credit: 0,
      poker_credit: 0,
      poker_wallet: 0,
    } as UserWallet;

    val.user_wallet = JSON.stringify(wallet);
    val.bet_limits = '[]';
    val.allow_game = '[]';
    val.world_football_game_settings = '[]';
    val.allow_game = JSON.stringify(this.allowGame.getValue());
    val.commission = JSON.stringify(this.commissionSetting.getValue());


    if (this.parentWallet && val.wallet > this.parentWallet) {
      return alert(this.translateService.instant('manageAgent.form.balanceInsufficient'));
    }

    // if (val.wash_code_commission_percent > val.revenue_share) {
    //   return alert(this.translateService.instant('biggerThanShare'));
    // }

    if (val.revenue_share > this.parentRevenueShare) {
      return alert(this.translateService.instant('biggerThanParent'));
    }

    if (!this.singlePanel.betTotalAmountValid) {
      return alert(this.translateService.instant('betTotalExceeded'));
    }

    if (this.myForm.valid && this.singlePanel.getFormValid()) {
      const singlePanelValue = this.singlePanel.getFormValue();
      val.single_bet_limits = singlePanelValue.single_bet_limits;
      val.single_bet_total_amounts = singlePanelValue.single_bet_total_amounts;

      this.agentService.addAgent(val).subscribe(
        () => {
          let successMsg = this.translateService.instant('common.addSuccess');
          alert(successMsg);
          this.location.back();
        },
        err => {
          alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`);
        }
      );
    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      this.markFormGroupTouched(this.myForm);
    }
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }



  getBetAmountList() {
    this.betAmountService.getBetAmountList().subscribe(
      res => {
        this.betamount = res.data.filter(e => e.type === 0);
      }
    );
  }

  getItemTypesList() {
    this.itemTypesService.getItemTypesList().subscribe(
      res => {
        this.itemTypes = res.data as ItemTypes[];

        // for (let i = 0; i < this.itemTypes.length; i++) {
        //   this.addSpec(this.itemTypes[i].id);
        // }

        this.getUser(this.id);
      }
    );
  }

  /**
   * 取得User資料 By ID
   */
  getUser(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data as User;
        this.parentRevenueShare = data.revenue_share;
        this.parentWashCodePercent = data.wash_code_commission_percent;
        this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);
        this.parentWallet = data.user_wallet.dragon_tiger_wallet;
        this.parentId = data.parent_id;
        this.parent = data;

        this.myForm.get('wash_code_commission_percent').setValue(data.wash_code_commission_percent);
        this.myForm.get('revenue_share').setValue(data.revenue_share);

        try {
          this.parentOperationItems = JSON.parse(data.operation_items);
        } catch (error) {
          console.log(error);
        }
        if (data.single_bet_limits) {
          this.parentSingleBetLimits = data.single_bet_limits;
        }

        this.getUserByParentId(this.id);
      }
    );
  }

  getUserByParentId(id: number) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data as User;
        if (data.identity !== UserRole.admin) {
          this.parentRevenueShare = data.revenue_share;
          this.parentWashCodePercent = data.wash_code_commission_percent;
          this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);
        }

      }
    );
  }

  generateRange() {
    const max = 100;
    for (let index = 0; index < (max / 10); index++) {
      this.washCodeRange.push((index + 1) / 10);
    }

    for (let index = 1; index <= (max / 10); index++) {
      this.revenueShareRange.push(index * 10);
    }
  }

  updateAgentWashCode() {
    const wash_code_commission_percent = Number(this.myForm.get('wash_code_commission_percent').value);
    const data = {
      id: this.id,
      wash_code_commission_percent
    };
    this.agentService.updateAgentWashCodeCommissionPercent(data).subscribe(() => {
      alert('更新成功');
    }, () => alert('非更新時段時間'));
  }

  get AgentAuthority() {
    return AgentAuthority;
  }

  /**
   * 產生密碼
   */
  setRandomCode(len: number) {
    const characters = '23456789';
    let code = '';
    let n = 0;
    let randomnumber = 0;
    while (n < len) {
      n++;
      randomnumber = Math.floor(characters.length * Math.random());
      code += characters.substring(randomnumber, randomnumber + 1);
    }
    this.myForm.controls['password'].setValue(code.toLocaleUpperCase());
  }

  onCheckboxChange(e) {
    const operationItem: FormArray = this.myForm.get('operation_items') as FormArray;

    if (e.target.checked) {
      operationItem.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      operationItem.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          operationItem.removeAt(i);
          return;
        }
        i++;
      });
    }
  }


  getSpecFormGroup(i: number): FormGroup {
    const spec_array = this.myForm.get('single_bet_limits') as FormArray;
    const formGroup = spec_array.controls[i] as FormGroup;
    return formGroup;
  }

  getItemValue(id: number) {
    if (this.parentSingleBetLimits) {
      const amount = this.parentSingleBetLimits.find(e => e.id === id).amount;
      return amount;
    }
  }

  getChecked(OperationItemID) {
    const operationItem = this.myForm.get('operation_items').value as Array<any>;
    // console.log(operationItem);

    return operationItem.includes(String(OperationItemID));
  }

  setChecked(operationItems: string) {
    if (operationItems === 'null') {
      return;
    }
    const items = JSON.parse(operationItems) as Array<string>;
    const operationItem = this.myForm.get('operation_items') as FormArray;

    items.forEach(item => {
      operationItem.push(new FormControl(item));
    });
  }

  cancel() {
    this.location.back();
  }

  isExistOperation(pageID: number) {
    return this.parentOperationItems.includes(String(pageID));
  }

  isParentOperationEmpty() {
    for (const item of DEFAULT_AGENT_OPERATION) {
      if (this.parentOperationItems.some(e => e == String(item))) {
        return false;
      };
    }
    return true;
  }

  async genInvationCode(retryTimes = 5) {
    let ok = false;
    this.myForm.get('invitation_code').setValue('');
    for (let index = 0; index < retryTimes; index++) {
      let invitationCode = genCode();
      await this.agentService.getByInvitationCode(invitationCode).toPromise().then(r => {
        if (!r.data) {
          this.myForm.get('invitation_code').setValue(invitationCode);
          ok = true;
        }
      });

      if (ok) return;
    }
    alert('產生邀請碼失敗，頁面將自動重新整理');
    window.location.reload();
  }

  getFormCtrlErrDesc(ctrl: AbstractControl) { return getFormCtrlErrDesc(ctrl) }
}
