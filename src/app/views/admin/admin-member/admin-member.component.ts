import { Location } from '@angular/common';
import { forkJoin } from 'rxjs';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PaginateConfig } from '../../../ckeditor/paginateconfig';
import { Status } from '../../../enum/status.enum';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { MemberService } from '../../../services/member.service';
import { Member } from '../../../models/member.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AgentService } from '../../../services/agent.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EnumValueToEnumKeyPipe } from '../../../pipe/enum-value-to-enum-key.pipe';
import { UserRole } from '../../../enum/user-role.enum';
import { TranslateService } from '@ngx-translate/core';
import { EnumTranslateService } from '../../../services/enum-translate.service';
import { addIndexToArray } from '../../../common/global';



@Component({
  selector: 'app-admin-member',
  templateUrl: './admin-member.component.html',
  styleUrls: ['./admin-member.component.scss']
})
export class AdminMemberComponent implements OnInit {

  @ViewChild('paginatorOfAgent', { static: true }) paginatorOfAgent: MatPaginator;
  @ViewChild('paginatorOfMember', { static: true }) paginatorOfMember: MatPaginator;

  @ViewChild('sortByAgent', { static: true }) sortByAgent: MatSort;
  @ViewChild('sortByMember', { static: true }) sortByMember: MatSort;
  displayedColumnsOfAgent: string[] = ['index', 'name', 'real_name', 'wallet', 'wash_code_commission_percent', 'revenue_share', 'function'];
  displayedColumnsOfMember: string[] = ['index', 'account', 'name', 'wallet', 'wash_code_commission_percent'];

  dataSourceOfAgent = new MatTableDataSource<User>();
  dataSourceOfMember = new MatTableDataSource<Member>();

  public config = new PaginateConfig().config;
  user: User[];
  member: Member[];
  id: any;
  parent_id: any;
  userID: number;
  identityID: number;
  userByIdentity: number;
  changeAgents: any;
  status: boolean = false;
  agentStatus: boolean = false;

  userInfo: User;
  agentArray: User[];
  memberArray: Member[];

  agentCount: number;
  memberCount: number;

  modalRef: BsModalRef;
  modal: any;

  allowGameStatus: number;

  agentLength: number;
  memberLength: number;

  interval: any;

  dataAgent: User[];
  dataMember: any;

  identity: number;

  showMember = false;

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private userService: UserService,
    private agentService: AgentService,
    private memberService: MemberService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: BsModalService,
  ) {
    this.route.params.subscribe(
      params => {
        this.getUserByParent(params.id);
        this.getMemberByAgent(params.id);
        this.getAgentCountByParentId(params.id);
        this.getMemberCountByParentId(params.id);
        this.agentAndMemberCount(params.id);
        this.getUser(params.id);
        this.id = params.id;
      }
    );

    this.route.queryParams.subscribe(
      params => {
        this.showMember = params.m === 'true';
        console.log(this.showMember);
      }
    )

    // this.dataSourceOfAgent.paginator = this.paginatorOfAgent;
    // this.dataSourceOfMember.paginator = this.paginatorOfMember;
  }

  ngOnInit() {
    this.parent_id = Number(this.route.snapshot.paramMap.get('parent_id'));
    this.userID = Number(localStorage.getItem('authenticatedID'));
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.getMemberByAgent(this.id);
    this.getUserByParent(this.id);
    this.getUser(this.id);
    this.getUserIdentity(this.userID);
    this.getAgentCountByParentId(this.id);
    this.getMemberCountByParentId(this.id);
    // this.agentAndMemberCount(this.id);
  }

  getUserByParent(id: number) {
    this.userService.getUserByParentID(id).subscribe(
      res => {
        const data = res.data;
        this.dataAgent = data;
        this.dataSourceOfAgent.data = this.dataAgent.filter(e => (e.identity === UserRole.topAgent || e.identity === UserRole.agent) && !e.clone_id)
          .reverse();
        this.dataSourceOfAgent.data = addIndexToArray(this.dataSourceOfAgent.data);
        this.agentLength = this.dataSourceOfAgent.data.length;
        // this.dataSourceOfAgent.paginator = this.paginatorOfAgent;
        this.dataSourceOfAgent.sort = this.sortByAgent;

        // console.log(this.dataSourceOfAgent.data);
      }
    );
  }

  getAgentCountByParentId(id) {
    this.userService.getUserByParentID(id).subscribe(
      res => {
        const data = res.data;
        const agentId = data.map(e => e.id) as Array<number>;
        const requestData = agentId.map(e => this.userService.getUserByParentID(e));
        forkJoin(requestData).subscribe(
          res => {
            this.agentArray = [];
            res.forEach(e => {
              this.agentArray.push(...e.data);
            });
            // console.log(this.agentArray);
          }
        );
      }
    );
  }

  getMemberCountByParentId(id) {
    this.userService.getUserByParentID(id).subscribe(
      res => {
        const data = res.data;
        const agentId = data.map(e => e.id) as Array<number>;
        const requestData = agentId.map(e => this.memberService.getMemberByAgent(e));
        forkJoin(requestData).subscribe(
          res => {
            this.memberArray = [];
            res.forEach(e => {
              this.memberArray.push(...e.data);
            });
            // console.log(this.memberArray);
          }
        );
      }
    );
  }

  getMemberByAgent(id: number) {
    this.memberService.getMemberByAgent(id).subscribe(
      res => {
        const data = res.data;
        this.dataMember = data;
        this.allowGameStatus = data.is_allow_play;
        this.dataSourceOfMember.data = this.dataMember.reverse();
        this.dataSourceOfMember.data = addIndexToArray(this.dataSourceOfMember.data);
        this.memberLength = this.dataSourceOfMember.data.length;
        // this.dataSourceOfMember.paginator = this.paginatorOfMember;
        this.dataSourceOfMember.sort = this.sortByMember;
        // console.log(data);
      }
    );
  }

  getAllowGameStatus(AllowGameStatus: number) {
    switch (AllowGameStatus) {
      case 0:
        return this.translateService.instant('game.gameIsDeactivated');
      case 1:
        return this.translateService.instant('game.gameIsActivated');
    }
  }

  getUser(id: number) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data;
        this.userByIdentity = data.identity;
        this.userInfo = data;
        // console.log(this.userInfo);
      }
    );
  }

  getUserIdentity(id: number) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data;
        this.identity = data.identity;
        if (this.identity !== UserRole.admin) {
          this.displayedColumnsOfMember.pop();
        }
      }
    );
  }

  getReceiptType(loginUserRole) {
    return new EnumValueToEnumKeyPipe().transform(loginUserRole, UserRole);
  }

  getTranslatedReceiptType(type: number) {
    return this.enumTranslateServ.getTranslatedReceiptType(type);
  }

  getGender(gender: string): string {
    switch (gender) {
      case 'MALE':
        return this.translateService.instant('manageAgent.male');
      case 'FEMALE':
        return this.translateService.instant('manageAgent.female');
    }
  }

  sortBy(array: Array<any>) {
    return array.sort((a, b) => b.id - a.id);
  }

  get Status() {
    return Status;
  }

  changeAgentStatus(user: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: user.id,
      status: user.status ? 0 : 1,
    };

    this.agentService.updateAgentStatus(data).subscribe(
      () => {
        user.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeMemberStatus(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      status: member.status ? 0 : 1
    };

    this.memberService.updateMemberStatus(data).subscribe(
      () => {
        member.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeAgentAllowGame(agent: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: agent.id,
      is_allow_play: agent.is_allow_play ? 0 : 1,
    };

    this.agentService.updateAgentIsAllowPlay(data).subscribe(
      () => {
        agent.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  deleteAgent(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.agentService.deleteAgent(id).subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          this.getUserByParent(this.id);
        },
        err => alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`)
      );
    }
  }

  deleteMember(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.memberService.deleteMember(id)
        .subscribe(
          () => {
            alert(this.translateService.instant('common.deleteSuccess'));
            this.getMemberByAgent(this.id);
          },
          err => alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`)
        );
    }
  }

  getOnlineShow(status: number): string {
    switch (status) {
      case 0:
        return this.translateService.instant('manageAgent.no');
      case 1:
        return this.translateService.instant('manageAgent.yes');
    }
  }

  agentAndMemberCount(id: number, showMember = false) {
    let agentCount = 0;
    let memberCount = 0;
    if (this.agentArray) {
      agentCount = this.agentArray.filter(e => e.parent_id === id && !e.clone_id).length;
    }
    if (this.memberArray) {
      memberCount = this.memberArray.filter(e => e.agent_id === id).length;
    }
    return showMember ? memberCount : agentCount;
    // return `${this.translateService.instant('manageAgent.agent')} ${agentCount} ${this.translateService.instant('manageAgent.member')} ${memberCount}`;
  }

  updateStatus(value) {

    const data = {
      id: value.id,
      status: value.status ? 0 : 1,
    };

    this.memberService.updateStatus(data).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
        this.getMemberByAgent(this.id);
      },
      err => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.memberService.getMember(id).subscribe(
      res => {
        this.modal = res.data as Member;
        this.modalRef = this.modalService.show(template);

        // console.log(this.modal);
      });
  }

  /**
   * 用ngClass更換按鈕顏色
   * @param is_allow_play 
   */
  changeBtnColor(is_allow_play) {
    switch (is_allow_play) {
      case 0:
        return 'btn-warning';
      case 1:
        return 'btn-secondary';
    }
  }

  applyFilterOfAgent(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceOfAgent.filter = filterValue.trim().toLowerCase();
    this.dataSourceOfMember.filter = filterValue.trim().toLowerCase();
  }

  applyFilterOfMember(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceOfMember.filter = filterValue.trim().toLowerCase();
  }

  back() {
    if (this.userInfo.parent_id === this.userID || this.userInfo.identity === UserRole.topAgent) {
      return this.router.navigate(['users']);
    }
    this.router.navigate([`agent-members/${this.userInfo.parent_id}`]);
    this.showMember = false;
  }

  get UserRole() { return UserRole }
}
