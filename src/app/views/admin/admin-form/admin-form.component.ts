import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Admin } from '../../../models/admin';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { AgentService } from '../../../services/agent.service';
import { Agent } from '../../../models/agent';
import { SystemService } from '../../../services/system.service';
import { SystemSetting } from '../../../models/system-setting';
import { BetAmountService } from '../../../services/bet-amount.service';
import { Betamount } from '../../../models/bet-amount';
import { EnumValueToEnumKeyPipe } from '../../../pipe/enum-value-to-enum-key.pipe';
import { UserRole } from '../../../enum/user-role.enum';
import { AgentAuthority } from '../../../enum/agent-authority.enum';
import { TranslateService } from '@ngx-translate/core';
import { EnumTranslateService } from '../../../services/enum-translate.service';
import { ItemTypes } from '../../../models/item-types';
import { ItemType } from '../../../models/item-type';
import { SingleBetPanelComponent } from '../../../component/single-bet-panel/single-bet-panel.component';
import { defaultAgentOperation } from '../../../common/global';
import { AuthenticationService } from '../../../services/auth.service';
import { UserWallet } from '../../../models/user-wallet';
import { checkUpdateWashCodeTime, genCode, MAX_WALLET_AMOUNT } from '../../../common/common';
import { AllowGameComponent } from '../../../component/allow-game/allow-game.component';
import { CommissionSettingComponent } from '../../../component/commission-setting/commission-setting.component';
import { UserWalletService } from '../../../services/user-wallet.service';
import { ConfirmModalComponent } from '../../../component/confirm-modal/confirm-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { switchMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { getFormCtrlErrDesc } from '../../../common/form';

const POSITIVE_INTEGER = /^\d+$/;
const DEFAULT_AGENT_OPERATION = defaultAgentOperation;
@Component({
  selector: 'app-admin-form',
  templateUrl: './admin-form.component.html',
  styleUrls: ['./admin-form.component.css'],
})
export class AdminFormComponent implements OnInit {
  @ViewChild('singlePanel') singlePanel: SingleBetPanelComponent;
  @ViewChild('allowGame') allowGame: AllowGameComponent;
  @ViewChild('commissionSetting') commissionSetting: CommissionSettingComponent;
  data: User;
  admin: Admin;
  Agent: Agent;
  SystemSetting: SystemSetting[];
  betamount: Betamount[];
  itemTypes: ItemTypes[];
  washCodeRange = [];
  revenueShareRange = [];
  user: User;
  parent: User;
  parentOperationItems: Array<string> = [];

  public myForm: FormGroup;
  public id: number;
  public parentID: number;
  public parent_id: number;
  public manageID: number;

  public parentWallet: number;
  public loginParentWallet: number;

  public updateRebateTimeDate: any;
  public updateWashCodeTimeStr: string;

  public agentID: number;
  public agentRebate: number;
  public agentWallet: number;
  public reportItem: any;
  public identity: number;
  public loginUserIndentity: number;
  public checkItem: any;
  public value: any;
  public ownRevenueShare: any;
  public parentRevenueShare = 100;
  public parentWashCodePercent = 1;
  public singleBetLimits: any;
  public showRevenue = false;
  public parentSingleBetLimits: any;
  public parentIdentity: any;
  public parentSingleBetTotal: number;
  public usedSingleBetTotal: number;
  public availableSingleBetTotal: number;

  singleBetLimitsOwn: string;
  oldPassword: string;

  // 確認modal
  modalRef: BsModalRef;


  constructor(
    private modalService: BsModalService,
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private fb: FormBuilder,
    private authServ: AuthenticationService,
    private userWalletServ: UserWalletService,
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location,
    private agentService: AgentService,
    private systemService: SystemService,
    private betAmountService: BetAmountService,
  ) {
    this.groupMyForm();
  }

  ngOnInit() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.parent_id = Number(this.route.snapshot.paramMap.get('parent_id'));
    this.manageID = Number(localStorage.getItem('authenticatedID'));
    this.loginUserIndentity = Number(localStorage.getItem('loginUserIdentity'));
    if (this.id) { this.setMyForm(); }
    if (this.id === 0) { this.getParentInfoByLoginUser(); }
    this.getSystemSettingList();
    this.getBetAmountList();
    this.generateRange();
    if (this.id) {
      this.showAgentPassword();
    } else {
      this.genInvationCode();
    }

    if (this.id && this.parent_id) {
      this.getUsers(this.id);
      this.getUserByParentId(this.parent_id);
    } else {
      this.getUsers(this.manageID);
    }

    if (this.id) {
      this.myForm.get('name').disable();
    }
  }

  getParent(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data;

        this.parentIdentity = data.identity;
        this.parentRevenueShare = data.revenue_share;
        this.parentWashCodePercent = data.wash_code_commission_percent;
        this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);
        this.myForm.get('wash_code_commission_percent').setValue(data.wash_code_commission_percent);
        this.myForm.get('revenue_share').setValue(data.revenue_share);
        console.log(data);
      }
    );
  }

  getUsers(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data as User;
        this.user = data;
        this.identity = data.identity;
        if (!this.id && data.identity !== UserRole.admin && data.identity !== UserRole.management) {
          this.parentRevenueShare = data.revenue_share;
          this.parentWashCodePercent = data.wash_code_commission_percent;
          this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);
          this.myForm.get('wash_code_commission_percent').setValue(data.wash_code_commission_percent);
          this.myForm.get('revenue_share').setValue(data.revenue_share);
          try {
            this.parentOperationItems = JSON.parse(data.operation_items);
          } catch (error) {
            console.log(error);
          }
        }
      }
    );
  }

  getUserByParentId(id: number) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data as User;

        if (data.identity !== UserRole.admin && data.identity !== UserRole.management) {
          this.parentRevenueShare = data.revenue_share;
          this.parentWashCodePercent = data.wash_code_commission_percent;
          this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);

          this.myForm.get('wash_code_commission_percent').setValue(data.wash_code_commission_percent);
          this.myForm.get('revenue_share').setValue(data.revenue_share);

          try {
            this.parentOperationItems = JSON.parse(data.operation_items);
          } catch (error) {
            console.log(error);
          }
        }
      }
    );
  }

  groupMyForm() {
    this.myForm = this.fb.group({
      'real_name': [null, [Validators.required, Validators.maxLength(10)]],
      'name': [null, [Validators.pattern('^[A-Za-z0-9]+$'), Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      'credit': [null],
      'wallet': [null, [Validators.min(0), Validators.max(MAX_WALLET_AMOUNT), Validators.pattern(POSITIVE_INTEGER)]],
      'rebate': [null],
      'password': [null, [Validators.required, Validators.pattern('^[A-Za-z0-9]+$'), Validators.minLength(6), Validators.maxLength(12)]],
      're_password': [null],
      'status': [true],
      'parent_id': [null],
      'identity': [null],
      'single_bet_amount': [0],
      'wash_code_commission_percent': [1, [Validators.required, Validators.max(1)]],
      'revenue_share': [100, [Validators.required, Validators.min(0), Validators.max(100)]],
      'operation_items': new FormArray([]),
      'remark': [null],
      'bank_account': [null],
      'invitation_code': []
    });
  }

  getTranslatedBetItem(id: number) {
    return this.enumTranslateServ.getTranslatedBetItem(ItemType.getNameByID(id));
  }

  setMyForm() {
    this.userService.getUser(this.id).subscribe(
      res => {
        const data = res.data as User;
        this.data = data;
        this.parentID = data.parent_id;
        this.agentID = data.id;
        this.agentWallet = data.user_wallet.dragon_tiger_wallet;
        this.identity = data.identity;
        this.ownRevenueShare = data.revenue_share;
        this.checkItem = JSON.parse(data.operation_items);
        if (data.single_bet_limits) {
          this.singleBetLimits = JSON.parse(data.single_bet_limits);
        }

        this.myForm.patchValue({
          real_name: data.real_name,
          name: data.name,
          rebate: data.rebate,
          re_password: data.re_password,
          status: data.status,
          parent_id: data.parent_id,
          identity: data.identity,
          single_bet_amount: data.single_bet_amount,
          wash_code_commission_percent: data.wash_code_commission_percent,
          revenue_share: data.revenue_share,
          credit: data.credit,
          remark: data.remark,
          bank_account: data.bank_account,
          invitation_code: data.invitation_code
        });
        this.getParentInfoByUserId();
        this.setChecked(data.operation_items);
      },
      err => {
        alert(`${this.translateService.instant('common.getFailed')} ${err.status} ${err.statusText}`);
      });
  }

  generateRange() {
    const max = 100;
    for (let index = 0; index < (max / 10); index++) {
      this.washCodeRange.push((index + 1) / 10);
    }

    for (let index = 1; index <= (max / 10); index++) {
      this.revenueShareRange.push(index * 10);
    }
  }

  revenueShare(event) {
    this.value = Number(event);
  }

  updateAgentWashCode() {
    const wash_code_commission_percent = Number(this.myForm.get('wash_code_commission_percent').value);
    const revenue_share = Number(this.myForm.get('revenue_share').value);
    const commissionStr = JSON.stringify(this.commissionSetting.getValue(0));
    const data = {
      id: this.agentID,
      wash_code_commission_percent,
      wash_code_commission_percent_cash: 0,
      revenue_share,
      game_result_rebat_share: 0,
      company_game_result_rebat_share: 0,
      commission: commissionStr,
    };
    return this.agentService.updateAgentWashCodeCommissionPercent(data);
  }

  updateAgentRevenueShare() {
    const data = {
      id: this.id,
      revenue_share: this.value,
    };
    this.agentService.IncrementAgentRevenueShare(data).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
      },
      err => {
        alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`);
      }
    );
  }

  submitForm(val) {
    if (!this.singlePanel.betTotalAmountValid) {
      return alert(this.translateService.instant('betTotalExceeded'));
    }

    if (this.checkPasswordIsSame()) {
      return alert(this.translateService.instant('samePassword'));
    }

    if (this.myForm.valid && this.singlePanel.getFormValid()) {
      const singlePanelValue = this.singlePanel.getFormValue();
      val.real_name = val.real_name;
      val.single_bet_limits = singlePanelValue.single_bet_limits;
      val.single_bet_total_amounts = singlePanelValue.single_bet_total_amounts;
      val.status = Number(val.status);
      val.single_bet_amount = Number(val.single_bet_amount);
      val.wash_code_commission_percent = Number(val.wash_code_commission_percent);
      val.revenue_share = Number(val.revenue_share);
      val.operation_items.push('1');
      val.operation_items = JSON.stringify(DEFAULT_AGENT_OPERATION);
      val.credit = val.wallet;

      let wallet = {
        dragon_tiger_credit: val.credit,
        dragon_tiger_wallet: val.wallet,
        sic_bo_credit: 0,
        sic_bo_wallet: 0,
        baccarat_wallet: 0,
        baccarat_credit: 0,
        poker_credit: 0,
        poker_wallet: 0,
      } as UserWallet;

      val.user_wallet = JSON.stringify(wallet);
      val.bet_limits = '[]';
      val.allow_game = '[]';
      val.world_football_game_settings = '[]';
      val.allow_game = JSON.stringify(this.allowGame.getValue());
      val.commission = JSON.stringify(this.commissionSetting.getValue());

      //   if (val.wash_code_commission_percent > val.revenue_share) {
      //     return alert(this.translateService.instant('biggerThanShare'));
      //   }

      if (val.revenue_share > this.parentRevenueShare) {
        return alert(this.translateService.instant('biggerThanParent'));
      }

      let req = [this.agentService.updateAgent(val)];
      let successMsg = this.translateService.instant('common.editSuccess');
      let failedMsg = this.translateService.instant('common.editFailed');

      if (this.id) {
        val.id = this.id;

        if (!this.isPasswordChanged()) {
          delete val.password;
        }

        req = [this.agentService.updateTopAgent(val)];
        if (this.loginUserIndentity === UserRole.topAgent || this.loginUserIndentity === UserRole.agent) {
          req = [this.agentService.updateAgent(val)];
        }

        if (this.checkUpdateWashCodeTime()) {
          req.push(this.updateAgentWashCode());
        }
      } else {
        val.parent_id = this.manageID;
        req = [this.agentService.addTopAgent(val)];
        if (this.loginUserIndentity === UserRole.topAgent || this.loginUserIndentity === UserRole.agent) {
          req = [this.agentService.addAgent(val)];
        }
        successMsg = this.translateService.instant('common.addSuccess');
        failedMsg = this.translateService.instant('common.addFailed');
      }

      forkJoin(req).subscribe(() => {
        alert(successMsg);
        this.location.back();
      }, err => {
        if (err.error) {
          return alert(`${failedMsg}: ${err.error.message}`);
        }
        alert(failedMsg);
      });

    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      this.markFormGroupTouched(this.myForm);
    }
  }


  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  getReceiptType(loginUserRole) {
    return new EnumValueToEnumKeyPipe().transform(loginUserRole, UserRole);
  }

  getTranslatedReceiptType(type: number) {
    return this.enumTranslateServ.getTranslatedReceiptType(type);
  }

  /**
   * 取得押注籌碼列表 (押注動作)
   */
  getBetAmountList() {
    this.betAmountService.getBetAmountList().subscribe(
      res => {
        this.betamount = res.data.filter(e => e.type === 0);
      }
    );
  }

  modifyUserSicboCredit(amount: string, decrement: boolean = false) {

    const credit = Number(amount);
    if (credit && POSITIVE_INTEGER.test(amount)) {
      let req = this.userWalletServ.incrementDragonTigerCreditAndWallet(this.id, credit, this.manageID);

      if (decrement) {
        req = this.userWalletServ.decrementDragonTigerCreditAndWallet(this.id, credit, this.manageID);
      }

      req.subscribe(
        () => {
          alert(this.translateService.instant('common.editSuccess'));
          this.setMyForm();
        },
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
    }
  }

  /**
    * 清空餘額
    */

  clearUserSicboCreditAndWallet() {
    this.modalRef = this.modalService.show(ConfirmModalComponent,
      { initialState: { message: this.translateService.instant('manageAgent.form.clearWarning') } })
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.userWalletServ.clearDragonTigerCreditAndWallet(this.id, this.manageID).subscribe(
          () => {
            alert(this.translateService.instant('common.editSuccess'));
            this.setMyForm();
          },
          err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
        );
      }
    })
  }

  /**
   * 取得系統設定列表
   */
  getSystemSettingList() {
    this.systemService.getSystemSettings().subscribe(
      res => {
        this.SystemSetting = res.data;
        if (this.SystemSetting.map(e => e.update_wash_code_time)) {
          this.updateWashCodeTimeStr = this.SystemSetting[0].update_wash_code_time;
          this.updateRebateTimeDate = this.SystemSetting.map(e => e.update_wash_code_time = JSON.parse(e.update_wash_code_time));

          if (this.id && !this.checkUpdateWashCodeTime()) {
            this.myForm.get('wash_code_commission_percent').disable();
            this.myForm.get('revenue_share').disable();
          }
        }
      }
    );
  }

  getWeekChange(week) {
    switch (week) {
      case 0:
        return '週日';
      case 1:
        return '週一';
      case 2:
        return '週二';
      case 3:
        return '週三';
      case 4:
        return '週四';
      case 5:
        return '週五';
      case 6:
        return '週六';
    }
  }

  getTranslatedWeek(val: number) {
    return this.enumTranslateServ.getTranslatedWeek(val);
  }

  /**
   * 產生密碼
   */
  setRandomCode(len: number) {
    const characters = '23456789';
    let code = '';
    let n = 0;
    let randomnumber = 0;
    while (n < len) {
      n++;
      randomnumber = Math.floor(characters.length * Math.random());
      code += characters.substring(randomnumber, randomnumber + 1);
    }
    this.myForm.controls['password'].setValue(code.toLocaleUpperCase());
  }

  /**
   * showMemberPassword - 檢視密碼
   */
  showAgentPassword() {
    this.agentService.getAgentRamdomPassword(this.id)
      .subscribe(
        res => {
          const password = atob(res.data);
          this.oldPassword = password;
          this.myForm.patchValue({
            password: password
          });
        },
        err => console.log(err)
      );
  }

  /**
   * 取得parent info(上級資料) By userID
   */
  getParentInfoByUserId() {
    this.userService.getUser(this.parentID).subscribe(
      res => {
        const data = res.data;
        this.parentWallet = data.user_wallet.dragon_tiger_wallet;
        this.parent = data;
        // console.log(data);
      }
    );
  }

  getParentInfoByLoginUser() {
    this.userService.getUser(this.manageID).subscribe(
      res => {
        const data = res.data;
        this.loginParentWallet = data.user_wallet.dragon_tiger_wallet;
        this.parent = data;
      }
    );
  }

  cancel() {
    this.location.back();
  }

  get AgentAuthority() {
    return AgentAuthority;
  }

  get UserRole() {
    return UserRole;
  }



  onCheckboxChange(e) {
    const operationItem: FormArray = this.myForm.get('operation_items') as FormArray;

    if (e.target.checked) {
      operationItem.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      operationItem.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          operationItem.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  getChecked(OperationItemID) {
    const operationItem = this.myForm.get('operation_items').value as Array<any>;
    // console.log(operationItem);

    return operationItem.includes(String(OperationItemID));
  }

  setChecked(operationItems: string) {
    if (operationItems === 'null') {
      return;
    }
    const items = JSON.parse(operationItems) as Array<string>;
    const operationItem = this.myForm.get('operation_items') as FormArray;

    items.forEach(item => {
      operationItem.push(new FormControl(item));
    });
  }

  isExistOperation(pageID: number) {
    return this.parentOperationItems.includes(String(pageID)) || this.isAdmin();
  }

  isParentOperationEmpty() {
    if (this.isAdmin()) {
      return false;
    }
    for (const item of DEFAULT_AGENT_OPERATION) {
      if (this.parentOperationItems.some(e => e == String(item))) {
        return false;
      };
    }
    return true;
  }

  isAdmin() {
    const isCreating = !this.id && this.loginUserIndentity === UserRole.admin;
    const isEditing = this.id && this.identity === UserRole.topAgent;
    return isCreating || isEditing;
  }

  checkPasswordIsSame() {
    const ctrl = this.myForm.get('password');
    return this.id && !ctrl.pristine && ctrl.value === this.oldPassword;
  }

  isPasswordChanged() {
    const ctrl = this.myForm.get('password');
    return this.id && !ctrl.pristine;
  }

  async genInvationCode(retryTimes = 5) {
    let ok = false;
    this.myForm.get('invitation_code').setValue('');
    for (let index = 0; index < retryTimes; index++) {
      let invitationCode = genCode();
      await this.agentService.getByInvitationCode(invitationCode).toPromise().then(r => {
        if (!r.data) {
          this.myForm.get('invitation_code').setValue(invitationCode);
          ok = true;
        }
      });

      if (ok) return;
    }
    alert('產生邀請碼失敗，頁面將自動重新整理');
    window.location.reload();
  }

  changeAgentAllowGame(agent: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: agent.id,
      is_allow_play: agent.is_allow_play ? 0 : 1,
    };


    if (this.parent.identity == UserRole.agent || this.parent.identity == UserRole.topAgent) {
      if (data.is_allow_play && !this.parent.is_allow_play) return alert('上級遊戲狀態已關閉無法修改');
    }

    this.agentService.updateAgentIsAllowPlay(data).subscribe(
      () => {
        agent.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  deleteAgent(id: number) {
    this.modalRef = this.modalService.show(ConfirmModalComponent,
      { initialState: { message: this.translateService.instant('common.wannaDelete') } })
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.agentService.deleteAgent(id).subscribe(
          () => {
            alert(this.translateService.instant('common.deleteSuccess'));
            this.cancel();
          },
          error => {
            alert(`${this.translateService.instant('common.deleteFailed')}: ${error.error.message}`);
          }
        );
      }
    })
  }

  checkUpdateWashCodeTime() {
    if (!this.updateWashCodeTimeStr) return false;
    return checkUpdateWashCodeTime(this.updateWashCodeTimeStr);
  }

  getFormCtrlErrDesc(ctrl: AbstractControl) { return getFormCtrlErrDesc(ctrl) }
}
