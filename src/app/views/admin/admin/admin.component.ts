import { Component, OnInit, ViewChild } from '@angular/core';
import { PaginateConfig } from '../../../ckeditor/paginateconfig';
import { Status } from '../../../enum/status.enum';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { MemberService } from '../../../services/member.service';
import { ActivatedRoute } from '@angular/router';
import { AgentService } from '../../../services/agent.service';
import { GameTypeEnum } from '../../../enum/game-type.enum';
import { forkJoin } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../../../enum/user-role.enum';
import { map } from 'rxjs/operators';
import { addIndexToArray } from '../../../common/global';
import { Member } from '../../../models/member.model';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  @ViewChild('paginatorOfAgent', { static: true }) paginatorOfAgent: MatPaginator;
  @ViewChild('paginatorOfMember', { static: true }) paginatorOfMember: MatPaginator;

  @ViewChild('sortByAgent', { static: true }) sortByAgent: MatSort;
  @ViewChild('sortByMember', { static: true }) sortByMember: MatSort;
  displayedColumnsOfAgent: string[] = ['index', 'name', 'real_name', 'wallet', 'wash_code_commission_percent', 'revenue_share', 'function'];
  displayedColumnsOfMember: string[] = ['index', 'account', 'name', 'nickname', 'wallet'];

  dataSourceOfAgent = new MatTableDataSource<User>();
  dataSourceOfMember = new MatTableDataSource<Member>();

  public config = new PaginateConfig().config;
  user: User[];
  member: Member[];
  id: any;
  parent_id: number;
  identityID: any;
  userID: number;
  agent: any;
  dataMember: any;
  memberByAgent: any;
  agentStatus: any;
  memberStatus: any;
  userChangeStatusButton: any;
  identity: number;

  loginUser: number;
  loginUserInfo: User;

  getUserByParentId: number;

  userIdArray: any;

  agentPeople: any;
  memberPeople: any;
  concatArrayByAgent: User[] = [];
  concatArrayByMember: Member[] = [];

  agentCount: number;
  memberCount: number;

  agentLength: number;
  memberLength: number;

  isTopAgentClicked = false;

  constructor(
    private translateService: TranslateService,
    private agentService: AgentService,
    private userService: UserService,
    private memberService: MemberService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.parent_id = Number(this.route.snapshot.paramMap.get('parent_id'));
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.userID = Number(localStorage.getItem('authenticatedID'));
    this.identityID = Number(localStorage.getItem('loginUserIdentity'));
    this.loginUser = Number(localStorage.getItem('authenticatedID'));
    this.getUserIdentityID(this.id);
    this.getMemberByAgentId();

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getUser();
    } else {
      this.getUserCountByParentID(this.loginUser);
    }

    this.getMemberCountByParentID(this.loginUser);

    // this.dataSourceOfAgent.paginator = this.paginatorOfAgent;
    // this.dataSourceOfMember.paginator = this.paginatorOfMember;
    this.dataSourceOfAgent.sort = this.sortByAgent;
    this.dataSourceOfMember.sort = this.sortByMember;
  }

  // 最高權限取得全部用戶
  getUser() {
    this.userService.getUsers().subscribe(
      res => {
        const data = res.data;
        this.agent = data;
        this.dataSourceOfAgent.data = this.agent.filter(e => e.identity === UserRole.topAgent).reverse();
        this.dataSourceOfAgent.data = addIndexToArray(this.dataSourceOfAgent.data);
        this.agentLength = this.dataSourceOfAgent.data.length;
        this.concatArrayByAgent = res.data;
      }
    );
  }

  getUserIdentityID(id) {
    this.userService.getUser(this.userID).subscribe(
      res => {
        const data = res.data;
        this.identity = data.identity;
      }
    );
  }

  /**
   * 取得代理資料 - By loginUser userID
   * @param id 
   */
  getUserCountByParentID(id) {
    this.userService.getUserByParentID(id).subscribe(
      res => {
        const data: User[] = res.data;
        this.agent = data;
        this.dataSourceOfAgent.data = this.agent.filter(e => e.identity === UserRole.topAgent || e.identity === UserRole.agent).
          reverse();
        this.dataSourceOfAgent.data = addIndexToArray(this.dataSourceOfAgent.data);
        this.agentLength = this.dataSourceOfAgent.data.length;
        this.concatArrayByAgent = data;
        this.getChildAgentAndMember(data.map(e => e.id));
      });
  }

  getChildAgentAndMember(ids: Array<number>) {
    const reqAgent = ids.map(e => this.userService.getUserByParentID(e));
    const reqMember = ids.map(e => this.memberService.getMemberByAgent(e));
    forkJoin(reqAgent).pipe(
      map(res => res.map(r => r.data))
    ).subscribe(r => {
      r.forEach(e => this.concatArrayByAgent.push(...e));
    });

    forkJoin(reqMember).pipe(
      map(res => res.map(r => r.data))
    ).subscribe(r => {
      r.forEach(e => this.concatArrayByMember.push(...e));
    });
  }

  /**
   * 取得會員資料 - By 
   */
  getMemberCountByParentID(id) {
    let req = this.memberService.getMemberByAgent(id);
    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      req = this.memberService.getMemberList();
    }

    req.subscribe(
      res => {
        this.concatArrayByMember = res.data
      }
    );
  }

  getMemberByAgentId() {
    this.memberService.getMemberByAgent(this.loginUser).subscribe(
      res => {
        const data = res.data;
        this.dataMember = data;
        this.dataSourceOfMember.data = addIndexToArray(this.dataMember.reverse());
        this.memberLength = this.dataSourceOfMember.data.length;
        // console.log(data);
      }
    );
  }

  getGender(gender: string): string {
    switch (gender) {
      case 'MALE':
        return this.translateService.instant('manageAgent.male');
      case 'FEMALE':
        return this.translateService.instant('manageAgent.female');
    }
  }

  deleteAgent(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.agentService.deleteAgent(id).subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          if (this.identity === UserRole.admin || this.identity === UserRole.management) {
            this.getUser();
          } else {
            this.getUserCountByParentID(this.loginUser);
          }
        },
        error => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${error.error.message}`);
        }
      );
    }
  }

  deleteMember(id) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.memberService.deleteMember(id).subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          // this.getUserCountByParentID(this.loginUser);
          this.getMemberCountByParentID(this.loginUser);
        },
        error => alert(`${this.translateService.instant('common.deleteFailed')}: ${error.error.message}`)
      );
    }
  }

  sortBy(array: Array<any>) {
    return array.sort((a, b) => b.id - a.id);
  }

  get Status() { return Status; }

  changeButtonClass(status) {
    switch (status) {
      case 0:
        return this.translateService.instant('common.inActive');
      case 1:
        return this.translateService.instant('common.active');
    }
  }

  changeAgentStatus(user: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: user.id,
      status: user.status ? 0 : 1
    };

    this.agentService.updateAgentStatus(data).subscribe(
      () => {
        user.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => {
        alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
      }
    );
  }

  changeMemberStatus(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      status: member.status ? 0 : 1
    };

    this.memberService.updateStatus(data).subscribe(
      () => {
        member.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => {
        alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
      }
    );
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }


  changeAgentAllowGame(agent: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: agent.id,
      is_allow_play: agent.is_allow_play ? 0 : 1,
    };

    this.agentService.updateAgentIsAllowPlay(data).subscribe(
      () => {
        agent.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  /**
   * kickMember - 踢會員
   * @param id
   */
  kickMember(id: number) {
    this.memberService.kickMember(id, GameTypeEnum.GameTypeSicBo)
      .subscribe(
        () => alert(this.translateService.instant('common.editSuccess')),
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
  }

  allowGameToggle(id, is_allow_play) {

    const data = {
      id: id,
      is_allow_play: is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        this.getMemberByAgentId();
        alert(this.translateService.instant('common.updateSuccess'));
      },
      err => (this.translateService.instant('common.updateFailed'))
    );
  }

  getAllowGameStatus(AllowGameStatus: number) {
    switch (AllowGameStatus) {
      case 0:
        return this.translateService.instant('game.gameIsDeactivated');
      case 1:
        return this.translateService.instant('game.gameIsActivated');
    }
  }

  agentAndMemberCount(id: number, showMember = false) {
    let agentCount = 0;
    let memberCount = 0;
    if (this.concatArrayByAgent) {
      agentCount = this.concatArrayByAgent.filter(e => e.parent_id === id && !e.clone_id).length;
    }
    if (this.concatArrayByMember) {
      memberCount = this.concatArrayByMember.filter(e => e.agent_id === id).length;
    }
    return showMember ? memberCount : agentCount;
    // return `${this.translateService.instant('manageAgent.agent')} ${agentCount} ${this.translateService.instant('manageAgent.member')} ${memberCount}`;
  }

  get UserRole() {
    return UserRole;
  }

  /**
   * [ngClass] 切換顏色
   */
  changeBtnColor(is_allow_play) {
    switch (is_allow_play) {
      case 0:
        return 'btn-warning';
      case 1:
        return 'btn-secondary';
    }
  }

  applyFilterOfAgent(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceOfAgent.filter = filterValue.trim().toLowerCase();
    this.dataSourceOfMember.filter = filterValue.trim().toLowerCase();
    this.isTopAgentClicked = false;
  }

  applyFilterOfMember(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceOfMember.filter = filterValue.trim().toLowerCase();
    this.isTopAgentClicked = false;
  }

  isTopAgentSearched(e: boolean) {
    this.isTopAgentClicked = e;
  }
}
