import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Member } from '../../../models/member.model';
import { MemberService } from '../../../services/member.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Gender } from '../../../enum/gender.enum';
import { MemberIdentity } from '../../../enum/member-identity.enum';
import { CountryCode } from '../../../enum/country-code.enum';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Status } from '../../../enum/status.enum';


declare var d3init: Function;

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['account', 'name', 'gender', 'car_license_number', 'created_at', 'updated_at', 'status', 'function'];
  dataSource = new MatTableDataSource<Member>();

  public data: Member[];
  public modalRef: BsModalRef;
  public modal: any;



  constructor(private memberService: MemberService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getList() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.data = res.data;
        // console.log(this.data)

        this.data.map(item => {
          item.gender = Gender[item.gender]
        })

        this.dataSource.data = this.data;

      });
  }

  delete(id) {
    if (confirm('確定刪除帳戶?')) {
      this.memberService.deleteMember(id).subscribe(
        () => {
          this.getList();
        },
        err => {
          alert(`操作失敗: ${err.error.message}`);
        }
      );
    }
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.memberService.getMember(id).subscribe(res => {
      this.modal = res.data as Member;
      this.modal.country = `${CountryCode[this.modal.country]} (${this.modal.country})`;
      this.modal.identity = this.modal.identity == MemberIdentity.Normal ? MemberIdentity.NAME_Normal : MemberIdentity.NAME_Salesman;
      this.modal.gender = Gender[this.modal.gender];
      this.modalRef = this.modalService.show(template);
    });
  }

  openLevelModal(template: TemplateRef<any>, id: number) {
    this.memberService.getLevelNodeAll(id).subscribe(res => {
      this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
      let data = [res];
      d3init(data);
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  get Status() { return Status }

}
