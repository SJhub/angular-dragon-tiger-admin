import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, AbstractControl } from '@angular/forms';
import { MemberService } from '../../../services/member.service';
import { ActivatedRoute } from '@angular/router';
import { Member } from '../../../models/member.model';
import { Location } from '@angular/common';
import { TaiwanDistricts } from '../../../enum/taiwan-districts';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';
import { SystemSetting } from '../../../models/system-setting';
import { SystemService } from '../../../services/system.service';
import { EnumTranslateService } from '../../../services/enum-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { SingleBetPanelComponent } from '../../../component/single-bet-panel/single-bet-panel.component';
import { AuthenticationService } from '../../../services/auth.service';
import { UserRole } from '../../../enum/user-role.enum';
import { MemberWalletService, WinTotalAmountLimitBody } from '../../../services/member-wallet.service';
import { forkJoin } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from '../../../component/confirm-modal/confirm-modal.component';
import { BetAmountService } from '../../../services/bet-amount.service';
import { GameTypeEnum } from '../../../enum/game-type.enum';
import { Betamount } from '../../../models/bet-amount';
import { changeColor, checkUpdateWashCodeTime, getFormatedDateStr, MAX_WALLET_AMOUNT } from '../../../common/common';
import { GameActionTypeEnum } from '../../../enum/game-action-type.enum';
import { AutoBetItemType, AutoBetItemTypeText } from '../../../enum/auto-bet-item-type.enum';
import { getArrByAutoBetType, getAutoBetTypeByArr } from '../../../common/global';
import { AgentService } from '../../../services/agent.service';
import { BetRecordService } from '../../../services/bet-record.service';
import { ChangeLogService } from '../../../services/change-log.service';
import { ChangeLog } from '../../../models/change-log';
import { ChangeLogColumn } from '../../../enum/change-log.enum';
import { Status } from '../../../enum/status.enum';
import { MemberWallet } from '../../../models/member-wallet';
import { getFormCtrlErrDesc } from '../../../common/form';

const POSITIVE_INTEGER = /^\d+$/;
@Component({
  selector: 'app-member-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.css']
})
export class MemberFormComponent implements OnInit {
  @ViewChild('singlePanel') singlePanel: SingleBetPanelComponent;
  public myForm: FormGroup;
  public id: number;
  parent_id: number;
  data: any;
  showMemberPasswordToggle = 0;

  allCity: string[] = [];
  allArea: string[] = [];
  selectCity = '';
  selectArea = '';
  memberCredit = 0;
  washCodeRange = [];
  parentWashCodePercent = 1;

  memberWallet: number;
  SystemSetting: SystemSetting[];

  User: User;
  parent: User;

  agentIdByMember: number;
  userWallet: number;
  updateRebateTimeDate: any[];
  singleBetLimits: any;

  oldPassword: string;

  identity: number;

  loginUserID: number;

  // 確認modal
  modalRef: BsModalRef;

  // 百家樂籌碼
  betamounts: Betamount[];
  // 籌碼設置
  autoBetAmounts = [];
  // 項目設置
  autoBetItems = [];

  updateWashCodeTimeStr: string;

  // VIP目前輸贏金額
  autoBetGameResult = 0;

  // 錢包
  newMemberWallet: MemberWallet;

  constructor(
    private changeLogServ: ChangeLogService,
    private betRecordServ: BetRecordService,
    private agentServ: AgentService,
    private betamountServ: BetAmountService,
    private memberWalletServ: MemberWalletService,
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private memberService: MemberService,
    private route: ActivatedRoute,
    private location: Location,
    private userService: UserService,
    private systemService: SystemService,
    private authServ: AuthenticationService
  ) {
    this.groupMyForm();
    this.identity = Number(this.authServ.getUserIdentity());
    this.loginUserID = Number(this.authServ.getUserId());
  }

  getBetAmounts() {
    this.betamountServ.getBetAmountList().subscribe(r => {
      let data: Betamount[] = r.data;
      data = data.filter(e => e.game_types_id === GameTypeEnum.GameTypeDragonTiger && e.status && e.type === GameActionTypeEnum.Bet);
      this.betamounts = data.sort((a, b) => a.amount - b.amount);
    });
  }

  /**
   * initCityArea - 取得城市資料
   */
  initCityArea() {
    this.allCity = Object.keys(TaiwanDistricts);
  }

  ngOnInit() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm();
      this.showAgentPassword();
      this.myForm.get('account').disable();
    }

    this.initCityArea();
    this.generateRange();
    this.getSystemSettingList();
    this.getBetAmounts();
  }

  // 表單初始化
  groupMyForm() {
    this.myForm = this.fb.group({
      'id': [null],
      'account': [null],
      'name': [null, [Validators.required, Validators.maxLength(5)]],
      'nickname': [null],
      'password': [null, [Validators.minLength(6), Validators.maxLength(12)]],
      'gender': [null],
      'birthday': [null],
      'credit': [null, [Validators.min(0), Validators.pattern(POSITIVE_INTEGER)]],
      'wallet': [null, [Validators.min(0), Validators.max(MAX_WALLET_AMOUNT), Validators.pattern(POSITIVE_INTEGER)]],
      'agent_id': [null],
      'status': [null],
      'is_allow_play': [null],
      'game_result_amount': [0],
      'indecrement_credit': [0],
      'bank_account': [null],
      'remark': [null],
      'wallet_limit': [0, [Validators.min(0), Validators.pattern(POSITIVE_INTEGER)]],
      'dragon_tiger_win_total_amount_limit': [0, [Validators.required, Validators.min(0), Validators.pattern(POSITIVE_INTEGER)]],
      'wash_code_commission_percent': [0, [Validators.required, Validators.max(1)]],
      'auto_bet': [0],
      'auto_bet_result': [0],
      'auto_bet_amount': [10000],
      'game_roles_id': [1],
    }, {
      //  validators: this.passwordMatches
    });
  }

  /**
   * 取得系統設定列表
   */
  getSystemSettingList() {
    this.systemService.getSystemSettings().subscribe(
      res => {
        this.SystemSetting = res.data;
        if (this.SystemSetting.map(e => e.update_wash_code_time)) {
          this.updateWashCodeTimeStr = this.SystemSetting[0].update_wash_code_time;
          this.updateRebateTimeDate = this.SystemSetting.map(e => e.update_wash_code_time = JSON.parse(e.update_wash_code_time));
          if (!this.checkUpdateWashCodeTime()) {
            this.myForm.get('wash_code_commission_percent').disable();
          }
        }
      }
    );
  }

  setForm() {
    this.memberService.getMember(this.id).subscribe(
      res => {
        const data = res.data as Member;
        this.getAgent(data.agent_id);
        this.data = data;
        this.agentIdByMember = data.agent_id;
        this.memberWallet = data.member_wallet.dragon_tiger_wallet;
        this.newMemberWallet = data.member_wallet;


        if (data.city) {
          this.setAreaByCity(data.city);
          this.selectCity = data.city;
        }

        if (data.area) {
          this.selectArea = data.area;
        }

        if (data.single_bet_limits) {
          this.singleBetLimits = data.single_bet_limits;
        }

        this.memberCredit = data.credit;

        this.myForm.patchValue({
          name: data.name,
          nickname: data.nickname,
          account: data.account,
          gender: data.gender,
          game_result_amount: data.game_result_amount,
          status: data.status,
          bank_account: data.bank_account,
          remark: data.remark,
          wash_code_commission_percent: data.wash_code_commission_percent,
          is_allow_play: data.is_allow_play,
          credit: data.credit,
          wallet_limit: data.wallet_limit,
          auto_bet: data.auto_bet,
          auto_bet_result: data.auto_bet_result,
          dragon_tiger_win_total_amount_limit: this.newMemberWallet.dragon_tiger_win_total_amount_limit,
        });

        if (data.auto_bet_amount) this.myForm.get('auto_bet_amount').setValue(data.auto_bet_amount);

        this.getUser(this.agentIdByMember);
        try {
          this.autoBetItems = getArrByAutoBetType(data.auto_bet_bit_type);
          this.autoBetAmounts = JSON.parse(data.auto_bet_amounts);
        } catch (error) {
          console.log(error);
        }

        if (this.identity === UserRole.admin) this.getAutoBetGameResultAmount(this.data);
      },
      err => {
        alert(`取得資料失敗 ${err.status} ${err.statusText}`);
      }
    );
  }

  getAgent(id: number) {
    this.userService.getUser(id).subscribe(r => {
      const user: User = r.data;
      this.singleBetLimits = user.single_bet_limits;
      this.parentWashCodePercent = user.wash_code_commission_percent;
      this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);
      this.parent = user;
    });
  }

  statusExchange(fromStatus: boolean): number {
    if (!fromStatus) {
      return 0;
    }
    return 1;
  }

  submitForm(val: any) {
    val.nickname = val.name;
    val.agent_id = this.id;
    val.status = this.statusExchange(val.status);
    val.is_allow_play = Number(val.is_allow_play);
    val.wallet = parseInt(val.wallet, 10);
    val.credit = Number(val.credit);
    val.wallet_limit = Number(val.wallet_limit);
    val.auto_bet = Number(val.auto_bet);
    val.auto_bet_result = Number(val.auto_bet_result);
    val.id = this.id;
    val.city = this.selectCity;
    val.area = this.selectArea;
    val.gender = 'MALE';
    val.bet_limits = '[]';
    val.allow_game = '[]';
    val.world_football_game_settings = '[]';
    val.cnt_of_special_card = '[]';
    val.game_result_amount_limit = '[]';
    val.auto_bet_amounts = JSON.stringify(this.autoBetAmounts);
    val.auto_bet_bit_type = getAutoBetTypeByArr(this.autoBetItems);
    val.auto_bet_amount = Number(val.auto_bet_amount);

    if (!this.singlePanel.betTotalAmountValid) {
      return alert(this.translateService.instant('betTotalExceeded'));
    }

    if (this.checkPasswordIsSame()) {
      return alert(this.translateService.instant('samePassword'));
    }

    if (!this.isPasswordChanged()) {
      delete val.password;
    }

    if (val.auto_bet_result % 1) {
      return alert('輸贏金額必須為整數');
    }

    if (val.wallet_limit && val.auto_bet_result > val.wallet_limit) {
      return alert('輸贏金額不得大於限贏金額');
    }

    if (this.myForm.valid && this.singlePanel.getFormValid()) {
      const singlePanelValue = this.singlePanel.getFormValue();
      val.single_bet_limits = singlePanelValue.single_bet_limits;

      const req = [this.memberService.updateMemberByAdmin(val)];
      if (this.checkUpdateWashCodeTime()) req.push(this.updateMemberWashCode());
      req.push(this.memberService.settingChangeNotify(val.id));
      forkJoin(req).subscribe(
        () => {
          alert(this.translateService.instant('common.editSuccess'));
          this.location.back();
        },
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      this.markFormGroupTouched(this.myForm);
    }

  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  cancel() {
    this.location.back();
  }

  /**
   * setAreaByCity - 以城市設定地區
   * @param city
   */
  setAreaByCity(city: string) {
    this.allArea = Object.keys(TaiwanDistricts[city]);
  }

  /**
   * cityOnChange - 當城市變更
   * @param city
   */
  cityOnChange(e: any) {
    this.selectCity = e;
    this.setAreaByCity(e);
  }

  /**
   * areaOnChange - 當地區變更
   * @param city
   */
  areaOnChange(e: any) {
    this.selectArea = e;
  }

  modifySicboCreditAndWallet(amount: string, decrement: boolean = false) {
    const credit = Number(amount);
    if (credit && POSITIVE_INTEGER.test(amount)) {
      let reqs = [this.memberWalletServ.incrementDragonTigerCreditAndWallet(this.id, credit, this.loginUserID)];
      if (decrement) {
        reqs = [this.memberWalletServ.decrementDragonTigerCreditAndWallet(this.id, credit, this.loginUserID)];
      }

      // reqs.push(this.memberService.settingChangeNotify(this.id));

      forkJoin(reqs).subscribe(
        () => {
          alert(this.translateService.instant('common.editSuccess'));
          this.setForm();
        },
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
    }
  }

  settlementSicboWallet() {
    this.modalRef = this.modalService.show(ConfirmModalComponent,
      { initialState: { message: this.translateService.instant('manageAgent.form.settlementWarning') } })
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        forkJoin([this.memberWalletServ.settlementSicboWallet(this.id, this.loginUserID)])
          .subscribe(
            () => {
              alert(this.translateService.instant('common.editSuccess'));
              this.setForm();
            },
            err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
          );
      }
    })
  }

  clearSicboCreditAndWallet() {
    this.modalRef = this.modalService.show(ConfirmModalComponent,
      { initialState: { message: this.translateService.instant('manageAgent.form.clearMemberWarning') } })
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        forkJoin([this.memberWalletServ.clearSicboCreditAndWallet(this.id, this.loginUserID)])
          .subscribe(
            () => {
              alert(this.translateService.instant('common.editSuccess'));
              this.setForm();
            },
            err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
          );
      }
    })
  }

  getWeekChange(week) {
    switch (week) {
      case 0:
        return '週日';
      case 1:
        return '週一';
      case 2:
        return '週二';
      case 3:
        return '週三';
      case 4:
        return '週四';
      case 5:
        return '週五';
      case 6:
        return '週六';
    }
  }

  /**
   * showMemberPassword - 檢視密碼
   */
  showAgentPassword() {
    this.memberService.getMemberRamdomPassword(this.id)
      .subscribe(
        res => {
          const password = atob(res.data);
          this.oldPassword = password;
          this.myForm.patchValue({
            password: password
          });
        },
        err => console.log(err)
      );
  }

  updateMemberWashCode() {
    const wash_code_commission_percent = Number(this.myForm.get('wash_code_commission_percent').value);
    const data = {
      id: this.id,
      wash_code_commission_percent,
      rebate_percent: 0
    };
    return this.memberService.updateMemberWashCodeCommissionPercent(data);
  }

  AllowPlayGame(event) {
    const data = {
      id: this.id,
      is_allow_play: event ? 1 : 0,
    };
    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        // alert('修改成功');
      },
      err => {
        alert('修改失敗');
      }
    );
  }

  generateRange() {
    const arr = new Array(10);
    for (let index = 0; index < arr.length; index++) {
      this.washCodeRange.push((index + 1) / 10);
    }
  }

  createSpec() {
    return this.fb.group({
      1: [null],
      17: [null],
      2: [null],
      3: [null],
      4: [null],
      5: [null],
      6: [null],
      7: [null],
      8: [null],
      9: [null],
      10: [null],
      11: [null],
      12: [null],
      13: [null],
      14: [null],
      15: [null],
      16: [null],
    })
  }

  /**
   * 取得User資料By ID
   */
  getUser(id) {
    this.userService.getUser(id).subscribe(
      res => {
        this.User = res.data;
        this.userWallet = this.User.user_wallet.dragon_tiger_wallet;
        // console.log(this.User);
      }
    );
  }

  getTranslatedWeek(val: number) {
    return this.enumTranslateServ.getTranslatedWeek(val);
  }

  checkPasswordIsSame() {
    const ctrl = this.myForm.get('password');
    return this.id && !ctrl.pristine && ctrl.value === this.oldPassword;
  }

  isPasswordChanged() {
    const ctrl = this.myForm.get('password');
    return this.id && !ctrl.pristine;
  }

  updateMemberAutoBet() {
    const formVal = this.myForm.getRawValue();
    const autoBet = Number(formVal.auto_bet);
    const autoBetResult = Number(formVal.auto_bet_result);
    const winTotalAmountLimit = Number(formVal.dragon_tiger_win_total_amount_limit)


    if (autoBetResult < 0 && Math.abs(autoBetResult) > this.memberWallet) {
      return alert('剩餘金額不足');
    }

    if (winTotalAmountLimit > 0 && autoBetResult > winTotalAmountLimit) {
      return alert('輸贏金額不可大於封頂限贏');
    }

    this.checkAgentIsOnline(this.agentIdByMember).then(isOnline => {
      if (isOnline && autoBet) return alert('代理在線無法修改');
      this.memberService.updateMemberAutoBet(this.id, this.loginUserID, autoBet, autoBetResult).subscribe(() => {
        alert(this.translateService.instant('common.editSuccess'));
      },
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
    });
  }

  isBetamountChecked(item: number) {
    return this.autoBetAmounts.includes(item);
  }

  isBetItemChecked(item: number) {
    return this.autoBetItems.includes(item);
  }

  onBetamountChecked(item: number, checked: boolean) {
    this.autoBetAmounts = [item];
    // if (checked) this.autoBetAmounts.push(item);
    // else this.autoBetAmounts = this.autoBetAmounts.filter(e => e != item);
    // this.autoBetAmounts = Array.from(new Set(this.autoBetAmounts));
  }

  onAutoBetItemChecked(item: number, checked: boolean) {
    if (checked) this.autoBetItems.push(item);
    else this.autoBetItems = this.autoBetItems.filter(e => e != item);
    this.autoBetItems = Array.from(new Set(this.autoBetItems));
  }

  deleteMember(id: number) {
    this.modalRef = this.modalService.show(ConfirmModalComponent,
      { initialState: { message: this.translateService.instant('common.wannaDelete') } })
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.memberService.deleteMember(id)
          .subscribe(
            () => {
              alert(this.translateService.instant('common.deleteSuccess'));
              this.cancel();
            },
            err => alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`)
          );
      }
    })
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    if (this.parent.identity == UserRole.agent || this.parent.identity == UserRole.topAgent) {
      if (data.is_allow_play && !this.parent.is_allow_play) return alert('上級遊戲狀態已關閉無法修改');
    }

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  checkUpdateWashCodeTime() {
    if (!this.updateWashCodeTimeStr) return false;
    return checkUpdateWashCodeTime(this.updateWashCodeTimeStr);
  }

  checkAgentIsOnline(agentID: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.agentServ.getAgentParentsIdByAgentsId(agentID).toPromise().then(r => {
        let ids: number[] = r.data ? r.data : [];
        ids.push(agentID);
        let reqs = ids.map(e => this.userService.getUser(e));
        if (ids.length) {
          forkJoin(reqs).toPromise().then(r => {
            let users: User[] = r.map(e => e.data);
            resolve(users.some(e => e.identity === UserRole.agent && e.is_hello));
          });
        } else {
          resolve(false);
        }
      })
    });
  }

  getAutoBetGameResultAmount(member: Member) {
    if (!member.reopen_auto_bet_at) return;

    this.changeLogServ.getChangeLogsByRoleID(member.id).subscribe(r => {
      let logs: ChangeLog[] = r.data;
      logs = logs.filter(e => e.role === UserRole.member && e.column_name === ChangeLogColumn.AutoBet && Number(e.new_value) == Status.InActive)
        .sort((a, b) => b.id - a.id);


      let st = member.reopen_auto_bet_at;
      let ed = getFormatedDateStr(new Date(), 'yyyy-MM-dd HH:mm:ss');
      if (!member.auto_bet && logs.length) ed = logs[0].created_at;
      let body = {
        game_types_id: GameTypeEnum.GameTypeDragonTiger,
        action: GameActionTypeEnum.Bet,
        start_date: st,
        end_date: ed,
        members_id: member.id,
        is_total: 1
      }
      this.betRecordServ.getResultTotalAmountByDateAndMember(body).subscribe(r => {
        let records: ResultRecord[] = Object.values(r.data);
        records.forEach(e => this.autoBetGameResult += e.total_game_result_amount);
      });

    });


  }

  updateBaccaratWalletLimit() {
    let val = this.myForm.value;
    val.dragon_tiger_win_total_amount_limit = Number(val.dragon_tiger_win_total_amount_limit);
    val.auto_bet_result = Number(val.auto_bet_result);

    let body: WinTotalAmountLimitBody = {
      id: this.id,
      game_types_id: GameTypeEnum.GameTypeDragonTiger,
      operator_id: this.loginUserID,
      amount: Number(val.dragon_tiger_win_total_amount_limit)
    }

    this.memberWalletServ.editWinTotalAmountLimit(body).subscribe(() => {
      this.resetBaccaratWalletLimit(false);
      alert(this.translateService.instant('common.editSuccess'));
    },
      err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
    );
  }

  resetBaccaratWalletLimit(alertMsg = true) {
    let body: WinTotalAmountLimitBody = {
      id: this.id,
      game_types_id: GameTypeEnum.GameTypeDragonTiger,
      operator_id: this.loginUserID,
      amount: 0
    }

    this.memberWalletServ.resetWinTotalAmountLimit(body).subscribe(() => {
      this.newMemberWallet.dragon_tiger_game_result_amount = 0;
      alertMsg && alert(this.translateService.instant('common.editSuccess'));
    },
      err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
    );
  }

  changeColor(val: number) { return changeColor(val) }

  getFormCtrlErrDesc(ctrl: AbstractControl) { return getFormCtrlErrDesc(ctrl) }

  get UserRole() { return UserRole }

  get AutoBetItemType() { return AutoBetItemType }

  get AutoBetItemTypeText() { return AutoBetItemTypeText }
}

interface ResultRecord {
  game_number: string;
  result: number;
  status: number;
  total_bet_amount: number;
  total_game_result_amount: number;
  created_at: string;
}
