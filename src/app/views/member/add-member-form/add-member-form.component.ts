import { UserService } from './../../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';
import { MemberService } from '../../../services/member.service';
import { ActivatedRoute } from '@angular/router';
import { Member } from '../../../models/member.model';
import { formatDate, Location } from '@angular/common';
import { TaiwanDistricts } from '../../../enum/taiwan-districts';
import { TranslateService } from '@ngx-translate/core';
import { SingleBetPanelComponent } from '../../../component/single-bet-panel/single-bet-panel.component';
import { User } from '../../../models/user';
import { UserRole } from '../../../enum/user-role.enum';
import { MemberWallet } from '../../../models/member-wallet';
import { AutoBetItemType } from '../../../enum/auto-bet-item-type.enum';
import { MAX_WALLET_AMOUNT } from '../../../common/common';
import { getFormCtrlErrDesc } from '../../../common/form';

const POSITIVE_INTEGER = /^\d+$/;
@Component({
  selector: 'app-add-member-form',
  templateUrl: './add-member-form.component.html',
  styleUrls: ['./add-member-form.component.css']
})
export class AddMemberFormComponent implements OnInit {
  @ViewChild('singlePanel') singlePanel: SingleBetPanelComponent;
  member: Member;
  myForm: FormGroup;

  id: number;
  userWallet: number;
  loginUserID: number;
  washCodeRange = [];
  loginUserIdentity: number;
  singleBetLimits: any;

  allCity: string[] = [];
  allArea: string[] = [];
  selectCity = '';
  selectArea = '';

  parentWashCodePercent = 1;

  parent: User;

  constructor(
    private translateService: TranslateService,
    private memberService: MemberService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private location: Location,
    private userService: UserService,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.loginUserID = Number(localStorage.getItem('authenticatedID'));
    this.loginUserIdentity = Number(localStorage.getItem('loginUserIdentity'));
    this.initCityArea();
    if (this.id === 0) {
      this.getUser(this.loginUserID);
    } else {
      this.getUser(this.id);
    }

    this.generateRange();
  }

  /**
   * 取得代理資料 By ID
   */
  getUser(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data;
        this.userWallet = data.user_wallet.dragon_tiger_wallet;
        this.parentWashCodePercent = data.wash_code_commission_percent;
        this.washCodeRange = this.washCodeRange.filter(e => e <= this.parentWashCodePercent);
        this.parent = data;
      }
    );
  }

  initCityArea() {
    this.allCity = Object.keys(TaiwanDistricts);
  }

  /**
   * setAreaByCity - 以城市設定地區
   * @param city
   */
  setAreaByCity(city: string) {
    this.allArea = Object.keys(TaiwanDistricts[city]);
  }

  /**
   * cityOnChange - 當城市變更
   * @param city
   */
  cityOnChange(e: any) {
    this.selectCity = e;
    this.setAreaByCity(e);
  }

  /**
   * areaOnChange - 當地區變更
   * @param city
   */
  areaOnChange(e: any) {
    this.selectArea = e;
  }

  groupForm() {
    this.myForm = this.fb.group({
      'id': [null],
      'account': [null, [Validators.pattern('^[A-Za-z0-9]+$'), Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      'name': [null, [Validators.required, Validators.maxLength(5)]],
      'nickname': [null, Validators.maxLength(6)],
      'password': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      'gender': [null],
      'birthday': [null],
      'credit': [0, [Validators.required, Validators.min(0), Validators.max(MAX_WALLET_AMOUNT), Validators.pattern(POSITIVE_INTEGER)]],
      'wallet': [null, [Validators.min(0), Validators.pattern(POSITIVE_INTEGER)]],
      'agent_id': [null],
      'status': [1],
      'is_allow_play': [1],
      'bank_account': [null],
      'remark': [null],
      'wallet_limit': [0, [Validators.min(0), Validators.pattern(POSITIVE_INTEGER)]],
      'dragon_tiger_win_total_amount_limit': [0, [Validators.min(0), Validators.pattern(POSITIVE_INTEGER)]],
      'wash_code_commission_percent': [0, [Validators.required, Validators.max(1)]],
      'auto_bet': [0],
      'auto_bet_result': [0],
      'game_roles_id': [1]
    });
  }

  submitForm(val) {
    if (this.id !== 0) {
      val.agent_id = this.id;
    } else {
      val.agent_id = this.loginUserID;
    }
    val.auto_bet = Number(val.auto_bet);
    val.auto_bet_result = Number(val.auto_bet_result);
    val.status = Number(val.status);
    val.is_allow_play = Number(val.is_allow_play);
    val.credit = Number(val.credit);
    val.wallet_limit = Number(val.wallet_limit);
    val.wallet = val.credit;
    val.birthday = formatDate(val.birthday, 'yyyy-MM-dd', 'en-US');
    val.city = this.selectCity;
    val.area = this.selectArea;
    val.gender = 'MALE';
    val.wash_code_commission_percent = Number(val.wash_code_commission_percent);
    val.nickname = val.nickname ? val.nickname : val.name;
    val.auto_bet_bit_type = AutoBetItemType.DragonTiger;

    let wallet = {
      dragon_tiger_credit: val.credit,
      dragon_tiger_wallet: val.wallet,
      sic_bo_credit: 0,
      sic_bo_wallet: 0,
      baccarat_wallet: 0,
      baccarat_credit: 0,
      poker_credit: 0,
      poker_wallet: 0,
      dragon_tiger_win_total_amount_limit: Number(val.dragon_tiger_win_total_amount_limit),
    } as MemberWallet;

    val.member_wallet = JSON.stringify(wallet);
    val.bet_limits = '[]';
    val.allow_game = '[]';
    val.world_football_game_settings = '[]';
    val.cnt_of_special_card = '[]';
    val.game_result_amount_limit = '[]';

    if (this.userWallet && val.wallet > this.userWallet) {
      return alert(this.translateService.instant('manageAgent.form.balanceInsufficient'));
    }

    if (!this.singlePanel.betTotalAmountValid) {
      return alert(this.translateService.instant('betTotalExceeded'));
    }

    if (val.auto_bet_result % 1) {
      return alert('輸贏金額必須為整數');
    }

    if (val.wallet_limit && val.auto_bet_result > val.wallet_limit) {
      return alert('輸贏金額不得大於限贏金額');
    }

    if (this.myForm.valid && this.singlePanel.getFormValid()) {
      const singlePanelValue = this.singlePanel.getFormValue();
      val.single_bet_limits = singlePanelValue.single_bet_limits;
      val.single_bet_total_amounts = singlePanelValue.single_bet_total_amounts;

      this.memberService.addMember(val).subscribe(
        () => {
          let successMsg = this.translateService.instant('common.addSuccess');
          alert(successMsg);
          this.location.back();
        },
        err => {
          alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`);
        }
      );
    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      this.markFormGroupTouched(this.myForm);
    }
  }

  /**
   * 產生密碼
   */
  setRandomCode(len: number) {
    const characters = '23456789';
    let code = '';
    let n = 0;
    let randomnumber = 0;
    while (n < len) {
      n++;
      randomnumber = Math.floor(characters.length * Math.random());
      code += characters.substring(randomnumber, randomnumber + 1);
    }
    this.myForm.controls['password'].setValue(code.toLocaleUpperCase());
  }

  /**
   * 表單驗證(必填欄位提示)
   */
  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  generateRange() {
    const arr = new Array(10);
    for (let index = 0; index < arr.length; index++) {
      this.washCodeRange.push((index + 1) / 10);
    }
  }

  cancel() {
    this.location.back();
  }

  getFormCtrlErrDesc(ctrl: AbstractControl) { return getFormCtrlErrDesc(ctrl) }

  get UserRole() { return UserRole }
}
