import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { AdminService } from '../../../services/admin.service';
import { Admin } from '../../../models/admin';
import { Location } from '@angular/common';
import { UserService } from '../../../services/user.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../../../enum/user-role.enum';
import { User } from '../../../models/user';
import { AgentService } from '../../../services/agent.service';
import { AuthenticationService } from '../../../services/auth.service';
import { UserLogin } from '../../../models/user-login';
import { Router } from '@angular/router';
import { UserWallet } from '../../../models/user-wallet';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  public myForm: FormGroup;
  public infoForm: FormGroup;
  public isAgent = false;
  public isClone = false;
  private id: string;
  public avatarUrl = 'assets/img/avatars/admin.png';

  user: User;
  lastLoginAt: string;
  oldPassword: string;
  userLogin: UserLogin;
  userWallet: UserWallet;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private admin: AdminService,
    private location: Location,
    private userService: UserService,
    private agentService: AgentService,
    private authServ: AuthenticationService,
    private router: Router) {
    this.groupMyForm();
  }

  ngOnInit() {
    this.inintMyForm();
  }

  passwordMatches(form: AbstractControl) {
    return form.get('password').value === form.get('re_password').value ? null : { equals: true };
  }

  groupMyForm() {
    this.myForm = this.fb.group({
      'real_name': [null, Validators.required],
      'name': [null, Validators.required],
      'old_password': [null, Validators.minLength(6)],
      'password': [null, Validators.minLength(6)],
      're_password': [null],
      'identity': [null],
      'credit': [null],
      'wallet': [null],
      'wash_code_commission_percent': [null],
      'revenue_share': [null],
      'status': true,
    }, { validators: this.passwordMatches });
  }

  inintMyForm() {
    this.id = localStorage.getItem('authenticatedID');
    this.admin.getUser(this.id).subscribe(
      res => {
        const data: User = res.data;
        this.userWallet = data.user_wallet;
        this.myForm.patchValue(data);
        this.isAgent = data.identity == UserRole.agent || data.identity == UserRole.topAgent;
        this.isClone = data.clone_id != 0;
        this.user = data;
        this.lastLoginAt = localStorage.getItem('last_login_at');
        this.getUserLogin();
        if (this.isAgent) {
          this.getAgentPassword();
          this.myForm.get('real_name').disable();
        }
      },
      err => {
        alert(`${this.translateService.instant('common.getFailed')} ${err.status} ${err.statusText}`);
      });
  }

  getAgentPassword() {
    this.agentService.getAgentRamdomPassword(Number(this.id))
      .subscribe(
        res => this.oldPassword = atob(res.data),
        err => console.log(err)
      );
  }

  getUserLogin() {
    try {
      this.userLogin = JSON.parse(this.authServ.getUserLogin());
    } catch (error) {
      console.log(error);
    }
  }

  submitForm(val) {
    if (this.checkPasswordIsSame()) {
      return alert(this.translateService.instant('samePassword'));
    }

    if (this.myForm.valid) {
      const user = val as Admin;
      user.id = Number(this.id);
      user.identity = val.identity;
      user.status = val.status;

      if (this.userLogin?.is_first_change_password) {
        if (val.old_password && val.old_password === val.password) {
          return alert(this.translateService.instant('samePassword'));
        }

        if ((val.password && !val.old_password) || (!val.password && val.old_password)) {
          return alert('請輸入舊密碼與新密碼');
        }
      }

      if (this.isAgent && this.userLogin && !this.userLogin.is_first_change_password) {
        return this.firstUpdate(user);
      }
      this.updateSelf(user);
    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
    }
  }

  firstUpdate(user: any) {
    this.agentService.updateAgentPasswordFirst(user.id, user.password).subscribe(
      () => {
        alert(this.translateService.instant('updatedFirstPWD'));
        this.authServ.logout();
        this.router.navigate(['/login']);
      },
      err => alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`)
    );
  }

  updateSelf(user: any) {
    let req = [this.userService.updateUserSelf(user).pipe(tap(() => { }, () => alert('帳戶修改失敗')))];
    if (user.password) {
      req.push(this.userService.updateUserPassword(user.id, user.old_password, user.password).pipe(tap(() => {
        alert('密碼更新成功，請重新登入!');
        this.authServ.logout();
        this.router.navigate(['/login']);
      }, () => alert('密碼修改失敗'))));
    }

    forkJoin(req).subscribe(
      () => {
        if (!user.password) {
          alert(this.translateService.instant('common.editSuccess'));
        }
      }
    );
  }

  cancel() {
    this.location.back();
  }

  checkPasswordIsSame() {
    const ctrl = this.myForm.get('password');
    return this.id && !ctrl.pristine && ctrl.value === this.oldPassword;
  }

  logout() {
    this.authServ.logout();
    this.router.navigate(['/login']);
  }
}
