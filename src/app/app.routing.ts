import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
// Import Containers
import { DefaultLayoutComponent } from './containers';
import { OperationItems } from './enum/operation-items.enum';
import { AdminFormComponent } from './views/admin/admin-form/admin-form.component';
import { AdminMemberComponent } from './views/admin/admin-member/admin-member.component';
import { AdminComponent } from './views/admin/admin/admin.component';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { MemberFormComponent } from './views/member/member-form/member-form.component';
import { MyAccountComponent } from './views/my-admin/my-account/my-account.component';
import { RegisterComponent } from './views/register/register.component';
import { AdminAgentComponent } from './views/admin/admin-agent/admin-agent.component';
import { AddMemberFormComponent } from './views/member/add-member-form/add-member-form.component';
import { AgentAuthority } from './enum/agent-authority.enum';
import { RegistrationMemberComponent } from './pages/registration-member/registration-member.component';
import { MyMemberComponent } from './pages/my-member/my-member.component';
import { MyAgentComponent } from './pages/my-agent/my-agent.component';
import { MyCloneComponent } from './pages/my-clone/my-clone.component';
import { MyCloneFormComponent } from './pages/my-clone/my-clone-form/my-clone-form.component';
import { OnlinePaymentComponent } from './pages/online-payment/online-payment.component';




export const routes: Routes = [{
    path: '',
    redirectTo: 'appadmin',
    pathMatch: 'full',
    data: {
        operation_itemID: OperationItems.OperationItemsNone
    }
},
{
    path: '404',
    component: P404Component,
    data: {
        title: 'Page 404'
    }
},
{
    path: '500',
    component: P500Component,
    data: {
        title: 'Page 500'
    }
},
{
    path: 'login',
    component: LoginComponent,
    data: {
        title: 'Login Page'
    }
},
{
    path: 'register',
    component: RegisterComponent,
    data: {
        title: 'Register Page'
    }
},
{
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
        title: 'Home'
    },
    children: [{
        path: 'appadmin',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule),
        data: {
            auth_manageID: AgentAuthority.OperationItemNone
        }
    },
    {
        path: 'agant-add-member/:id',
        component: AddMemberFormComponent,
        data: {
            title: '新增會員',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'member-edit/:id',
        component: MemberFormComponent,
        data: {
            title: '修改會員',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'system',
        loadChildren: () => import('./views/system/system.module').then(m => m.SystemModule)
    },
    // {
    //     path: 'comment',
    //     component: CommentListComponent,
    //     data: {
    //         title: '評論管理'
    //     }
    // },
    {
        path: 'users',
        component: AdminComponent,
        data: {
            title: '代理管理',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'top-agent',
        component: AdminFormComponent,
        data: {
            title: '新增帳戶',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'top-agent-edit/:id/:parent_id',
        component: AdminFormComponent,
        data: {
            title: '修改帳戶',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'admin-agent/:id',
        component: AdminAgentComponent,
        data: {
            title: '新增代理',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'admin-agent-edit/:id',
        component: AdminAgentComponent,
        data: {
            title: '修改代理',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'agent-members/:id',
        component: AdminMemberComponent,
        data: {
            title: '代理管理',
            auth_manageID: AgentAuthority.OperationItemUser
        }
    },
    {
        path: 'members',
        component: RegistrationMemberComponent,
        data: {
            title: '會員管理',
            auth_manageID: AgentAuthority.OperationItemRegistrationMembers
        }
    },
    {
        path: 'my-account',
        component: MyAccountComponent,
        data: {
            title: '我的帳戶',
            auth_manageID: AgentAuthority.OperationItemNone
        }
    },
    {
        path: 'my-agents',
        component: MyAgentComponent,
        data: {
            title: '新增代理',
            auth_manageID: AgentAuthority.OperationItemMyAgents
        }
    },
    {
        path: 'my-members',
        component: MyMemberComponent,
        data: {
            title: '新增會員',
            auth_manageID: AgentAuthority.OperationItemMyMembers
        }
    },
    {
        path: 'my-clones',
        component: MyCloneComponent,
        data: {
            title: '新增子帳號',
            auth_manageID: AgentAuthority.OperationItemMyClones
        }
    },
    {
        path: 'my-clones/form',
        component: MyCloneFormComponent,
        data: {
            title: '新增子帳號',
            auth_manageID: AgentAuthority.OperationItemMyClones
        }
    },
    {
        path: 'my-clones/form/:id',
        component: MyCloneFormComponent,
        data: {
            title: '新增子帳號',
            auth_manageID: AgentAuthority.OperationItemMyClones
        }
    },
    {
        path: 'online-payment',
        component: OnlinePaymentComponent,
        data: {
            title: '線上付款',
            auth_manageID: AgentAuthority.OperationItemOnlinePayment
        }
    },
    ]
},
{
    path: 'dealers',
    canActivate: [AuthGuard],
    loadChildren: () => import('./views/system/system.module').then(m => m.SystemModule)
},
{ path: '**', component: P404Component }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }