import { INavData } from '@coreui/angular';
import { AuthenticationService } from './services/auth.service';
import { DefaultLayoutComponent } from '../../src/app/containers/default-layout/default-layout.component';
import { from } from 'rxjs';

export const ADMIN_NAVITEMS: INavData[] = [
  // {
  //   name: '儀表版',
  //   url: '/dashboard',
  //   icon: 'icon-speedometer',
  //   // badge: {
  //   //   variant: 'info',
  //   //   text: 'NEW'
  //   // }
  // },
  // {
  //   name: 'Colors',
  //   url: '/theme/colors',
  //   icon: 'icon-drop'
  // },

  {
    name: '後台總覽',
    url: '/appadmin',
    icon: 'icon-home'
  },
  {
    divider: true
  },
  {
    title: true,
    name: '後台設定'
  },
  {
    name: '最新消息',
    url: '/news',
    icon: 'fa fa-bullhorn'
  },
  //{
  //  name: '會員管理',
  //  url: '/members',
  //  icon: 'icon-people'
  //},
  // {
  //   name: '商家管理',
  //   url: '/store',
  //   icon: 'icon-briefcase',
  //   children: [
  //     {
  //       name: '商家管理',
  //       url: '/store/stores',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '商家審核',
  //       url: '/store/store-audit',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '商家分類',
  //       url: '/store/store-types',
  //       icon: 'icon-layers'
  //     },
  //   ]
  // },
  // {
  //   name: '分類管理',
  //   url: '/category',
  //   icon: 'icon-tag',
  //   children: [
  //     {
  //       name: '首頁分類',
  //       url: '/category/service-types',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '商品分類',
  //       url: '/category/product-types',
  //       icon: 'icon-layers'
  //     },
  //     // {
  //     //   name: '次要分類管理',
  //     //   url: '/category/minor-category',
  //     //   icon: 'icon-layers'
  //     // },
  //     {
  //       name: '標籤管理',
  //       url: '/category/tags',
  //       icon: 'icon-layers'
  //     }
  //   ]
  // },
  // {
  //   name: '商品管理',
  //   url: '/product',
  //   icon: 'icon-diamond',
  //   children: [
  //     {
  //       name: '商品管理',
  //       url: '/product/products',
  //       icon: 'icon-layers'
  //     },
  //     // {
  //     //   name: '活動管理',
  //     //   url: '/product/activities',
  //     //   icon: 'icon-layers'
  //     // },
  //     {
  //       name: '套餐管理',
  //       url: '/product/combo-set',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '滿件優惠',
  //       url: '/product/full-discount',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '折扣代碼',
  //       url: '/product/discount-codes',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '金流管理',
  //       url: '/product/payment',
  //       icon: 'icon-layers'
  //     }
  //   ]
  // },
  // {
  //   name: '系統管理',
  //   url: '/system',
  //   icon: 'icon-settings',
  //   children: [
  //     {
  //       name: '首頁輪播',
  //       url: '/system/carousels',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '廣告管理',
  //       url: '/system/ad-list',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '標籤管理',
  //       url: '/system/tag-list',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '基本設置',
  //       url: '/system/meta',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '其他設置',
  //       url: '/system/other',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '聯絡信箱',
  //       url: '/system/contact',
  //       icon: 'icon-layers'
  //     },
  //     {
  //       name: '跑馬燈',
  //       url: '/system/marquee',
  //       icon: 'icon-layers'
  //     },
  //   ]
  // },
  // {
  //   name: '評論管理',
  //   url: '/comment',
  //   icon: 'icon-bubble'
  // },
  {
    divider: true
  },
  {
    title: true,
    name: '帳號設定'
  },
  {
    name: '代理管理',
    url: '/users',
    icon: 'icon-user'
  },
  {
    name: '我的帳戶',
    url: '/my-account',
    icon: 'fa fa-shield'
  },

];

export const MANAGEMENT_NAVITEMS: INavData[] = [
  {
    name: '我的帳戶',
    url: '/my-account',
    icon: 'fa fa-shield'
  },
  {
    name: '代理管理',
    url: '/users',
    icon: 'fa fa-user-circle-o'
  },
  {
    title: true,
    name: '記錄查詢'
  },
  {
    name: '押注記錄',
    url: '/system/bet-record-list',
    icon: 'icon-menu'
  },
  {
    name: '打賞記錄',
    url: '/system/donate-record-list',
    icon: 'icon-menu'
  },
  {
    name: '遊戲記錄',
    url: '/system/calc-item-result-record-list',
    icon: 'icon-menu'
  },
  {
    name: '進階查詢',
    url: '/system/search-result',
    icon: 'icon-menu'
  },
  {
    name: '報表紀錄',
    url: '/system/report-record-list',
    icon: 'icon-menu'
  },
  {
    title: true,
    name: '系統設定'
  },
  {
    name: '單注設置',
    url: '/system/single-items',
    icon: 'icon-settings'
  },
  {
    title: true,
    name: '公告訊息'
  },
  {
    name: '站內公告',
    url: '/system/insite-proclamations',
    icon: 'icon-settings'
  },
];

export const DEFAULT_NAVITEMS: INavData[] = [
  {
    name: '我的帳戶',
    url: '/my-account',
    icon: 'fa fa-shield'
  },
  {
    name: '代理管理',
    url: '/users',
    icon: 'fa fa-user-circle-o'
  },
  {
    title: true,
    name: '記錄查詢'
  },
  {
    name: '報表紀錄',
    url: '/system/report-record-list',
    icon: 'icon-menu'
  },
  {
    name: '進階查詢',
    url: '/system/search-result',
    icon: 'icon-menu'
  },
  {
    name: '押注記錄',
    url: '/system/bet-record-list',
    icon: 'icon-menu'
  },
  {
    name: '打賞記錄',
    url: '/system/donate-record-list',
    icon: 'icon-menu'
  },
  {
    name: '遊戲記錄',
    url: '/system/calc-item-result-record-list',
    icon: 'icon-menu'
  },
  {
    title: true,
    name: '系統設定'
  },
  // {
  //   name: '荷官操作',
  //   url: '/system/croupier-operation',
  //   icon: 'icon-settings'
  // },
  {
    name: '遊戲設置',
    url: '/system/draw-settings',
    icon: 'icon-settings'
  },
  {
    name: '單注設置',
    url: '/system/single-items',
    icon: 'icon-settings'
  },
  // {
  //   name: '項目設置',
  //   url: '/system/game-settings-list',
  //   icon: 'icon-settings'
  // },
  {
    name: '籌碼設置',
    url: '/system/bet-amounts',
    icon: 'icon-settings'
  },
  {
    name: '其他設置',
    url: '/system/other-settings',
    icon: 'icon-settings'
  },
  {
    name: '留言過濾',
    url: '/system/message-blacklists',
    icon: 'icon-settings'
  },
  {
    title: true,
    name: '公告訊息'
  },
  {
    name: '即時公告',
    url: '/system/proclamations',
    icon: 'icon-settings'
  },
  {
    name: '一般公告',
    url: '/system/normal-proclamations',
    icon: 'icon-settings'
  },
  {
    name: '站內公告',
    url: '/system/insite-proclamations',
    icon: 'icon-settings'
  },
  {
    title: true,
    name: '帳戶權限'
  },
  {
    name: '帳戶權限',
    url: '/system/agent-authority',
    icon: 'icon-settings'
  },
];
