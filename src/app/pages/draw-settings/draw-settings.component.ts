
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { addIndexToArray, defaultAgentID } from '../../common/global';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { BetRecord } from '../../models/bet-record';
import { GameType } from '../../models/game-type';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { MemberService } from '../../services/member.service';
import { UserService } from '../../services/user.service';

const INTERVAL = 3000;
const DEFAULT_AGENT_ID = defaultAgentID;
@Component({
  selector: 'app-draw-settings',
  templateUrl: './draw-settings.component.html',
  styleUrls: ['./draw-settings.component.css']
})
export class DrawSettingsComponent implements OnInit, OnDestroy {

  dataSource = new MatTableDataSource<Member>();
  displayedColumns: string[] = ['account', 'bet_item'];

  user: User;
  selectedMember: Member;

  // 判斷遊戲狀態
  gameTypes: GameType[];
  gameTypeID = GameTypeEnum.GameTypeDragonTiger;
  gameType: GameType;

  playingMemberInterval: any;
  gameTypeInterval: any;

  isAdminDemo = false;

  // 會員下注即時紀錄
  betRecords: BetRecord[] = [];
  currentMembers: Member[];

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private translateServ: TranslateService,
    private agentServ: AgentService,
    private userServ: UserService,
    private memberServ: MemberService,
    private gameTypeServ: GameTypeService,
    private authServ: AuthenticationService,
  ) {
  }

  ngOnDestroy(): void {
    clearInterval(this.playingMemberInterval);
    clearInterval(this.gameTypeInterval);
  }

  ngOnInit(): void {
    this.dataSource.filterPredicate = (data, filter) => data.account.includes(filter) || data.nickname.includes(filter);
    this.getUser();
    this.startGetGameType();
  }

  getUser() {
    const identity = Number(this.authServ.getUserIdentity());
    let id = Number(this.authServ.getUserId());

    this.isAdminDemo = identity === UserRole.admin && Number(this.authServ.getUserCloneId()) !== 0;

    // 分身帳號使用本尊ID進行查詢
    if (!this.isAdminDemo && this.authServ.getUserCloneId()) {
      id = Number(this.authServ.getUserCloneId());
    }

    this.userServ.getUser(id).subscribe(r => {
      this.user = r.data;
      this.startGetMembers();
    });
  }

  startGetMembers() {
    this.fetchMember();
    this.playingMemberInterval = setInterval(() => {
      this.fetchMember();
    }, INTERVAL);
  }

  startGetGameType() {
    this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
    this.gameTypeInterval = setInterval(() =>
      this.gameTypeServ.getGameType(this.gameTypeID).subscribe(r => this.gameType = r.data), INTERVAL);
  }

  fetchMember() {
    if (this.user.identity === UserRole.admin || this.user.identity === UserRole.management) {
      return this.getMembers();
    }
    this.getChildAgentByUserID(this.user.id);
  }

  getMembers() {
    this.memberServ.getMemberList().subscribe(r => this.setMember(r.data));
  }

  getChildAgentByUserID(id: number) {
    this.agentServ.getAgentChildsIdByAgentsId(id).subscribe(r => this.getChildMembersByParentIDs(r.data));
  }

  getChildMembersByParentIDs(ids: Array<number>) {
    ids = ids ? ids : [];
    const req = ids.map(id => this.memberServ.getMemberByAgent(id));
    req.push(this.memberServ.getMemberByAgent(this.user.id));
    forkJoin(req).pipe(map(res => res.map(r => r.data)))
      .subscribe(r => {
        const members: Member[] = [];
        r.forEach(e => members.push(...e));
        this.setMember(members);
      });
  }

  setMember(members: Member[]) {
    if (this.gameType && this.gameType.status === Status.Active) {
      // const filteredMembers = members.filter(m => m.agent_id !== DEFAULT_AGENT_ID && m.is_play_game);
      const filteredMembers = members.filter(m => m.agent_id !== DEFAULT_AGENT_ID);
      filteredMembers.forEach(e => { })
      return this.currentMembers = filteredMembers;
    }

    // 遊戲關閉清空會員表
    return this.currentMembers = [];
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateServ.instant('common.turnedOn');
    const turnedOffMsg = this.translateServ.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberServ.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateServ.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateServ.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  checkAllowChange(member: Member) {
    if (this.user.identity !== UserRole.admin) {
      return member.agent_id === this.user.id;
    }
    return true;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  onGameTypeChanged(value: string) {
  }

  setBetRecords(data: BetRecord[]) {
    this.betRecords = data;
  }

  getMemberData(memberID: number) {
    let bets = this.betRecords.filter(bet => bet.members_id === memberID);
    return bets;
  }

  setMemberToTable(data: Member[]) {
    let table = this.dataSource.data;
    let existIDs = table.map(e => e.id);

    // 插入新會員
    data.forEach(e => {
      if (table.findIndex(t => t.id === e.id) == -1) this.dataSource.data.push(e);
    });

    // 刪除不存在會員
    existIDs.forEach(id => {
      if (data.findIndex(e => e.id === id) == -1) {
        let idx = table.findIndex(e => e.id === id);
        this.dataSource.data.splice(idx, 1);
      }
    });
    this.dataSource.data = this.dataSource.data;
  }

  get Status() { return Status }
}
