import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { changeColor } from '../../../common/common';
import { getToday, getYesterday, getThisWeek, getLastWeek, getThisMonth, getLastMonth } from '../../../common/date';
import { addIndexToArray } from '../../../common/global';
import { ChangeLogColumnTypeText, ChangeLogColumn, ChangeLogColumnText } from '../../../enum/change-log.enum';
import { GameActionTypeEnum } from '../../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../../enum/game-type.enum';
import { Status } from '../../../enum/status.enum';
import { BetRecord } from '../../../models/bet-record';
import { ChangeLog } from '../../../models/change-log';
import { BetRecordService } from '../../../services/bet-record.service';

@Component({
  selector: 'app-vip-operation-record',
  templateUrl: './vip-operation-record.component.html',
  styleUrls: ['./vip-operation-record.component.css']
})
export class VipOperationRecordComponent implements OnInit {
  @Input() membersID: number;
  @Input() set changeLogs(data: ChangeLog[]) {
    if (data) {

      this.data = data;

      // 取會員下注紀錄
      this.getMemberBetRecordsByChangeLog(data).subscribe(r => {
        let obj = r.data;
        this.resultRecords = Object.values(obj);

        // map輸贏金額
        data.map(e => {
          let gameResult: any = 0;
          if (e.column_name == ChangeLogColumn.AutoBet && Number(e.new_value) == Status.InActive) {
            gameResult = this.getGameResultByChangeLog(e);
          }
          e['game_result_amount'] = gameResult;
        });


        this.onSearched();
        this.loaded = true;
      });
    }
  }


  displayedColumns: string[] = ['index', 'updated_at', 'events', 'game_result'];
  dataSource = new MatTableDataSource<ChangeLog>();
  loaded = false;

  data: ChangeLog[] = [];
  startDate: string;
  endDate: string;

  resultRecords: ResultRecord[];
  totalGameResult = 0;

  constructor(private betRecordServ: BetRecordService) { }

  ngOnInit(): void {
  }

  getEventText(data: ChangeLog) {
    let column = ChangeLogColumnTypeText[data.column_name];
    let newVal = Number(data.new_value);
    let action = newVal ? '啟用' : '停用';
    switch (data.column_name) {
      case ChangeLogColumn.AutoBet:
        return `${action}${column}`;
      case ChangeLogColumn.AutoBetResult:
        return `${column}更新為${Number(data.new_value)}`;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  clear() {
    this.startDate = '';
    this.endDate = '';
    this.dataSource.data = this.data;
  }

  getToday() {
    let res = getToday();
    res.ed = res.st;
    this.setDate(res);
  }

  getYesterday() {
    let res = getYesterday();
    res.ed = res.st;
    this.setDate(res);
  }

  getThisWeek() {
    this.setDate(getThisWeek());
  }

  getLastWeek() {
    this.setDate(getLastWeek());
  }

  getThisMonth() {
    this.setDate(getThisMonth());
  }

  getLastMonth() {
    this.setDate(getLastMonth());
  }

  setDate(date: { st: string, ed: string }) {
    this.startDate = date.st;
    this.endDate = date.ed;
    this.onSearched();
  }

  onSearched() {
    let data = this.data;

    if (this.startDate) {
      let t = new Date(this.startDate);
      t.setHours(0, 0, 0);
      data = data.filter(e => new Date(e.created_at).getTime() >= t.getTime());
    }

    if (this.endDate) {
      let t = new Date(this.endDate);
      t.setHours(0, 0, 0);
      t.setDate(t.getDate() + 1);
      data = data.filter(e => new Date(e.created_at).getTime() <= t.getTime());
    }


    this.totalGameResult = 0;
    data.forEach(e => this.totalGameResult += e['game_result_amount']);

    this.dataSource.data = data;
    this.loaded = true;
  }

  getMemberBetRecordsByChangeLog(data: ChangeLog[]) {
    let st = data[data.length - 1].created_at;
    let ed = data[0].created_at;
    let body = {
      game_types_id: GameTypeEnum.GameTypeSicBo,
      action: GameActionTypeEnum.Bet,
      start_date: st,
      end_date: ed,
      members_id: this.membersID,
      is_total: 1
    }
    return this.betRecordServ.getResultTotalAmountByDateAndMember(body);
  }

  getGameResultByChangeLog(log: ChangeLog) {
    if (!this.resultRecords) return '';
    let st = this.data[this.data.length - 1].created_at;
    let et = log.created_at;
    let idx = this.data.findIndex(e => e.id === log.id);

    for (let i = 0; i < this.data.length; i++) {
      const e = this.data[i];
      if (i > idx && e.column_name == ChangeLogColumn.AutoBet) {
        switch (Number(e.new_value)) {
          // 找前一筆開啟VIP功能紀錄時間
          case Status.Active:
            st = e.created_at;
            break;

          // 連續關閉則返回0
          case Status.InActive:
            return 0;
        }
        break;
      }
    }


    // VIP啟動區間內輸贏結果加總
    let gameResult = 0;
    let records = this.resultRecords.filter(e => {
      let t = new Date(e.created_at).getTime();
      return t >= new Date(st).getTime() && t <= new Date(et).getTime();
    });

    records.forEach(e => {
      gameResult += e.total_game_result_amount;
    });


    return gameResult;
  }

  changeColor(val: number) { return changeColor(val) }

  get ChangeLogColumn() { return ChangeLogColumn }

  get ChangeLogColumnText() { return ChangeLogColumnText }

  get ChangeLogColumnTypeText() { return ChangeLogColumnTypeText }

  get Status() { return Status }
}

interface ResultRecord {
  game_number: string;
  result: number;
  status: number;
  total_bet_amount: number;
  total_game_result_amount: number;
  created_at: string;
}