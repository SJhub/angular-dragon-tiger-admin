import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VipOperationRecordComponent } from './vip-operation-record.component';

describe('VipOperationRecordComponent', () => {
  let component: VipOperationRecordComponent;
  let fixture: ComponentFixture<VipOperationRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VipOperationRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VipOperationRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
