import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { forkJoin, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { changeColor } from '../../common/common';
import { getToday, getYesterday, getThisWeek, getLastWeek, getThisMonth, getLastMonth } from '../../common/date';
import { defaultAgentID } from '../../common/global';
import { ChangeLogColumn } from '../../enum/change-log.enum';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { ChangeLog } from '../../models/change-log';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { BetRecordService } from '../../services/bet-record.service';
import { ChangeLogService } from '../../services/change-log.service';
import { MemberService } from '../../services/member.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-vip-record',
  templateUrl: './vip-record.component.html',
  styleUrls: ['./vip-record.component.css']
})
export class VipRecordComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterColumns: string[] = [ChangeLogColumn.AutoBet, ChangeLogColumn.AutoBetResult];
  displayedColumns: string[] = [
    'index',
    'account',
    'nickname',
    'auto_bet',
    'is_allow_play',
    'is_agent_online',
    'auto_bet_result',
    'total_auto_bet_result',
    'number_of_game',
    'last_update'
  ];
  dataSource = new MatTableDataSource<any>();
  loaded = false;

  // modal
  modalRef: BsModalRef;
  showData: Member;

  users: User[];
  members: Member[];
  changeLogData: ChangeLog[] = [];
  startDate: string;
  endDate: string;
  subscription: Subscription;

  totalGameResult = 0;

  constructor(
    private agentServ: AgentService,
    private betRecordServ: BetRecordService,
    private authServ: AuthenticationService,
    private userServ: UserService,
    private memberServ: MemberService,
    private changeLogServ: ChangeLogService,
    private modalService: BsModalService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    forkJoin([
      this.userServ.getUsers(),
      this.memberServ.getMemberList(),
      this.changeLogServ.getChangeLogs()
    ]).subscribe(r => {
      this.users = r[0].data;
      this.members = r[1].data;
      this.changeLogData = r[2].data;
      this.getToday();
    });
  }

  mapData(members: Member[], logs: ChangeLog[]) {
    logs = logs.filter(e => this.filterColumns.includes(e.column_name));
    members = members.filter(e => e.agent_id !== defaultAgentID);

    this.dataSource.data.map(e => e['total_auto_bet_result'] = '');
    this.subscription && this.subscription.unsubscribe();
    let betApi = [];
    members.map(e => {
      let data = this.getChangeLogsByRoleID(logs, [UserRole.member], e.id);
      e['logs'] = data;
      e['last_update'] = data.length ? data[0].created_at : '';
      e['is_agent_online'] = this.isAgentOnline(e.agent_id);
      data.length && betApi.push(this.getMemberBetRecordsByChangeLog(e.id, data).pipe(tap(r => e['game_result_records'] = Object.values(r.data))));
    });

    const data = members.filter(e => e['logs'].length);
    this.dataSource.sort = this.sort;
    this.subscription = forkJoin(betApi).subscribe(() => this.mapMemberTotalAutoBetGameResultAmount(data));
    if (!betApi.length) this.loaded = true;
  }

  openModal(template: any, data: Member) {
    console.log(data);

    this.showData = data;
    this.modalRef = this.modalService.show(template, { class: 'modal-xl' });
  }

  getChangeLogsByRoleID(logs: ChangeLog[], roles: number[], roleID: number) {
    let data = logs.filter(e => roles.includes(e.role) && e.role_id == roleID);
    data = data.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime());
    return data;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    const value = filterValue.trim().toLowerCase();
    if (value) this.dataSource.data = this.members;
    else this.onSearched();
    this.dataSource.filter = value;
  }

  updateMemberAutoBet(member: Member) {
    const loginUserID = Number(this.authServ.getUserId());
    const autoBet = Number(member.auto_bet);
    const autoBetResult = Number(member.auto_bet_result);

    this.checkAgentIsOnline(member.agent_id).then(isOnline => {
      member['is_agent_online'] = isOnline;
      if (autoBet && isOnline) {
        return alert('代理在線無法修改');
      }
      this.memberServ.updateMemberAutoBet(member.id, loginUserID, autoBet, autoBetResult).subscribe(
        () => alert(this.translateService.instant('common.editSuccess')),
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
    });
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: Number(member.is_allow_play),
    };


    let parent = this.users.find(e => e.id === member.agent_id);
    if (data.is_allow_play && parent && !parent.is_allow_play) return alert('上級遊戲狀態已關閉無法修改');

    this.memberServ.updateMemberIsAllowPlay(data).subscribe(
      () => {
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  getToday() {
    let res = getToday();
    res.ed = res.st;
    this.setDate(res);
  }

  getYesterday() {
    let res = getYesterday();
    res.ed = res.st;
    this.setDate(res);
  }

  getThisWeek() {
    this.setDate(getThisWeek());
  }

  getLastWeek() {
    this.setDate(getLastWeek());
  }

  getThisMonth() {
    this.setDate(getThisMonth());
  }

  getLastMonth() {
    this.setDate(getLastMonth());
  }

  setDate(date: { st: string, ed: string }) {
    this.startDate = date.st;
    this.endDate = date.ed;
    this.onSearched();
  }

  onSearched() {
    this.loaded = false;
    this.dataSource.data = [];
    this.totalGameResult = 0;
    let data = this.changeLogData;

    if (this.startDate) {
      let t = new Date(this.startDate);
      t.setHours(0, 0, 0);
      data = data.filter(e => new Date(e.created_at).getTime() >= t.getTime());
    }

    if (this.endDate) {
      let t = new Date(this.endDate);
      t.setHours(0, 0, 0);
      t.setDate(t.getDate() + 1);
      data = data.filter(e => new Date(e.created_at).getTime() <= t.getTime());
    }

    this.mapData(this.members, data);
  }

  isAgentOnline(agentID: number) {
    let user = this.users.find(e => e.id === agentID);
    let parents: User[] = [];
    if (user) {
      parents.push(user);
      let parent = this.users.find(e => e.id === user.parent_id);
      while (parent && parent.identity == UserRole.agent) {
        parents.push(user);
        parent = this.users.find(e => e.id === parent.parent_id);
      }
    }
    return parents.some(e => e.identity == UserRole.agent && e.is_hello);
  }

  getMemberBetRecordsByChangeLog(memberID: number, data: ChangeLog[]) {
    let st = data[data.length - 1].created_at;
    let ed = data[0].created_at;
    let body = {
      game_types_id: GameTypeEnum.GameTypeDragonTiger,
      action: GameActionTypeEnum.Bet,
      start_date: st,
      end_date: ed,
      members_id: memberID,
      is_total: 1
    }
    return this.betRecordServ.getResultTotalAmountByDateAndMember(body);
  }

  mapMemberTotalAutoBetGameResultAmount(data: Member[]) {
    this.totalGameResult = 0;
    data.map(member => {
      let totalGameResult = 0;
      let totalGameCnt = 0;
      let logs: ChangeLog[] = member['logs'];
      let resultRecords: ResultRecord[] = member['game_result_records'];
      logs.forEach(e => {
        if (e.column_name == ChangeLogColumn.AutoBet && Number(e.new_value) == Status.InActive) {
          const res = this.getGameResultByChangeLog(resultRecords, logs, e);
          totalGameResult += res.result;
          totalGameCnt += res.number_of_game;
        }
      });
      member['total_auto_bet_result'] = totalGameResult;
      member['number_of_game'] = totalGameCnt;
      this.totalGameResult += totalGameResult;
    });
    this.dataSource.data = data.filter(e => e['total_auto_bet_result'] || e.auto_bet);
    this.loaded = true;
  }

  getGameResultByChangeLog(resultRecords: ResultRecord[], logs: ChangeLog[], target: ChangeLog) {
    if (!resultRecords.length || !logs.length) return { result: 0, number_of_game: 0 };
    let st = logs[logs.length - 1].created_at;
    let et = target.created_at;
    let idx = logs.findIndex(e => e.id === target.id);


    for (let i = 0; i < logs.length; i++) {
      const e = logs[i];
      if (i > idx && e.column_name == ChangeLogColumn.AutoBet) {
        switch (Number(e.new_value)) {
          // 找前一筆開啟VIP功能紀錄時間
          case Status.Active:
            st = e.created_at;
            break;

          // 連續關閉則返回0
          case Status.InActive:
            return { result: 0, number_of_game: 0 };
        }
        break;
      }
    }
    // let previous = logs.find((e, i) => i > idx && e.column_name == ChangeLogColumn.AutoBet && Number(e.new_value) == Status.Active);
    // previous && (st = previous.created_at);


    // VIP啟動區間內輸贏結果加總
    let gameResult = 0;
    let gameCnt = 0;
    let records = resultRecords.filter(e => {
      let t = new Date(e.created_at).getTime();
      return t >= new Date(st).getTime() && t <= new Date(et).getTime();
    });

    records.forEach(e => {
      gameResult += e.total_game_result_amount;
      gameCnt++;
    });


    return { result: gameResult, number_of_game: gameCnt };
  }

  checkAgentIsOnline(agentID: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.agentServ.getAgentParentsIdByAgentsId(agentID).toPromise().then(r => {
        let ids: number[] = r.data ? r.data : [];
        ids.push(agentID);
        let reqs = ids.map(e => this.userServ.getUser(e));
        if (ids.length) {
          forkJoin(reqs).toPromise().then(r => {
            let users: User[] = r.map(e => e.data);
            resolve(users.some(e => e.identity === UserRole.agent && e.is_hello));
          });
        } else {
          resolve(false);
        }
      })
    });
  }

  changeColor(val: number) { return changeColor(val) }

  get Status() { return Status }
}

interface ResultRecord {
  game_number: string;
  result: number;
  status: number;
  total_bet_amount: number;
  total_game_result_amount: number;
  created_at: string;
}
