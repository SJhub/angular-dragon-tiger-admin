import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VipRecordComponent } from './vip-record.component';

describe('VipRecordComponent', () => {
  let component: VipRecordComponent;
  let fixture: ComponentFixture<VipRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VipRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VipRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
