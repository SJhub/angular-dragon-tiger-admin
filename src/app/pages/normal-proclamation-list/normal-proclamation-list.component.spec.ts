import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalProclamationListComponent } from './normal-proclamation-list.component';

describe('NormalProclamationListComponent', () => {
  let component: NormalProclamationListComponent;
  let fixture: ComponentFixture<NormalProclamationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalProclamationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalProclamationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
