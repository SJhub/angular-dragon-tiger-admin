import { ProclamationTypesEnum } from './../../enum/proclamation-type.enum';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Proclamation } from '../../models/proclamation';
import { ProclamationService } from '../../services/proclamation.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Status } from '../../enum/status.enum';
import { ImgFullPathPipe } from '../../pipe/img-full-path.pipe';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';

@Component({
  selector: 'app-normal-proclamation-list',
  templateUrl: './normal-proclamation-list.component.html',
  styleUrls: ['./normal-proclamation-list.component.css']
})
export class NormalProclamationListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // displayedColumns: string[] = ['title', 'pic', 'type', 'status', 'created_at', 'updated_at', 'function'];
  displayedColumns: string[] = ['index', 'title', 'pic', 'status', 'created_at', 'updated_at', 'function'];
  dataSource = new MatTableDataSource<Proclamation>();

  public data: Proclamation[];
  public modalRef: BsModalRef;
  public modal: any;
  public avatarUrl: string;
  public pic: any;

  /**
   * proclamationTypes - 公告類型
   * const (
   *   ProclamationTypeGeneral = iota
   *   ProclamationTypeGame
   * )
   */
  proclamationTypes = ['一般', '遊戲'];
  defaultType = ProclamationTypesEnum.ProclamationTypeGeneral;

  constructor(
    private translateService: TranslateService,
    private proclamationService: ProclamationService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.getProclamations();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filterProclamations(data: any): any {
    return data.filter(it => it['type'] === this.defaultType);
  }

  getProclamations() {
    this.proclamationService.getProclamations()
      .subscribe(
        res => {
          this.data = this.filterProclamations(res.data);
          this.dataSource.data = addIndexToArray(this.data);
        },
        error => console.log(error)
      );
  }

  openCropper(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
  }

  getImageCropperUrl(e) {
    this.avatarUrl = e;
    // this.myForm.get('pic').setValue(e);
    this.modalRef.hide();
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.proclamationService.getProclamation(id)
      .subscribe(res => {
        this.modal = res.data as Proclamation;
        this.modalRef = this.modalService.show(template);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delProclamation(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.proclamationService.deleteProclamation(id)
        .subscribe(
          () => {
            this.getProclamations();
            alert(this.translateService.instant('common.deleteSuccess'));
          },
          err => {
            alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
          }
        );
    }
  }

  getStatusName(status: number): string {
    if (status === Status.InActive) {
      return this.translateService.instant('common.inActive');
    }

    return this.translateService.instant('common.active');
  }

  addProclamation(title: string) {
    const proclamation = {
      title: title,
      pic: new ImgFullPathPipe().transform(this.avatarUrl),
      status: Status.Active,
      type: this.defaultType,
    };
    // val.pic = new ImgFullPathPipe().transform(val.pic);

    this.proclamationService.addProclamation(proclamation)
      .subscribe(
        () => {
          this.getProclamations();
          alert(this.translateService.instant('common.addSuccess'));
        },
        err => {
          alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`);
        }
      );
  }

  get Status() { return Status }

}
