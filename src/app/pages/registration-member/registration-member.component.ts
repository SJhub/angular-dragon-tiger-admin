import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray, defaultAgentID } from '../../common/global';
import { Status } from '../../enum/status.enum';
import { Member } from '../../models/member.model';
import { MemberService } from '../../services/member.service';

const DEFAULT_AGENT_ID = defaultAgentID;

@Component({
  selector: 'app-registration-member',
  templateUrl: './registration-member.component.html',
  styleUrls: ['./registration-member.component.css']
})
export class RegistrationMemberComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource = new MatTableDataSource<Member>();
  displayedColumns: string[] = [
    'index',
    'account',
    'name',
    'nickname',
    'wallet',
    'wash_code_commission_percent',
    'is_online',
    'is_play_game',
    'is_allow_play',
    'status',
    'ip',
    'function'];

  constructor(
    private translateService: TranslateService,
    private memberServ: MemberService
  ) { }

  ngOnInit(): void {
    this.getMembersByParentID(DEFAULT_AGENT_ID);
  }

  getMembersByParentID(parentID: number) {
    this.memberServ.getMemberList().subscribe(r => {
      const members: Member[] = r.data;
      this.dataSource.data = members.filter(m => m.agent_id === parentID);
      this.dataSource.data = addIndexToArray(this.dataSource.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  changeMemberStatus(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      status: member.status ? 0 : 1
    };

    this.memberServ.updateMemberStatus(data).subscribe(
      () => {
        member.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberServ.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  deleteMember(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.memberServ.deleteGarbageMember(id)
        .subscribe(
          () => {
            alert(this.translateService.instant('common.deleteSuccess'));
            this.getMembersByParentID(DEFAULT_AGENT_ID);
          },
          err => alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`)
        );
    }
  }

  get Status() { return Status }

}
