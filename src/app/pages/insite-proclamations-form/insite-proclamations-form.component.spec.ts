import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsiteProclamationsFormComponent } from './insite-proclamations-form.component';

describe('InsiteProclamationsFormComponent', () => {
  let component: InsiteProclamationsFormComponent;
  let fixture: ComponentFixture<InsiteProclamationsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsiteProclamationsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsiteProclamationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
