import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { Proclamation } from '../../models/proclamation';
import { ProclamationService } from '../../services/proclamation.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UploadFileService } from '../../services/upload-file.service';
import { AuthenticationService } from '../../services/auth.service';
import { options } from '../../ckeditor/config';
import { ProclamationTypesEnum } from '../../enum/proclamation-type.enum';

@Component({
  selector: 'app-insite-proclamations-form',
  templateUrl: './insite-proclamations-form.component.html',
  styleUrls: ['./insite-proclamations-form.component.css']
})
export class InsiteProclamationsFormComponent implements OnInit {

  myForm: FormGroup;
  modalRef: BsModalRef;
  config = options;

  proclamation: Proclamation;

  id: number;
  avatarUrl: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private proclamationService: ProclamationService,
    private modalService: BsModalService,
    private uploadFileServ: UploadFileService,
    private authService: AuthenticationService,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm(this.id);
    }
  }

  groupForm() {
    this.myForm = this.fb.group({
      title: [null],
      body: [null],
      status: [1]
    })
  }

  setForm(id) {
    this.proclamationService.getProclamation(id).subscribe(
      res => {
        const data = res.data as Proclamation;
        console.log(data);
        this.myForm.patchValue(data);
      }
    );
  }

  submit(val) {
    val.id = this.id;
    val.status = Number(val.status);
    val.type = ProclamationTypesEnum.ProclamationTypeInSite;
    if (this.myForm.valid) {
      if (!this.id) {
        this.proclamationService.addProclamation(val).subscribe(
          () => {
            alert('新增成功');
            this.location.back();
          },
          err => {
            alert('新增失敗');
          }
        );
      } else {
        this.proclamationService.updateProclamation(val).subscribe(
          () => {
            alert('修改成功');
            this.location.back();
          },
          err => {
            alert('修改失敗');
          }
        );
      }
    }
  }

  openCropper(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
  }

  fileUploadRequest(e) {
    const file = e.data.requestData.upload.file as File;
    const fileName = this.uploadFileServ.getRandomFileName(file.name);
    const file$ = new File([file.slice()], fileName);
    e.data.requestData.upload;
    e.data.requestData.file = file$;
    e.data.requestData.token = this.authService.getToken();
    delete e.data.requestData.upload;
  }

  cancel() {
    this.location.back();
  }

}
