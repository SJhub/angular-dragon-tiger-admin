import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewDrawRecordComponent } from './renew-draw-record.component';

describe('RenewDrawRecordComponent', () => {
  let component: RenewDrawRecordComponent;
  let fixture: ComponentFixture<RenewDrawRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewDrawRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewDrawRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
