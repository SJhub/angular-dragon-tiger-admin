import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { GameTypeService } from '../../services/game-type.service';
import { GameType } from '../../models/game-type';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { PlayGame } from '../../models/play-game';
import { PlayGameService } from '../../services/play-game.service';
import { SystemService } from '../../services/system.service';
import { commonEnum } from '../../enum/common.enum';
import { Observable, timer } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-croupier-operation',
  templateUrl: './croupier-operation.component.html',
  styleUrls: ['./croupier-operation.component.scss']
})
export class CroupierOperationComponent implements OnInit {


  myForm: FormGroup;
  GameType: GameType[];
  gameModeType: number;
  gameTypesID: any;
  systemSetting: any;
  countDownTimer: Observable<number>;
  currentCountTime: number;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private gameTypeService: GameTypeService,
    private playGameService: PlayGameService,
    private systemService: SystemService,
  ) {
    this.groupForm();
  }


  ngOnInit(): void {
    this.gameTypeOnChange(1);
    this.getSystemSetting();
    this.gemeType();
  }

  getSystemSetting(id: number = 1) {
    this.systemService.getSystemSetting(id).subscribe(
      res => {
        const data = res.data;
        this.systemSetting = data;

        this.myForm.patchValue({
          wash_code_amount: data.wash_code_amount,
          draw_seconds: data.draw_seconds,
        });

        // console.log(data);
      }
    );
  }

  groupForm() {
    this.myForm = this.fb.group({
      'draw_result': [null, Validators.pattern(/^[0-6]{1,3}$/)],
      'draw_seconds': [null],
    });
  }

  /**
   * gameTypeOnChange - when select opetion change
   * @param gameTypesID
   */
  gemeType() {
    this.gameTypeService.getGameTypes().subscribe(
      res => {
        this.GameType = res.data;
        // console.log(this.GameType);
      }
    );
  }

  checkGameIsMarket(): boolean {
    if (this.gameTypesID > GameTypeEnum.GameTypeSicBo) {
      return false;
    }

    return true;
  }

  newGame(val: any) {

    if (this.gameModeType === 1) {
      alert(this.translateService.instant('currentModeIsAuto'));
      return;
    }

    if (!this.checkGameIsMarket()) {
      alert(this.translateService.instant('gameIsNotToMarket'));
      return;
    }

    if (this.myForm.valid) {

      const playGame = {
        game_types_id: Number(this.gameTypesID),
        countdown: Number(val.draw_seconds)
      } as PlayGame;

      this.playGameService.addPlayGame(playGame)
        .subscribe(
          () => {
            this.startCountDown(2300);
            // alert('新增成功');
          },
          err => alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`)
        );
    }
  }

  /**
   * 顯示開獎倒數秒數
   */
  startCountDown(delay: number) {
    const draw_seconds = this.myForm.get('draw_seconds').value;
    this.countDownTimer = timer(delay, 1000).pipe(
      take(draw_seconds),
      map(s => draw_seconds - s - 1),
      tap(s => this.currentCountTime = s));
  }

  /**
   * invalidDrawResult - 此局無效
   * @param val
   */
  invalidDrawResult() {

    if (this.gameModeType === 1) {
      alert(this.translateService.instant('currentModeIsAuto'));
      return;
    }

    if (!this.checkGameIsMarket()) {
      alert(this.translateService.instant('gameIsNotToMarket'));
      return;
    }

    if (this.myForm.valid) {
      this.systemService.updateDrawResult(this.systemSetting.id, commonEnum.INVALID_RESULT)
        .subscribe(
          () => alert('送出成功'),
          err => alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`)
        );
    }
  }

  /**
  * updateDrawResult - 更新開獎結果
  * @param val
  */
  updateDrawResult(val: any) {

    if (this.gameModeType === 1) {
      alert(this.translateService.instant('currentModeIsAuto'));
      return;
    }

    if (val.draw_result.length < 3 || val.draw_result.length > 3) {
      alert(this.translateService.instant('pleaseEnter3Numbers'));
      return;
    }

    if (val.draw_result.includes('7') || val.draw_result.includes('8') || val.draw_result.includes('9')) {
      alert(this.translateService.instant('pleaseEnterANumberBelow6'));
      return;
    }

    if (!this.checkGameIsMarket()) {
      alert(this.translateService.instant('gameIsNotToMarket'));
      return;
    }

    if (this.myForm.valid) {

      this.systemService.updateDrawResult(this.systemSetting.id, val.draw_result)
        .subscribe(
          () => {
            alert(this.translateService.instant('common.submitSuccess'));
            this.myForm.get('draw_result').setValue('');
          },
          err => alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`)
        );
    }
  }

  gameTypeOnChange(gameType) {
    this.gameTypesID = gameType;
    // console.log(gameType);
  }

  checkNumber(e: any, formValue: string) {
    const key = e.key
    const regxSingle = /[0-6]/;
    const regxResult = /^[1-6]{1,3}$|^0{1,3}$/;


    if (!regxSingle.test(key)) {
      e.preventDefault();
    }

    if (formValue && !regxResult.test(formValue + key)) {
      e.preventDefault();
    }
  }
}
