import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CroupierOperationComponent } from './croupier-operation.component';

describe('CroupierOperationComponent', () => {
  let component: CroupierOperationComponent;
  let fixture: ComponentFixture<CroupierOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CroupierOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CroupierOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
