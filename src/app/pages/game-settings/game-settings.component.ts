import { GameType } from './../../models/game-type';
import { ItemType } from './../../models/item-type';
import { LimitType } from './../../models/limit-type';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GameSetting } from '../../models/game-setting';
import { GameSettingService } from '../../services/game-setting.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-game-settings',
  templateUrl: './game-settings.component.html',
  styleUrls: ['./game-settings.component.css']
})
export class GameSettingsComponent implements OnInit {

  gameSetting: GameSetting;
  myForm: FormGroup;
  private id: number;
  data: any;

  constructor(
    private gameSettingService: GameSettingService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private location: Location,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm();
    }
  }

  groupForm() {
    this.myForm = this.fb.group({
      'game_type': [{value: null, disabled: true}],
      'item_type': [{value: null, disabled: true}],
      'limit_type': [{value: null, disabled: true}],
      'amount': [null]
    });
  }

  setForm() {
    this.gameSettingService.getGameSetting(this.id).subscribe(
      res => {
        const data = res.data;

        this.myForm.patchValue({
          game_type: this.getGameTypeByID(data.game_type),
          item_type: this.getItemTypeByID(data.item_type),
          limit_type: this.getLimitTypeByID(data.limit_type),
          amount: data.amount,
        });
      }
    );
  }

  submit(val: any) {
    if (this.myForm.valid) {
      val.amount = Number(val.amount);
      val.id = this.id;

      this.gameSettingService.updateGameSetting(val).subscribe(
        () => {
          this.location.back();
          alert('修改成功');
        },
        err => {
          alert(`操作失敗: ${err.error.message}`);
        }
      );
    }
  }

  cancel() {
      this.location.back();
  }

  getGameTypeByID(id: number) {
    return GameType.getNameByID(id);
  }

  getItemTypeByID(id: number) {
    return ItemType.getNameByID(id);
  }

  getLimitTypeByID(id: number) {
    return LimitType.getNameByID(id);
  }
}
