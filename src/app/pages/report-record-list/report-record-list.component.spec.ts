import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRecordListComponent } from './report-record-list.component';

describe('ReportRecordListComponent', () => {
  let component: ReportRecordListComponent;
  let fixture: ComponentFixture<ReportRecordListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportRecordListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRecordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
