import { UserRole } from './../../enum/user-role.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Status } from '../../enum/status.enum';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { GameTypeService } from '../../services/game-type.service';
import { GameType } from '../../models/game-type';
import { AgentService } from '../../services/agent.service';
import { MemberService } from '../../services/member.service';
import { CurrentReportsSicboService } from '../../services/current-reports-sicbo.service';
import { TranslateService } from '@ngx-translate/core';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { Member } from '../../models/member.model';
import { BetRecordService } from '../../services/bet-record.service';
import { BetRecord } from '../../models/bet-record';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { CalcItemResultRecord } from '../../models/calc-item-result-record';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SettlementRecordsService } from '../../services/settlement-records.service';
import { SettlementRecord } from '../../models/settlement-record';
import { ItemTypesService } from '../../services/item-types.service';
import { ItemTypes } from '../../models/item-types';
import { AdminOwnRecord, CalcOwnRecord, getLastCalcDay, isDateRangeIncludedToday, isMonthFirstDayBeforeCalc, isWeekFirstDayBeforeCalc, MemberJoinRecords, UsersJoinRecordWithSettlementCalc } from '../../common/report';
import { ReportByDateRangeComponent } from '../../component/report-by-date-range/report-by-date-range.component';
import { ReportByDateRangeMemberComponent } from '../../component/report-by-date-range-member/report-by-date-range-member.component';
import { AuthenticationService } from '../../services/auth.service';
import { defaultAgentID } from '../../common/global';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { GameTypeEnum } from '../../enum/game-type.enum';

const DEFAULT_AGENT_ID = defaultAgentID;
@Component({
  selector: 'app-report-record-list',
  templateUrl: './report-record-list.component.html',
  styleUrls: ['./report-record-list.component.css']
})
export class ReportRecordListComponent implements OnInit {
  @ViewChild('sortByAgent', { static: true }) sortByAgent: MatSort;
  @ViewChild('paginatorOfAgent', { static: true }) paginatorOfAgent: MatPaginator;

  @ViewChild('sortByMember', { static: true }) sortByMember: MatSort;
  @ViewChild('paginatorOfMember', { static: true }) paginatorOfMember: MatPaginator;


  displayedColumns: string[] = ['name', 'account', 'total_amount', 'number_of_game', 'game_result_amount',
    'revenue_share', 'settlement_amount', 'is_allow_play', 'status', 'function'];

  displayedColumnsByMember: string[] = ['name', 'members_id', 'account', 'total_amount', 'number_of_game',
    'game_result_amount', 'settlement_amount', 'is_allow_play', 'status', 'records'];

  dataSourceOfAgent = new MatTableDataSource<User>();
  dataSourceOfMember = new MatTableDataSource<Member>();
  dataSourceOfBetRecord = new MatTableDataSource<BetRecord>();

  Member: Member[];
  User: User[];
  GameType: GameType[];
  ItemTypes: ItemTypes[];
  currentUser: User;

  modalRef: BsModalRef;

  startDateValue: any;
  endDateValue: any;

  gameTypeID = GameTypeEnum.GameTypeSicBo;

  agentTotalAmount: any;
  agentActiveTotalAmount: any;
  agentNumberOfGame: any;
  agentGameResultAmount: any;
  agentWashCodeCommission: any;
  agentWashCodeCommissionPercent: any;
  agentRevenueShare: any;
  agentTotalDonate: any;
  agentTotalSettlementAmount: any;
  agentSettlementAmount: any;
  memberOfSettlementAmount: any;
  theTopSettlementAmount: any;
  selfProfit: any;

  memberTotalAmount: number;
  memberActiveTotalAmount: number;
  memberGameNumber: number;
  memberGameResultAmount: number;
  memberWashCodeCommissionPercent: number;
  memberWashCodeCommission: number;
  memberTotalDonate: any;
  memberSettlementAmount: any;

  totalAmount: any;
  activeTotalAmount: any;
  totalGameNumber: any;
  totalGameResultAmount: any;
  totalWashCodeCommission: any;
  totalWashCodeCommissionPercent: any;
  totalRevenueShare: any;
  totalDonate: any;
  totalSettlementAmount: any;
  totalMemberSettlementAmount = 0;
  totalAgentSettlementAmount = 0;
  totalTopSettlementAmount: any;
  totalSelfProfit: any;
  children: any;
  childrenMember: any = [];
  allAgents: User[];

  loginUserName: string;
  loginUserId: number;

  dataAgent = false;

  agentStatus: any;
  memberStatus: any;
  identity: number;
  dataOfResultAmountTotal: any;
  CalcItemResultRecordByDate: any;
  active = false;
  yes: any;
  newday: any;
  loopCircle = false;


  now = new Date();
  nowDayOfWeek = this.now.getDay() - 1;
  nowDay = this.now.getDate();
  nowMonth = this.now.getMonth();
  nowYear = this.now.getFullYear();

  settlementRecords: SettlementRecord[];
  realTimeRecords: any;

  isTopAgentClicked = false;

  isAdminClone = false;

  submitted = false;


  constructor(
    private settlementRecordServ: SettlementRecordsService,
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private userService: UserService,
    private agentService: AgentService,
    private gameTypeService: GameTypeService,
    private memberService: MemberService,
    private currentReportsSicboService: CurrentReportsSicboService,
    private betRecordService: BetRecordService,
    private calcItemResultRecordService: CalcItemResultRecordService,
    private modalService: BsModalService,
    private itemTypesService: ItemTypesService,
    private authServ: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    const gameTypeID = this.route.snapshot.queryParamMap.get('g');
    gameTypeID && this.selectGameType(Number(gameTypeID), false);
  }

  ngOnInit(): void {
    this.loginUserName = this.authServ.getLoggedInUserName();
    this.loginUserId = Number(localStorage.getItem('authenticatedID'));
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.isAdminClone = this.identity === UserRole.admin && Number(this.authServ.getUserCloneId()) !== 0;
    // 分身帳號使用本尊ID進行查詢
    if (!this.isAdminClone && this.authServ.getUserCloneId()) {
      this.loginUserId = Number(this.authServ.getUserCloneId());
    }

    this.getUserByParentID(this.loginUserId);
    this.getMemberByAgentID(this.loginUserId);
    this.getGameTypeList();
    this.getSettlementRecords();
    this.getItemTypesList();



    this.dataSourceOfAgent.paginator = this.paginatorOfAgent;
    this.dataSourceOfAgent.sort = this.sortByAgent;
    this.dataSourceOfMember.paginator = this.paginatorOfMember;
    this.dataSourceOfMember.sort = this.sortByMember;
  }

  getUserByParentID(id: number) {
    let req = this.userService.getUserByParentID(id);
    if (this.isRoleAdmin()) {
      req = this.userService.getUsers();
    }
    req.subscribe(res => {
      const users = res.data as User[];
      this.children = users;
      if (this.identity === UserRole.admin) {
        return this.allAgents = users.filter(e => e.id != DEFAULT_AGENT_ID && e.identity === UserRole.topAgent && !e.clone_id);
      }
      this.allAgents = users.filter(e => !e.clone_id);
    });

    this.userService.getUser(id).subscribe(r => this.currentUser = r.data);
  }

  findRecordByUserID(records: User[], id: number) {
    return records.filter(e => e.agents_id === id);
  }

  getAgentStatusByID(id: number) {
    if (!this.children) {
      return;
    }
    const agent = this.children.find(e => e.id === id);
    if (agent) {
      return agent.status;
    }
  }

  getMemberByAgentID(id: number) {
    if (!this.isRoleAdmin()) {
      let req = this.memberService.getMemberByAgent(id);
      req.subscribe(
        res => {
          const members: Member[] = res.data;
          this.childrenMember = members.filter(e => e.agent_id !== DEFAULT_AGENT_ID);
        });
    }
  }

  getMemberStatusByID(members_id: number) {
    if (!this.childrenMember) {
      return;
    }
    const member = this.childrenMember.find(e => e.id === members_id);
    if (member) {
      return member.status;
    }
  }

  /**
   * 以上級代理ID 及開始及結束日期搜尋代理日報表
   */
  getAgentDailyReportsSearchByTypeAndParentBetweenDate(body) {
    this.submitted = true;
    this.calcOwnProfit();

    let req = [this.agentService.getAgentDailyReportsSearchByTypeAndParentBetweenDate(body)];

    if (this.isRoleAdmin()) {
      req = [this.agentService.getAgentDailyReportsSearchByTypeAndAgentBetweenDate(body)];
    }

    if (isDateRangeIncludedToday(this.startDateValue, this.endDateValue)) {
      req.push(this.getCurrentReportRequest(this.gameTypeID));
    }

    forkJoin(req).pipe(map(res => res.map(r => r.data))).subscribe(
      res => {
        let dataAgent: User[] = res[0];

        this.dataAgent = true;
        this.loopCircle = true;

        dataAgent = dataAgent.filter(e => e.is_top);

        // 即時報表
        if (res[1]) {
          this.realTimeRecords = res[1];
          let agentRecords: User[] = this.realTimeRecords.agent_daily_report;

          if (!this.isRoleAdmin()) {
            agentRecords = agentRecords.filter(e => e.parent_id === this.loginUserId);
          }

          dataAgent.push(...agentRecords.filter(e => e.is_top));
        }

        this.dataSourceOfAgent.data = UsersJoinRecordWithSettlementCalc(
          dataAgent,
          this.allAgents,
          this.settlementRecords,
          this.startDateValue,
          this.endDateValue
        );
        // 以代理ID及開始及結束日期搜尋會員日報表
        const memberReportByAgentID = {
          game_types_id: this.gameTypeID,
          start_date: this.startDateValue,
          end_date: this.endDateValue,
          agents_id: this.loginUserId,
        };
        this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
      }
    );
  }

  /**
   * 以開始及結束日期搜尋代理日報表
   */
  getAgentDailyReportsSearchByTypeAndAgentBetweenDate(data) {
    this.agentService.getAgentDailyReportsSearchByTypeAndAgentBetweenDate(data).subscribe(
      res => {

        const dataAgent = res.data;
        this.dataAgent = true;
        this.dataSourceOfAgent.data = dataAgent;
        this.loopCircle = true;
      }
    );
  }

  /**
   * 以代理ID及開始及結束日期搜尋會員日報表
   */
  getMemberDailyReportsSearchByTypeAndAgentBetweenDate(body) {
    let req = this.memberService.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(body);
    if (this.isRoleAdmin()) {
      body.agents_id = 0;
      req = this.memberService.getMemberDailyReportsSearchByTypeAndMemberBetweenDate(body);
    }

    req.subscribe(
      res => {
        const data = res.data;

        if (isDateRangeIncludedToday(this.startDateValue, this.endDateValue) && this.realTimeRecords) {
          data.push(...this.realTimeRecords.member_daily_report);
        }

        this.dataSourceOfMember.data = MemberJoinRecords(data, this.childrenMember);
        this.calcAgentAndMemberSettlementAmount();

        this.submitted = false;
      }
    );
  }

  /**
   * 取得遊戲項目列表
   */
  getItemTypesList() {
    this.itemTypesService.getItemTypesList().subscribe(
      res => {
        this.ItemTypes = res.data;
        // console.log(this.ItemTypes);
      }
    );
  }

  /**
   * 取得項目類型中文名稱 By item_type_id
   */
  getItemTypeChtName(item_types_id: number): string {
    const chtName = this.ItemTypes.find(e => e.id === item_types_id);
    if (chtName) { return chtName.cht_name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得開獎結果 By startDate and endDate
   */
  getCalcItemResultRecordByTypeBetweenDate(template: TemplateRef<any>, id: number, startDate: string, endDate: string) {
    this.calcItemResultRecordService.getCalcItemResultRecordByTypeBetweenDate(id, startDate, endDate).subscribe(
      res => {
        const data = res.data as CalcItemResultRecord[];
        this.CalcItemResultRecordByDate = data;
        this.modalRef = this.modalService.show(template, { class: 'modal-xl' });
      }
    );
  }

  openModal(template: TemplateRef<any>, member_id: number) {
    let start = this.yes;
    let end = this.newday;
    start += ' 12:01:00';
    end += ' 12:00:00';
    const data = {
      game_types_id: 1,
      start_date: start,
      end_date: end,
      members_id: member_id,
      is_total: 0,
    };
    this.betRecordService.getResultTotalAmountByDateAndMember(data).subscribe(
      res => {
        const data = res.data;
        this.dataSourceOfBetRecord.data = data;
        this.dataOfResultAmountTotal = this.dataSourceOfBetRecord.data;
        let start = this.yes;
        let end = this.newday;
        start += ' 12:01:00';
        end += ' 12:00:00';
        this.getCalcItemResultRecordByTypeBetweenDate(template, this.gameTypeID, start, end);
      }
    );
  }

  getTopSettlementAmount(betamount, game_result_amount) {
    if (betamount && game_result_amount) {
      return (betamount + game_result_amount).toFixed(2);
    }
    return '';
  }

  /**
   * 取得會員列表
   */
  getMemberList() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.Member = res.data;
      }
    );
  }

  /**
   * 取得會員姓名
   */
  getMemberName(members_id: number): string {
    const memberName = this.childrenMember.find(e => e.id === members_id);
    if (memberName) { return memberName.nickname; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得遊戲類型列表
   */
  getGameTypeList() {
    this.gameTypeService.getGameTypes().subscribe(
      res => {
        this.GameType = res.data;
      }
    );
  }

  /**
   * 遊戲類型下拉選單
   */
  selectGameType(gameTypeID: number, submit = true) {
    if (this.gameTypeID === GameTypeEnum.GameTypeRoulette) {
      return alert(this.translateService.instant('gameIsNotToMarket'));
    }

    this.gameTypeID = Number(gameTypeID);
    this.router.navigate([], {
      queryParams: { g: this.gameTypeID },
      queryParamsHandling: 'merge'
    });
    submit && this.btnOK();
  }

  /**
   * 更改按鈕class顏色
   * @param is_allow_play
   */
  changeBtnColor(is_allow_play) {
    switch (is_allow_play) {
      case 0:
        return 'btn-success';
      case 1:
        return 'btn-danger';
    }
  }

  today() {
    this.submitted = true;
    this.agentWashCodeCommissionPercent = 0;
    this.agentRevenueShare = 0;

    const lastCalcDay = new Date(getLastCalcDay());
    const today = formatDate(lastCalcDay.setDate(lastCalcDay.getDate() + 1), 'yyyy-MM-dd', 'en-US');
    this.startDateValue = today;
    this.endDateValue = today;
    this.dataAgent = true;
    this.active = true;

    const day = new Date();
    let yes = formatDate(day.setDate(day.getDate() - 2), 'yyyy-MM-dd', 'en-US');
    let newday = formatDate(day.setDate(day.getDate() + 1), 'yyyy-MM-dd', 'en-US');

    this.yes = yes;
    this.newday = newday;
    this.calcOwnProfit();

    let currentAPI = this.getCurrentReportRequest(this.gameTypeID);

    currentAPI.subscribe(
      res => {
        let data = res.data.agent_daily_report;
        let dataMember = res.data.member_daily_report;
        this.realTimeRecords = res.data;

        if (!this.isRoleAdmin()) {
          data = data.filter(e => e.parent_id === this.loginUserId);
        }

        data = data.filter(e => e.is_top);

        this.dataSourceOfAgent.data = UsersJoinRecordWithSettlementCalc(
          data,
          this.allAgents,
          this.settlementRecords,
          this.startDateValue,
          this.endDateValue
        );
        this.dataSourceOfMember.data = MemberJoinRecords(dataMember, this.childrenMember);
        this.calcAgentAndMemberSettlementAmount();
        this.submitted = false;
      }
    );
  }

  yesterday() {
    const yesterday = getLastCalcDay();

    this.startDateValue = yesterday;
    this.endDateValue = yesterday;
    this.dataAgent = false;
    this.active = false;
    this.yes = yesterday;
    this.newday = yesterday;

    let data = {
      game_types_id: this.gameTypeID,
      start_date: yesterday,
      end_date: yesterday,
      parent_id: this.loginUserId,
    } as any;

    let admin = {
      game_types_id: this.gameTypeID,
      start_date: yesterday,
      end_date: yesterday,
    } as any;

    if (this.isRoleAdmin()) {
      data = admin;
    }
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(data);
  }

  week() {
    let offset = 0;
    if (isWeekFirstDayBeforeCalc()) {
      offset = 7;
    }
    console.log(offset);

    const today = new Date();
    const dayOfWeek = today.getDay();
    const weekStart = formatDate(today.setDate(today.getDate() - dayOfWeek - offset), 'yyyy-MM-dd', 'en-US');
    const weekEnd = formatDate(today.setDate(today.getDate() + 6), 'yyyy-MM-dd', 'en-US');
    this.startDateValue = weekStart;
    this.endDateValue = weekEnd;
    this.yes = weekStart;
    this.newday = weekEnd;
    this.dataAgent = false;
    this.active = false;

    let adminData = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      // agents_id: this.loginUserId,
    };

    let data = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.loginUserId,
    } as any;

    if (this.isRoleAdmin()) {
      data = adminData;
    }
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(data);
  }

  lastWeek() {
    let offset = 0;
    if (isWeekFirstDayBeforeCalc()) {
      offset = 7;
    }
    const weekStartDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 8 - offset), 'yyyy-MM-dd', 'en-US');
    const weekEndDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 2 - offset), 'yyyy-MM-dd', 'en-US');
    this.startDateValue = weekStartDate;
    this.endDateValue = weekEndDate;
    this.dataAgent = false;
    this.active = false;

    const StartDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 8 - offset), 'yyyy-MM-dd', 'en-US');
    const EndDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 2 - offset), 'yyyy-MM-dd', 'en-US');
    this.yes = StartDate;
    this.newday = EndDate;

    let admin = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      // parent_id: this.loginUserId,
    } as any;

    let data = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.loginUserId,
    } as any;

    if (this.isRoleAdmin()) {
      data = admin;
    }
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(data);
  }

  month() {
    let offset = 0;
    if (isMonthFirstDayBeforeCalc()) {
      offset = -1;
    }
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month + offset, 1);
    const lastDay = new Date(year, month + offset + 1, 0);
    const formatFirstDay = formatDate(firstDay, 'yyyy-MM-dd', 'en-US');
    const formatLastDay = formatDate(lastDay, 'yyyy-MM-dd', 'en-US');
    this.startDateValue = formatFirstDay;
    this.endDateValue = formatLastDay;
    this.dataAgent = false;
    this.active = false;

    const day = new Date();
    const Year = day.getFullYear();
    const Month = day.getMonth();
    const FirstDay = new Date(year, month, 1);
    const LastDay = new Date(year, month + 1, 0);
    const FormatFirstDay = formatDate(FirstDay, 'yyyy-MM-dd', 'en-US');
    const FormatLastDay = formatDate(LastDay, 'yyyy-MM-dd', 'en-US');
    this.yes = FormatFirstDay;
    this.newday = FormatLastDay;

    let data = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.loginUserId,
    } as any;

    let admin = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      // agents_id: this.loginUserId,
    } as any;

    if (this.isRoleAdmin()) {
      data = admin;
    }
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(data);
  }

  lastMonth() {
    let offset = 0;
    if (isMonthFirstDayBeforeCalc()) {
      offset = -1;
    }
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month + offset - 1, 1);
    const lastDay = new Date(year, month + offset, 0);

    const formatFirstDay = formatDate(firstDay, 'yyyy-MM-dd', 'en-US');
    const formatLastDay = formatDate(lastDay, 'yyyy-MM-dd', 'en-US');
    this.startDateValue = formatFirstDay;
    this.endDateValue = formatLastDay;
    this.dataAgent = false;
    this.active = false;

    const day = new Date();
    const Year = day.getFullYear();
    const Month = day.getMonth();
    const FirstDay = new Date(Year, Month - 1, 1);
    const LastDay = new Date(Year, Month, 0);
    const FormatFirstDay = formatDate(FirstDay, 'yyyy-MM-dd', 'en-US');
    const FormatLastDay = formatDate(LastDay, 'yyyy-MM-dd', 'en-US');
    this.yes = FormatFirstDay;
    this.newday = FormatLastDay;

    let data = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.loginUserId,
    } as any;

    let admin = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      // agents_id: this.loginUserId,
    } as any;

    if (this.isRoleAdmin()) {
      data = admin;
    }
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(data);
  }

  // 更新代理帳號狀態功能
  changeStatus(agents_id: number, event: any) {
    const data = {
      id: agents_id,
      status: event ? 1 : 0,
    };
    this.agentService.updateAgentStatus(data).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
      },
      err => {
        alert(this.translateService.instant('common.updateFailed'));
      }
    );
  }

  changeAgentStatus(user: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: user.id,
      status: user.status ? 0 : 1,
    };

    this.agentService.updateAgentStatus(data).subscribe(
      () => {
        user.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }


  changeMemberStatus(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      status: member.status ? 0 : 1
    };

    this.memberService.updateMemberStatus(data).subscribe(
      () => {
        member.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeAgentAllowGame(agent: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: agent.id,
      is_allow_play: agent.is_allow_play ? 0 : 1,
    };

    this.agentService.updateAgentIsAllowPlay(data).subscribe(
      () => {
        agent.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  btnOK() {
    if (!this.startDateValue) {
      return alert(this.translateService.instant('manageAgent.inputStartDate'));
    }
    if (!this.endDateValue) {
      return alert(this.translateService.instant('manageAgent.inputEndDate'));
    }
    if (this.endDateValue < this.startDateValue) {
      return alert(this.translateService.instant('manageAgent.inputCorrectDate'));
    }
    this.dataAgent = false;
    this.active = false;

    let data = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.loginUserId,
    } as any;

    let admin = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      // agents_id: this.loginUserId,
    } as any;

    if (this.isRoleAdmin()) {
      data = admin;
    }
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(data);
  }

  getSettlementRecords() {
    this.settlementRecordServ.getSettlementRecords().subscribe(r => {
      this.settlementRecords = r.data;
      this.today();
    });
  }

  calcOwnProfit() {
    return this.currentUser?.settlement_amount - (this.totalMemberSettlementAmount + this.totalAgentSettlementAmount);
  }

  calcOwnRecord(agentRecords: any, memberRecords: any) {
    this.currentUser = CalcOwnRecord(this.currentUser, agentRecords, memberRecords);
  }

  calcAgentAndMemberSettlementAmount() {
    this.totalAgentSettlementAmount = 0;
    this.totalMemberSettlementAmount = 0;
    const agentRecords = this.dataSourceOfAgent.data;
    const memberRecords = this.dataSourceOfMember.data;

    agentRecords.forEach(e => this.totalAgentSettlementAmount += e.settlement_amount);
    memberRecords.forEach(e => this.totalMemberSettlementAmount += e.settlement_amount);

    if (this.currentUser.identity === UserRole.admin) {
      return this.currentUser = AdminOwnRecord(this.currentUser, this.dataSourceOfAgent.data);
    }

    this.calcOwnRecord(agentRecords, memberRecords);
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  isRoleAdmin() {
    return this.identity === UserRole.admin;
  }

  openReocrdModal(user: User) {
    this.modalService.show(ReportByDateRangeComponent, {
      initialState: {
        user: user,
        startDate: this.startDateValue,
        endDate: this.endDateValue,
        settlementRecords: this.settlementRecords,
        realTimeRecords: this.realTimeRecords
      }
      , class: 'modal-xl'
    });
  }

  openMemberReocrdModal(member: Member) {
    this.modalService.show(ReportByDateRangeMemberComponent, {
      initialState: {
        member: member,
        startDate: this.startDateValue,
        endDate: this.endDateValue,
        realTimeRecords: this.realTimeRecords
      }
      , class: 'modal-xl'
    });
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }

  isTopAgentSearched(e: boolean) {
    this.isTopAgentClicked = e;
  }

  getCurrentReportRequest(gameTypeID: number) {
    switch (gameTypeID) {
      case GameTypeEnum.GameTypeSicBo:
        return this.currentReportsSicboService.getcurrentReportsSicbo();
      case GameTypeEnum.GameTypeBaccarat:
        return this.currentReportsSicboService.getcurrentReportsBaccarat();
    }
  }

  get Status() { return Status; }

  get UserRole() { return UserRole }
}
