import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentAuthorityComponent } from './agent-authority.component';

describe('AgentAuthorityComponent', () => {
  let component: AgentAuthorityComponent;
  let fixture: ComponentFixture<AgentAuthorityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentAuthorityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
