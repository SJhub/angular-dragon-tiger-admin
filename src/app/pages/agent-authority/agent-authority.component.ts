import { Status } from './../../enum/status.enum';
import { UserService } from './../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../models/user';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EnumValueToEnumKeyPipe } from '../../pipe/enum-value-to-enum-key.pipe';
import { UserRole } from '../../enum/user-role.enum';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { MemberService } from '../../services/member.service';
import { GameTester } from '../../models/game-tester';
import { addIndexToArray } from '../../common/global';
import { StreamersMemberService } from '../../services/streamers-member.service';

const BLOCKED_ADMIN_ID = [1, 2];
@Component({
  selector: 'app-agent-authority',
  templateUrl: './agent-authority.component.html',
  styleUrls: ['./agent-authority.component.css']
})
export class AgentAuthorityComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['index', 'name', 'real_name', 'identity', 'status',
    'created_at', 'function'];
  dataSource = new MatTableDataSource<any>();

  users: User[];
  gameTesters: GameTester[];
  selectedRole = UserRole.admin;

  constructor(
    private translateService: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private userService: UserService,
    private memberService: MemberService,
    private streamerService: StreamersMemberService
  ) { }

  ngOnInit(): void {
    this.getUserList();
    this.getGameTesters();

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getGameTesters() {
    this.memberService.getGameTesters().subscribe(r => {
      this.gameTesters = r.data;
      if (this.selectedRole === UserRole.gametester) {
        this.setGameTestData();
      }
    });
  }

  /**
   * 取得用戶列表 - 篩選最高權限跟管理者
   */
  getUserList() {
    this.userService.getUsers().subscribe(
      res => {
        const data = res.data as User[];
        this.users = data;
        if (this.selectedRole !== UserRole.gametester) {
          this.onRoleSelect(String(this.selectedRole));
        }
      }
    );
  }

  deleteUser(userId: number, user: User) {

    if (BLOCKED_ADMIN_ID.includes(userId)) {
      return alert('預設帳號無法刪除!!');
    }

    console.log(user);


    if (confirm('確定要刪除嗎?')) {
      let req = this.userService.deleteUser(userId);

      if (user.identity === UserRole.gametester) {
        req = this.memberService.deleteMember(userId);
      }

      if (user.identity === UserRole.streamer) {
        req = this.streamerService.deleteStreamer(user.name);
      }

      req.subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          this.getUserList();
          this.getGameTesters();
        },
        err => alert(this.translateService.instant('common.deleteFailed'))
      );
    }
  }

  /**
   * 身分轉換
   * @param loginUserRole
   */
  getReceiptType(loginUserRole) {
    return new EnumValueToEnumKeyPipe().transform(loginUserRole, UserRole);
  }

  getTranslatedReceiptType(user: User) {
    if (user.identity === UserRole.admin && user.clone_id) {
      return this.translateService.instant('system.managementDemo');
    }
    return this.enumTranslateServ.getTranslatedReceiptType(user.identity);
  }

  get Status() {
    return Status;
  }

  get UserRole() {
    return UserRole;
  }

  /**
   * 搜尋
   * @param event
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onRoleSelect(val: string) {
    const identity = Number(val);
    if (identity === UserRole.gametester) {
      return this.setGameTestData();
    }

    let users = this.users.filter(e => e.identity === identity && !e.clone_id);
    if (identity === UserRole.managementDemo) {
      users = this.users.filter(e => e.identity === UserRole.admin && e.clone_id);
    }
    this.dataSource.data = addIndexToArray(users);
  }

  setGameTestData() {
    this.dataSource.data = addIndexToArray(this.gameTesters);
    this.dataSource.data.map(e => e.identity = UserRole.gametester);
  }

  onStatusChanged(user: User) {
    user.status = Number(user.status);
    this.userService.updateUserStatus(user).subscribe(
      () => alert(this.translateService.instant('common.editSuccess')),
      () => alert(this.translateService.instant('common.editFailed'))
    );
  }
}
