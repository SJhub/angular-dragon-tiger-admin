import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageBlacklistEditComponent } from './message-blacklist-edit.component';

describe('MessageBlacklistEditComponent', () => {
  let component: MessageBlacklistEditComponent;
  let fixture: ComponentFixture<MessageBlacklistEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageBlacklistEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageBlacklistEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
