import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MessageBlacklistService } from '../../services/message-blacklist.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MessageBlacklist } from '../../models/message-blacklist';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-message-blacklist-edit',
  templateUrl: './message-blacklist-edit.component.html',
  styleUrls: ['./message-blacklist-edit.component.css']
})
export class MessageBlacklistEditComponent implements OnInit {

  messageBlacklist: MessageBlacklist;
  myForm: FormGroup;
  private id: number;
  data: any;

  constructor(
    private translateService: TranslateService,
    private messageBlacklistService: MessageBlacklistService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private location: Location,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm();
    }
  }

  groupForm() {
    this.myForm = this.fb.group({
      'message': ['']
    });
  }

  setForm() {
    this.messageBlacklistService.getMessageBlacklist(this.id)
      .subscribe(
        res => {
          const data = res.data;
          this.myForm.patchValue({
            message: data.message,
          });
        }
      );
  }

  submit(val: any) {
    if (this.myForm.valid) {

      val.id = this.id;
      this.messageBlacklistService.updateMessageBlacklist(val)
        .subscribe(
          () => {
            this.location.back();
            alert(this.translateService.instant('common.editSuccess'));
          },
          err => {
            alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
          }
        );
    }
  }

  cancel() {
    this.location.back();
  }
}
