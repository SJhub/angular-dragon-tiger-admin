import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { switchMap } from 'rxjs/operators';
import { addIndexToArray } from '../../common/global';
import { Status } from '../../enum/status.enum';
import { AutoDonateSetting } from '../../models/auto-donate-setting';
import { BotMembers } from '../../models/bot-member';
import { GameType } from '../../models/game-type';
import { AutoDonateSettingsService } from '../../services/auto-donate-settings.service';
import { BotMembersService } from '../../services/bot-members.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';

const MAX_SETTING_LIMITED = 5; // 時段最大筆數
@Component({
    selector: 'app-bot-setting',
    templateUrl: './bot-setting.component.html',
    styleUrls: ['./bot-setting.component.css']
})
export class BotSettingComponent implements OnInit {

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    displayedColumns: string[] = ['index', 'game_types_id', 'start_time', 'count_of_member', 'status', 'function'];
    dataSource = new MatTableDataSource<AutoDonateSetting>();

    gameTypes: GameType[];
    isFetched = false;


    constructor(
        private gameTypeServ: GameTypeService,
        private botMemberServ: BotMembersService,
        private autoDonateServ: AutoDonateSettingsService,
        private enumTranslateServ: EnumTranslateService,
        private translateService: TranslateService
    ) {
    }

    ngOnInit(): void {
        this.getAutoDonateSettings();
        this.getGameTypes();
    }

    getAutoDonateSettings() {
        this.autoDonateServ.getAutoDonateSettings().subscribe(r => {
            this.dataSource.data = addIndexToArray(r.data);
            this.isFetched = true;
        });
    }

    getGameTypes() {
        this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
    }

    delete(id: number) {
        if (confirm(this.translateService.instant('common.wannaDelete'))) {
            this.autoDonateServ.deleteAutoDonateSetting(id).subscribe(() => {
                this.getAutoDonateSettings();
                alert(this.translateService.instant('common.deleteSuccess'));
            },
                err => {
                    alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
                });
        }
    }

    getTranslatedGameTypeByID(id: number) {
        return this.enumTranslateServ.getTranslatedGameTypeByID(id);
    }

    isSettingInLimited() {
        return true;
        // return this.isFetched && this.dataSource.data.length < MAX_SETTING_LIMITED;
    }

    get Status() { return Status }
}
