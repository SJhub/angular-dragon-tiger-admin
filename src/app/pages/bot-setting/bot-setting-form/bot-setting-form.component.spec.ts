import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotSettingFormComponent } from './bot-setting-form.component';

describe('BotSettingFormComponent', () => {
  let component: BotSettingFormComponent;
  let fixture: ComponentFixture<BotSettingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotSettingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotSettingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
