import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { Status } from '../../enum/status.enum';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { MemberService } from '../../services/member.service';

@Component({
  selector: 'app-my-member',
  templateUrl: './my-member.component.html',
  styleUrls: ['./my-member.component.css']
})
export class MyMemberComponent implements OnInit {
  @Input() showMember = false;
  @Input() addMember = false;
  @Input() set agentID(id: number) {
    if (id) this.getMembersByAgentID(id);
  }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['index', 'account', 'name', 'wallet', 'wash_code_commission_percent'];
  dataSource = new MatTableDataSource<Member>();
  loaded = false;

  currentUser: User;

  constructor(
    private memberServ: MemberService,
    private translateService: TranslateService,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  getMembersByAgentID(id: number) {
    this.loaded = false;
    this.dataSource.data = [];
    let req = this.memberServ.getMemberByAgent(id);
    req.subscribe(r => {
      let data: Member[] = r.data;
      this.dataSource.data = data;
      this.loaded = true;
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  changeMemberStatus(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      status: member.status ? 0 : 1
    };

    this.memberServ.updateStatus(data).subscribe(
      () => {
        member.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => {
        alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
      }
    );
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberServ.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  deleteMember(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.memberServ.deleteMember(id)
        .subscribe(
          () => {
            alert(this.translateService.instant('common.deleteSuccess'));
            this.dataSource.data = this.dataSource.data.filter(e => e.id != id);
          },
          err => alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`)
        );
    }
  }

  previous() {
    this.location.back();
  }

  get Status() { return Status }
}
