import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloneAgentFormComponent } from './clone-agent-form.component';

describe('CloneAgentFormComponent', () => {
  let component: CloneAgentFormComponent;
  let fixture: ComponentFixture<CloneAgentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloneAgentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloneAgentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
