import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AgentAuthority } from '../../../enum/agent-authority.enum';
import { UserRole } from '../../../enum/user-role.enum';
import { User } from '../../../models/user';
import { AgentService } from '../../../services/agent.service';
import { AuthenticationService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-clone-agent-form',
  templateUrl: './clone-agent-form.component.html',
  styleUrls: ['./clone-agent-form.component.css']
})
export class CloneAgentFormComponent implements OnInit {
  myForm: FormGroup;
  loginUser: User;
  cloneUser: User;

  constructor(
    private route: ActivatedRoute,
    private agentServ: AgentService,
    private authServ: AuthenticationService,
    private userServ: UserService,
    private translateServ: TranslateService,
    private location: Location,
    private fb: FormBuilder
  ) { this.groupMyForm(); }

  ngOnInit(): void {
    const loginID = Number(this.authServ.getUserId());
    const cloneID = Number(this.route.snapshot.paramMap.get('id'));
    this.getLoginUser(loginID);
    this.getCloneAgent(cloneID);
  }

  groupMyForm() {
    this.myForm = this.fb.group({
      'real_name': [null, Validators.required],
      'name': [null, [Validators.required, Validators.minLength(6)]],
      'password': [null, Validators.pattern('^[A-Za-z0-9]+$')],
      're_password': [null],
      'status': [true],
      'parent_id': [null],
      'identity': [null],
      'operation_items': new FormArray([]),
    }, { validators: this.passwordMatches });
  }

  passwordMatches(form: AbstractControl) {
    return form.get('password').value === form.get('re_password').value ? null : { equals: true };
  }

  getLoginUser(id: number) {
    this.userServ.getUser(id).subscribe(r => this.loginUser = r.data);
  }

  getCloneAgent(id: number) {
    if (id) {
      this.userServ.getUser(id).subscribe(r => {
        this.cloneUser = r.data;
        this.myForm.patchValue({
          real_name: this.cloneUser.real_name,
          name: this.cloneUser.name,
          status: this.cloneUser.status
        });
        this.myForm.get('name').disable();
        this.setChecked(this.cloneUser.operation_items);
      });
    }
  }

  submitForm(val: User) {
    console.log(val);

    if (this.myForm.valid) {
      this.submit(val);
    } else {
      alert(this.translateServ.instant('common.inputErrorsPleaseReEenter'));
    }
  }

  submit(user: User) {
    const cloneUser = this.cloneUser ? this.cloneUser : this.loginUser;
    cloneUser.clone_id = this.cloneUser ? this.cloneUser.clone_id : this.loginUser.id;
    cloneUser.name = user.name;
    cloneUser.real_name = user.real_name;
    cloneUser.password = user.password;
    cloneUser.status = Number(user.status);
    cloneUser.operation_items = JSON.stringify(Array.from(new Set(user.operation_items)));

    cloneUser.wallet = 0;
    cloneUser.credit = 0;
    cloneUser.revenue_share = 0;

    let req = this.agentServ.addAgent(cloneUser);
    let msgSuccess = this.translateServ.instant('common.addSuccess');
    // msgSuccess += `\n${this.translateServ.instant('manageAgent.form.editWithin24Hours')}`;
    let msgFailed = this.translateServ.instant('common.addFailed');

    if (this.cloneUser) {
      msgSuccess = this.translateServ.instant('common.editSuccess');
      msgFailed = this.translateServ.instant('common.editFailed');
      req = this.agentServ.updateAgent(cloneUser);
    }

    req.subscribe(
      () => {
        alert(msgSuccess);
        this.cancel();
      },
      () => alert(msgFailed));
  }

  onCheckboxChange(e) {
    const operationItem: FormArray = this.myForm.get('operation_items') as FormArray;

    if (e.target.checked) {
      operationItem.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      operationItem.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          operationItem.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  getChecked(OperationItemID) {
    const operationItem = this.myForm.get('operation_items').value as Array<any>;
    return operationItem.includes(String(OperationItemID));
  }

  setChecked(auth_managementID: string) {
    if (auth_managementID === 'null') {
      return;
    }
    const items = JSON.parse(auth_managementID) as Array<string>;
    const authmanageID = this.myForm.get('operation_items') as FormArray;

    items.forEach(item => {
      authmanageID.push(new FormControl(item));
    });
  }

  cancel() {
    this.location.back();
  }

  get AgentAuthority() { return AgentAuthority }

  get UserRole() { return UserRole }
}
