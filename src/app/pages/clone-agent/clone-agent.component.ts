import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-clone-agent',
  templateUrl: './clone-agent.component.html',
  styleUrls: ['./clone-agent.component.css']
})
export class CloneAgentComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource = new MatTableDataSource<User>();
  displayedColumns: string[] = [
    'index',
    'name',
    'real_name',
    'status',
    'lastLogin',
    'created_at',
    'function'];


  loginIdentity: number;
  allAgents: User[];

  constructor(
    private userServ: UserService,
    private agentServ: AgentService,
    private authServ: AuthenticationService,
    private translateServ: TranslateService
  ) { }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.getCloneAgents();
  }

  getCloneAgents() {
    this.loginIdentity = Number(this.authServ.getUserIdentity());
    const id = Number(this.authServ.getUserId());
    let req = this.userServ.getUsers();

    if (this.loginIdentity !== UserRole.admin) {
      req = this.userServ.getUserByCloneId(id);
    } else {
      this.displayedColumns.splice(1, 0, 'clone_name');
      this.getAllAgents();
    }

    req.subscribe(r => {
      const users: User[] = r.data;
      this.dataSource.data = addIndexToArray(users.filter(e => e.identity !== UserRole.admin && e.clone_id));
    });
  }

  getAllAgents() {
    this.userServ.getUsers().subscribe(r => {
      const users: User[] = r.data;
      this.allAgents = users.filter(e => e.identity === UserRole.topAgent || e.identity === UserRole.agent);
    });
  }

  getCloneName(cloneID: number) {
    const user = this.allAgents?.find(e => e.id === cloneID);
    return user ? user.name : '';
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteAgent(id: number) {
    if (confirm(this.translateServ.instant('common.wannaDelete'))) {
      this.agentServ.deleteAgent(id).subscribe(() => {
        this.getCloneAgents();
        alert(this.translateServ.instant('common.deleteSuccess'));
      },
        err => {
          alert(`${this.translateServ.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  get Status() { return Status }

  get UserRole() { return UserRole }
}
