import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloneAgentComponent } from './clone-agent.component';

describe('CloneAgentComponent', () => {
  let component: CloneAgentComponent;
  let fixture: ComponentFixture<CloneAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloneAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloneAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
