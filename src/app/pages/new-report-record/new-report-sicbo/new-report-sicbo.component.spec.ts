import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReportSicboComponent } from './new-report-sicbo.component';

describe('NewReportSicboComponent', () => {
  let component: NewReportSicboComponent;
  let fixture: ComponentFixture<NewReportSicboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReportSicboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReportSicboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
