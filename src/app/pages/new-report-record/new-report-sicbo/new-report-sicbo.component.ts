import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalService } from 'ngx-bootstrap/modal';
import { groupAgentDailyReport, groupMemberDailyReport } from '../../../common/reportV2';
import { ReportByDateRangeMemberComponent } from '../../../component/report-by-date-range-member/report-by-date-range-member.component';
import { AgentDailyReport } from '../../../models/agent-daily-report';
import { MemberDailyReport } from '../../../models/member-daily-report';
import { Member } from '../../../models/member.model';
import { User } from '../../../models/user';

@Component({
  selector: 'app-new-report-sicbo',
  templateUrl: './new-report-sicbo.component.html',
  styleUrls: ['./new-report-sicbo.component.scss']
})
export class NewReportSicboComponent implements OnInit {
  @Input() startDate: string;
  @Input() endDate: string;
  @Input() loaded = false;
  @Input() totalAgentSettlement = 0;
  @Input() totalMemberSettlement = 0;
  @Input() topAgentTitle = false;
  @Input() loginUserIsAgent = true;
  @Input() isSpecify = true;
  @Input() set agentRecords(data: AgentDailyReport[]) {
    if (data) this.dataSourceOfAgent.data = groupAgentDailyReport(data).filter(e => e.number_of_game);
  }
  @Input() set memberRecords(data: MemberDailyReport[]) {
    if (data) {
      this.dataSourceOfMember.data = groupMemberDailyReport(data).filter(e => e.number_of_game);
      this.dataMembers = data;
    }
  }
  @Output() onAgentClick = new EventEmitter<User>();
  dataSourceOfAgent = new MatTableDataSource<AgentDailyReport>();
  dataSourceOfMember = new MatTableDataSource<MemberDailyReport>();

  showMember = false;

  displayedColumns = [
    'index',
    'account',
    'name',
    'total_amount',
    'number_of_game',
    'game_result_amount',
    'wash_code_commission_percent',
    'wash_code_commission',
    'revenue_share',
    'settlement_amount',
    'action'
  ];

  displayedColumnsByMember = [
    'index',
    // 'members_id',
    'account',
    'name',
    'total_amount',
    'number_of_game',
    'game_result_amount',
    'wash_code_commission_percent',
    'wash_code_commission',
    'settlement_amount',
    'views'
  ];

  dataMembers: MemberDailyReport[];

  constructor(private modalService: BsModalService,) { }

  ngOnInit(): void {
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }


  view(data: AgentDailyReport, showMember = false) {
    this.onAgentClick.emit(data.user);
    this.showMember = showMember;
  }

  showHeader(data: any, isMember = false) {
    return;
    let minIndex = 5; // 第5列後才觸發插入標題
    let row = { show_header: true };
    let tableData: any = this.dataSourceOfAgent.data;
    isMember && (tableData = this.dataSourceOfMember.data)
    let previousIndex = tableData.findIndex(e => e.show_header);
    tableData = tableData.filter(e => !e.show_header);
    let dataIndex = tableData.findIndex(e => e?.user?.id === data?.user?.id);
    isMember && (dataIndex = tableData.findIndex(e => e?.member?.id === data?.member?.id));
    if (dataIndex >= minIndex && previousIndex != dataIndex && !data.show_header) tableData.splice(dataIndex, 0, row);

    isMember && (this.dataSourceOfMember.data = tableData);
    !isMember && (this.dataSourceOfAgent.data = tableData);
  }

  openMemberReocrdModal(data: MemberDailyReport) {
    this.modalService.show(ReportByDateRangeMemberComponent, {
      initialState: {
        member: data.member,
        startDate: this.startDate,
        endDate: this.endDate,
        records: this.dataMembers
      }
      , class: 'modal-xl'
    });
  }
}
