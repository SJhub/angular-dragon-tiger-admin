import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';
import { getToday, DATE_LINE_HOUR, getYesterday, getThisWeek, getLastWeek, getThisMonth, getLastMonth } from '../../common/date';
import { defaultAgentID } from '../../common/global';
import { isDateRangeIncludedToday } from '../../common/report';
import { grandTotalAgentDailyReport } from '../../common/reportV2';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { UserRole } from '../../enum/user-role.enum';
import { AgentDailyReport } from '../../models/agent-daily-report';
import { GameType } from '../../models/game-type';
import { MemberDailyReport } from '../../models/member-daily-report';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { CurrentReportsSicboService } from '../../services/current-reports-sicbo.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { MemberService } from '../../services/member.service';
import { UserService } from '../../services/user.service';
import { NewReportSicboComponent } from './new-report-sicbo/new-report-sicbo.component';

@Component({
  selector: 'app-new-report-record',
  templateUrl: './new-report-record.component.html',
  styleUrls: ['./new-report-record.component.css']
})
export class NewReportRecordComponent implements OnInit {
  @ViewChild('reportSicbo') reportSicbo: NewReportSicboComponent;
  loginUserName: string;
  loginUserIdentity: number;
  loginUserID: number;
  loginUserIsAgent = true;
  currentUser: User;
  isCurrentAgent = false;
  isTopAgentClicked = false;

  gameTypeID = GameTypeEnum.GameTypeDragonTiger;
  gameTypes: GameType[];

  loaded = false;

  // 日期
  startDate: string;
  endDate: string;

  // 報表資料
  agents: User[];
  grandTotalReport: AgentDailyReport;
  agentRecords: AgentDailyReport[];
  memberRecords: MemberDailyReport[];

  // 代理&會員總交收
  totalAgentSettlement = 0;
  totalMemberSettlement = 0;
  ownProfit = 0;

  // 公司獲利
  cmpProfit = 0;

  // 紀錄點擊階層
  breadcrumbs: User[] = [];


  constructor(
    private currentReportServ: CurrentReportsSicboService,
    private userServ: UserService,
    private agentServ: AgentService,
    private memberServ: MemberService,
    private translateServ: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private gameTypeServ: GameTypeService,
    private authServ: AuthenticationService,
    private route: ActivatedRoute
  ) {
    this.loginUserIdentity = Number(this.authServ.getUserIdentity());
    this.loginUserID = Number(this.authServ.getUserId());
    this.loginUserName = this.authServ.getLoggedInUserName();
    this.loginUserIsAgent = this.loginUserIdentity === UserRole.agent || this.loginUserIdentity === UserRole.topAgent;
  }

  ngOnInit(): void {
    this.getCurrentUser(this.loginUserID);
    this.getToday(false);
  }

  getCurrentUser(id: number) {
    this.reset();
    this.userServ.getUser(id).subscribe(r => {
      let data: User = r.data;
      this.currentUser = data;
      this.getAgentsByUser(data);
    });
  }

  getAgentsByUser(user: User) {
    let isAgent = (identity: number) => identity == UserRole.agent || identity == UserRole.topAgent
    let req = this.userServ.getUsers();
    this.isCurrentAgent = isAgent(user.identity);
    if (this.isCurrentAgent) req = this.userServ.getUserByParentID(user.id);

    req.subscribe(r => {
      let data: User[] = r.data;

      // 過濾出代理不含子帳號
      data = data.filter(e => isAgent(e.identity) && e.id != defaultAgentID);

      // 管理員查看最高代理
      if (!this.isCurrentAgent) {
        data = data.filter(e => e.identity == UserRole.topAgent);
      }

      this.agents = data;

      this.search();
    })
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }

  getToday(fetch = true) {
    let res = getToday();
    res.ed = res.st;
    this.setDate(res, fetch);
  }

  getYesterday() {
    let res = getYesterday();
    res.ed = res.st;
    this.setDate(res);
  }

  getThisWeek() {
    this.setDate(getThisWeek());
  }

  getLastWeek() {
    this.setDate(getLastWeek());
  }

  getThisMonth() {
    this.setDate(getThisMonth());
  }

  getLastMonth() {
    this.setDate(getLastMonth());
  }

  search() {
    if (!this.startDate) {
      return alert(this.translateServ.instant('manageAgent.inputStartDate'));
    }
    if (!this.endDate) {
      return alert(this.translateServ.instant('manageAgent.inputEndDate'));
    }
    if (this.endDate < this.startDate) {
      return alert(this.translateServ.instant('manageAgent.inputCorrectDate'));
    }
    this.setDate({ st: this.startDate, ed: this.endDate });
  }

  setDate(date: { st: string, ed: string }, fetch = true) {
    this.startDate = date.st;
    this.endDate = date.ed;
    fetch && this.fetchReportRecords();
  }

  fetchReportRecords() {
    this.reset();
    this.getBaccaratReportRecords(GameTypeEnum.GameTypeDragonTiger, this.startDate, this.endDate);
  }

  getBaccaratReportRecords(gameType: number, start: string, end: string) {
    let isCurrent = isDateRangeIncludedToday(start, end);
    let records: CurrentReportResponse = { agent_daily_report: [], member_daily_report: [] };
    let reqs = [];


    // 判斷查詢是否包含即時報表並加入api
    if (isCurrent) {
      let c = this.getCurrentReportRequest(gameType)
        .pipe(tap(r => {
          let data: CurrentReportResponse = r.data;
          let data1 = data.agent_daily_report;
          let data2 = data.member_daily_report;
          records.agent_daily_report.push(...data1);
          records.member_daily_report.push(...data2);
          console.log(data1);
          console.log(data2);

        }));
      reqs.push(c);
    }

    // 加入代理日報表查詢api
    let query = this.getAgentDailyReportQuery(gameType);
    let tmp = query.map(q =>
      this.agentServ.getAgentDailyReportsSearchByTypeAndAgentBetweenDate(q).pipe(
        tap(r => {
          let data: AgentDailyReport[] = r.data;
          records.agent_daily_report.push(...data);
        })))
    reqs.push(...tmp);

    // 加入會員日報表查詢api
    query = this.getMemberDailyReportQuery(gameType);
    tmp = query.map(q =>
      this.memberServ.getMemberDailyReportsSearchByTypeAndMemberBetweenDate(q).pipe(
        tap(r => {
          let data: MemberDailyReport[] = r.data;
          records.member_daily_report.push(...data);
        })))
    reqs.push(...tmp);

    forkJoin(reqs).pipe(finalize(() => this.loaded = true)).subscribe(() => {
      let agentIDs = this.agents.map(u => u.id);
      let agentRecords = records.agent_daily_report.filter(e => e.is_top && agentIDs.includes(e.agents_id));
      let memberRecords = records.member_daily_report.filter(e => e.agents_id == this.currentUser.id);
      agentRecords.map(r => r.user = this.agents.find(e => e.id == r.agents_id));

      let grandAgentRecords = agentRecords;
      // 代理則過濾出自己紀錄後加總
      if (this.isCurrentAgent) grandAgentRecords = records.agent_daily_report.filter(e => e.is_top && e.agents_id == this.currentUser.id);
      this.grandTotalReport = grandTotalAgentDailyReport(grandAgentRecords);
      this.agentRecords = agentRecords;
      this.memberRecords = memberRecords;
      this.grandTotalSettlement(this.agentRecords, this.memberRecords);
    });
  }

  getAgentDailyReportQuery(gameType: number): AgentDailyReportByTypeAndAgent[] {
    let query: AgentDailyReportByTypeAndAgent = {
      game_types_id: gameType,
      start_date: this.startDate,
      end_date: this.endDate,
    }
    return [query];
  }

  getMemberDailyReportQuery(gameType: number): MemberDailyReportByTypeAndMember[] {
    let query: MemberDailyReportByTypeAndMember = {
      game_types_id: gameType,
      start_date: this.startDate,
      end_date: this.endDate,
    }
    return [query];
  }

  getCurrentReportRequest(gameTypeID: number) {
    switch (gameTypeID) {
      case GameTypeEnum.GameTypeDragonTiger:
        return this.currentReportServ.getcurrentReportsDragonTiger();
    }
  }

  reset() {
    this.loaded = false;
    this.totalAgentSettlement = 0;
    this.totalMemberSettlement = 0;
    this.agentRecords = [];
    this.memberRecords = [];
  }

  grandTotalSettlement(data1: AgentDailyReport[], data2: MemberDailyReport[]) {

    data1.forEach(e => {
      this.totalAgentSettlement += e.settlement_amount;
    });

    data2.forEach(e => {
      this.totalMemberSettlement += e.settlement_amount;
    });

    this.ownProfit = this.grandTotalReport.settlement_amount - (this.totalAgentSettlement + this.totalMemberSettlement);

    // 公司獲利 = 會員輸贏結果*-1 - 洗碼退佣
    this.cmpProfit = this.grandTotalReport.game_result_amount * -1 - this.grandTotalReport.settlement_amount;
  }

  onAgentClick(data: User) {
    this.breadcrumbs.push(data);
    this.getCurrentUser(data.id);
  }

  back() {
    this.reportSicbo.showMember = false;
    this.breadcrumbs.pop();
    let len = this.breadcrumbs.length;
    if (len) {
      let data = this.breadcrumbs[len - 1];
      return this.getCurrentUser(data.id);
    }
    this.getCurrentUser(this.loginUserID);
  }

  showBreadcrumb(key: string) {
    let len = this.breadcrumbs.length;
    if (len) {
      // if (len >= 2) {
      //   return `${this.breadcrumbs[len - 2].name}/${this.breadcrumbs[len - 1].name}`;
      // }
      return `${this.breadcrumbs[len - 1][key]}`;
    }
    return this.currentUser ? `${this.currentUser[key]}` : '';
  }

  get GameTypeEnum() { return GameTypeEnum }
}

interface CurrentReportResponse {
  agent_daily_report: AgentDailyReport[];
  member_daily_report: MemberDailyReport[];
}

interface AgentDailyReportByTypeAndAgent {
  game_types_id: number;
  start_date: string;
  end_date: string;
  agents_id?: number;
}

interface MemberDailyReportByTypeAndMember {
  game_types_id: number;
  start_date: string;
  end_date: string;
  members_id?: number;
}