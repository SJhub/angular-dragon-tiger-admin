import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReportRecordComponent } from './new-report-record.component';

describe('NewReportRecordComponent', () => {
  let component: NewReportRecordComponent;
  let fixture: ComponentFixture<NewReportRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReportRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReportRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
