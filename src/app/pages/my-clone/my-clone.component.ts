import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-my-clone',
  templateUrl: './my-clone.component.html',
  styleUrls: ['./my-clone.component.css']
})
export class MyCloneComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['name', 'real_name', 'status', 'last_login_at'];
  dataSource = new MatTableDataSource<User>();
  loaded = false;

  loginID: number;

  constructor(
    private userServ: UserService,
    private agentServ: AgentService,
    private authServ: AuthenticationService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.loginID = Number(this.authServ.getUserId());
    this.getUserByID(this.loginID);
  }

  getUserByID(id: number) {
    this.userServ.getUserByParentID(id).subscribe(r => {
      let data: User[] = r.data;
      data = data.filter(e => e.clone_id && (e.identity === UserRole.agent || e.identity === UserRole.topAgent));
      this.dataSource.data = data;
      this.loaded = true;
    })
  }

  changeAgentStatus(user: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: user.id,
      status: user.status ? 0 : 1
    };

    this.agentServ.updateAgentStatus(data).subscribe(
      () => {
        user.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => {
        alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
      }
    );
  }

  get Status() { return Status }
}
