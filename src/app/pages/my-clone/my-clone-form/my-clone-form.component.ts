import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { forkJoin } from 'rxjs';
import { genCode } from '../../../common/common';
import { defaultAgentOperation } from '../../../common/global';
import { ConfirmModalComponent } from '../../../component/confirm-modal/confirm-modal.component';
import { Status } from '../../../enum/status.enum';
import { User } from '../../../models/user';
import { UserWallet } from '../../../models/user-wallet';
import { AgentService } from '../../../services/agent.service';
import { AuthenticationService } from '../../../services/auth.service';
import { BetAmountService } from '../../../services/bet-amount.service';
import { ItemTypesService } from '../../../services/item-types.service';
import { UserService } from '../../../services/user.service';

const DEFAULT_AGENT_OPERATION = defaultAgentOperation;

@Component({
  selector: 'app-my-clone-form',
  templateUrl: './my-clone-form.component.html',
  styleUrls: ['./my-clone-form.component.css']
})
export class MyCloneFormComponent implements OnInit {

  id: number;
  myForm: FormGroup;
  loginUserID: number;
  loginUser: User;

  // 確認modal
  modalRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private agentService: AgentService,
    private userServ: UserService,
    private location: Location,
    private translateService: TranslateService,
    private authServ: AuthenticationService,
  ) {
    this.groupMyForm();
    this.loginUserID = Number(this.authServ.getUserId());
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.genInvationCode();
    this.userServ.getUser(this.loginUserID).subscribe(r => this.loginUser = r.data);
    if (this.id) this.setForm(this.id);
  }

  groupMyForm() {
    this.myForm = this.fb.group({
      'name': [null, [Validators.pattern('^[A-Za-z0-9]+$'), Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      'password': [null, [Validators.required, Validators.pattern('^[A-Za-z0-9]+$'), Validators.minLength(6), Validators.maxLength(12)]],
      're_password': [null],
      'real_name': [null, [Validators.required, Validators.maxLength(10)]],
      'invitation_code': []
    });
  }

  setForm(id: number) {
    this.userServ.getUser(id).subscribe(r => {
      let data: User = r.data;
      this.myForm.patchValue({
        name: data.name,
        real_name: data.real_name,
      });
    });
    this.myForm.get('password').setValidators([Validators.pattern('^[A-Za-z0-9]+$'), Validators.minLength(6), Validators.maxLength(12)]);
    this.myForm.get('name').disable();
  }

  submitForm(val) {
    let wallet = {
      sic_bo_credit: 0,
      sic_bo_wallet: 0,
      baccarat_wallet: 0,
      baccarat_credit: 0,
      poker_credit: 0,
      poker_wallet: 0,
    } as UserWallet;

    val.id = this.id;
    val.clone_id = this.loginUserID;
    val.parent_id = this.loginUserID;
    val.single_bet_limits = this.loginUser.single_bet_limits;
    val.single_bet_total_amounts = this.loginUser.single_bet_total_amounts;
    val.bet_limits = '[]';
    val.allow_game = '[]';
    val.world_football_game_settings = '[]';
    val.allow_game = '[]';
    val.commission = this.loginUser.commission;
    val.operation_items = JSON.stringify(DEFAULT_AGENT_OPERATION);
    val.user_wallet = JSON.stringify(wallet);
    val.status = Status.Active;

    let req = this.agentService.addAgent(val);
    let successMsg = this.translateService.instant('common.addSuccess');
    let failedMsg = this.translateService.instant('common.addFailed');
    if (this.id) {
      successMsg = this.translateService.instant('common.editSuccess');
      failedMsg = this.translateService.instant('common.editFailed');
      req = this.agentService.updateAgent(val);
    }
    if (this.myForm.valid) {
      req.subscribe(
        () => {
          alert(successMsg);
          this.location.back();
        },
        err => {
          alert(`${failedMsg}: ${err.error.message}`);
        }
      );
    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      this.markFormGroupTouched(this.myForm);
    }
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }


  cancel() {
    this.location.back();
  }

  deleteAgent(id: number) {

    this.modalRef = this.modalService.show(ConfirmModalComponent,
      { initialState: { message: this.translateService.instant('common.wannaDelete') } })
    this.modalRef.content.onClose.subscribe(result => {
      if (result) {
        this.agentService.deleteAgent(id).subscribe(
          () => {
            alert(this.translateService.instant('common.deleteSuccess'));
            this.cancel();
          },
          error => {
            alert(`${this.translateService.instant('common.deleteFailed')}: ${error.error.message}`);
          }
        );
      }
    })
  }

  async genInvationCode(retryTimes = 5) {
    let ok = false;
    this.myForm.get('invitation_code').setValue('');
    for (let index = 0; index < retryTimes; index++) {
      let invitationCode = genCode();
      await this.agentService.getByInvitationCode(invitationCode).toPromise().then(r => {
        if (!r.data) {
          this.myForm.get('invitation_code').setValue(invitationCode);
          ok = true;
        }
      });

      if (ok) return;
    }
    alert('產生邀請碼失敗，頁面將自動重新整理');
    window.location.reload();
  }
}
