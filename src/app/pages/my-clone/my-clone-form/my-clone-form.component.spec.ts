import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCloneFormComponent } from './my-clone-form.component';

describe('MyCloneFormComponent', () => {
  let component: MyCloneFormComponent;
  let fixture: ComponentFixture<MyCloneFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCloneFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCloneFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
