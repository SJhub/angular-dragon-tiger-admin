import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCloneComponent } from './my-clone.component';

describe('MyCloneComponent', () => {
  let component: MyCloneComponent;
  let fixture: ComponentFixture<MyCloneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCloneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCloneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
