import { MemberService } from './../../services/member.service';
import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Action } from '../../enum/action.enum';
import { BetRecord } from '../../models/bet-record';
import { GameType } from '../../models/game-type';
import { EnumToArrayPipe } from '../../pipe/enum-to-array.pipe';
import { BetRecordService } from '../../services/bet-record.service';
import { GameTypeService } from '../../services/game-type.service';
import { Member } from '../../models/member.model';
import { GameStatusType } from '../../models/game-status-type';
import { GameResultType } from '../../models/game-result-type';
import { GameActionType } from '../../models/game-action-type';
import { BetRecordStatusEnum } from '../../enum/bet-record-status.enum';
import { TranslateService } from '@ngx-translate/core';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { UserRole } from '../../enum/user-role.enum';
import { GameResultTypeEnum } from '../../enum/game-result-type.enum';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['game_type', 'game_number', 'action', 'amount', 'members_id', 'result', 'draw_result', 'status', 'created_at'];
  dataSource = new MatTableDataSource<BetRecord>();

  betRecords: BetRecord[];
  GameType: GameType[];
  Member: Member[];

  data: any;

  gameTypeValue: number;
  actionValue: number;

  betTotal: number = 0;
  cancelBetTotal: number = 0;
  donateTotal: number = 0;

  GameTypesID: number;
  Action: number;

  startDateValue: string;
  endDateValue: string;

  todayFormat = (new Date());

  ActionValue = 3;
  identity: number;
  id: number;

  now = new Date();
  nowDayOfWeek = this.now.getDay() - 1;
  nowDay = this.now.getDate();
  nowMonth = this.now.getMonth();
  nowYear = this.now.getFullYear();

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private betRecordService: BetRecordService,
    private gameTypeService: GameTypeService,
    private memberService: MemberService,
  ) { }

  ngOnInit(): void {
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.id = Number(localStorage.getItem('authenticatedID'));
    // this.getBetRecordList();
    this.getGameTypeList();
    if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getMemberList(); }
    this.selectAction(3);
    this.searchToday();

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.filterPredicate = (data: BetRecord, filter: string): boolean => {
      const game_types_id = this.getGameTypeName(data.game_types_id);
      const game_number = String(data.game_number);
      const action = this.getGameActionTypeByID(data.action);
      const amount = String(data.amount);
      const members_id = this.getMemberName(data.members_id);
      const result = this.getGameResultTypeByID(data.result);
      const draw_result = String(data.draw_result);
      const status = this.getGameStatusType(data.status);
      const created_at = data.created_at;

      return game_types_id.includes(filter) || game_number.includes(filter) || action.includes(filter) || amount.includes(filter)
        || members_id.includes(filter) || result.includes(filter) || draw_result.includes(filter) || status.includes(filter) ||
        created_at.includes(filter);
    };
  }

  /**
   * 以開始及結束日期搜尋押注記錄資料 - 最高權限&管理者
   */
  getBetRecordsSearchByDate(data) {
    this.betRecordService.getBetRecordsSearchByDate(data).subscribe(
      res => {
        this.data = res.data;
        this.dataSource.data = this.data;

        const actionByBet = this.dataSource.data.filter(e => e.action === 0);
        const betAmount = actionByBet.map(e => e.amount);

        const actionByCancelBet = this.dataSource.data.filter(e => e.action === 1);
        const cancelBetAmount = actionByCancelBet.map(e => e.amount);

        const actionByDonate = this.dataSource.data.filter(e => e.action === 2);
        const donateAmount = actionByDonate.map(e => e.amount);

        betAmount.forEach(e => {
          this.betTotal = this.betTotal + e;
        });

        cancelBetAmount.forEach(e => {
          this.cancelBetTotal = this.cancelBetTotal + e;
        });

        donateAmount.forEach(e => {
          this.donateTotal = this.donateTotal + e;
        });

      }
    );
  }

  /**
   * 以代理ID及開始及結束日期搜尋押注記錄資料
   */
  getBetRecordsSeachByAgentAndDate(data) {
    this.betRecordService.getBetRecordsSeachByAgentAndDate(data).subscribe(
      res => {
        this.data = res.data;
        this.dataSource.data = this.data;

        const actionByBet = this.dataSource.data.filter(e => e.action === 0);
        const betAmount = actionByBet.map(e => e.amount);

        const actionByCancelBet = this.dataSource.data.filter(e => e.action === 1);
        const cancelBetAmount = actionByCancelBet.map(e => e.amount);

        const actionByDonate = this.dataSource.data.filter(e => e.action === 2);
        const donateAmount = actionByDonate.map(e => e.amount);

        betAmount.forEach(e => {
          this.betTotal = this.betTotal + e;
        });

        cancelBetAmount.forEach(e => {
          this.cancelBetTotal = this.cancelBetTotal + e;
        });

        donateAmount.forEach(e => {
          this.donateTotal = this.donateTotal + e;
        });

      }
    );
  }

  getGameTypeList() {
    this.gameTypeService.getGameTypes().subscribe(
      res => {
        this.GameType = res.data;
      }
    );
  }

  getGameTypeName(game_types_id: number): string {
    const gameName = this.GameType.find(e => e.id === game_types_id);
    if (gameName) { return this.enumTranslateServ.getTranslatedGameName(gameName.en_name); }
    return this.translateService.instant('common.noData');
  }

  getMemberList() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.Member = res.data;
      }
    );
  }

  getMemberName(members_id: number): string {
    const memberName = this.Member.find(e => e.id === members_id);
    if (memberName) { return memberName.name; }
    return this.translateService.instant('common.noData');
  }

  getGameActionTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedAction(id);
    // return GameActionType.getNameByID(id);
  }

  getGameResultTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameResultType(id);
    // return GameResultType.getNameByID(id);
  }

  getGameStatusType(status: number) {
    return this.enumTranslateServ.getTranslatedGameStatusType(status);
    // return GameStatusType.getName(status);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  get action() {
    return new EnumToArrayPipe().transform(Action);
  }

  compareFn(a, b) {
    return 0;
  }

  selectGameType(gameTypeValue: number) {
    this.gameTypeValue = gameTypeValue;
  }

  selectAction(actionValue: number) {
    this.actionValue = actionValue;
  }

  searchToday() {
    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    const today = new Date();
    const formatToday = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
    const tomorrow = formatDate(today.setDate(today.getDate() + 1), 'yyyy-MM-dd', 'en-US');

    this.startDateValue = formatToday;
    this.endDateValue = formatToday;

    const adminData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: formatToday,
      end_date: tomorrow,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: formatToday,
      end_date: tomorrow,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(adminData);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  searchYesterday() {
    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    const today = new Date();
    const formatToday = formatDate(today, 'yyyy-MM-dd', 'en-US');
    const yesterday = formatDate(today.setDate(today.getDate() - 1), 'yyyy-MM-dd', 'en-US');

    this.startDateValue = yesterday;
    this.endDateValue = yesterday;

    const data = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: yesterday,
      end_date: formatToday,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: yesterday,
      end_date: formatToday,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(data);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  week() {
    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    const today = new Date();
    const tomorrow = formatDate(today.setDate(today.getDate()), 'yyyy-MM-dd', 'en-US');
    const dayOfWeek = today.getDay();
    const weekStart = formatDate(today.setDate(today.getDate() - dayOfWeek), 'yyyy-MM-dd', 'en-US');

    this.startDateValue = weekStart;
    this.endDateValue = tomorrow;


    const data = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: weekStart,
      end_date: tomorrow,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: weekStart,
      end_date: tomorrow,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(data);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  lastWeek() {
    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    const weekStartDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 8), 'yyyy-MM-dd', 'en-US');
    const weekEndDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 2), 'yyyy-MM-dd', 'en-US');
    this.startDateValue = weekStartDate;
    this.endDateValue = weekEndDate;

    const data = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: this.startDateValue,
      end_date: this.endDateValue,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(data);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  month() {
    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    const todayOfToday = new Date();
    const todayOfMonth = new Date();
    const formatMonth = formatDate(todayOfMonth.setDate(1), 'yyyy-MM-dd', 'en-US');
    const formatToday = formatDate(todayOfToday, 'yyyy-MM-dd', 'en-US');

    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month, 1);
    const lastDay = new Date(year, month + 1, 0);
    const formatFirstDay = formatDate(firstDay, 'yyyy-MM-dd', 'en-US');
    const formatLastDay = formatDate(lastDay, 'yyyy-MM-dd', 'en-US');
    this.startDateValue = formatFirstDay;
    this.endDateValue = formatLastDay;

    const data = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: formatMonth,
      end_date: formatToday,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: formatMonth,
      end_date: formatToday,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(data);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  lastMonth() {
    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month - 1, 1);
    const lastDay = new Date(year, month, 0);

    const formatFirstDay = formatDate(firstDay, 'yyyy-MM-dd', 'en-US');
    const formatLastDay = formatDate(lastDay, 'yyyy-MM-dd', 'en-US');

    this.startDateValue = formatFirstDay;
    this.endDateValue = formatLastDay;

    const data = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: formatFirstDay,
      end_date: formatLastDay,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: formatFirstDay,
      end_date: formatLastDay,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(data);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  startDate(startDateValue: string) {
    this.startDateValue = startDateValue;
  }

  endDate(endDateValue: string) {
    this.endDateValue = endDateValue;
  }

  btnOK() {

    this.betTotal = 0;
    this.cancelBetTotal = 0;
    this.donateTotal = 0;

    if (!this.startDateValue) {
      alert(this.translateService.instant('manageAgent.inputStartDate'));
    }
    if (!this.endDateValue) {
      alert(this.translateService.instant('manageAgent.inputEndDate'));
    }

    const end_date = this.endDateValue + ' 23:59:59';
    console.log(end_date);
    

    const data = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: this.startDateValue,
      end_date: end_date,
    };

    const agentData = {
      game_types_id: Number(this.gameTypeValue),
      action: Number(this.actionValue),
      start_date: this.startDateValue,
      end_date: end_date,
      agents_id: this.id
    };

    if (this.identity === UserRole.admin || this.identity === UserRole.management) {
      this.getBetRecordsSearchByDate(data);
    }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) {
      this.getBetRecordsSeachByAgentAndDate(agentData);
    }
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  getTranslatedAction(value: number) {
    return this.enumTranslateServ.getTranslatedAction(value);
  }

  get GameResultTypeEnum() { return GameResultTypeEnum }
}
