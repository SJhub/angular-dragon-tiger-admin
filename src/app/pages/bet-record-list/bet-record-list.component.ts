import { UserRole } from './../../enum/user-role.enum';
import { GameType } from './../../models/game-type';
import { ItemType } from './../../models/item-type';
import { LimitType } from './../../models/limit-type';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BetRecord } from '../../models/bet-record';
import { BetRecordService } from '../../services/bet-record.service';
import { GameTypeService } from '../../services/game-type.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { BetRecordStatusEnum } from '../../enum/bet-record-status.enum';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameResultTypeEnum } from '../../enum/game-result-type.enum';
import { addIndexToArray } from '../../common/global';
import { formatDate } from '@angular/common';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { TranslateService } from '@ngx-translate/core';

const CROSSING_HOUR = 12; // 以中午12時為基準取資料
@Component({
  selector: 'app-bet-record-list',
  templateUrl: './bet-record-list.component.html',
  styleUrls: ['./bet-record-list.component.css']
})
export class BetRecordListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = [
    'index', 'game_type', 'item_type', 'game_number', 'action',
    'amount', 'members_id', 'result', 'draw_result',
    'status', 'created_at', 'function'
  ];
  dataSource = new MatTableDataSource<BetRecord>();

  public data: BetRecord[];
  public modalRef: BsModalRef;
  public modal: any;

  betRecords: BetRecord[];
  gameTypes: GameType[];
  gameTypesID: number;

  id: number;
  identity: number;

  constructor(
    private translateService: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private betRecordService: BetRecordService,
    private modalService: BsModalService,
    private gameTypeService: GameTypeService,
  ) { }

  ngOnInit() {
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.id = Number(localStorage.getItem('authenticatedID'));
    this.getGameTypes();

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * 取得遊戲類型
   */
  getGameTypes() {
    this.gameTypeService.getGameTypes()
      .subscribe(
        res => {
          this.gameTypes = res.data;
          if (this.gameTypes.length > 0) {
            this.gameTypesID = this.gameTypes[0]['id'];
          }

          this.getBetRecords();
        },
        err => console.log(err)
      );
  }

  getBetRecords() {
    const now = new Date();
    const currentHour = now.getHours();

    let t1 = new Date();
    t1.setHours(CROSSING_HOUR, 0, 0);

    if (currentHour < CROSSING_HOUR) {
      t1.setDate(now.getDate() - 1);
    }

    const st = formatDate(t1, 'yyyy-MM-dd HH:mm:ss', 'en_US');
    const et = formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en_US');

    const data = {
      game_types_id: GameTypeEnum.GameTypeSicBo,
      start_date: st,
      end_date: et,
      agents_id: this.id
    }

    let api = this.betRecordService.getBetRecordsSearchByDate(data);

    if (this.identity !== UserRole.admin) {
      api = this.betRecordService.getBetRecordsSeachByAgentAndDate(data);
    }

    api.subscribe(res => {
      this.data = this.filterBetRecords(res.data);
      this.betRecords = this.data;
      const data = this.findBetRecordsByGameTypesID(this.gameTypesID);
      this.dataSource.data = addIndexToArray(data);
    });
  }

  filterBetRecords(data: any): any {
    return data.filter(it => it['action'] !== GameActionTypeEnum.Donate);
  }

  /**
   * 取得押注紀錄 - 代理底下的會員
   */
  getBetRecordByGameTypeAndMember(gameTypeId: number, memberId: number) {
    this.betRecordService.getBetRecordByGameTypeAndMember(gameTypeId, memberId).subscribe(
      res => {
        const data = res.data;
        console.log(data);
      }
    );
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.betRecordService.getBetRecord(id)
      .subscribe(res => {
        this.modal = res.data as BetRecord;
        this.modalRef = this.modalService.show(template);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * getGameTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameTypeByID(id);
    // return GameType.getNameByID(id);
  }

  /**
   * getItemTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getItemTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedBetItem(ItemType.getNameByID(id));
  }

  /**
   * getLimitTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getLimitTypeByID(id: number) {
    return LimitType.getNameByID(id);
  }

  /**
   * getGameActionTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameActionTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedAction(id);
    // return GameActionType.getNameByID(id);
  }

  /**
   * getGameResultTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameResultTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameResultType(id);
    // return GameResultType.getNameByID(id);
  }

  /**
   * getGameStatusType - 用狀態 轉換 名稱
   * @param status
   */
  getGameStatusType(status: number) {
    return this.enumTranslateServ.getTranslatedGameStatusType(status);
    // return GameStatusType.getName(status);
  }

  /**
     * find bet records by game types id
     * @param id
     */
  findBetRecordsByGameTypesID(id: number): any[] {
    if (id <= 0) {
      return [];
    }

    if (this.betRecords.length <= 0) {
      return [];
    }

    const data = this.betRecords.filter(
      it => it.game_types_id === Number(id)
    );

    return data;
  }

  /**
   * gameTypeOnChange - when select opetion change
   * @param gameTypesID
   */
  gameTypeOnChange(gameTypesID: number) {
    this.gameTypesID = gameTypesID;
    const data = this.findBetRecordsByGameTypesID(this.gameTypesID);

    if (data.length > 0) {
      this.dataSource.data = data;
    }
  }

  /**
   * updateStatus - 更新狀態
   * @param id
   * @param status
   */
  updataStatus(id: number, status: number) {
    this.betRecordService.updateBetRecordStatus(id, status)
      .subscribe(
        () => {
          this.getBetRecords();
          alert(this.translateService.instant('common.updateSuccess'));
        },
        err => alert(this.translateService.instant('common.updateFailed'))
      );
  }

  /**
   * updateStatusFinish - 更新狀態完成
   * @param id
   */
  updateStatusFinish(id: number, status: number) {
    if (status === 4) {
      alert('投注狀態為作廢,無法更改投注狀態');
      return;
    }
    if (status !== Number(BetRecordStatusEnum.Finish)) {
      this.updataStatus(id, Number(BetRecordStatusEnum.Finish));
    }
  }

  /**
   * updateStatusUnusual - 更新狀態異常
   * @param id
   */
  updateStatusUnusual(id: number, status: number) {
    if (status === 4) {
      return alert('投注狀態為作廢,無法更改投注狀態');
    }
    if (status !== Number(BetRecordStatusEnum.Unusual)) {
      if (confirm('變更後將無法改回!')) {
        this.updataStatus(id, Number(BetRecordStatusEnum.Unusual));
      }
    }
  }

  /**
   * updateStatusDeleted - 更新狀態刪除
   * @param id
   */
  updateStatusDeleted(id: number, status: number) {
    if (status === 4) {
      alert('投注狀態為作廢,無法更改投注狀態');
      return;
    }
    if (status !== Number(BetRecordStatusEnum.Deleted)) {
      this.updataStatus(id, Number(BetRecordStatusEnum.Deleted));
    }
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  get GameResultTypeEnum() { return GameResultTypeEnum }

  get BetRecordStatusEnum() { return BetRecordStatusEnum }
}
