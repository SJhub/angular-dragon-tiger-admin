import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetRecordListComponent } from './bet-record-list.component';

describe('BetRecordListComponent', () => {
  let component: BetRecordListComponent;
  let fixture: ComponentFixture<BetRecordListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetRecordListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetRecordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
