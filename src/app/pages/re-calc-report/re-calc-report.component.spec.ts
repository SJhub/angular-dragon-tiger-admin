import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReCalcReportComponent } from './re-calc-report.component';

describe('ReCalcReportComponent', () => {
  let component: ReCalcReportComponent;
  let fixture: ComponentFixture<ReCalcReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReCalcReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReCalcReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
