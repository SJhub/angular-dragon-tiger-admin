import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { finalize, switchMap } from 'rxjs/operators';
import { getLastCalcDay } from '../../common/report';
import { OperationLogType } from '../../enum/operation-log-type.enum';
import { OperationLog } from '../../models/operation-log';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/auth.service';
import { OperationLogService } from '../../services/operation-log.service';
import { ReportsService } from '../../services/reports.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-re-calc-report',
  templateUrl: './re-calc-report.component.html',
  styleUrls: ['./re-calc-report.component.css']
})
export class ReCalcReportComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<OperationLog>();
  displayedColumns: string[] = ['users_id', 'value', 'updated_at'];

  weekFirstDay: string;
  lastCalcDay: string;
  submitted = false;

  users: User[];

  constructor(
    private reportServ: ReportsService,
    private translateServ: TranslateService,
    private authServ: AuthenticationService,
    private userServ: UserService,
    private operationLogServ: OperationLogService,
  ) { }

  ngOnInit(): void {
    this.lastCalcDay = getLastCalcDay();
    this.weekFirstDay = this.getThisWeekFirstDay(this.lastCalcDay);
    this.getOperationLogs();
  }

  getOperationLogs() {
    this.userServ.getUsers().pipe(
      switchMap(r => {
        this.users = r.data;
        return this.operationLogServ.getOperationLogsByActionType(OperationLogType.ReCalcDailyReport);
      }))
      .subscribe(r => {
        const logs: OperationLog[] = r.data;
        logs.map(e => {
          const user = this.users.find(user => user.id === e.users_id);

          if (user) {
            e.user = user;
          }

          try {
            e.value = JSON.parse(e.value);
          } catch {
            e.value = { closing_day: 'error' };
          }
        });
        this.dataSource.data = r.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }



  submit(date: string) {
    if (date) {
      this.submitted = true;
      const arr = date.split('-').map(e => Number(e));
      const userID = Number(this.authServ.getUserId());
      const userIdentity = Number(this.authServ.getUserIdentity());
      this.reportServ.reportsSicboReCalc(userID, userIdentity, arr[0], arr[1], arr[2])
        .pipe(finalize(() => this.submitted = false))
        .subscribe(
          () => {
            alert(this.translateServ.instant('common.submitSuccess'));
            this.getOperationLogs();
          },
          err => alert(`${this.translateServ.instant('common.submitFailed')}: ${err.error.message}`)
        );
    }
  }

  getThisWeekFirstDay(lastCalcDay: string) {
    const lastDay = new Date(lastCalcDay);
    const now = new Date();
    let dayOfWeek = now.getDay();
    const weekFirstDay = new Date().setDate(now.getDate() - dayOfWeek);

    // 跨週則取上禮拜第一天
    if (weekFirstDay >= lastDay.getTime()) {
      dayOfWeek = 7;
    }

    return formatDate(new Date().setDate(now.getDate() - dayOfWeek), 'yyyy-MM-dd', 'en-US');
  }
}
