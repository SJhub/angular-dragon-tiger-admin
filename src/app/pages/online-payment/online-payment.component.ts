import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { PaymentLog, PaymentLogService } from '../../services/payment-log.service';

const MinimumAmount = 100;
@Component({
  selector: 'app-online-payment',
  templateUrl: './online-payment.component.html',
  styleUrls: ['./online-payment.component.css']
})
export class OnlinePaymentComponent implements OnInit {
  @ViewChild('payment') payment: ElementRef;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = [
    'index',
    'created_at',
    'order_number',
    'pay_amount',
    'is_payment',
    'payment_at',
    'status',
  ];
  dataSource = new MatTableDataSource<PaymentLog>();
  loaded = false;

  payAmount: number;

  customOrderNo: string;

  // 黑貓posUrl
  catUrl = environment.catUrl;
  returnUrl = environment.catUrl + '/api/v1/ccat-payment-result';

  result: PaymentResult;

  success = false;
  loading = false;

  constructor(
    private paymentLogServ: PaymentLogService,
    private route: ActivatedRoute,
  ) {
    const result = Object.assign({}, this.route.snapshot.queryParams) as PaymentResult;
    if (result.lidm) {
      this.result = result;
      if (this.result.success == 'SUCCESS') this.success = true;
      window.location.href = window.location.href.split("?")[0];
    }
  }

  ngOnInit(): void {
    this.paymentLogServ.getPaymentLogs().subscribe(r => {
      let data: PaymentLog[] = r.data;
      this.dataSource.data = data;
      this.dataSource.sort = this.sort;
      this.loaded = true;
    });
  }

  submit() {
    if (!this.payAmount) return alert('請輸入付款金額');
    if (this.payAmount < MinimumAmount) return alert(`付款金額不可低於${MinimumAmount}`);

    this.loading = true;
    const body = {
      pay_amount: this.payAmount
    } as PaymentLog;
    this.paymentLogServ.addPaymentLog(body).subscribe(r => {
      let data: PaymentLog = r.data;
      this.customOrderNo = data.order_number;
      setTimeout(() => this.payment.nativeElement.submit(), 1000);
    });

  }

  reset() {
    this.result = null;
    this.success = false;
  }

  get OrderStatus() { return OrderStatus }

  get OrderStatusText() { return OrderStatusText }
}

interface PaymentResult {
  lidm: string;
  success: string;
}

enum OrderStatus {
  Failed,
  Complete,
  Processing,
  Cancel,
  Deleted
}

enum OrderStatusText {
  '交易失敗',
  '完成',
  '處理中',
  '取消',
  '刪除'
}
