import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BetAmountService } from '../../services/bet-amount.service';
import { Betamount } from '../../models/bet-amount';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-bet-amount-edit',
  templateUrl: './bet-amount-edit.component.html',
  styleUrls: ['./bet-amount-edit.component.css']
})
export class BetAmountEditComponent implements OnInit {

  myForm: FormGroup;

  Betamount: Betamount[];

  id: number;

  type: number;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private location: Location,
    private betAmountService: BetAmountService,
    private route: ActivatedRoute,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) { this.setForm(); }
  }

  /**
   * 建立表單
   */
  groupForm() {
    this.myForm = this.fb.group({
      type: [null],
      amount: [null, Validators.min(0)],
      status: [1]
    });
  }

  /**
   * 設定表單
   */
  setForm() {
    this.betAmountService.getBetAmount(this.id).subscribe(
      res => {
        const data = res.data;
        this.type = data.type;
        this.myForm.patchValue(data);
      }
    );
  }

  /**
   * 送出表單
   */
  submitForm(val) {
    val.id = this.id;
    val.type = Number(this.type);
    if (this.myForm.valid) {
      this.betAmountService.updateBetAmount(val).subscribe(
        () => {
          alert(this.translateService.instant('common.updateSuccess'));
          this.location.back();
        },
        err => alert(this.translateService.instant('common.updateFailed'))
      );
    }
  }

  cancel() {
    this.location.back();
  }

}
