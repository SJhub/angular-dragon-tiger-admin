import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetAmountEditComponent } from './bet-amount-edit.component';

describe('BetAmountEditComponent', () => {
  let component: BetAmountEditComponent;
  let fixture: ComponentFixture<BetAmountEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetAmountEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetAmountEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
