import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateRecordListComponent } from './donate-record-list.component';

describe('DonateRecordListComponent', () => {
  let component: DonateRecordListComponent;
  let fixture: ComponentFixture<DonateRecordListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateRecordListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateRecordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
