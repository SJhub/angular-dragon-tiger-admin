import { MemberService } from './../../services/member.service';
import { GameType } from './../../models/game-type';
import { ItemType } from './../../models/item-type';
import { LimitType } from './../../models/limit-type';
import { GameActionType } from './../../models/game-action-type';
import { GameResultType } from './../../models/game-result-type';
import { GameStatusType } from './../../models/game-status-type';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BetRecord } from '../../models/bet-record';
import { BetRecordService } from '../../services/bet-record.service';
import { GameTypeService } from '../../services/game-type.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { Member } from '../../models/member.model';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { UserRole } from '../../enum/user-role.enum';
import { from, zip, of } from 'rxjs';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { DonateRecordsComponent } from '../../component/donate-records/donate-records.component';
import { addIndexToArray, defaultAgentID } from '../../common/global';
import { AuthenticationService } from '../../services/auth.service';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { formatDate } from '@angular/common';

const DEFAULT_AGENT_ID = defaultAgentID;
@Component({
  selector: 'app-donate-record-list',
  templateUrl: './donate-record-list.component.html',
  styleUrls: ['./donate-record-list.component.css']
})
export class DonateRecordListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['index', 'game_type', 'members_id', 'amount', 'records'];
  dataSource = new MatTableDataSource<BetRecord>();

  public data: BetRecord[];
  public modalRef: BsModalRef;
  public modal: any;

  betRecords: BetRecord[];
  gameTypes: GameType[];
  Member: Member[];

  gameTypesID = GameTypeEnum.GameTypeSicBo;
  identity: number;
  id: number;

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private betRecordService: BetRecordService,
    private modalService: BsModalService,
    private gameTypeService: GameTypeService,
    private memberService: MemberService,
    private authServ: AuthenticationService
  ) { }

  ngOnInit() {
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.id = Number(localStorage.getItem('authenticatedID'));

    // 分身帳號使用本尊ID進行查詢
    if (this.authServ.getUserCloneId()) {
      this.id = Number(this.authServ.getUserCloneId());
    }

    this.getGameTypes();
    if (this.identity === UserRole.admin) { this.getMemberList(); }

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getGameTypes() {
    this.gameTypeService.getGameTypes()
      .subscribe(
        res => {
          this.gameTypes = res.data;
          this.getDonateRecords();
        },
        err => console.log(err)
      );
  }

  getDonateRecords() {
    if (this.identity === UserRole.admin) {
      this.getBetRecords();
    } else {
      this.getBetRecordByGameTypeAndAgent(this.gameTypesID, this.id);
    }
  }

  filterDonataRecords(data: BetRecord[]): any {
    return data.filter(it => it['action'] === GameActionTypeEnum.Donate && it.member.agent_id !== DEFAULT_AGENT_ID);
  }

  /**
   * 取得押注紀錄列表 - 動作"打賞"
   */
  getBetRecords() {
    const body = this.getSearchBody();
    this.betRecordService.getBetRecordsSearchByDate(body)
      .subscribe(
        res => {
          this.data = this.filterDonataRecords(res.data);
          this.betRecords = this.data;
          let data = this.findBetRecordsByGameTypesID(this.gameTypesID);
          data = this.groupDonateRecords(data);
          this.dataSource.data = addIndexToArray(data);
        },
        error => console.log(error)
      );
  }

  /**
   * 取得押注記錄資料 by 代理 ID - 動作"打賞"
   * @param id
   */
  getBetRecordByGameTypeAndAgent(gameTypeId: number, agentsId: number) {
    const body = this.getSearchBody();
    body['game_types_id'] = gameTypeId;
    body['agents_id'] = agentsId;
    this.betRecordService.getBetRecordsSeachByAgentAndDate(body).subscribe(
      res => {
        this.data = this.filterDonataRecords(res.data);
        this.betRecords = this.data;
        let data = this.findBetRecordsByGameTypesID(this.gameTypesID);
        data = this.groupDonateRecords(data);
        this.dataSource.data = addIndexToArray(data);
      },
      error => console.log(error)
    );
  }

  getSearchBody() {
    const now = new Date();
    const endDate = formatDate(new Date().setDate(now.getDate() + 1), 'yyyy-MM-dd', 'en-US');
    const body = {
      game_types_id: this.gameTypesID,
      action: GameActionTypeEnum.Donate,
      start_date: '2010-01-01',
      end_date: endDate
    }
    return body;
  }

  openModal(record: BetRecord) {
    const donateRecords = this.betRecords.filter(e => e.members_id === record.members_id);
    this.modalService.show(DonateRecordsComponent, {
      initialState: { donateRecords, member: record.member }, class: 'modal-xl'
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * getGameTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameTypeByID(id);
    // return GameType.getNameByID(id);
  }

  /**
   * getItemTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getItemTypeByID(id: number) {
    return ItemType.getNameByID(id);
  }

  /**
   * getLimitTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getLimitTypeByID(id: number) {
    return LimitType.getNameByID(id);
  }

  /**
   * getGameActionTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameActionTypeByID(id: number) {
    return GameActionType.getNameByID(id);
  }

  /**
   * getGameResultTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameResultTypeByID(id: number) {
    return GameResultType.getNameByID(id);
  }

  /**
   * getGameStatusType - 用狀態 轉換 名稱
   * @param status
   */
  getGameStatusType(status: number) {
    return GameStatusType.getName(status);
  }

  /**
   * 取得會員列表
   */
  getMemberList() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.Member = res.data;
      }
    );
  }

  /**
   * getMemberInfo - 用ID取得會員資料
   * @param member_id
   */
  getMemberInfo(member_id: number): string {
    const memberInfo = this.Member.find(e => e.id === member_id);
    if (memberInfo) { return `${memberInfo.account}/${memberInfo.name}`; }
    return '查無資料';
  }

  /**
     * find bet records by game types id
     * @param id
     */
  findBetRecordsByGameTypesID(id: number): any[] {
    if (id <= 0) {
      return [];
    }

    if (this.betRecords.length <= 0) {
      return [];
    }

    const data = this.betRecords.filter(
      it => it.game_types_id === Number(id)
    );

    return data;
  }

  /**
   * gameTypeOnChange - when select opetion change
   * @param gameTypesID
   */
  gameTypeOnChange(gameTypesID: number) {
    this.gameTypesID = Number(gameTypesID);
    this.getDonateRecords();
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  groupDonateRecords(records: BetRecord[]) {
    const groupedRecords: BetRecord[] = [];

    from(records)
      .pipe(
        groupBy(
          record => record.members_id,
          r => r
        ),
        mergeMap(group => zip(of(group.key), group.pipe(toArray())))
      )
      .subscribe(g => {
        const joinedRecord = this.joinDonateRecords(g[1]);
        groupedRecords.push(joinedRecord);
      });

    return groupedRecords;
  }

  joinDonateRecords(records: BetRecord[]): BetRecord {
    const first = Object.assign({}, records.pop());
    records.forEach(r => first.amount += r.amount);
    return first;
  }
}
