import { UserService } from './../../services/user.service';
import { ActivatedRoute } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AgentAuthority } from '../../enum/agent-authority.enum';
import { commonStatusEnum } from '../../enum/common.status.enum';
import { DealersService } from '../../services/dealers.service';
import { AgentService } from '../../services/agent.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../../enum/user-role.enum';
import { StreamersMemberService } from '../../services/streamers-member.service';
import { MemberService } from '../../services/member.service';
import { Member } from '../../models/member.model';
import { GameTester } from '../../models/game-tester';
import { User } from '../../models/user';
import { UserWallet } from '../../models/user-wallet';
import { GameTypeEnum } from '../../enum/game-type.enum';

@Component({
  selector: 'app-agent-authority-edit',
  templateUrl: './agent-authority-edit.component.html',
  styleUrls: ['./agent-authority-edit.component.css']
})
export class AgentAuthorityEditComponent implements OnInit {

  myForm: FormGroup;
  member: Member;

  id: number;
  name: string;
  identity: number;
  dealerItem: any;
  loginUserID: number;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private userService: UserService,
    private dealersService: DealersService,
    private agentService: AgentService,
    private streamersMemberService: StreamersMemberService,
    private memberService: MemberService,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.loginUserID = Number(localStorage.getItem('authenticatedID'));
    if (this.id) {
      this.setForm(this.id);
      this.myForm.get('identity').disable();
    }
    this.changeRadio(AgentAuthority.DragonTiger);
    this.subscribeIdentity();
  }

  groupForm() {
    this.myForm = this.fb.group({
      'account': [null],
      'game_account': [null, Validators.pattern('^[A-Za-z0-9]+$')],
      'name': [null, [Validators.required, Validators.pattern('^[A-Za-z0-9]+$'), Validators.minLength(6)]],
      'real_name': [null, Validators.required],
      'password': [null, Validators.minLength(6)],
      're_password': [null],
      'wallet': [null],
      'identity': [2, Validators.required],
      'status': [1],
      'operation_items': new FormArray([new FormControl('24')]),
    });
  }

  setForm(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data: User = res.data;
        this.name = data.name;
        this.identity = data.identity;

        this.myForm.patchValue({
          account: data.account,
          name: data.name,
          real_name: data.real_name,
          identity: data.identity,
          status: data.status,
          wallet: data.wallet
        });

        if (data.identity === UserRole.admin && data.clone_id) {
          this.myForm.get('identity').setValue(UserRole.managementDemo);
        }

        this.myForm.get('name').disable();
        this.setChecked(data.operation_items);
      }
    );
  }

  submitForm(val) {

    let wallet = {
      poker_wallet: 0,
      poker_credit: 0,
      baccarat_credit: 0,
      baccarat_wallet: 0,
      sic_bo_credit: 0,
      sic_bo_wallet: 0
    } as UserWallet;

    val.user_wallet = JSON.stringify(wallet);
    val.member_wallet = JSON.stringify(wallet);

    if (!this.passwordMatches()) {
      alert(commonStatusEnum.PASSWORD_NOT_MATCH);
      return;
    }
    val = Object.assign({}, val);

    if (val.identity === UserRole.gametester) {
      return this.addGameTester(val);
    }

    if (this.myForm.valid) {
      if (!this.id) {
        if (val.identity === UserRole.dealers || val.identity === UserRole.staff) {
          val.parent_id = this.loginUserID;
          val.operation_items = JSON.stringify(val.operation_items);
          let req = val.identity === UserRole.dealers ? this.dealersService.addDealers(val) : this.dealersService.addStaff(val);
          req.subscribe(
            () => {
              alert(this.translateService.instant('common.addSuccess'));
              this.location.back();
            },
            err => alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`)
          );
        } else if (val.identity === UserRole.streamer) {
          val.parent_id = this.loginUserID;
          val.account = val.name;
          val.name = val.name;
          val.nickname = val.name;
          val.gender = 'MALE';
          val.operation_items = JSON.stringify(val.operation_items);
          this.streamersMemberService.addStreamersMember(val).subscribe(
            () => {
              this.streamersMemberService.addStreamersUser(val).subscribe(
                () => {
                  alert('新增成功');
                  this.location.back();
                },
                err => {
                  alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`)
                }
              );
            },
            err => {
              alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`)
            }
          );
        } else {
          val.credit = val.wallet;
          val.parent_id = this.loginUserID;
          val.operation_items = JSON.stringify(val.operation_items);
          val.status = val.status ? 1 : 0;
          if (val.identity === UserRole.managementDemo) {
            val.identity = UserRole.admin;
            val.clone_id = this.loginUserID;
          }
          this.userService.addUser(val).subscribe(
            () => {
              alert(this.translateService.instant('common.addSuccess'));
              this.location.back();
            },
            err => alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`)
          );
        }
      } else {
        if (val.identity === UserRole.dealers || val.identity === UserRole.staff) {

          if (this.identity === UserRole.admin || this.identity === UserRole.management) {
            alert('無法切換荷官或直播主');
            return;
          }

          val.id = this.id;
          val.operation_items = JSON.stringify(val.operation_items);
          val.status = Number(val.status);
          let req = val.identity === UserRole.dealers ? this.dealersService.updateDealers(val) : this.dealersService.updateStaff(val)
          req.subscribe(
            () => {
              alert('修改成功');
              this.location.back();
            },
            err => {
              alert('修改失敗');
            }
          );
        } else if (val.identity === UserRole.streamer) {

          if (this.identity === UserRole.admin || this.identity === UserRole.management) {
            alert('無法切換荷官或直播主');
            return;
          }

          val.id = this.id;
          val.gender = 'MALE';
          val.account = val.name;
          val.operation_items = JSON.stringify(val.operation_items);
          val.nickname = val.real_name;
          val.status = Number(val.status);
          this.streamersMemberService.updateStreamersMember(val).subscribe(
            () => {
              this.streamersMemberService.updateStreamersUser(val).subscribe(
                () => {
                  alert('修改成功');
                  this.location.back();
                },
                err => {
                  alert('修改失敗');
                }
              );
            },
            err => {
              alert('修改失敗');
            }
          );
        } else {
          val.id = this.id;
          val.name = this.name;
          val.credit = val.wallet;
          val.operation_items = JSON.stringify(val.operation_items);
          val.status = val.status ? 1 : 0;
          if (val.identity === UserRole.managementDemo) {
            val.identity = UserRole.admin;
            val.clone_id = this.loginUserID;
          }
          this.userService.updateUser(val).subscribe(
            () => {
              alert(this.translateService.instant('common.editSuccess'));
              this.location.back();
            },
            err => alert(this.translateService.instant('common.editFailed'))
          );
        }
      }



    } else {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      this.markFormGroupTouched(this.myForm);
    }
  }

  /**
   * 兩次密碼不一致
   */
  passwordMatches() {
    const password = this.myForm.get('password').value;
    const repassword = this.myForm.get('re_password').value;

    if (!password && !repassword) {
      return true;
    }

    if (password !== repassword) {
      return false;
    }

    return true;
  }

  /**
  * 表單驗證(必填欄位提示)
  */
  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  cancel() {
    this.location.back();
  }

  get AgentAuthority() {
    return AgentAuthority;
  }

  get UserRole() {
    return UserRole;
  }

  changeRadio(event) {

    const selectGameType: FormArray = this.myForm.get('operation_items') as FormArray;
    if (this.myForm.get('identity').value === UserRole.dealers) {
      selectGameType.clear();
      selectGameType.push(new FormControl(event.target.value));
    }
    // console.log(event.target);
  }

  /**
   * 勾選checkbox
   * @param checkboxValue
   */
  onCheckboxChange(checkboxValue) {
    const authmanageID: FormArray = this.myForm.get('operation_items') as FormArray;

    if (this.myForm.get('identity').value === 5) {
      authmanageID.clear();
    }

    if (checkboxValue.target.checked) {
      authmanageID.push(new FormControl(checkboxValue.target.value));
    } else {
      let i = 0;
      authmanageID.controls.forEach((item: FormControl) => {
        if (item.value === checkboxValue.target.value) {
          authmanageID.removeAt(i);
          return;
        }
        i++;
      });
    }
    // console.log(authmanageID.value);
  }

  /**
   * 取得checkbox的值
   * @param authmanagementID
   */
  getChecked(authmanagementID) {
    const authmanageID = this.myForm.get('operation_items').value as Array<any>;
    // console.log(authmanagementID);

    return authmanageID.includes(String(authmanagementID));
  }

  /**
   * 把值設定進去
   * @param auth_managementID
   */
  setChecked(auth_managementID: string) {
    if (auth_managementID === 'null') {
      return;
    }
    // console.log(auth_managementID)
    const items = JSON.parse(auth_managementID) as Array<string>;
    const authmanageID = this.myForm.get('operation_items') as FormArray;

    items.forEach(item => {
      authmanageID.push(new FormControl(item));
    });
  }

  subscribeIdentity() {
    this.myForm.get('identity').valueChanges.subscribe(e => {
      const authmanageID: FormArray = this.myForm.get('operation_items') as FormArray;
      authmanageID.clear();
    });
  }

  addGameTester(val: any) {
    const body = {
      account: val.game_account,
      name: val.real_name,
      nickname: val.real_name,
      gender: 'MALE',
      password: val.password
    } as GameTester;

    if (Object.values(body).some(e => !e) || body.password.length < 6) {
      alert(this.translateService.instant('common.inputErrorsPleaseReEenter'));
      return this.markFormGroupTouched(this.myForm);
    }

    this.memberService.addGameTester(body).subscribe(
      () => {
        alert(this.translateService.instant('common.addSuccess'));
        this.location.back();
      },
      err => alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`));
  }

  get GameTypeEnum() { return GameTypeEnum }
}
