import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentAuthorityEditComponent } from './agent-authority-edit.component';

describe('AgentAuthorityEditComponent', () => {
  let component: AgentAuthorityEditComponent;
  let fixture: ComponentFixture<AgentAuthorityEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentAuthorityEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentAuthorityEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
