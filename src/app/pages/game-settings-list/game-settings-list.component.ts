import { GameType } from './../../models/game-type';
import { ItemType } from './../../models/item-type';
import { LimitType } from './../../models/limit-type';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { GameSetting } from '../../models/game-setting';
import { GameSettingService } from '../../services/game-setting.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { GameTypeService } from '../../services/game-type.service';


@Component({
  selector: 'app-game-settings-list',
  templateUrl: './game-settings-list.component.html',
  styleUrls: ['./game-settings-list.component.css']
})
export class GameSettingsListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // displayedColumns: string[] = ['game_type', 'item_type', 'limit_type', 'amount', 'created_at', 'updated_at', 'function'];
  displayedColumns: string[] = ['id', 'game_type', 'item_type', 'limit_type', 'amount', 'function'];
  dataSource = new MatTableDataSource<GameSetting>();

  public data: GameSetting[];
  public GameType: GameType[];

  public modalRef: BsModalRef;
  public modal: any;

  gameTypeID: number;

  constructor(
    private gameSettingService: GameSettingService,
    private modalService: BsModalService,
    private gameTypeService: GameTypeService,
  ) { }

  ngOnInit() {
    this.selectGameType(1);
    this.getGameTypesList();

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getGameSettings() {
    this.gameSettingService.getGameSettings()
      .subscribe(
        res => {
          this.data = res.data;
          this.dataSource.data = this.data.filter(e => e.game_type === Number(this.gameTypeID));
        },
        error => console.log(error)
      );
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.gameSettingService.getGameSetting(id)
      .subscribe(res => {
        this.modal = res.data as GameSetting;
        this.modalRef = this.modalService.show(template);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * getGameTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameTypeByID(id: number) {
    return GameType.getNameByID(id);
  }

  /**
   * getItemTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getItemTypeByID(id: number) {
    return ItemType.getNameByID(id);
  }

  /**
   * getLimitTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getLimitTypeByID(id: number) {
    return LimitType.getNameByID(id);
  }

  /**
   * getGameTypesList()
   */

  getGameTypesList() {
    this.gameTypeService.getGameTypes().subscribe(
      res => {
        this.GameType = res.data;
      }
    );
  }

  /**
   * gameTypeID 取得gameType ID
   */
  selectGameType(gameTypeID: number) {
    this.gameTypeID = gameTypeID;
    this.getGameSettings();
  }
}
