import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSettingsListComponent } from './game-settings-list.component';

describe('GameSettingsListComponent', () => {
  let component: GameSettingsListComponent;
  let fixture: ComponentFixture<GameSettingsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSettingsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSettingsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
