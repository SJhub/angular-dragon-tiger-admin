import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MessageBlacklist } from '../../models/message-blacklist';
import { MessageBlacklistService } from '../../services/message-blacklist.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';

@Component({
  selector: 'app-message-blacklist',
  templateUrl: './message-blacklist.component.html',
  styleUrls: ['./message-blacklist.component.css']
})
export class MessageBlacklistComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['index', 'message', 'created_at', 'updated_at', 'function'];
  dataSource = new MatTableDataSource<MessageBlacklist>();

  public data: MessageBlacklist[];
  public modalRef: BsModalRef;
  public modal: any;

  constructor(
    private translateService: TranslateService,
    private messageBlacklistService: MessageBlacklistService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.getMessageBlacklists();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getMessageBlacklists() {
    this.messageBlacklistService.getMessageBlacklists()
      .subscribe(
        res => {
          this.data = res.data;
          this.dataSource.data = addIndexToArray(this.data);
        },
        error => console.log(error)
      );
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.messageBlacklistService.getMessageBlacklist(id)
      .subscribe(res => {
        this.modal = res.data as MessageBlacklist;
        this.modalRef = this.modalService.show(template);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addMessageBlacklist(message: string) {
    this.messageBlacklistService.addMessageBlacklist({ message })
      .subscribe(
        () => {
          this.getMessageBlacklists();
          alert(this.translateService.instant('common.addSuccess'));
        },
        err => {
          alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`);
        }
      );
  }

  delMessageBlacklist(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.messageBlacklistService.deleteMessageBlacklist(id)
        .subscribe(
          () => {
            this.getMessageBlacklists();
            alert(this.translateService.instant('common.deleteSuccess'));
          },
          err => {
            alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
          }
        );
    }
  }

}
