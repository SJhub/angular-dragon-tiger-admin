import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageBlacklistComponent } from './message-blacklist.component';

describe('MessageBlacklistComponent', () => {
  let component: MessageBlacklistComponent;
  let fixture: ComponentFixture<MessageBlacklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageBlacklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageBlacklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
