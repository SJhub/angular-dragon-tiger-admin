import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcItemResultRecordListComponent } from './calc-item-result-record-list.component';

describe('CalcItemResultRecordListComponent', () => {
  let component: CalcItemResultRecordListComponent;
  let fixture: ComponentFixture<CalcItemResultRecordListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcItemResultRecordListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcItemResultRecordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
