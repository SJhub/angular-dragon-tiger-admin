import { GameType } from './../../models/game-type';
import { ItemType } from './../../models/item-type';
import { LimitType } from './../../models/limit-type';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CalcItemResultRecord } from '../../models/calc-item-result-record';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { GameTypeService } from '../../services/game-type.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { BetRecordService } from '../../services/bet-record.service';
import { BetRecord } from '../../models/bet-record';
import { ItemTypes } from '../../models/item-types';
import { ItemTypesService } from '../../services/item-types.service';
import { PlayGameService } from '../../services/play-game.service';
import { switchMap } from 'rxjs/operators';
import { PlayGame } from '../../models/play-game';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../../enum/user-role.enum';
import { addIndexToArray, baccaratDrawResultStr, baccaratPokerResultStr, defaultAgentID, getDrawResultDetect, getDrawResultToBetItem, isDrawResultDifferent } from '../../common/global';
import { CalcItemRecordPanelComponent } from '../../component/calc-item-record-panel/calc-item-record-panel.component';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { formatDate } from '@angular/common';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { AuthenticationService } from '../../services/auth.service';
import { AiResultComponent } from '../../component/ai-result/ai-result.component';
import { getFormatedDateStr } from '../../common/common';
import { getToday, getTimeRangeByDate } from '../../common/date';

const CROSSING_HOUR = 12; // 以中午12時為基準取資料
@Component({
  selector: 'app-calc-item-result-record-list',
  templateUrl: './calc-item-result-record-list.component.html',
  styleUrls: ['./calc-item-result-record-list.component.css']
})
export class CalcItemResultRecordListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['index', 'game_type', 'game_number', 'play_game', 'game_result_amount', 'active_total_amount'];
  dataSource = new MatTableDataSource<CalcItemResultRecord>();

  public data: CalcItemResultRecord[];
  public modalRef: BsModalRef;
  public modal: any;

  gameTypesID = GameTypeEnum.GameTypeDragonTiger;
  calcItemResultRecords: CalcItemResultRecord[];
  gameTypes: GameType[];
  BetRecord: BetRecord[];
  ItemTypes: ItemTypes[];
  playGames: PlayGame[];

  id: number;
  identity: number;
  countByIndex: number;
  countByFirst: number;

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private calcItemResultRecordService: CalcItemResultRecordService,
    private modalService: BsModalService,
    private gameTypeService: GameTypeService,
    private betRecordService: BetRecordService,
    private itemTypesService: ItemTypesService,
    private playGameService: PlayGameService,
    private authServ: AuthenticationService
  ) { }

  ngOnInit() {
    this.id = Number(localStorage.getItem('authenticatedID'));
    this.identity = Number(localStorage.getItem('loginUserIdentity'));

    // 分身帳號使用本尊ID進行查詢
    if (this.authServ.getUserCloneId()) {
      this.id = Number(this.authServ.getUserCloneId());
    }

    this.getGameTypes();
    this.getItemTypesList();

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getGameTypes() {
    this.gameTypeService.getGameTypes()
      .subscribe(
        res => {
          this.gameTypes = res.data;
          this.getCalcItemResultRecords(this.gameTypesID);
        },
        err => console.log(err)
      );
  }

  getCalcItemResultRecords(gameTypeID: number) {
    let betRecords: BetRecord[] = null;

    const today = getToday();
    const range = getTimeRangeByDate(today.st);

    // 星期一則以星期日中午12點開始
    let start = new Date(today.st);
    if (start.getDay() == 1) {
      start.setDate(start.getDate() - 1);
      today.st = getFormatedDateStr(start, 'yyyy-MM-dd');
    }

    const st = `${today.st} ${range.st}`;
    const et = getFormatedDateStr(new Date(), 'yyyy-MM-dd HH:mm:ss');


    const data = {
      game_types_id: gameTypeID,
      start_date: st,
      end_date: et,
      action: GameActionTypeEnum.Bet,
      agents_id: this.id
    }

    let betApi = this.betRecordService.getBetRecordsSearchByDate(data);

    if (this.identity !== UserRole.admin) {
      betApi = this.betRecordService.getBetRecordsSeachByAgentAndDate(data);
    }

    betApi.pipe(
      switchMap(res => {
        const records: BetRecord[] = res.data;
        betRecords = records.filter(e => e.member.agent_id !== defaultAgentID);
        return this.playGameService.getPlayGameBetweenDate(gameTypeID, st, et);
      }),
      switchMap(res => {
        const playGames: PlayGame[] = res.data;
        this.playGames = this.calcPlayGameResultAmount(playGames, betRecords);
        return this.calcItemResultRecordService.getCalcItemResultRecordByTypeBetweenDate(gameTypeID, st, et);
      })
    ).subscribe(res => {
      this.data = res.data;
      this.calcItemResultRecords = this.data;
      this.data.map(e => {
        e.play_game = this.playGames.find(item => item.game_number === e.game_number);
        if (e.play_game) {
          if (e.play_game.draw_result === '000' || !e.item_types_ids.length) {
            return e.item_types_ids = this.translateService.instant('invalidDrawResult');
          }

          e.use_detect = 0;

          switch (e.play_game.game_types_id) {
            case GameTypeEnum.GameTypeSicBo:
              e.item_types_ids = this.getItemTypesName(getDrawResultToBetItem(e.play_game.draw_result));
              break;
            case GameTypeEnum.GameTypeBaccarat:
              e.item_types_ids = baccaratDrawResultStr(JSON.parse(e.play_game.draw_result), this.translateService, true);
              e.use_detect = getDrawResultDetect(e.play_game.draw_result);
          }
        }
      });
      const data = this.findCalcItemResultRecordsByGameTypesID(gameTypeID);
      this.dataSource.data = addIndexToArray(data);
    });

  }

  calcPlayGameResultAmount(playGame: PlayGame[], betRecords: BetRecord[]) {
    playGame.map(e => {
      let gameResultAmount = 0;
      let totalAmount = 0;
      const records = betRecords.filter(r => r.game_number === e.game_number);
      records.forEach(r => {
        gameResultAmount += r.game_result_amount;
        totalAmount += r.amount;
      });
      // 輸贏結果
      e.game_result_amount = gameResultAmount;
      // 總投注
      e.total_amount = totalAmount;
    });
    return playGame;
  }

  /**
   * 取得項目列表
   */
  getItemTypesList() {
    this.itemTypesService.getItemTypesList().subscribe(
      res => {
        this.ItemTypes = res.data;
        // console.log(this.ItemTypes);
      }
    );
  }

  /**
   * 取得遊戲項目中文名稱
   * @param
   * @param item_types_ids
   */
  getItemTypesName(item_types_ids: any): Array<string> {
    item_types_ids = item_types_ids.sort((a, b) => a - b);
    item_types_ids = item_types_ids.map(item => {
      const itemTypesName = this.ItemTypes.find(e => e.id === item);
      if (itemTypesName) { return this.enumTranslateServ.getTranslatedBetItem(itemTypesName.en_name); }
      return this.translateService.instant('common.noData');
    });
    return item_types_ids;
  }

  openModal(template: TemplateRef<any>, id: number) {
    this.calcItemResultRecordService.getCalcItemResultRecord(id)
      .subscribe(res => {
        this.modal = res.data as CalcItemResultRecord;
        this.modalRef = this.modalService.show(template);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * getGameTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameTypeByID(id);
    // return GameType.getNameByID(id);
  }

  /**
   * getItemTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getItemTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedBetItem(ItemType.getNameByID(id));
  }

  /**
   * getLimitTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getLimitTypeByID(id: number) {
    return LimitType.getNameByID(id);
  }

  /**
   * getGameActionTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameActionTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedAction(id);
    // return GameActionType.getNameByID(id);
  }

  /**
   * getGameResultTypeByID - 用 ID 轉換 名稱
   * @param id
   */
  getGameResultTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameResultType(id);
    // return GameResultType.getNameByID(id);
  }

  /**
   * 取得押注紀錄列表
   */
  getBetRecordList() {
    this.betRecordService.getBetRecords().subscribe(
      res => {
        this.BetRecord = res.data;
        // console.log(this.BetRecord);
      }
    );
  }

  /**
   * getGameNumber - 取得遊戲局號
   * @param game_number
   */
  getGameNumber(game_number: number): string {
    const gameNumber = this.BetRecord.find(e => e.game_number === game_number);
    if (gameNumber) { return gameNumber.draw_result; }
    return this.translateService.instant('common.noData');
  }

  // 改變開獎結果顏色by ID
  changeResultByID(element) {
    let classStr = null;
    if (this.identity === UserRole.admin) {
      const playGame: PlayGame = element.play_game;
      if (!playGame) {
        return classStr;
      }

      if (playGame.game_types_id != GameTypeEnum.GameTypeSicBo) {
        return classStr;
      }

      const index = this.dataSource.data.findIndex(e => e.game_number === playGame.game_number);

      if (index === -1) {
        return classStr;
      }

      const lastIndex = this.dataSource.data.length - 1;
      const self = playGame.draw_result.split('');
      let next: Array<string> = null;
      let prev: Array<string> = null;
      if (index != lastIndex) {
        const nextPlayGame = this.dataSource.data[index + 1];
        if (nextPlayGame.play_game) {
          next = nextPlayGame.play_game.draw_result.split('');
          const tmp = this.findSameResult(self, next);
          if (tmp) {
            classStr = tmp;
          }
        }
      }

      if (index) {
        const prevPlayGame = this.dataSource.data[index - 1];
        if (prevPlayGame.play_game) {
          prev = prevPlayGame.play_game?.draw_result.split('');
          const tmp = this.findSameResult(self, prev);
          if (tmp) {
            classStr = tmp;
          }
        }
      }
    }
    return classStr;
  }

  findSameResult(self: Array<string>, target: Array<string>) {
    let tmp = target;
    let times = 0;

    if (self.includes('0')) {
      return;
    }

    self.forEach(e => {
      const index = tmp.indexOf(e);
      if (index != -1) {
        tmp.splice(index, 1);
        times += 1;
      }
    });

    if (times === 3) {
      return 'same-result';
    }

    if (times >= 2) {
      return 'text-danger';
    }
  }

  // 字串切割排序完再組合成陣列
  sortByNumber(value) {
    if (value) {
      const number = value.split('');
      number.sort((x, y) => x - y);
      const newNumber = number.join('');
      return Array.of(newNumber);
    }
  }

  /**
     * find records by game types id
     * @param id
     */
  findCalcItemResultRecordsByGameTypesID(id: number): any[] {
    if (id <= 0) {
      return [];
    }

    if (this.calcItemResultRecords.length <= 0) {
      return [];
    }

    const data = this.calcItemResultRecords.filter(
      it => it.game_types_id === Number(id)
    );

    return data;
  }

  /**
   * gameTypeOnChange - when select opetion change
   * @param gameTypesID
   */
  gameTypeOnChange(gameTypesID: string) {
    this.getCalcItemResultRecords(Number(gameTypesID));
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  openCalcItemRecord(gameNumber: number) {
    const gameTypesID = this.gameTypesID;
    if (this.identity === UserRole.admin) {
      this.modalService.show(CalcItemRecordPanelComponent, { initialState: { gameNumber, gameTypesID }, class: 'modal-xl' });
    }
  }

  openAiResult(playGame: PlayGame) {
    if (this.identity === UserRole.admin) {
      this.modalService.show(AiResultComponent, { initialState: { playGame }, class: 'modal-xl' });
    }
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }

  showInvalidGame(show: boolean) {
    if (show) {
      return this.dataSource.data = this.data.filter(e => e.play_game?.draw_result === '000')
    }
    return this.dataSource.data = this.data;
  }

  isDrawResultDifferent(drawResult: string) {
    return isDrawResultDifferent(drawResult);
  }

  get UserRole() { return UserRole }

  get GameTypeEnum() { return GameTypeEnum }
}
