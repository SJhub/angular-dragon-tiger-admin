import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRecordMemberListComponent } from './report-record-member-list.component';

describe('ReportRecordMemberListComponent', () => {
  let component: ReportRecordMemberListComponent;
  let fixture: ComponentFixture<ReportRecordMemberListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportRecordMemberListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRecordMemberListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
