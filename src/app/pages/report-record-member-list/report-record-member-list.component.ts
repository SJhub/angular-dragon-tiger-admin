import { UserRole } from './../../enum/user-role.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Member } from '../../models/member.model';
import { MemberService } from '../../services/member.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Status } from '../../enum/status.enum';
import { BetRecordService } from '../../services/bet-record.service';
import { BetRecord } from '../../models/bet-record';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { GameTypeService } from '../../services/game-type.service';
import { GameType } from '../../models/game-type';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { CalcItemResultRecord } from '../../models/calc-item-result-record';
import { ItemTypesService } from '../../services/item-types.service';
import { ItemTypes } from '../../models/item-types';
import { GameResultType } from '../../models/game-result-type';
import { AgentService } from '../../services/agent.service';
import { formatDate, Location } from '@angular/common';
import { User } from '../../models/user';
import { CurrentReportsSicboService } from '../../services/current-reports-sicbo.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../services/user.service';
import { SettlementRecord } from '../../models/settlement-record';
import { SettlementRecordsService } from '../../services/settlement-records.service';
import { CalcOwnProfitByID, CalcOwnRecord, getLastCalcDay, isDateRangeIncludedToday, isMonthFirstDayBeforeCalc, isWeekFirstDayBeforeCalc, MemberJoinRecords, UsersJoinRecordWithSettlementCalc } from '../../common/report';
import { ReportByDateRangeComponent } from '../../component/report-by-date-range/report-by-date-range.component';
import { ReportByDateRangeMemberComponent } from '../../component/report-by-date-range-member/report-by-date-range-member.component';
import { map, switchMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { GameTypeEnum } from '../../enum/game-type.enum';

@Component({
  selector: 'app-report-record-member-list',
  templateUrl: './report-record-member-list.component.html',
  styleUrls: ['./report-record-member-list.component.css']
})
export class ReportRecordMemberListComponent implements OnInit {
  @ViewChild('sortByAgent', { static: true }) sortByAgent: MatSort;
  @ViewChild('paginatorOfAgent', { static: true }) paginatorOfAgent: MatPaginator;

  @ViewChild('sortByMember', { static: true }) sortByMember: MatSort;
  @ViewChild('paginatorOfMember', { static: true }) paginatorOfMember: MatPaginator;

  displayedColumnsByAgent: string[] = ['name', 'account', 'total_amount', 'number_of_game',
    'game_result_amount', 'revenue_share', 'settlement_amount', 'is_allow_play', 'status', 'function'];

  displayedColumnsByMember: string[] = ['name', 'members_id', 'account', 'total_amount', 'number_of_game',
    'game_result_amount', 'settlement_amount', 'is_allow_play', 'status', 'records'];

  dataSourceOfMember = new MatTableDataSource<Member>();
  dataSourceOfBetRecord = new MatTableDataSource<BetRecord>();
  dataSourceOfAgent = new MatTableDataSource<User>();

  modal: BetRecord;
  modalRef: BsModalRef;

  Member: Member[];
  GameType: GameType[];
  CalcItemResultRecord: CalcItemResultRecord[];
  ItemTypes: ItemTypes[];

  currentUser: User;
  parrentUser: User;
  agentID: number;
  gameTypeID = GameTypeEnum.GameTypeSicBo;
  numberOfBoard: number;
  totalBetRecord: number;

  membersId: any;
  dataArray: Array<number>;
  BetRecords: any;
  resultGameNumber: any;

  startDateValue: any;
  endDateValue: any;

  dataOfResultAmountTotal: any;
  dataObjectKeys: any;
  dataObjectValues: any;
  data: any;
  CalcItemResultRecordByDate: any;

  memberTotalAmount: number;
  memberActiveTotalAmount: number;
  memberGameNumber: number;
  memberGameResultAmount: number;
  memberWashCodeCommissionPercent: number;
  memberWashCodeCommission: number;
  memberTotalDonate: any;
  memberSettlementAmount: any;

  agentTotalAmount: any;
  agentActiveTotalAmount: any;
  agentNumberOfGame: any;
  agentGameResultAmount: any;
  agentWashCodeCommission: any;
  agentWashCodeCommissionPercent: any;
  agentRevenueShare: any;
  agentTotalDonate: any;
  agentTotalSettlementAmount: any;
  agentSettlementAmount: any;
  memberOfSettlementAmount: any;
  theTopSettlementAmount: any;
  selfProfit: any;

  totalAmount: any;
  activeTotalAmount: any;
  totalGameNumber: any;
  totalGameResultAmount: any;
  totalWashCodeCommission: any;
  totalWashCodeCommissionPercent: any;
  totalRevenueShare: any;
  totalDonate: any;
  totalSettlementAmount: any;
  totalMemberSettlementAmount = 0;
  totalAgentSettlementAmount = 0;
  totalTopSettlementAmount: any;
  totalSelfProfit: any;

  agentIdByReportId: number;

  dataAgent = false;
  dataMember = false;

  agentStatus: number;
  memberStatus: number;
  identity: number;
  userID: number;
  agentChild: any;
  memberChild: any;
  active: boolean;
  click: boolean;
  yes: string;
  newday: string;

  now = new Date();
  nowDayOfWeek = this.now.getDay() - 1;
  nowDay = this.now.getDate();
  nowMonth = this.now.getMonth();
  nowYear = this.now.getFullYear();

  settlementRecords: SettlementRecord[];
  realTimeRecords: any;

  isAdminClone = false;

  submitted = false;

  constructor(
    private settlementRecordServ: SettlementRecordsService,
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private memberService: MemberService,
    private betRecordService: BetRecordService,
    private gameTypeService: GameTypeService,
    private calcItemResultRecordService: CalcItemResultRecordService,
    private itemTypesService: ItemTypesService,
    private agentService: AgentService,
    private currentReportsSicboService: CurrentReportsSicboService,
    private userService: UserService,
    private location: Location,
    private router: Router,
    private auth: AuthenticationService
  ) {
    const gameTypeID = this.route.snapshot.queryParamMap.get('g');
    gameTypeID && this.selectGameType(Number(gameTypeID), false);
  }

  ngOnInit(): void {
    this.agentID = Number(this.route.snapshot.paramMap.get('id'));
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.userID = Number(this.auth.getUserId());
    this.isAdminClone = this.identity === UserRole.admin && Number(this.auth.getUserCloneId()) !== 0;

    this.getGameTypeList();
    this.getItemTypesList();
    // this.getUsersByParentID(this.agentID);
    this.getMembersByParentID(this.agentID);
    this.getSettlementRecords();

    this.dataSourceOfAgent.paginator = this.paginatorOfAgent;
    this.dataSourceOfAgent.sort = this.sortByAgent;
    this.dataSourceOfMember.paginator = this.paginatorOfMember;
    this.dataSourceOfMember.sort = this.sortByMember;

    this.dataSourceOfMember.filterPredicate = (data: Member, filter: string): boolean => {

      const name = this.getMemberName(data.members_id).includes(filter);
      const account = data.account.includes(filter);
      const total_amount = String(data.total_amount);
      const game_result_amount = String(data.game_result_amount).includes(filter);
      const number_of_game = String(data.number_of_game).includes(filter);
      const status = this.statusChange(data.status).includes(filter);
      return name || account || total_amount.includes(filter) || game_result_amount || number_of_game || status;
    };

    this.dataSourceOfAgent.filterPredicate = (data: User, filter: string): boolean => {
      const name = data.name.includes(filter);
      const account = data.account.includes(filter);
      const total_amount = String(data.total_amount).includes(filter);
      const platform_profit = String(data.platform_profit).includes(filter);
      const rebate_percent = String(data.rebate_percent).includes(filter);
      const rebate_amount = String(data.rebate_amount).includes(filter);
      const number_of_game = String(data.number_of_game).includes(filter);
      const game_result_amount = String(data.game_result_amount).includes(filter);
      const settlement_amount = String(data.settlement_amount).includes(filter);
      const status = this.statusChange(data.status).includes(filter);

      return name || account || total_amount || platform_profit || rebate_percent || rebate_amount || number_of_game ||
        game_result_amount || settlement_amount || status;
    };
  }

  subscribeParams() {
    this.route.params.subscribe(
      params => {

        this.dataSourceOfAgent.data = [];
        this.dataSourceOfMember.data = [];

        this.agentID = Number(params.id);
        this.startDateValue = params.startDate;
        this.endDateValue = params.endDate;
        this.getUsersByParentID(this.agentID);
        this.getMembersByParentID(this.agentID);
      }
    );
  }

  startSearchReport() {
    if (this.startDateValue && this.endDateValue) {
      const today = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
      if (today === this.startDateValue && today === this.endDateValue) {
        return this.today();
      }
      // 以上級代理ID 及開始及結束日期搜尋代理日報表
      const agentReportByParentID = {
        game_types_id: this.gameTypeID,
        start_date: this.startDateValue,
        end_date: this.endDateValue,
        parent_id: this.agentID,
      };

      // 以代理ID及開始及結束日期搜尋會員日報表
      const memberReportByAgentID = {
        game_types_id: this.gameTypeID,
        start_date: this.startDateValue,
        end_date: this.endDateValue,
        agents_id: this.agentID,
      };

      this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);
    }
  }

  selectGameType(gameTypeID: number, submit = true) {
    if (this.gameTypeID === GameTypeEnum.GameTypeRoulette) {
      return alert(this.translateService.instant('gameIsNotToMarket'));
    }

    this.gameTypeID = Number(gameTypeID);
    this.router.navigate([], {
      queryParams: { g: this.gameTypeID },
      queryParamsHandling: 'merge'
    });
    submit && this.btnOK();
  }

  startDate(startDateValue) {
    this.startDateValue = startDateValue;
  }

  endDate(endDateValue) {
    this.endDateValue = endDateValue;
  }

  // 取得自己底下的用戶By ID
  getUsersByParentID(id: number) {
    this.userService.getUserByParentID(id).subscribe(
      res => {
        const data = res.data as User[];
        this.agentChild = data.filter(e => !e.clone_id);
        // console.log(data);
      }
    );

    this.userService.getUser(id)
      .pipe(switchMap(r => {
        this.currentUser = r.data;
        this.parrentUser = null;
        this.startSearchReport();
        return this.userService.getUser(this.currentUser.parent_id);
      }))
      .subscribe(r => {
        if (this.currentUser.identity !== UserRole.topAgent) {
          this.parrentUser = r.data;
        }
      });
  }

  // 取得agent status By ID
  getAgentStatusByID(agents_id: number) {
    if (!this.agentChild) {
      return;
    }
    const agentStatus = this.agentChild.find(e => e.id === agents_id);
    if (agentStatus) {
      return agentStatus.status;
    }
  }

  // 取得自己底下的會員By ID
  getMembersByParentID(id: number) {
    let req = this.memberService.getMemberByAgent(id);
    req.subscribe(
      res => {
        this.memberChild = res.data;
      }
    );
  }

  // 取得會員的狀態 By ID
  getMemberStatusByID(members_id: number) {
    if (!this.memberChild) {
      return;
    }
    const memberStatus = this.memberChild.find(e => e.id === members_id);
    if (memberStatus) {
      return memberStatus.status;
    }
  }

  changeAgentStatus(user: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: user.id,
      status: user.status ? 0 : 1,
    };

    this.agentService.updateAgentStatus(data).subscribe(
      () => {
        user.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeMemberStatus(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      status: member.status ? 0 : 1
    };

    this.memberService.updateMemberStatus(data).subscribe(
      () => {
        member.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeAgentAllowGame(agent: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: agent.id,
      is_allow_play: agent.is_allow_play ? 0 : 1,
    };

    this.agentService.updateAgentIsAllowPlay(data).subscribe(
      () => {
        agent.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }
  /**
   * 以上級代理ID 及開始及結束日期搜尋代理日報表
   */
  getAgentDailyReportsSearchByTypeAndParentBetweenDate(body) {
    this.submitted = true;
    let req = [this.agentService.getAgentDailyReportsSearchByTypeAndParentBetweenDate(body)];

    if (isDateRangeIncludedToday(this.startDateValue, this.endDateValue)) {
      req.push(this.getCurrentReportRequest(this.gameTypeID));
    }

    forkJoin(req).pipe(map(res => res.map(r => r.data))).subscribe(
      res => {
        let data: User[] = res[0];
        this.dataAgent = true;

        data = data.filter(e => e.is_top);

        // 即時報表
        if (res[1]) {
          this.realTimeRecords = res[1];
          let agentRecords: User[] = this.realTimeRecords.agent_daily_report;
          agentRecords = agentRecords.filter(e => e.parent_id === this.agentID && e.is_top);
          data.push(...agentRecords);
        }

        this.dataSourceOfAgent.data = UsersJoinRecordWithSettlementCalc(
          data,
          this.agentChild,
          this.settlementRecords,
          this.startDateValue,
          this.endDateValue
        );


        const memberReportByAgentID = {
          game_types_id: this.gameTypeID,
          start_date: this.startDateValue,
          end_date: this.endDateValue,
          agents_id: this.agentID,
        };
        this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
      }
    );
  }

  /**
   * 以代理ID及開始及結束日期搜尋會員日報表
   */
  getMemberDailyReportsSearchByTypeAndAgentBetweenDate(data) {
    this.calcOwnProfit();
    this.memberService.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(data).subscribe(
      res => {
        const data = res.data;

        if (isDateRangeIncludedToday(this.startDateValue, this.endDateValue) && this.realTimeRecords) {
          data.push(...this.realTimeRecords.member_daily_report);
        }

        this.dataSourceOfMember.data = MemberJoinRecords(data, this.memberChild);
        this.dataMember = true
        this.calcAgentAndMemberSettlementAmount();
        this.submitted = false;
      }
    );
  }


  /**
   * 取得會員列表
   */
  getMemberList() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.Member = res.data;
      }
    );
  }

  /**
   * 取得會員姓名
   */
  getMemberName(members_id: number): string {
    const memberName = this.memberChild.find(e => e.id === members_id);
    if (memberName) { return memberName.name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得遊戲類型列表
   */
  getGameTypeList() {
    this.gameTypeService.getGameTypes().subscribe(
      res => {
        this.GameType = res.data;
        // console.log(this.GameType);
      }
    );
  }

  /**
   * 取得遊戲中文名稱
   */
  getGameChtName(game_types_id: number): string {
    const chtName = this.GameType.find(e => e.id === game_types_id);
    if (chtName) { return chtName.cht_name; }
    return this.translateService.instant('common.noData');
  }

  getGameResultTypeByID(id: number) {
    return GameResultType.getNameByID(id);
  }

  /**
   * 用遊戲局號(game_number)取得項目類型(item_type_id)
   */
  getItemTypeIdByGameNumber(game_number: number): any {
    const itemTypeId = this.CalcItemResultRecord.find(e => e.game_number === game_number);
    if (itemTypeId) { return itemTypeId.item_types_ids; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得遊戲項目列表
   */
  getItemTypesList() {
    this.itemTypesService.getItemTypesList().subscribe(
      res => {
        this.ItemTypes = res.data;
        // console.log(this.ItemTypes);
      }
    );
  }

  /**
   * 取得項目類型中文名稱 By item_type_id
   */
  getItemTypeChtName(item_types_id: number): string {
    const chtName = this.ItemTypes.find(e => e.id === item_types_id);
    if (chtName) { return chtName.cht_name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得開獎結果 By startDate and endDate
   */
  getCalcItemResultRecordByTypeBetweenDate(template: TemplateRef<any>, id: number, startDate: string, endDate: string) {
    this.calcItemResultRecordService.getCalcItemResultRecordByTypeBetweenDate(id, startDate, endDate).subscribe(
      res => {
        const data = res.data;
        this.CalcItemResultRecordByDate = data;

        this.modalRef = this.modalService.show(template, { class: 'modal-xl' });
        // console.log(this.CalcItemResultRecordByDate);
      }
    );
  }

  openModal(template: TemplateRef<any>, member_id: number) {
    const day = new Date();
    let start = formatDate(day.setDate(day.getDate() - 2), 'yyyy-MM-dd', 'en-US');
    let end = formatDate(day.setDate(day.getDate() + 1), 'yyyy-MM-dd', 'en-US');
    start += ' 12:01:00';
    end += ' 12:00:00';
    const data = {
      game_types_id: 1,
      start_date: start,
      end_date: end,
      members_id: member_id,
      is_total: 0,
    };
    this.betRecordService.getResultTotalAmountByDateAndMember(data).subscribe(
      res => {
        const data = res.data;
        this.dataSourceOfBetRecord.data = data;
        this.dataOfResultAmountTotal = this.dataSourceOfBetRecord.data;
        let start = this.yes;
        let end = this.newday;
        start += ' 12:01:00';
        end += ' 12:00:00';
        this.getCalcItemResultRecordByTypeBetweenDate(template, this.gameTypeID, start, end);
      }
    );
  }

  get Status() { return Status; }

  statusChange(status) {
    switch (status) {
      case 0:
        return this.translateService.instant('common.inActive');
      case 1:
        return this.translateService.instant('betRecord.normal');
    }
  }

  btnOK() {
    if (!this.startDateValue) {
      return alert(this.translateService.instant('manageAgent.inputStartDate'));
    }
    if (!this.endDateValue) {
      return alert(this.translateService.instant('manageAgent.inputEndDate'));
    }
    if (this.endDateValue < this.startDateValue) {
      return alert(this.translateService.instant('manageAgent.inputCorrectDate'));
    }
    this.active = false;

    // 以代理ID及開始及結束日期搜尋會員日報表
    const memberReportByAgentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      agents_id: this.agentID,
    };

    // 以上級代理ID 及開始及結束日期搜尋代理日報表
    const agentReportByParentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.agentID,
    };

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);
    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    // this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
  }

  today() {
    this.submitted = true;
    const lastCalcDay = new Date(getLastCalcDay());
    const today = formatDate(lastCalcDay.setDate(lastCalcDay.getDate() + 1), 'yyyy-MM-dd', 'en-US');
    this.startDateValue = today;
    this.endDateValue = today;
    this.active = true;
    this.calcOwnProfit();
    // const day = new Date();
    // let yes = formatDate(day.setDate(day.getDate() - 2), 'yyyy-MM-dd', 'en-US');
    // let newday = formatDate(day.setDate(day.getDate() + 1), 'yyyy-MM-dd', 'en-US');
    // this.yes = yes;
    // this.newday = newday;
    let start = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
    let end = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en_us');
    start += ' 12:01:00';
    this.yes = start;
    this.newday = end;

    const currentAPI = this.getCurrentReportRequest(this.gameTypeID);
    currentAPI.subscribe(
      res => {
        const data = res.data;
        const agentData = data.agent_daily_report.filter(e => e.parent_id === this.agentID && e.is_top);
        const dataMember = data.member_daily_report.filter(e => e.agents_id === this.agentID);
        this.dataAgent = true;
        this.dataMember = true;
        this.realTimeRecords = res.data;

        this.dataSourceOfAgent.data = UsersJoinRecordWithSettlementCalc(
          agentData,
          this.agentChild,
          this.settlementRecords,
          this.startDateValue,
          this.endDateValue
        );

        this.dataSourceOfMember.data = MemberJoinRecords(dataMember, this.memberChild);

        this.calcAgentAndMemberSettlementAmount();
        this.submitted = false;
      }
    );
  }

  yesterday() {
    const yesterday = getLastCalcDay();

    this.click = true;
    this.active = false;
    this.startDateValue = yesterday;
    this.endDateValue = yesterday;
    this.yes = yesterday;
    this.newday = yesterday;


    // 以代理ID及開始及結束日期搜尋會員日報表
    const memberReportByAgentID = {
      game_types_id: this.gameTypeID,
      start_date: yesterday,
      end_date: yesterday,
      agents_id: this.agentID,
    };

    // 以上級代理ID 及開始及結束日期搜尋代理日報表
    const agentReportByParentID = {
      game_types_id: this.gameTypeID,
      start_date: yesterday,
      end_date: yesterday,
      parent_id: this.agentID,
    };

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    // this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
  }

  week() {
    let offset = 0;
    if (isWeekFirstDayBeforeCalc()) {
      offset = 7;
    }
    const today = new Date();
    const dayOfWeek = today.getDay();
    const weekStart = formatDate(today.setDate(today.getDate() - dayOfWeek - offset), 'yyyy-MM-dd', 'en-US');
    const weekEnd = formatDate(today.setDate(today.getDate() + 6), 'yyyy-MM-dd', 'en-US');


    this.yes = weekStart;
    this.newday = weekEnd;
    this.startDateValue = weekStart;
    this.endDateValue = weekEnd;
    this.active = false;

    // 以代理ID及開始及結束日期搜尋會員日報表
    const memberReportByAgentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      agents_id: this.agentID,
    };

    // 以上級代理ID 及開始及結束日期搜尋代理日報表
    const agentReportByParentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.agentID,
    };

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    // this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
  }

  lastWeek() {
    let offset = 0;
    if (isWeekFirstDayBeforeCalc()) {
      offset = 7;
    }
    const weekStartDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 8 - offset), 'yyyy-MM-dd', 'en-US');
    const weekEndDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 2 - offset), 'yyyy-MM-dd', 'en-US');
    this.startDateValue = weekStartDate;
    this.endDateValue = weekEndDate;
    this.active = false;

    const StartDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 8 - offset), 'yyyy-MM-dd', 'en-US');
    const EndDate = formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek - 2 - offset), 'yyyy-MM-dd', 'en-US');
    this.yes = StartDate;
    this.newday = EndDate;

    // 以代理ID及開始及結束日期搜尋會員日報表
    const memberReportByAgentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      agents_id: this.agentID,
    };

    const agentReportByParentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.agentID,
    };
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);
    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
  }

  month() {
    let offset = 0;
    if (isMonthFirstDayBeforeCalc()) {
      offset = -1;
    }
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month + offset, 1);
    const lastDay = new Date(year, month + offset + 1, 0);
    const formatFirstDay = formatDate(firstDay, 'yyyy-MM-dd', 'en-US');
    const formatLastDay = formatDate(lastDay, 'yyyy-MM-dd', 'en-US');
    this.startDateValue = formatFirstDay;
    this.endDateValue = formatLastDay;
    this.yes = formatFirstDay;
    this.newday = formatLastDay;
    this.active = false;


    // 以代理ID及開始及結束日期搜尋會員日報表
    const memberReportByAgentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      agents_id: this.agentID,
    };

    // 以上級代理ID 及開始及結束日期搜尋代理日報表
    const agentReportByParentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.agentID,
    };

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    // this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
  }

  lastMonth() {
    let offset = 0;
    if (isMonthFirstDayBeforeCalc()) {
      offset = -1;
    }
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const firstDay = new Date(year, month + offset - 1, 1);
    const lastDay = new Date(year, month + offset, 0);
    this.active = false;
    const formatFirstDay = formatDate(firstDay, 'yyyy-MM-dd', 'en-US');
    const formatLastDay = formatDate(lastDay, 'yyyy-MM-dd', 'en-US');
    this.startDateValue = formatFirstDay;
    this.endDateValue = formatLastDay;

    const day = new Date();
    const Year = day.getFullYear();
    const Month = day.getMonth();
    const FirstDay = new Date(Year, Month - 1, 1);
    const LastDay = new Date(Year, Month, 0);
    const FormatFirstDay = formatDate(FirstDay, 'yyyy-MM-dd', 'en-US');
    const FormatLastDay = formatDate(LastDay, 'yyyy-MM-dd', 'en-US');
    this.yes = FormatFirstDay;
    this.newday = FormatLastDay;

    // 以代理ID及開始及結束日期搜尋會員日報表
    const memberReportByAgentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      agents_id: this.agentID,
    };

    // 以上級代理ID 及開始及結束日期搜尋代理日報表
    const agentReportByParentID = {
      game_types_id: this.gameTypeID,
      start_date: this.startDateValue,
      end_date: this.endDateValue,
      parent_id: this.agentID,
    };

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    this.getAgentDailyReportsSearchByTypeAndParentBetweenDate(agentReportByParentID);

    // this.getAgentDailyReportsSearchByTypeAndAgentBetweenDate();
    // this.getMemberDailyReportsSearchByTypeAndAgentBetweenDate(memberReportByAgentID);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceOfMember.filter = filterValue.trim().toLowerCase();
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  getSettlementRecords() {
    this.settlementRecordServ.getSettlementRecords().subscribe(r => {
      this.settlementRecords = r.data;
      this.subscribeParams();
    });
  }

  calcOwnProfit() {
    return this.currentUser?.settlement_amount - (this.totalMemberSettlementAmount + this.totalAgentSettlementAmount);
  }

  calcOwnRecord(agentRecords: any, memberRecords: any) {
    this.currentUser = CalcOwnRecord(this.currentUser, agentRecords, memberRecords);
  }


  calcAgentAndMemberSettlementAmount() {
    this.totalAgentSettlementAmount = 0;
    this.totalMemberSettlementAmount = 0;
    const agentRecords = this.dataSourceOfAgent.data;
    const memberRecords = this.dataSourceOfMember.data;

    agentRecords.forEach(e => this.totalAgentSettlementAmount += e.settlement_amount);
    memberRecords.forEach(e => this.totalMemberSettlementAmount += e.settlement_amount);

    this.calcOwnRecord(agentRecords, memberRecords);
  }

  openReocrdModal(user: User) {
    this.modalService.show(ReportByDateRangeComponent, {
      initialState: {
        user: user,
        startDate: this.startDateValue,
        endDate: this.endDateValue,
        settlementRecords: this.settlementRecords,
        realTimeRecords: this.realTimeRecords
      }
      , class: 'modal-xl'
    });
  }

  openMemberReocrdModal(member: Member) {
    this.modalService.show(ReportByDateRangeMemberComponent, {
      initialState: {
        gameTypeID: this.gameTypeID,
        member: member,
        startDate: this.startDateValue,
        endDate: this.endDateValue,
        realTimeRecords: this.realTimeRecords
      }
      , class: 'modal-xl'
    });
  }

  isRoleAdmin() {
    return this.identity === UserRole.admin;
  }

  back() {
    if (this.currentUser.parent_id === this.userID || this.currentUser.identity === UserRole.topAgent) {
      return this.router.navigate(['system/report-record-list'], { queryParams: { g: this.gameTypeID } });
    }
    this.router.navigate([`system/report-record-member-list/${this.currentUser.parent_id}/${this.startDateValue}/${this.endDateValue}`], { queryParams: { g: this.gameTypeID } });
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }

  getCurrentReportRequest(gameTypeID: number) {
    switch (gameTypeID) {
      case GameTypeEnum.GameTypeSicBo:
        return this.currentReportsSicboService.getcurrentReportsSicbo();
      case GameTypeEnum.GameTypeBaccarat:
        return this.currentReportsSicboService.getcurrentReportsBaccarat();
    }
  }

  get UserRole() { return UserRole }
}
