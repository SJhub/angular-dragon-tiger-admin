import { Component, OnInit } from '@angular/core';
import { SystemService } from '../../services/system.service';
import { GameTypeService } from '../../services/game-type.service';
import { SystemSetting } from '../../models/system-setting';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EnumToArrayPipe } from '../../pipe/enum-to-array.pipe';
import { Week } from '../../enum/week.enum';
import { UpdateRebateTime } from '../../models/update-rebat-time';
import { UploadFileService } from '../../services/upload-file.service';
import { AuthenticationService } from '../../services/auth.service';
import { options } from '../../ckeditor/config';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { GameType } from '../../models/game-type';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { ContactUsService } from '../../services/contact-us.service';
import { ContactUs } from '../../models/contact-us';
import { ContactUsType } from '../../enum/contact-us-type.enum';
import { forkJoin } from 'rxjs';
import { GeneralSystemSettingService } from '../../services/general-system-setting.service';

@Component({
  selector: 'app-other-settings',
  templateUrl: './other-settings.component.html',
  styleUrls: ['./other-settings.component.css']
})
export class OtherSettingsComponent implements OnInit {

  systemSettings: SystemSetting[];
  systemSetting: SystemSetting;
  myForm: FormGroup;
  gameTypes: GameType[];
  gameTypesID: number;

  gameTypeId: number;
  gameTypeShowStatus: number;

  config = options;

  // 合作
  contactUs: ContactUs;

  // APP版本號
  AppReleaseVersion: string;

  constructor(
    private generalSystemServ: GeneralSystemSettingService,
    private contactUsServ: ContactUsService,
    private translateService: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private systemService: SystemService,
    private gameTypeService: GameTypeService,
    private fb: FormBuilder,
    private uploadFileServ: UploadFileService,
    private authService: AuthenticationService,
  ) {
    this.groupForm();
  }

  ngOnInit() {
    this.generalSystemServ.getAppReleaseVersion(1).subscribe(r => this.AppReleaseVersion = r.data);
    this.getGameTypes();
    this.contactUsServ.getContactUs(ContactUsType.Cooperation).subscribe(r => {
      this.contactUs = r.data;
      this.myForm.get('line_id').setValue(this.contactUs.line_id);
    });
  }

  getSystemSettings() {
    this.systemService.getSystemSettings()
      .subscribe(
        res => {
          this.systemSettings = res.data;
          this.setForm();
        },
        err => console.log(err)
      );
  }

  getGameTypes() {
    this.gameTypeService.getGameTypes()
      .subscribe(
        res => {
          let data: GameType[] = res.data;
          data = data.filter(e => e.id === GameTypeEnum.GameTypeSicBo);
          this.gameTypes = data;

          this.getSystemSettings();
        },
        err => console.log(err)
      );
  }

  groupForm() {
    this.myForm = this.fb.group({
      'wash_code_amount': [null],
      'default_member_credit': [null],
      'terms_of_service': [null],
      'sec_of_repeat_msg': [null],
      'default_rebate': [null],
      'update_wash_code_time': [null],
      'start_time': [null],
      'end_time': [null],
      'week': [null],
      'stop_bet_sec': [null],
      'app_foreground_idle_sec': [null, Validators.required],
      'app_background_idle_sec': [null, Validators.required],
      'close_game_tips': [null, Validators.required],
      'rest_game_tips': [null, Validators.required],
      'break_game_tips': [null, Validators.required],
      'status': [null],
      'show_status': [null],
      'open_registration': [Status.InActive],
      'member_login_show_status': [Status.InActive],
      'apple_login_show_status': [Status.InActive],
      'redirect_url': [null],
      "app_redirect_url_line": [null],
      "app_redirect_url_wechat": [null],
      "auto_donate_amount": [null, Validators.required],
      "line_id": [null, Validators.required]
    });
  }

  /**
   * setForm - set form
   * @param id - default value is 1
   */
  setForm(id: number = 1) {
    this.systemService.getSystemSetting(id).subscribe(
      res => {
        const data = res.data;
        this.systemSetting = data;
        const updateRebateTime: UpdateRebateTime[] = JSON.parse(this.systemSetting.update_wash_code_time);
        // console.log(this.systemSetting);

        this.myForm.patchValue({
          wash_code_amount: data.wash_code_amount,
          default_member_credit: data.default_member_credit,
          terms_of_service: data.terms_of_service,
          sec_of_repeat_msg: data.sec_of_repeat_msg,
          default_rebate: data.default_rebate,
          stop_bet_sec: data.stop_bet_sec,
          app_foreground_idle_sec: data.app_foreground_idle_sec,
          app_background_idle_sec: data.app_background_idle_sec,
          close_game_tips: data.close_game_tips,
          rest_game_tips: data.rest_game_tips,
          break_game_tips: data.break_game_tips,
          week: updateRebateTime[0].week,
          start_time: updateRebateTime[0].start_time,
          end_time: updateRebateTime[0].end_time,
          status: data.status,
          show_status: data.show_status,
          open_registration: data.open_registration,
          redirect_url: data.redirect_url,
          app_redirect_url_line: data.app_redirect_url_line,
          app_redirect_url_wechat: data.app_redirect_url_wechat,
          auto_donate_amount: data.auto_donate_amount,
          member_login_show_status: data.member_login_show_status,
          apple_login_show_status: data.apple_login_show_status,
        });
      }
    );
  }

  submit(val: any) {
    const washCodeAmount = parseInt(val.wash_code_amount, 10);

    const updateRebateTime = [{
      week: Number(val.week),
      start_time: val.start_time,
      end_time: val.end_time,
    }];

    val.update_wash_code_time = JSON.stringify(updateRebateTime);

    val.id = 1;
    val.default_member_credit = Number(val.default_member_credit);
    val.open_registration = Number(val.open_registration);
    val.close_hall_tips = "遊戲關閉";
    val.close_entry_tips = "遊戲關閉";
    val.call_landlord_sec = 3;
    val.call_multiple_sec = 3;
    val.shot_poker_sec = 3;
    val.commission_percent = 3;
    this.contactUs.line_id = val.line_id;
    if (this.myForm.valid) {
      forkJoin([this.systemService.updateSystemSetting(val), this.contactUsServ.editContactUs(this.contactUs)]).subscribe(
        () => {
          alert(this.translateService.instant('common.editSuccess'));
          this.setForm();
        },
        err => alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`)
      );
    } else {
      this.markFormGroupTouched(this.myForm);
    }
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  get Week() {
    return new EnumToArrayPipe().transform(Week);
  }

  compareFn(a, b) {
    return 0;
  }

  /**
   * find system setting by game types id
   * @param id
   */
  findSystemSettingByGameTypesID(id: number) {
    if (id <= 0) {
      return;
    }

    if (this.systemSettings.length <= 0) {
      return;
    }

    const findObj = this.systemSettings.find(
      it => it.game_types_id === Number(id)
    );

    if (findObj === undefined) {
      return;
    }

    return findObj;
  }

  /**
   * gameTypeOnChange - when select opetion change
   * @param gameTypesID
   */
  gameTypeOnChange(gameTypesID: number) {
    this.gameTypesID = gameTypesID;
    const findObj = this.findSystemSettingByGameTypesID(this.gameTypesID);

    if (findObj) {
      this.setForm(this.gameTypesID);
      this.systemSetting = findObj;
    }
  }

  updateGameTypeStatus(item: any, event: any) {
    this.gameTypeId = item.id;
    this.gameTypeShowStatus = event.target.checked ? 1 : 0;
    console.log(this.gameTypeId, this.gameTypeShowStatus);
    this.gameTypeService.updateFameTypeShowStatus(this.gameTypeId, this.gameTypeShowStatus).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  updateOpenRegistration(checked: boolean) {
    this.systemService.updateOpenRegistration(this.systemSetting.id, Number(checked)).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    )
  }

  updateMemberLoginShowStatus(checked: boolean) {
    this.systemService.updateMemberLoginShowStatus(this.systemSetting.id, Number(checked)).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    )
  }

  updateAppleLoginShowStatus(checked: boolean) {
    this.systemService.updateAppleLoginShowStatus(this.systemSetting.id, Number(checked)).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    )
  }

  fileUploadRequest(e) {
    const file = e.data.requestData.upload.file as File;
    const fileName = this.uploadFileServ.getRandomFileName(file.name);
    const file$ = new File([file.slice()], fileName);
    e.data.requestData.upload;
    e.data.requestData.file = file$;
    e.data.requestData.token = this.authService.getToken();
    delete e.data.requestData.upload;
  }

  translateGame(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  getTranslatedWeek(type: number) {
    return this.enumTranslateServ.getTranslatedWeek(type);
  }

  updateAppVersion(value: string) {
    if (value) {
      this.generalSystemServ.editAppReleaseVersion(1, value).subscribe(
        () => alert(this.translateService.instant('common.updateSuccess')),
        () => alert(this.translateService.instant('common.updateFailed'))
      )
    }
  }
}
