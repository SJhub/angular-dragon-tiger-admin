import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProclamationTypesEnum } from '../../enum/proclamation-type.enum';
import { Status } from '../../enum/status.enum';
import { Proclamation } from '../../models/proclamation';
import { ProclamationService } from '../../services/proclamation.service';

@Component({
  selector: 'app-ad-proclamations',
  templateUrl: './ad-proclamations.component.html',
  styleUrls: ['./ad-proclamations.component.css']
})
export class AdProclamationsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['body', 'created_at', 'status', 'function'];
  dataSource = new MatTableDataSource<Proclamation>();

  modal: Proclamation;
  modalRef: BsModalRef;

  constructor(
    private translateService: TranslateService,
    private proclamationService: ProclamationService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.getProclamations();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getProclamations() {
    this.proclamationService.getProclamations().subscribe(r => {
      const data: Proclamation[] = r.data;
      this.dataSource.data = data.filter(e => e.type === ProclamationTypesEnum.ProclamationTypeAd);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  };

  openModal(template: TemplateRef<any>, id: number) {
    this.proclamationService.getProclamation(id)
      .subscribe(res => {
        this.modal = res.data as Proclamation;
        this.modalRef = this.modalService.show(template, { class: 'modal-l' });
      });
  }

  delete(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.proclamationService.deleteProclamation(id).subscribe(() => {
        this.getProclamations();
        alert(this.translateService.instant('common.deleteSuccess'));
      },
        err => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  get Status() { return Status }

}
