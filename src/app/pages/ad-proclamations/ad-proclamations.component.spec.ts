import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdProclamationsComponent } from './ad-proclamations.component';

describe('AdProclamationsComponent', () => {
  let component: AdProclamationsComponent;
  let fixture: ComponentFixture<AdProclamationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdProclamationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdProclamationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
