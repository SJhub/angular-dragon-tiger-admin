import { UserService } from './../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ItemTypes } from '../../models/item-types';
import { ItemTypesService } from '../../services/item-types.service';
import { User } from '../../models/user';
import { SingleItems } from '../../models/single-items';
import { SingleItemsService } from '../../services/single-items.service';
import { Status } from '../../enum/status.enum';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../../enum/user-role.enum';

@Component({
  selector: 'app-single-items',
  templateUrl: './single-items.component.html',
  styleUrls: ['./single-items.component.css']
})
export class SingleItemsComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['agents_id', 'item_types_id', 'amount', 'created_at', 'status', 'status-setting', 'function'];
  dataSource = new MatTableDataSource<SingleItems>();

  ItemTypes: ItemTypes[];
  User: User[];
  updateSingleItem: SingleItems;

  id: number;
  identity: number;
  userId = 'null';
  itemTypeId = 'null';
  amount: number;
  selectedUser: number;
  itemTypesId: any;
  agentItemTypeAmont: number;
  agentLimitData: SingleItems[];
  filterItemType: any;
  itemTypeAmount: number;
  parentId: number;
  agentId: number;
  myselfAgentId: number;
  status: number;
  agent: any;
  itemType: any;
  updateAmount: any;
  singleItemsId: number;
  obj: any;
  updateItemType = false;

  constructor(
    private translateService: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private itemTypesService: ItemTypesService,
    private userService: UserService,
    private singleItemsService: SingleItemsService,
  ) { }

  ngOnInit(): void {
    this.id = Number(localStorage.getItem('authenticatedID'));
    this.identity = Number(localStorage.getItem('loginUserIdentity'));
    this.getItemTypesList();
    this.getUser(this.id);
    if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getUsers(); }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getUserByParentID(this.id); }
    if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getSingleItems(); }
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getSingleItemLimitsByParent(this.id); }

    this.dataSource.filterPredicate = (data: SingleItems, filter: string): boolean => {
      const name = this.getUserName(data.agents_id).includes(filter);
      const itemTypeName = this.getItemTypeName(data.item_types_id).includes(filter);
      const amount = String(data.amount).includes(filter);
      const create_at = data.created_at.includes(filter);

      return name || itemTypeName || amount || create_at;
    };

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * 取得單項單項設置列表 - 最高權限&管理者
   */
  getSingleItems() {
    this.singleItemsService.getSingleItemLimits().subscribe(
      res => {
        this.dataSource.data = res.data.reverse();
      }
    );
  }

  /**
   * 取得單項單項設置列表 - 超級代理&一般代理
   */
  getSingleItemLimitsByParent(id) {
    this.singleItemsService.getSingleItemLimitsByParent(id).subscribe(
      res => {
        const data = res.data;
        this.dataSource.data = res.data;
        // console.log(data);
      }
    );
  }

  /**
   * 取得用戶列表
   */
  getUsers() {
    this.userService.getUsers().subscribe(
      res => {
        this.User = res.data as User[];
      }
    );
  }

  getUser(id) {
    this.userService.getUser(id).subscribe(
      res => {
        const data = res.data;
        this.parentId = data.id;
      }
    );
  }

  /**
   * 取得用戶資料 by parent id
   * @param userId
   */
  getUserByParentID(parentId) {
    this.userService.getUserByParentID(parentId).subscribe(
      res => {
        this.User = res.data as User[];
        // console.log(this.User);
      }
    );
  }

  /**
   * 取得用戶名稱
   */
  getUserName(userId: number): string {
    const userName = this.User.find(e => e.id === Number(userId));
    if (userName) { return userName.real_name; }
    return this.translateService.instant('common.noData');
  }

  /**
  * 取得項目類型列表
  * @param event
  */
  getItemTypesList() {
    this.itemTypesService.getItemTypesList().subscribe(
      res => {
        this.ItemTypes = res.data.slice(0, 17);
      }
    );
  }

  /**
   * 取得項目名稱
   */
  getItemTypeName(itemTypeId: number): string {
    const itemTypeName = this.ItemTypes.find(e => e.id === Number(itemTypeId));
    if (itemTypeName) { return this.enumTranslateServ.getTranslatedBetItem(itemTypeName.en_name); }
    return this.translateService.instant('common.noData');
  }

  /**
   * 用戶選單 - 選擇用戶
   */
  selectUser(userId) {
    this.selectedUser = userId;
    this.itemTypesId = 0;
    this.updateItemType = false;
    this.itemTypeId = 'null';
    this.singleItemsId = 0;
    this.amount = 0;
  }

  /**
   * 項目選單 - 選擇項目
   */
  itemTypes(itemTypesId: number) {
    this.itemTypesId = itemTypesId;
    this.getSingleItemLimitsByAgnet(this.selectedUser);
    if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getSingleItemLimitsByMyself(this.id); }
  }

  /**
   * 以代理ID 取得單項限制資料 - 選單選擇的代理單項限制資料
   */
  getSingleItemLimitsByAgnet(id) {
    this.singleItemsService.getSingleItemLimitsByAgnet(id).subscribe(
      res => {
        const data = res.data as SingleItems[];
        this.agentLimitData = data;
        this.filterItemType = this.agentLimitData.filter(e => e.item_types_id === Number(this.itemTypesId));
        if (this.filterItemType.length) {
          this.itemTypeAmount = this.filterItemType[0].amount;
          this.agentId = this.filterItemType[0].agents_id;
        }

        // console.log(this.filterItemType);
      }
    );
  }

  /**
   * 以代理ID 取得單項限制資料 - 自己的單項限制資料
  */
  getSingleItemLimitsByMyself(id) {
    this.singleItemsService.getSingleItemLimitsByAgnet(id).subscribe(
      res => {
        const data = res.data as SingleItems[];
        this.agentLimitData = data;
        this.filterItemType = this.agentLimitData.filter(e => e.item_types_id === Number(this.itemTypesId));
        if (this.filterItemType.length) {
          this.itemTypeAmount = this.filterItemType[0].amount;
          this.myselfAgentId = this.filterItemType[0].agents_id;
        }

        // console.log(this.filterItemType);
      }
    );
  }

  submit() {
    if (this.singleItemsId) {
      const obj = {
        id: this.singleItemsId,
        amount: Number(this.amount),
        status: status ? 0 : 1
      };
      this.singleItemsService.updateSingleItemLimits(obj).subscribe(
        () => {
          alert(this.translateService.instant('common.editSuccess'));
          if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getSingleItems(); }
          if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getSingleItemLimitsByParent(this.id); }
          this.userId = 'null';
          this.itemTypeId = 'null';
          this.amount = 0;
          this.singleItemsId = 0;
          return;
        },
        err => alert(this.translateService.instant('common.editFailed'))
      );
    } else {
      if (this.identity === UserRole.admin || this.identity === UserRole.management) {
        if (this.filterItemType.length) {
          alert(this.translateService.instant('singleItem.alreadySet'));
          return;
        }
      } else {
        if (this.myselfAgentId === this.agentId) {
          if (this.filterItemType.length) {
            alert(this.translateService.instant('singleItem.alreadySet'));
            return;
          }
        }
      }
      if (this.amount > this.itemTypeAmount) {
        alert(this.translateService.instant('singleItem.settableInsufficient'));
        return;
      }
      const data = {
        item_types_id: Number(this.itemTypesId),
        agents_id: Number(this.selectedUser),
        amount: Number(this.amount),
        parents_id: this.parentId,
        status: 1
      } as SingleItems;
      if (!this.selectedUser) {
        alert(this.translateService.instant('singleItem.pleaseSelectAgent'));
        return;
      }
      if (!this.itemTypesId) {
        alert(this.translateService.instant('singleItem.pleaseSelectItem'));
        return;
      }
      if (!this.amount) {
        alert(this.translateService.instant('singleItem.pleaseEnterAmount'));
        return;
      }
      this.singleItemsService.addSingleItemLimits(data).subscribe(
        () => {
          alert(this.translateService.instant('common.addSuccess'));
          if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getSingleItems(); }
          if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getSingleItemLimitsByParent(this.id); }

          this.userId = 'null';
          this.itemTypeId = 'null';
          this.amount = 0;
        },
        err => alert(this.translateService.instant('common.addFailed'))
      );
    }
  }

  update(id) {
    this.singleItemsId = id;
    this.updateItemType = true;
    this.singleItemsService.getSingleItemLimit(id).subscribe(
      res => {
        const data = res.data as SingleItems;
        this.userId = data.agents_id;
        this.itemTypeId = data.item_types_id;
        this.amount = data.amount;


        this.getSingleItemLimitsByAgnet(id);

      }
    );
  }

  updateStatus(id, status) {
    const data = {
      id: id,
      status: status ? 0 : 1
    };
    this.singleItemsService.updateSingleItemStatus(data).subscribe(
      () => {
        alert(this.translateService.instant('common.editSuccess'));
        if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getSingleItems(); }
        if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getSingleItemLimitsByParent(this.id); }
      },
      err => alert(this.translateService.instant('common.editFailed'))
    );
  }

  /**
   * [ngClass] 切換顏色
   */
  changeBtnColor(status) {
    switch (status) {
      case 0:
        return 'btn-warning';
      case 1:
        return 'btn-secondary';
    }
  }

  delete(id) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.singleItemsService.deleteSingleItemLimits(id).subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          if (this.identity === UserRole.admin || this.identity === UserRole.management) { this.getSingleItems(); }
          if (this.identity === UserRole.topAgent || this.identity === UserRole.agent) { this.getSingleItemLimitsByParent(this.id); }
        },
        err => alert(this.translateService.instant('common.deleteFailed'))
      );
    }
  }

  get Status() { return Status; }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getTranslatedBetItem(name: string) {
    return this.enumTranslateServ.getTranslatedBetItem(name);
  }
}
