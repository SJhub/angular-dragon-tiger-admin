import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleItemsComponent } from './single-items.component';

describe('SingleItemsComponent', () => {
  let component: SingleItemsComponent;
  let fixture: ComponentFixture<SingleItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
