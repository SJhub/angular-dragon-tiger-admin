import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { getFormatedDateStr } from '../../common/common';
import { getTimeRangeByDate, getToday } from '../../common/date';
import { BaccaratPanelComponent } from '../../component/baccarat-panel/baccarat-panel.component';
import { DragonTigerPanelComponent } from '../../component/dragon-tiger-panel/dragon-tiger-panel.component';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { CalcItemResultRecord } from '../../models/calc-item-result-record';
import { GameType } from '../../models/game-type';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';

const SINGLE_BET_NUMBER = 17;
const CROSSING_HOUR = 0; // 以中午12時為基準取資料

@Component({
  selector: 'app-lottery-statistics',
  templateUrl: './lottery-statistics.component.html',
  styleUrls: ['./lottery-statistics.component.css']
})
export class LotteryStatisticsComponent implements OnInit {
  @ViewChild('baccaratPanel') baccaratPanel: BaccaratPanelComponent;
  @ViewChild('dragonTigerPanel') dragonTigerPanel: DragonTigerPanelComponent;
  gameTypes: GameType[];
  currentType: number;
  myForm: FormGroup;

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private calcItemResultRecordServ: CalcItemResultRecordService,
    private gameTypeServ: GameTypeService,
    private fb: FormBuilder
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.gameTypeServ.getGameTypes().subscribe(r => {
      this.gameTypes = r.data;
      this.currentType = GameTypeEnum.GameTypeDragonTiger;
      this.getCalcItemResultActiveRecords(this.currentType);
    });
  }


  getCalcItemResultActiveRecords(gameTypeID: number) {
    const today = getToday();
    const range = getTimeRangeByDate(today.st);

    // 星期一則以星期日中午12點開始
    let start = new Date(today.st);
    if (start.getDay() == 1) {
      start.setDate(start.getDate() - 1);
      today.st = getFormatedDateStr(start, 'yyyy-MM-dd');
    }

    const st = `${today.st} ${range.st}`;
    const et = getFormatedDateStr(new Date(), 'yyyy-MM-dd HH:mm:ss');

    this.calcItemResultRecordServ.getCalcItemResultActiveRecords(gameTypeID, st, et)
      .subscribe(r => {
        let activeResults: CalcItemResultRecord[] = r.data;
        const itemTypeIDs: number[] = [];
        activeResults = activeResults.filter(e => !e.is_renew);
        activeResults.forEach(e => {
          try {
            const ids = JSON.parse(e.item_types_ids);
            itemTypeIDs.push(...ids);
          } catch (error) {
            console.log(error);
          }
        });
        switch (gameTypeID) {
          case GameTypeEnum.GameTypeSicBo:
            this.patchValueToPanel(itemTypeIDs);
            break;
          case GameTypeEnum.GameTypeBaccarat:
            this.baccaratPanel.patchValueToPanel(itemTypeIDs);
            break;
          case GameTypeEnum.GameTypeDragonTiger:
            this.dragonTigerPanel.patchValueToPanel(itemTypeIDs);
            break;
        }
      });
  }


  patchValueToPanel(ids: number[]) {
    const grouped = ids.reduce((r, a) => {
      r[a] = r[a] || [];
      r[a].push(a);
      return r;
    }, Object.create(null));

    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const index = i + 1;
      const ctrl = this.myForm.get(`${index}`).get('amount');
      let times = 0;
      if (grouped[index]) {
        times = grouped[index].length;
      }
      ctrl.setValue(times);
    }
  }

  /**
   * 表單建構
   */
  groupForm() {
    this.myForm = this.fb.group([]);
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      this.myForm.addControl(`${i + 1}`, this.createSingleBetLimit(i + 1));
    }
  }

  createSingleBetLimit(id: number) {
    return this.fb.group({
      id: [id],
      amount: [0]
    });
  }

  getValue(name: string, ctrlName: string) {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.value;
  }

  onGameTypeChanged(value: string) {
    this.getCalcItemResultActiveRecords(Number(value));
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  get GameType() { return GameTypeEnum }
}
