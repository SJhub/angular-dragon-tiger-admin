import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotteryStatisticsComponent } from './lottery-statistics.component';

describe('LotteryStatisticsComponent', () => {
  let component: LotteryStatisticsComponent;
  let fixture: ComponentFixture<LotteryStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotteryStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotteryStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
