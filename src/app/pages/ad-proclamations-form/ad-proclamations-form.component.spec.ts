import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdProclamationsFormComponent } from './ad-proclamations-form.component';

describe('AdProclamationsFormComponent', () => {
  let component: AdProclamationsFormComponent;
  let fixture: ComponentFixture<AdProclamationsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdProclamationsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdProclamationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
