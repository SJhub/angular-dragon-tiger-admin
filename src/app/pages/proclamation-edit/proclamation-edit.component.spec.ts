import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProclamationEditComponent } from './proclamation-edit.component';

describe('ProclamationEditComponent', () => {
  let component: ProclamationEditComponent;
  let fixture: ComponentFixture<ProclamationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProclamationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProclamationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
