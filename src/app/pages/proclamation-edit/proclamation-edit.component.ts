import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProclamationService } from '../../services/proclamation.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Proclamation } from '../../models/proclamation';
import { Status } from '../../enum/status.enum';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UploadFileService } from '../../services/upload-file.service';
import { AuthenticationService } from '../../services/auth.service';
import { options } from '../../ckeditor/config';
import { ImgFullPathPipe } from '../../pipe/img-full-path.pipe';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-proclamation-edit',
  templateUrl: './proclamation-edit.component.html',
  styleUrls: ['./proclamation-edit.component.css']
})
export class ProclamationEditComponent implements OnInit {

  proclamation: Proclamation;
  myForm: FormGroup;
  private id: number;
  data: any;
  modalRef: BsModalRef;
  newsId: any;
  avatarUrl: string;
  config = options;

  constructor(
    private translateService: TranslateService,
    private proclamationService: ProclamationService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private location: Location,
    private authService: AuthenticationService,
    private modalService: BsModalService,
    private uploadFileServ: UploadFileService,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm();
    }
  }

  groupForm() {
    this.myForm = this.fb.group({
      'title': [''],
      'pic': [''],
      'status': [true],
      'type': [0],
      'body': [null],
    });
  }

  setForm() {
    this.proclamationService.getProclamation(this.id)
      .subscribe(
        res => {
          const data = res.data;
          this.avatarUrl = data.pic;
          this.myForm.patchValue({
            title: data.title,
            pic: data.pic,
            status: data.status,
            type: data.type,
            body: data.body,
          });
        }
      );
  }

  submit(val: any) {
    if (this.myForm.valid) {

      val.id = this.id;
      // val.pic = this.avatarUrl;
      val.status = val.status ? Status.Active : Status.InActive;

      this.proclamationService.updateProclamation(val)
        .subscribe(
          () => {
            this.location.back();
            alert(this.translateService.instant('common.editSuccess'));
          },
          err => {
            alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
          }
        );
    }
  }

  cancel() {
    this.location.back();
  }

  openCropper(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
  }

  getImageCropperUrl(e) {
    const img = new ImgFullPathPipe().transform(e);
    this.avatarUrl = img;
    this.myForm.get('pic').setValue(this.avatarUrl);
    this.modalRef.hide();
  }

  fileUploadRequest(e) {
    const file = e.data.requestData.upload.file as File;
    const fileName = this.uploadFileServ.getRandomFileName(file.name);
    const file$ = new File([file.slice()], fileName);
    e.data.requestData.upload;
    e.data.requestData.file = file$;
    e.data.requestData.token = this.authService.getToken();
    delete e.data.requestData.upload;
  }

}
