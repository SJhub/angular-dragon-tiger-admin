import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsiteProclamationsComponent } from './insite-proclamations.component';

describe('InsiteProclamationsComponent', () => {
  let component: InsiteProclamationsComponent;
  let fixture: ComponentFixture<InsiteProclamationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsiteProclamationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsiteProclamationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
