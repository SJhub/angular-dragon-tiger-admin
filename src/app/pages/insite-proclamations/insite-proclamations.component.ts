import { UserRole } from './../../enum/user-role.enum';
import { formatDate } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProclamationTypesEnum } from '../../enum/proclamation-type.enum';
import { Status } from '../../enum/status.enum';
import { Proclamation } from '../../models/proclamation';
import { ProclamationService } from '../../services/proclamation.service';
import { addIndexToArray } from '../../common/global';

@Component({
    selector: 'app-insite-proclamations',
    templateUrl: './insite-proclamations.component.html',
    styleUrls: ['./insite-proclamations.component.css']
})
export class InsiteProclamationsComponent implements OnInit {

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    // displayedColumns: string[] = ['title', 'pic', 'type', 'status', 'created_at', 'updated_at', 'function'];
    displayedColumns: string[] = ['message', 'title', 'created_at', 'status', 'function', 'views'];
    dataSource = new MatTableDataSource<Proclamation>();

    public data: Proclamation[];
    public modalRef: BsModalRef;
    public Proclamation: Proclamation[];
    public modal: any;

    updateDate: any;
    userLoginLastAt: any;
    formatUserLoginLastAt: string;
    loginUserIdentity: number;
    messageArr = [];


    /**
     * proclamationTypes - 公告類型
     * const (
     *   ProclamationTypeGeneral = iota
     *   ProclamationTypeGame
     * )
     */
    proclamationTypes = ['一般', '遊戲'];
    defaultType = ProclamationTypesEnum.ProclamationTypeInSite;

    constructor(
        private translateService: TranslateService,
        private proclamationService: ProclamationService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
        this.userLoginLastAt = localStorage.getItem('last_login_at');
        this.loginUserIdentity = Number(localStorage.getItem('loginUserIdentity'));
        this.getProclamations();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        if (this.loginUserIdentity === UserRole.admin || this.loginUserIdentity === UserRole.management) {
            this.displayedColumns.splice(0, 1);
            this.displayedColumns.pop();
        } else {
            this.displayedColumns.splice(3, 2);
        }
        this.displayedColumns.unshift('index');

    }

    filterProclamations(data: any): any {
        return data.filter(it => it['type'] === this.defaultType);
    }

    getProclamations() {
        this.proclamationService.getProclamations()
            .subscribe(
                res => {
                    this.data = this.filterProclamations(res.data);
                    this.dataSource.data = addIndexToArray(this.data);
                    this.updateDate = this.dataSource.data.map(e => (e.updated_at));
                },
                error => console.log(error)
            );
    }

    unRead(dateTime) {
        const datetime = formatDate(dateTime, 'yyyy-MM-dd HH:mm:ss', 'en-US');
        if (this.userLoginLastAt) {
            this.formatUserLoginLastAt = formatDate(this.userLoginLastAt, 'yyyy-MM-dd HH:mm:ss', 'en-US');
        }
        const mesaageDate = new Date(datetime);
        const transDate = mesaageDate.getTime();
        const userLogin = new Date(this.formatUserLoginLastAt);
        const transUserDate = userLogin.getTime();

        if (transDate < transUserDate) {
            return this.translateService.instant('readed');
        } else {
            return this.translateService.instant('unRead');
        }
    }

    changeColor(message) {
        switch (message) {
            case this.translateService.instant('unRead'):
                return 'badge-danger';
        }
    }

    openModal(template: TemplateRef<any>, id: number) {
        this.proclamationService.getProclamation(id)
            .subscribe(res => {
                this.modal = res.data as Proclamation;
                this.modalRef = this.modalService.show(template, { class: 'modal-l' });
            });
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    delProclamation(id: number) {
        if (confirm(this.translateService.instant('common.wannaDelete'))) {
            this.proclamationService.deleteProclamation(id)
                .subscribe(
                    () => {
                        this.getProclamations();
                        alert(this.translateService.instant('common.deleteSuccess'));
                    },
                    err => {
                        alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
                    }
                );
        }
    }

    getStatusName(status: number): string {
        if (status === Status.InActive) {
            return this.translateService.instant('common.inActive');
        }

        return this.translateService.instant('common.active');
    }

    addProclamation(title: string, body: string) {
        if (this.loginUserIdentity === UserRole.admin) {
            const proclamation = {
                title: title,
                body: body,
                status: Status.Active,
                type: this.defaultType,
            };

            this.proclamationService.addProclamation(proclamation)
                .subscribe(
                    () => {
                        this.getProclamations();
                        alert(this.translateService.instant('common.addSuccess'));
                    },
                    err => {
                        alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`);
                    }
                );
        } else {
            alert('沒有權限');
            return;
        }
    }

    get UserRole() {
        return UserRole;
    }

    get Status() {
        return Status;
    }

}
