import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotAccountComponent } from './bot-account.component';

describe('BotAccountComponent', () => {
  let component: BotAccountComponent;
  let fixture: ComponentFixture<BotAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
