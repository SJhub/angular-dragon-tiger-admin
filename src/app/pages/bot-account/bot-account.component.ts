import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { AutoDonateMember } from '../../models/auto-donate-member';
import { BotMembers } from '../../models/bot-member';
import { AutoDonateMembersService } from '../../services/auto-donate-members.service';

@Component({
  selector: 'app-bot-account',
  templateUrl: './bot-account.component.html',
  styleUrls: ['./bot-account.component.css']
})
export class BotAccountComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['index', 'name', 'function'];
  dataSource = new MatTableDataSource<AutoDonateMember>();

  isFetched = false;
  isAdding = false;

  constructor(
    private translateService: TranslateService,
    private autoDonateMemberServ: AutoDonateMembersService
  ) { }

  ngOnInit(): void {
    this.getBotMembers();
  }

  getBotMembers() {
    this.autoDonateMemberServ.getAutoDonateMembers().subscribe(r => {
      this.dataSource.data = addIndexToArray(r.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isFetched = true;
    });
  }

  addBots(name: string) {
    name = name.trim();
    if (name) {
      const member = {
        game_types_id: GameTypeEnum.GameTypeSicBo,
        name,
      } as AutoDonateMember;
      this.autoDonateMemberServ.addAutoDonateMember(member).subscribe(
        () => this.getBotMembers(),
        () => alert(this.translateService.instant('common.addFailed'))
      );
    }
  }

  deleteBot(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.autoDonateMemberServ.deleteAutoDonateMember(id).subscribe(() => {
        this.getBotMembers();
      },
        err => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  onEdit(e: BotMembers) {
    e.edit = !e.edit;
    if (!e.edit) {
      if (e.name) {
        this.autoDonateMemberServ.editAutoDonateMember(e).subscribe();
      }
    }
  }
}
