import { BetRecord } from './../../models/bet-record';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BetRecordService } from '../../services/bet-record.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Status } from '../../enum/status.enum';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthenticationService } from '../../services/auth.service';

@Component({
  selector: 'app-bet-record-edit',
  templateUrl: './bet-record-edit.component.html',
  styleUrls: ['./bet-record-edit.component.css']
})
export class BetRecordEditComponent implements OnInit {

  record: BetRecord;
  myForm: FormGroup;
  private id: number;
  data: any;

  constructor(
    private betRecordService: BetRecordService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private location: Location,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm();
    }
  }

  groupForm() {
    this.myForm = this.fb.group({
      'message': ['']
    });
  }

  setForm() {
    this.betRecordService.getBetRecord(this.id)
      .subscribe(
        res => {
          const data = res.data;
          this.myForm.patchValue({
            message: data.message,
          });
        }
      );
  }

  submit(val: any) {
    if (this.myForm.valid) {

      val.id = this.id;
      const status = Number(val.status);

      this.betRecordService.updateBetRecordStatus(val.id, status)
        .subscribe(
          () => {
            this.location.back();
            alert('修改成功');
          },
          err => {
            alert(`操作失敗: ${err.error.message}`);
          }
        );
    }
  }

  cancel() {
    this.location.back();
  }

}
