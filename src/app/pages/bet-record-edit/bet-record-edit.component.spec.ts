import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetRecordEditComponent } from './bet-record-edit.component';

describe('BetRecordEditComponent', () => {
  let component: BetRecordEditComponent;
  let fixture: ComponentFixture<BetRecordEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetRecordEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetRecordEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
