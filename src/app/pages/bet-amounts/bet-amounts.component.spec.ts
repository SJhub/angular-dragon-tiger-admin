import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetAmountsComponent } from './bet-amounts.component';

describe('BetAmountsComponent', () => {
  let component: BetAmountsComponent;
  let fixture: ComponentFixture<BetAmountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetAmountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetAmountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
