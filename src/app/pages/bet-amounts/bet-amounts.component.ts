import { Component, OnInit, ViewChild } from '@angular/core';
import { Betamount } from '../../models/bet-amount';
import { BetAmountService } from '../../services/bet-amount.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Status } from '../../enum/status.enum';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';

const MAX_AUTO_BET = 3;
@Component({
  selector: 'app-bet-amounts',
  templateUrl: './bet-amounts.component.html',
  styleUrls: ['./bet-amounts.component.css']
})
export class BetAmountsComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['index', 'amount', 'updated_at', 'status'];
  dataSource = new MatTableDataSource<Betamount>();

  Betamount: Betamount[];

  actionType: number;

  data: any;

  constructor(
    private translateService: TranslateService,
    private betAmountService: BetAmountService,
  ) { }

  ngOnInit(): void {
    this.actionTypeOnChange(0);
    this.dataSource.sort = this.sort;
  }

  /**
   * 取得押注金額列表
   */
  getBetAmountList() {
    this.betAmountService.getBetAmountList().subscribe(
      res => {
        let data: Betamount[] = res.data;
        data = data.filter(e => e.game_types_id === GameTypeEnum.GameTypeDragonTiger);
        this.data = data;
        this.dataSource.data = addIndexToArray(this.data.filter(e => e.type === Number(this.actionType)));
        // console.log(this.dataSource.data);
      }
    );
  }

  actionTypeOnChange(actionType: number) {
    this.actionType = actionType;
    this.getBetAmountList();
  }

  updateBetAmountStatus(value: Betamount) {

    const data = {
      id: value.id,
      status: value.status ? 0 : 1,
    } as Betamount;

    this.betAmountService.updateBetAmountStatus(data).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
        this.getBetAmountList();
      },
      err => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  updateBetAmountIsAutoBet(value: Betamount, e: any) {
    const autoBetAmount = this.dataSource.data.filter(e => e.is_auto_bet).length;

    const data = {
      id: value.id,
      is_auto_bet: value.is_auto_bet ? 0 : 1,
    } as Betamount;

    if (data.is_auto_bet && autoBetAmount >= MAX_AUTO_BET) {
      e.preventDefault();
      return alert(`最多${MAX_AUTO_BET}個`);
    }
    if (!data.is_auto_bet && autoBetAmount == 1) {
      e.preventDefault();
      return alert(`最少1個`);
    };


    this.betAmountService.updateBetAmountIsAutoBet(data).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
        this.getBetAmountList();
      },
      err => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  changeBtnColor(status) {
    switch (status) {
      case 0:
        return 'btn-success';
      case 1:
        return 'btn-danger';
    }
  }

  get Status() { return Status; }

  get GameActionType() { return GameActionTypeEnum }
}
