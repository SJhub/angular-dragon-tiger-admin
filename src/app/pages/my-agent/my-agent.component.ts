import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { MemberService } from '../../services/member.service';
import { UserService } from '../../services/user.service';
import { MyMemberComponent } from '../my-member/my-member.component';

@Component({
  selector: 'app-my-agent',
  templateUrl: './my-agent.component.html',
  styleUrls: ['./my-agent.component.css']
})
export class MyAgentComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('myMember') myMember: MyMemberComponent;

  displayedColumns: string[] = ['index', 'name', 'real_name', 'wallet', 'wash_code_commission_percent', 'revenue_share', 'function'];
  dataSource = new MatTableDataSource<User>();
  loaded = false;

  loginID: number;
  nodes: User[] = [];
  current: User;
  isTopAgent = false;

  // 下線
  childAgents: User[];
  childMembers: Member[];


  constructor(
    private userServ: UserService,
    private memberServ: MemberService,
    private agentServ: AgentService,
    private authServ: AuthenticationService,
    private translateService: TranslateService,
    private router: Router
  ) {
    this.isTopAgent = Number(this.authServ.getUserIdentity()) == UserRole.topAgent;
  }

  ngOnInit(): void {
    this.loginID = Number(this.authServ.getUserId());
    this.getUserByID(this.loginID);
  }

  getUserByID(id: number) {
    this.loaded = false;
    this.dataSource.data = [];
    this.userServ.getUserByParentID(id).subscribe(r => {
      let data: User[] = r.data;
      data = data.filter(e => !e.clone_id && (e.identity === UserRole.agent || e.identity === UserRole.topAgent));
      this.getChildAgentAndMember(data.map(e => e.id));
      this.dataSource.data = data;
      this.loaded = true;
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  changeAgentAllowGame(agent: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: agent.id,
      is_allow_play: agent.is_allow_play ? 0 : 1,
    };

    this.agentServ.updateAgentIsAllowPlay(data).subscribe(
      () => {
        agent.is_allow_play = data.is_allow_play;
        let msg = this.translateService.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  changeAgentStatus(user: User) {
    const turnedOnMsg = this.translateService.instant('common.turnedOn');
    const turnedOffMsg = this.translateService.instant('common.turnedOff');
    const data = {
      id: user.id,
      status: user.status ? 0 : 1
    };

    this.agentServ.updateAgentStatus(data).subscribe(
      () => {
        user.status = data.status;
        let msg = this.translateService.instant('manageAgent.accountStatus');
        msg += data.status ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => {
        alert(`${this.translateService.instant('common.editFailed')}: ${err.error.message}`);
      }
    );
  }

  view(data: User) {
    this.current = data;
    this.nodes.push(data);
    this.loaded = false;
    this.dataSource.data = [];
    this.getUserByID(data.id);
  }

  pop() {
    this.nodes.pop();
    if (this.nodes.length) {
      this.current = this.nodes[this.nodes.length - 1];
      this.getUserByID(this.current.id);
    }
    else {
      this.getUserByID(this.loginID);
      this.current = null;
    }
  }

  deleteAgent(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.agentServ.deleteAgent(id).subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          this.dataSource.data = this.dataSource.data.filter(e => e.id != id);
        },
        err => alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`)
      );
    }
  }

  viewMember(data: User) {
    let url = 'my-members';
    let params = {
      ag: btoa(String(data.id)),
    };
    this.router.navigate([url], { queryParams: params });
  }

  getChildAgentAndMember(ids: Array<number>) {
    this.childAgents = [];
    this.childMembers = [];

    const req1 = ids.map(e => this.userServ.getUserByParentID(e));
    const req2 = ids.map(e => this.memberServ.getMemberByAgent(e));
    forkJoin(req1).pipe(
      map(res => res.map(r => r.data))
    ).subscribe(r => r.forEach(e => this.childAgents.push(...e)));

    forkJoin(req2).pipe(
      map(res => res.map(r => r.data))
    ).subscribe(r => r.forEach(e => this.childMembers.push(...e)));
  }

  getChildCount(id: number, isMember = false) {
    let agentCount = 0;
    let memberCount = 0;
    if (this.childAgents) {
      agentCount = this.childAgents.filter(e => e.parent_id === id && !e.clone_id).length;
    }
    if (this.childMembers) {
      memberCount = this.childMembers.filter(e => e.agent_id === id).length;
    }
    return isMember ? memberCount : agentCount;
  }

  get Status() { return Status }
}
