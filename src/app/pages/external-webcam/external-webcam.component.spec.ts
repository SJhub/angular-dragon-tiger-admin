import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalWebcamComponent } from './external-webcam.component';

describe('ExternalWebcamComponent', () => {
  let component: ExternalWebcamComponent;
  let fixture: ComponentFixture<ExternalWebcamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalWebcamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalWebcamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
