import { Location } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { options } from '../../ckeditor/config';
import { ProclamationTypesEnum } from '../../enum/proclamation-type.enum';
import { Proclamation } from '../../models/proclamation';
import { ImgFullPathPipe } from '../../pipe/img-full-path.pipe';
import { AuthenticationService } from '../../services/auth.service';
import { ProclamationService } from '../../services/proclamation.service';
import { UploadFileService } from '../../services/upload-file.service';

@Component({
  selector: 'app-normal-proclamation-edit',
  templateUrl: './normal-proclamation-edit.component.html',
  styleUrls: ['./normal-proclamation-edit.component.css']
})
export class NormalProclamationEditComponent implements OnInit {
  myForm: FormGroup;
  modalRef: BsModalRef;
  config = options;

  proclamation: Proclamation;

  id: number;
  avatarUrl: string;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private proclamationService: ProclamationService,
    private modalService: BsModalService,
    private uploadFileServ: UploadFileService,
    private authService: AuthenticationService,
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.id) {
      this.setForm(this.id);
    }
  }

  groupForm() {
    this.myForm = this.fb.group({
      title: [null, Validators.required],
      body: [null],
      pic: [null],
      status: [1]
    })
  }

  setForm(id: number) {
    this.proclamationService.getProclamation(id).subscribe(
      res => {
        const data = res.data as Proclamation;
        this.myForm.patchValue(data);
      }
    );
  }

  submit(val) {
    val.id = this.id;
    val.status = Number(val.status);
    val.type = ProclamationTypesEnum.ProclamationTypeGeneral;
    if (this.myForm.valid) {
      let req = this.proclamationService.addProclamation(val);
      let msgSuccess = this.translateService.instant('common.addSuccess');
      let msgFailed = this.translateService.instant('common.addFailed');
      if (this.id) {
        msgSuccess = this.translateService.instant('common.editSuccess');
        msgFailed = this.translateService.instant('common.editFailed');
        req = this.proclamationService.updateProclamation(val);
      }

      req.subscribe(
        () => {
          alert(msgSuccess);
          this.location.back();
        },
        () => alert(msgFailed)
      );
    }
  }

  openCropper(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
  }

  getImageCropperUrl(e) {
    const img = new ImgFullPathPipe().transform(e);
    this.avatarUrl = img;
    this.myForm.get('pic').setValue(this.avatarUrl);
    this.modalRef.hide();
  }

  fileUploadRequest(e) {
    const file = e.data.requestData.upload.file as File;
    const fileName = this.uploadFileServ.getRandomFileName(file.name);
    const file$ = new File([file.slice()], fileName);
    e.data.requestData.upload;
    e.data.requestData.file = file$;
    e.data.requestData.token = this.authService.getToken();
    delete e.data.requestData.upload;
  }

  cancel() {
    this.location.back();
  }
}
