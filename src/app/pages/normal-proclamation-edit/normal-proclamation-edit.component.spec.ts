import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalProclamationEditComponent } from './normal-proclamation-edit.component';

describe('NormalProclamationEditComponent', () => {
  let component: NormalProclamationEditComponent;
  let fixture: ComponentFixture<NormalProclamationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalProclamationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalProclamationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
