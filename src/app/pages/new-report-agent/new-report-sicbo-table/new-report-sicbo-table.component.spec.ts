import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReportSicboTableComponent } from './new-report-sicbo-table.component';

describe('NewReportSicboTableComponent', () => {
  let component: NewReportSicboTableComponent;
  let fixture: ComponentFixture<NewReportSicboTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReportSicboTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReportSicboTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
