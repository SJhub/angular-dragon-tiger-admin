import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReportMemberComponent } from './new-report-member.component';

describe('NewReportMemberComponent', () => {
  let component: NewReportMemberComponent;
  let fixture: ComponentFixture<NewReportMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReportMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReportMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
