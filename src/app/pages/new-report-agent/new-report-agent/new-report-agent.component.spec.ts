import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReportAgentComponent } from './new-report-agent.component';

describe('NewReportAgentComponent', () => {
  let component: NewReportAgentComponent;
  let fixture: ComponentFixture<NewReportAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReportAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReportAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
