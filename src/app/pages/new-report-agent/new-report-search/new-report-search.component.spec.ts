import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReportSearchComponent } from './new-report-search.component';

describe('NewReportSearchComponent', () => {
  let component: NewReportSearchComponent;
  let fixture: ComponentFixture<NewReportSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewReportSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReportSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
