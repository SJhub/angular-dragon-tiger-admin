import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Observable } from 'rxjs';
import { isFormCtrlInvalid, markFormGroupAsTouched } from '../../common/form';
import { BaccaratCroupierComponent } from '../../component/baccarat-croupier/baccarat-croupier.component';
import { DragonTigerCroupierComponent } from '../../component/dragon-tiger-croupier/dragon-tiger-croupier.component';
import { commonEnum } from '../../enum/common.enum';
import { GameMode } from '../../enum/game-mode.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { GameType } from '../../models/game-type';
import { ItemTypes } from '../../models/item-types';
import { PlayGame } from '../../models/play-game';
import { SystemSetting } from '../../models/system-setting';
import { AuthenticationService } from '../../services/auth.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { ItemTypesService } from '../../services/item-types.service';
import { LivesRestartService } from '../../services/lives-restart.service';
import { PlayGameService } from '../../services/play-game.service';
import { SystemService } from '../../services/system.service';

@Component({
  selector: 'app-timely-bet',
  templateUrl: './timely-bet.component.html',
  styleUrls: ['./timely-bet.component.css']
})
export class TimelyBetComponent implements OnInit {
  @ViewChild('baccaratCroupier') baccaratCroupier: BaccaratCroupierComponent;
  @ViewChild('dragonTigerCroupier') dragonTigerCroupier: DragonTigerCroupierComponent;

  tabIndex: number;
  loginIdentity: number;
  defaultGameTypeID = GameTypeEnum.GameTypeDragonTiger;

  systemSettings: SystemSetting[];
  currentSysSetting: SystemSetting;

  gameTypes: GameType[];
  currentGameType: GameType;

  itemTypes: ItemTypes[];

  gameCreating = false;

  // 直播機剩餘秒數
  liveRemainingTime1: Observable<string>;
  liveRemainingTime2: Observable<string>;

  // 開放日
  openWeek: number[] = [];

  // 預設系統設定
  defaultSystemSetting: SystemSetting;
  form: FormGroup;

  constructor(
    private systemService: SystemService,
    private gameTypeService: GameTypeService,
    private playGameService: PlayGameService,
    private liveRestartServ: LivesRestartService,
    private itemTypesService: ItemTypesService,
    private authServ: AuthenticationService,
    private translateService: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private fb: FormBuilder,
  ) {
    this.groupDefaultSystemSetting();
  }

  ngOnInit() {
    this.loginIdentity = Number(this.authServ.getUserIdentity());
    this.getGameData();
    this.getLiveRemainingTime();
  }

  getLiveRemainingTime() {
    this.liveRemainingTime1 = this.liveRestartServ.getRemainingTimeByGameLiveNumber(1);
    this.liveRemainingTime2 = this.liveRestartServ.getRemainingTimeByGameLiveNumber(2);
  }

  getGameData() {
    forkJoin([
      this.gameTypeService.getGameTypes(),
      this.systemService.getSystemSettings()
    ]).subscribe(r => {
      this.gameTypes = r[0].data;
      this.systemSettings = r[1].data;
      this.defaultSystemSetting = this.systemSettings[0];
      this.form.patchValue(this.defaultSystemSetting);
      this.onGameTypeChanged(this.defaultGameTypeID);
    });
  }

  onGameTypeChanged(gameTypeID: number) {
    console.log(gameTypeID);

    this.currentGameType = this.gameTypes.find(e => e.id == gameTypeID);
    this.currentSysSetting = this.systemSettings.find(e => e.game_types_id == gameTypeID);
    // this.openWeek = JSON.parse(this.currentGameType.time_to_open_week);
  }


  /**
   * 取得遊戲項目列表
   */
  getItemTypeList() {
    this.itemTypesService.getItemTypesList().subscribe(r => this.itemTypes = r.data);
  }

  updateDrawSeconds(value: number) {
    value = Number(value);
    if (value > 0) {
      this.systemService.updateDrawSeconds(this.currentSysSetting.id, value).subscribe(
        () => alert(this.translateService.instant('common.updateSuccess')),
        err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
      );
    }
  }

  updateGameTypeTime() {
    if (
      this.currentGameType.time_to_open &&
      this.currentGameType.time_to_close &&
      this.currentGameType.rest_min
    ) {
      this.currentGameType.rest_min = Number(this.currentGameType.rest_min);
      this.gameTypeService.updateGameType(this.currentGameType).subscribe(
        () => alert(this.translateService.instant('common.updateSuccess')),
        err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
      );
    }
  }

  newGame() {
    if (this.currentGameType.mode === GameMode.Auto) {
      return alert(this.translateService.instant('currentModeIsAuto'));
    }

    this.gameCreating = true;

    const playGame = {
      game_types_id: Number(this.currentGameType.id),
      countdown: Number(this.currentSysSetting.draw_seconds)
    } as PlayGame;

    this.playGameService.addPlayGame(playGame).subscribe(
      () => {
        alert(this.translateService.instant('common.addSuccess'));
        this.gameCreating = false;
      },
      err => {
        alert(`${this.translateService.instant('common.addFailed')}: ${err.error.message}`)
        this.gameCreating = false;
      });
  }


  /**
   * updateDrawResult - 更新開獎結果
   * @param val
   */
  updateDrawResult(val: string) {
    let drawResult = val;

    switch (this.currentGameType.id) {
      case GameTypeEnum.GameTypeSicBo:
        if (!val || val.length < 3) {
          return alert(this.translateService.instant('pleaseEnter3Numbers'));
        }

        if (this.currentGameType.mode === GameMode.Auto) {
          return alert(this.translateService.instant('currentModeIsAuto'));
        }

        if (val.includes('7') || val.includes('8') || val.includes('9')) {
          return alert(this.translateService.instant('pleaseEnterANumberBelow6'));
        }
        break;
      case GameTypeEnum.GameTypeDragonTiger:
        drawResult = this.dragonTigerCroupier.getFormValue();
        break;
      case GameTypeEnum.GameTypeBaccarat:
        drawResult = JSON.stringify(this.baccaratCroupier.getFormValue());
      default:
        break;
    }

    this.systemService.updateDrawResult(this.currentSysSetting.id, drawResult).subscribe(
      () => {
        alert(this.translateService.instant('common.submitSuccess'));
      },
      err => {
        alert(`${this.translateService.instant('common.submitFailed')}: ${err.error.message}`);
      }
    );
  }

  /**
   * invalidDrawResult - 此局無效
   * @param val
   */
  invalidDrawResult() {

    if (this.currentGameType.mode === GameMode.Auto) {
      return alert(this.translateService.instant('currentModeIsAuto'));
    }

    this.systemService.updateDrawResult(this.currentSysSetting.id, commonEnum.INVALID_RESULT)
      .subscribe(
        () => alert(this.translateService.instant('common.submitSuccess')),
        err => alert(`${this.translateService.instant('common.submitFailed')}: ${err.error.message}`)
      );
  }


  /**
   * 遊戲自動模式
   */
  setGameAuto() {
    this.changeGameMode(GameMode.Auto);
  }

  /**
   * 遊戲手動模式
   */
  setGameManual() {
    this.changeGameMode(GameMode.Manual);
  }

  /**
   * 修改遊戲模式
   */
  changeGameMode(mode: number) {
    this.gameTypeService.updateGameTypeMode(this.currentGameType.id, Number(mode))
      .subscribe(() => {
        this.currentGameType.mode = mode;
        return alert(this.translateService.instant('changeSuccess'));
      }, err => alert(this.translateService.instant('changeFailed') + `: ${err.error.message}`));
  }


  updateGameTypeStatus(check: boolean) {
    const status = Number(check);
    this.gameTypeService.updateGameTypeStatus(this.currentGameType.id, status)
      .subscribe(
        () => alert(this.translateService.instant('changeSuccess')),
        () => alert(this.translateService.instant('changeFailed'))
      );
  }

  selectedGameTypeId(gameTypeId: number) {
    if (gameTypeId > 1) {
      alert(this.translateService.instant('gameIsNotToMarket'));
      return;
    }
  }

  updateRestGameStatus(status: boolean) {
    const now = new Date();
    const restMin = this.currentGameType.rest_min;
    now.setMinutes(now.getMinutes() + Number(restMin));
    const timeToRest = status ? formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en-US') : null;
    this.gameTypeService.updateRestGameStatus(this.currentGameType.id, Number(status), timeToRest).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    );

  }

  updateBreakGameStatus(status: boolean) {
    const now = new Date();
    const restMin = this.currentGameType.rest_min;
    now.setMinutes(now.getMinutes() + Number(restMin));
    const timeToRest = status ? formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en-US') : null;
    this.gameTypeService.updateBreakGameStatus(this.currentGameType.id, Number(status), timeToRest).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    );

  }

  checkNumber(e: any, val: string) {
    const key = e.key
    const regxSingle = /[0-6]/;
    const regxResult = /^[1-6]{1,3}$|^0{1,3}$/;

    if (!regxSingle.test(key)) {
      e.preventDefault();
    }

    if (val && !regxResult.test(val + key)) {
      e.preventDefault();
    }
  }


  /**
   * 取得狀態名稱
   */
  get Status() { return Status; }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  getTranslatedWeek(type: number) {
    return this.enumTranslateServ.getTranslatedWeek(type);
  }

  /**
   * 重啟直播機
   */
  restartLive(num: number) {
    if (this.liveRestartServ.checkRestartTimeIsValid(num)) {
      const userID = Number(this.authServ.getUserId());
      // 本地測試
      // this.liveRestartServ.logRestartTime(num);
      // this.getLiveRemainingTime();
      // return;
      return this.liveRestartServ.restartByGameLiveNumber(num, userID).subscribe(
        () => {
          this.getLiveRemainingTime();
          alert(this.translateService.instant('common.updateSuccess'));
        },
        () => {
          alert(this.translateService.instant('common.updateFailed'));
        }
      );
    };
    return alert(this.translateService.instant('restartingStream'));
  }

  diffTime(time1: string, time2: string) {
    const t1 = this.getTimeObj(time1);
    const t2 = this.getTimeObj(time2);

    if (t1.hh > t2.hh) {
      return true;
    }

    if (t1.hh === t2.hh) {
      if (t1.mm > t2.mm) {
        return true;
      }
    }
    return false;
  }

  getTimeObj(str: string) {
    const time = str.split(':');
    return { hh: time[0], mm: time[1] };
  }

  /**
   * 開放日
   */
  onDayClick(checked: boolean, value: number) {
    if (checked) {
      return this.openWeek.push(value);
    }

    const i = this.openWeek.findIndex(e => e === value);
    i != -1 && this.openWeek.splice(i, 1);
  }

  isOpenWeekChecked(value: number) {
    return this.openWeek.includes(value);
  }

  updateOpenWeek() {
    this.openWeek = this.openWeek.sort((a, b) => a - b);
    const value = JSON.stringify(this.openWeek);
    this.gameTypeService.updateTimeToOpenWeek(this.currentGameType.id, value).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  updateSysSetting() {
    if (!this.form.valid) {
      return markFormGroupAsTouched(this.form);
    }
    let form = this.form.getRawValue();
    this.defaultSystemSetting.sec_of_repeat_msg = form.sec_of_repeat_msg;
    this.defaultSystemSetting.stop_bet_sec = form.stop_bet_sec;
    this.defaultSystemSetting.app_foreground_idle_sec = form.app_foreground_idle_sec;
    this.defaultSystemSetting.app_background_idle_sec = form.app_background_idle_sec;
    this.defaultSystemSetting.close_hall_tips = '尚未開放';
    this.defaultSystemSetting.close_entry_tips = '尚未開放';
    this.defaultSystemSetting.break_game_tips = form.break_game_tips;
    this.defaultSystemSetting.rest_game_tips = form.rest_game_tips;
    forkJoin([
      this.systemService.updateSystemSetting(this.currentSysSetting),
      this.systemService.updateSystemSetting(this.defaultSystemSetting)
    ]).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  groupDefaultSystemSetting() {
    this.form = this.fb.group({
      sec_of_repeat_msg: [0, [Validators.required, Validators.min(0)]],
      stop_bet_sec: [0, [Validators.required, Validators.min(0)]],
      app_foreground_idle_sec: [0, [Validators.required, Validators.min(0)]],
      app_background_idle_sec: [0, [Validators.required, Validators.min(0)]],
      rest_game_tips: ['', [Validators.required, Validators.maxLength(20)]],
      break_game_tips: ['', [Validators.required, Validators.maxLength(20)]],
    });
  }

  isFormCtrlInvalid(ctrl: AbstractControl) { return isFormCtrlInvalid(ctrl) }

  get UserRole() { return UserRole }

  get GameTypeEnum() { return GameTypeEnum }

  get Weeks() { return [0, 1, 2, 3, 4, 5, 6] }
}
