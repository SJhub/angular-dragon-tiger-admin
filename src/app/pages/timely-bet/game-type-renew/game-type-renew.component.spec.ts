import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTypeRenewComponent } from './game-type-renew.component';

describe('GameTypeRenewComponent', () => {
  let component: GameTypeRenewComponent;
  let fixture: ComponentFixture<GameTypeRenewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameTypeRenewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTypeRenewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
