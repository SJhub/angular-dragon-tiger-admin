import { formatDate } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaccaratCroupierComponent } from '../../../component/baccarat-croupier/baccarat-croupier.component';
import { DragonTigerCroupierComponent } from '../../../component/dragon-tiger-croupier/dragon-tiger-croupier.component';
import { GameMode } from '../../../enum/game-mode.enum';
import { GameTypeEnum } from '../../../enum/game-type.enum';
import { GameType } from '../../../models/game-type';
import { PlayGame } from '../../../models/play-game';
import { AuthenticationService } from '../../../services/auth.service';
import { EnumTranslateService } from '../../../services/enum-translate.service';
import { GameTypeService } from '../../../services/game-type.service';
import { PlayGameService } from '../../../services/play-game.service';
import { RenewDrawResult, SystemService } from '../../../services/system.service';

@Component({
  selector: 'app-game-type-renew',
  templateUrl: './game-type-renew.component.html',
  styleUrls: ['./game-type-renew.component.css']
})
export class GameTypeRenewComponent implements OnInit {
  @ViewChild('baccaratCroupier') baccaratCroupier: BaccaratCroupierComponent;
  @ViewChild('dragonTigerCroupier') dragonTigerCroupier: DragonTigerCroupierComponent;
  @Input() onlyLastGame = false;

  gameTypeID = GameTypeEnum.GameTypeDragonTiger;
  gameTypes: GameType[];
  currentGameType: GameType;

  latestPlayGames: PlayGame[];
  currentPlayGame: PlayGame;

  loaded = false;

  constructor(
    private authServ: AuthenticationService,
    private playGameServ: PlayGameService,
    private translateServ: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private gameTypeServ: GameTypeService,
    private systemServ: SystemService
  ) { }

  ngOnInit(): void {
    this.gameTypeServ.getGameTypes().subscribe(r => {
      this.gameTypes = r.data;
      this.onGameTypeChanged(this.gameTypeID);
    });
  }

  refresh() {
    this.onGameTypeChanged(this.gameTypeID);
  }

  onGameTypeChanged(id: number) {
    this.currentGameType = this.gameTypes.find(e => e.id == id);
    this.getPlayGameToday(this.currentGameType.id);
  }

  onGameNumberChanged(gameNumber: number) {
    const gameResult = this.latestPlayGames.filter(e => e.game_number === gameNumber);
    this.currentPlayGame = gameResult[0];
    // this.baccaratCroupier.setFormValue(this.currentPlayGame.draw_result);
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  getPlayGameToday(gameTypeID: number) {
    this.loaded = false;
    this.currentPlayGame = null;
    const now = new Date();
    let t1 = new Date();
    let t2 = new Date();
    t1.setDate(now.getDate() - 1);
    t1.setHours(12, 0, 0);
    t2.setHours(12, 0, 0);

    if (now.getHours() >= 12) {
      t1 = t2;
    }

    const startDate = formatDate(t1, 'yyyy-MM-dd HH:mm:ss', 'en-US');
    const endDate = formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en-US');

    this.playGameServ.getPlayGameBetweenDate(gameTypeID, startDate, endDate)
      .subscribe(r => {
        let playGames: PlayGame[] = r.data;
        playGames = playGames.filter(e => e.mode === GameMode.Manual);
        const num = 10;
        this.loaded = true;

        if (playGames.length) {
          this.latestPlayGames = playGames.splice(playGames.length - num, num).sort((a, b) => b.id - a.id);

          if (this.onlyLastGame) {
            this.onGameNumberChanged(this.latestPlayGames[0].game_number);
          }
          return;
        }
        this.latestPlayGames = [];
      });
  }

  updateResultRenew(val: string) {
    let newDrawResult = val;

    if (this.currentPlayGame) {
      switch (this.currentGameType.id) {
        case GameTypeEnum.GameTypeSicBo:
          if (val.length > 3) {
            return alert(this.translateServ.instant('pleaseEnter3Numbers'));
          }

          if (val.includes('7') || val.includes('8') || val.includes('9')) {
            return alert(this.translateServ.instant('pleaseEnterANumberBelow6'));
          }

          if (val === this.currentPlayGame.draw_result) {
            return alert(this.translateServ.instant('doNotEnterRepeatedValues'));
          }

          if (this.currentGameType.mode === GameMode.Auto) {
            return alert(this.translateServ.instant('currentModeIsAuto'));
          }
          break;

        case GameTypeEnum.GameTypeBaccarat:
          newDrawResult = JSON.stringify(this.baccaratCroupier.getFormValue());
          this.baccaratCroupier.resetForm();
          break;
        case GameTypeEnum.GameTypeDragonTiger:
          newDrawResult = this.dragonTigerCroupier.getFormValue();
          this.dragonTigerCroupier.resetForm();
          break;
      }

      if (val === '000') {
        newDrawResult = '000';
      }

      const renewDrawResult: RenewDrawResult = {
        id: this.currentPlayGame.id,
        users_id: Number(this.authServ.getUserId()),
        game_types_id: this.currentPlayGame.game_types_id,
        game_number: this.currentPlayGame.game_number,
        draw_result: newDrawResult
      }

      this.systemServ.updateResultRenew(renewDrawResult).subscribe(
        () => {
          alert(this.translateServ.instant('common.updateSuccess'));
          this.getPlayGameToday(this.currentGameType.id);
        },
        () => alert(this.translateServ.instant('common.updateFailed'))
      );
    }
  }

  updateWithInvalid() {
    if (this.currentPlayGame) {
      this.updateResultRenew('000');
    }
  }

  checkNumber(e: any, val: string) {
    const key = e.key
    const regxSingle = /[0-6]/;
    const regxResult = /^[1-6]{1,3}$|^0{1,3}$/;

    if (!regxSingle.test(key)) {
      e.preventDefault();
    }

    if (val && !regxResult.test(val + key)) {
      e.preventDefault();
    }
  }

  get GameTypeEnum() { return GameTypeEnum }
}
