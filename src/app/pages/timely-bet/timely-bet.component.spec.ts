import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelyBetComponent } from './timely-bet.component';

describe('TimelyBetComponent', () => {
  let component: TimelyBetComponent;
  let fixture: ComponentFixture<TimelyBetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelyBetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelyBetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
