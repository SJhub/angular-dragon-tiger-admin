import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { options } from '../../../ckeditor/config';
import { GameTypeEnum } from '../../../enum/game-type.enum';
import { GameType } from '../../../models/game-type';
import { AuthenticationService } from '../../../services/auth.service';
import { EnumTranslateService } from '../../../services/enum-translate.service';
import { GameTypeService } from '../../../services/game-type.service';
import { UploadFileService } from '../../../services/upload-file.service';

@Component({
  selector: 'app-game-type-intro',
  templateUrl: './game-type-intro.component.html',
  styleUrls: ['./game-type-intro.component.css']
})
export class GameTypeIntroComponent implements OnInit {
  config = options;
  gameTypeID = GameTypeEnum.GameTypeDragonTiger;
  gameTypes: GameType[];
  currentGameType: GameType;

  constructor(
    private translateServ: TranslateService,
    private enumTranslateServ: EnumTranslateService,
    private authServ: AuthenticationService,
    private gameTypeServ: GameTypeService,
    private uploadFileServ: UploadFileService
  ) { }

  ngOnInit(): void {
    this.gameTypeServ.getGameTypes().subscribe(r => {
      this.gameTypes = r.data;
      this.onGameTypeChanged(this.gameTypeID);
    });
  }

  onGameTypeChanged(id: number) {
    this.currentGameType = this.gameTypes.find(e => e.id == id);
  }

  updateGameType() {
    delete this.currentGameType.play_member_plus;
    this.gameTypeServ.updateGameType(this.currentGameType).subscribe(
      () => alert(this.translateServ.instant('common.updateSuccess')),
      () => alert(this.translateServ.instant('common.updateFailed'))
    );
  }

  fileUploadRequest(e) {
    const file = e.data.requestData.upload.file as File;
    const fileName = this.uploadFileServ.getRandomFileName(file.name);
    const file$ = new File([file.slice()], fileName);
    e.data.requestData.upload;
    e.data.requestData.file = file$;
    e.data.requestData.token = this.authServ.getToken();
    delete e.data.requestData.upload;
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }
}
