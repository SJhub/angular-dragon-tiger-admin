import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTypeIntroComponent } from './game-type-intro.component';

describe('GameTypeIntroComponent', () => {
  let component: GameTypeIntroComponent;
  let fixture: ComponentFixture<GameTypeIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameTypeIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTypeIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
