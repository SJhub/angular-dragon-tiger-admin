import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualOnlineMembersComponent } from './virtual-online-members.component';

describe('VirtualOnlineMembersComponent', () => {
  let component: VirtualOnlineMembersComponent;
  let fixture: ComponentFixture<VirtualOnlineMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualOnlineMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualOnlineMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
