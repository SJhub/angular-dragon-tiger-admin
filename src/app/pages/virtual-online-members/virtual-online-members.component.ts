import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { AutoDonateSetting } from '../../models/auto-donate-setting';
import { GameType } from '../../models/game-type';
import { VirtualOnline } from '../../models/virtual-online';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { VirtualOnlineMemberService } from '../../services/virtual-online-member.service';

const MAX_SETTING_LIMITED = 5; // 時段最大筆數

@Component({
  selector: 'app-virtual-online-members',
  templateUrl: './virtual-online-members.component.html',
  styleUrls: ['./virtual-online-members.component.css']
})
export class VirtualOnlineMembersComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['index', 'game_types_id', 'start_time', 'number_of_member', 'status', 'function'];
  dataSource = new MatTableDataSource<AutoDonateSetting>();

  gameTypes: GameType[];
  isFetched = false;


  constructor(
    private gameTypeServ: GameTypeService,
    private virtualOnlineServ: VirtualOnlineMemberService,
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.getVirtualOnlines();
    this.getGameTypes();
  }

  getVirtualOnlines() {
    this.virtualOnlineServ.getVirtualOnlines().subscribe(r => {
      let data: VirtualOnline[] = r.data;
      data = data.filter(e => e.game_types_id === GameTypeEnum.GameTypeSicBo);
      this.dataSource.data = addIndexToArray(data);
      this.isFetched = true;
    });
  }

  getGameTypes() {
    this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
  }

  delete(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.virtualOnlineServ.deleteVirtualOnline(id).subscribe(() => {
        this.getVirtualOnlines();
        alert(this.translateService.instant('common.deleteSuccess'));
      },
        err => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  getTranslatedGameTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameTypeByID(id);
  }

  isSettingInLimited() {
    return true;
    // return this.isFetched && this.dataSource.data.length < MAX_SETTING_LIMITED;
  }

  get Status() { return Status }
}
