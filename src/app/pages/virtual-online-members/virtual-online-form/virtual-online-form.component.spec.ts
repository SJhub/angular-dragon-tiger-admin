import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualOnlineFormComponent } from './virtual-online-form.component';

describe('VirtualOnlineFormComponent', () => {
  let component: VirtualOnlineFormComponent;
  let fixture: ComponentFixture<VirtualOnlineFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualOnlineFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualOnlineFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
