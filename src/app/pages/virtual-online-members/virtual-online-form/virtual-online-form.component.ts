import { formatDate, Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../../common/global';
import { GameTypeEnum } from '../../../enum/game-type.enum';
import { AutoDonateSetting } from '../../../models/auto-donate-setting';
import { BotMembers } from '../../../models/bot-member';
import { GameType } from '../../../models/game-type';
import { VirtualOnline } from '../../../models/virtual-online';
import { AutoDonateMembersService } from '../../../services/auto-donate-members.service';
import { AutoDonateSettingsService } from '../../../services/auto-donate-settings.service';
import { BotMembersService } from '../../../services/bot-members.service';
import { EnumTranslateService } from '../../../services/enum-translate.service';
import { GameTypeService } from '../../../services/game-type.service';
import { VirtualOnlineMemberService } from '../../../services/virtual-online-member.service';

const LIMITED_BOTS_NUM = 50; // 單次新增限制

@Component({
  selector: 'app-virtual-online-form',
  templateUrl: './virtual-online-form.component.html',
  styleUrls: ['./virtual-online-form.component.css']
})
export class VirtualOnlineFormComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['index', 'name', 'function'];
  dataSource = new MatTableDataSource<BotMembers>();

  id: number;
  autoDonateMemberLength: number;
  autoDonate: AutoDonateSetting;
  myForm: FormGroup;
  gameTypes: GameType[];
  period: Array<string>;
  existPeriod = [];
  botNumber: number;
  isFetched = false;
  isAdding = false;

  constructor(
    private autoDonateMemberServ: AutoDonateMembersService,
    private virtualOnlineServ: VirtualOnlineMemberService,
    private botMemberServ: BotMembersService,
    private gameTypeServ: GameTypeService,
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder
  ) {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.period = this.getTimePeriod(0, 24);
    this.groupForm();
    this.setForm();
  }

  ngOnInit(): void {
    this.getGameTypes();
    this.getVirtualOnlines();
    this.getBotMembers();
  }

  getBotMembers() {
    this.autoDonateMemberServ.getAutoDonateMembers().subscribe(r => this.autoDonateMemberLength = r.data.length);
  }

  groupForm() {
    this.myForm = this.fb.group({
      'id': [null],
      'game_types_id': [GameTypeEnum.GameTypeSicBo],
      'start_time': ["", Validators.required],
      'end_time': ["", Validators.required],
      'number_of_member': [null, Validators.required],
      'status': [1, Validators.required],
    });
  }

  setForm() {
    if (this.id) {
      this.virtualOnlineServ.getVirtualOnline(this.id).subscribe(r => {
        const data: AutoDonateSetting = r.data;
        this.myForm.patchValue(data);
        this.autoDonate = data;
      });

      this.getBots();
    }
  }

  submit(val: VirtualOnline) {
    if (this.myForm.valid) {
      val.number_of_member = Number(val.number_of_member);
      val.status = Number(val.status);
      let req = this.virtualOnlineServ.addVirtualOnline(val);
      if (this.id) {
        req = this.virtualOnlineServ.editVirtualOnline(val);
      }
      req.subscribe(r => {
        const settingID = r.data;
        const num = Number(this.botNumber);
        // // 自動添加機器人
        // if (num && settingID) {
        //   if (num + this.dataSource.data.length > this.autoDonateMemberLength) {
        //     return alert(this.translateService.instant('autoDonateMemberNotEnough'));
        //   }
        //   this.addBotMembersRand(val.game_types_id, settingID, num, false);
        // }

        let msg = this.translateService.instant('common.addSuccess');
        if (this.id) {
          msg = this.translateService.instant('common.editSuccess');
        }
        alert(msg);
        this.location.back();
      }, () => alert(this.translateService.instant('common.addFailed')));
    }

    this.markFormGroupTouched(this.myForm);
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  getVirtualOnlines() {
    this.virtualOnlineServ.getVirtualOnlines().subscribe(r => {
      const data: AutoDonateSetting[] = r.data;
      this.setExistTimePeriod(data);
    });
  }

  getBots() {
    this.botMemberServ.getBotMembersBySettingID(this.id).subscribe(r => {
      this.dataSource.data = addIndexToArray(r.data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isFetched = true;
    });
  }

  addBotMembersRand(game_types_id: number, auto_donate_settings_id: number, number: number, fetch = true) {
    this.isAdding = true;
    number = Number(number);
    this.botMemberServ.addBotMembersRand(game_types_id, auto_donate_settings_id, number).subscribe(r => {
      this.isAdding = false;
      if (fetch) {
        this.getBots();
      }
    });
  }

  addBots() {
    const num = Number(this.botNumber);
    this.botNumber = null;
    if (num > 0) {
      if (num + this.dataSource.data.length > this.autoDonateMemberLength) {
        return alert(this.translateService.instant('autoDonateMemberNotEnough'));
      }

      if (num > LIMITED_BOTS_NUM) {
        return alert(this.translateService.instant('botSettings.limitedBots', { num: LIMITED_BOTS_NUM }));
      }
      this.addBotMembersRand(this.autoDonate.game_types_id, this.autoDonate.id, num);
    }
  }

  getTimePeriod(start: number, range: number) {
    const period = [];
    const date = new Date();
    date.setMinutes(0);
    for (let i = 0; i < range; i++) {
      date.setHours(start + i);
      period.push(formatDate(date, 'HH:mm', 'en-US'));
    }
    return period;
  }

  setExistTimePeriod(settings: AutoDonateSetting[]) {
    settings.forEach(e => {
      if (e.id !== this.id) {
        const start = Number(e.start_time.substring(0, 2));
        const end = Number(e.end_time.substring(0, 2));
        const range = end - start + 1;
        this.existPeriod.push(...this.getTimePeriod(start, range));
      }
    });
  }

  nextPeriod(p: string) {
    const index = this.period.findIndex(e => e === p);
    if (index === this.period.length - 1) {
      return this.period[0];
    }
    return this.period[index + 1];
  }

  isBigThanStart(value: string) {
    const ctrl = this.myForm.get('start_time').value;
    const start = Number(ctrl.substring(0, 2));
    const end = Number(value.substring(0, 2));
    return end <= start;
  }

  onStartTimeChange() {
    this.myForm.get('end_time').setValue("");
  }

  getGameTypes() {
    this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
  }

  getTranslatedGameType(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  cancel() {
    this.location.back();
  }

  deleteBot(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.botMemberServ.deleteBotMember(id).subscribe(() => {
        this.getBots();
      },
        err => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  deleteAllBots() {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.botMemberServ.deleteAllBotMemberBySettingID(this.id).subscribe(() => {
        this.getBots();
      },
        err => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  onEdit(e: BotMembers) {
    e.edit = !e.edit;
    if (!e.edit) {
      if (e.name) {
        this.botMemberServ.editBotMember(e).subscribe();
      }
    }
  }

  isPeriodExist(start: string) {
    return this.existPeriod.includes(start);
  }
}
