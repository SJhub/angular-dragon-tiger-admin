import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AuthenticationService } from '../../services/auth.service';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Roles } from '../../enum/roles.enum';
import { GameTypeService } from '../../services/game-type.service';
import { MemberService } from '../../services/member.service';
import { Member } from '../../models/member.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin, fromEvent, timer } from 'rxjs';
import { last, map, repeat, switchMap, takeUntil, throttleTime } from 'rxjs/operators';
import { Status } from '../../enum/status.enum';
import { EnumValueToEnumKeyPipe } from '../../pipe/enum-value-to-enum-key.pipe';
import { UserRole } from '../../enum/user-role.enum';
import { PlayGameService } from '../../services/play-game.service';
import { PlayGame } from '../../models/play-game';
import { formatDate, Location } from '@angular/common';
import { BetRecordService } from '../../services/bet-record.service';
import { GameType } from '../../models/game-type';
import { ItemTypesService } from '../../services/item-types.service';
import { ItemTypes } from '../../models/item-types';
import { LanguageService } from '../../services/language.service';
import { NavItemService } from '../../services/nav-item.service';
import { TranslateService } from '@ngx-translate/core';
import { GameEventService } from '../../services/game-event.service';
import { EventEnum } from '../../enum/event.enum';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { AgentService } from '../../services/agent.service';
import { defaultAgentID } from '../../common/global';

const IDLE_MIN = 5; // 閒置自動登出時間(分)
const ONLINE_MIN = 5; // 上線人數刷新間隔(秒)
@Component({
    selector: 'app-dashboard',
    templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {

    @ViewChild('memberBetsort', { static: true }) memberBetsort: MatSort;
    @ViewChild('memberInHallsort', { static: true }) memberInHallsort: MatSort;
    @ViewChild('memberInGameSort', { static: true }) memberInGameSort: MatSort;

    displayedColumns: string[] = ['account', 'ip', 'is_allow_play', 'function'];
    dataSource = new MatTableDataSource<Member>();

    InGamedisplayedColumns: string[] = ['InGameAccount', 'ip', 'is_allow_play', 'modal'];
    InGamedataSource = new MatTableDataSource<Member>();

    memberBetDisplayedColumns: string[] = ['game_types_id', 'game_number', 'item_types_id', 'action', 'amount', 'created_at'];
    memberBetDataSource = new MatTableDataSource<any>();

    public sidebarMinimized = false;
    public navItems;
    public real_name: string;
    public avatarUrl = 'default';
    users: User[];
    loginUserID: number;
    loginUser: User;
    userRolesName: string;
    datamap: any;
    Member: Member[];
    modalOfmemberOnline: Member[];
    modalRef: BsModalRef;
    modalRefMemberBet: BsModalRef;
    loginUserRole: number;
    identity: number;
    agentID: number;
    identityNumber: number;

    memberIsLogin: number;
    memberIsPlayGame: number;

    intervalInHall: any;
    intervalInGame: any;
    intervalMember: any;
    intervalAgent: any;
    intervalRemote: any;

    memberOnlineNum = 0;
    agentOnlineNum = 0;

    num = 0;
    interval: any;
    lastGameData: PlayGame;
    lastGameTypesId: number;
    lastGameNumber: any;
    lastGameCreatedAt: any;
    data: any;
    intervalBet: any;
    memberInfo: Member;
    nickName: any;
    memberName: any;
    memberAllowPlay: any;
    memberID: number;
    intervalMemberBet: any;
    GameType: GameType[];
    ItemTypes: ItemTypes[];
    dataOfBet = false;
    dataOfHall = false;
    dataOfGame = false;
    timelyBetData: any;

    currentLanguage: string;
    deviceLoginAt: string;

    lastUrl: string;
    sameUrlNavigation = false;

    constructor(
        private enumTranslateServ: EnumTranslateService,
        private translateService: TranslateService,
        private navItemService: NavItemService,
        private languageService: LanguageService,
        private authenticationService: AuthenticationService,
        private router: Router,
        private userService: UserService,
        private agentService: AgentService,
        private gameTypeService: GameTypeService,
        private memberService: MemberService,
        private modalService: BsModalService,
        private playGameService: PlayGameService,
        private betRecordService: BetRecordService,
        private itemTypesService: ItemTypesService,
        private gameEventService: GameEventService,
    ) {
        const idleTime$ = timer(0, 1000);
        const mouseMove$ = fromEvent<MouseEvent>(document, 'mousemove');

        const countdown = idleTime$.pipe(
            takeUntil(mouseMove$),
            repeat()
        ).subscribe(time => {
            if (time > IDLE_MIN * 60 && (this.identity == UserRole.agent || this.identity == UserRole.topAgent)) {
                this.authenticationService.logout();
                this.router.navigate(['/login']);
                // alert(this.translateService.instant('system.autoLogout'));
                countdown.unsubscribe();
                window.location.reload();
            }
        });

        // when onSameUrlNavigation: 'reload'，會重新觸發 router event
        this.router.onSameUrlNavigation = 'reload';
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.sameUrlNavigation = event.url === this.lastUrl;
                this.lastUrl = event.url;
            }
        });
    }

    ngOnInit() {
        this.loginUserID = Number(localStorage.getItem('authenticatedID'));
        this.getRealName();
        this.getUser();
        this.getGameTypeList();
        this.getItemTypeList();
        this.getCurrentLanguage();

        this.memberBetDataSource.sort = this.memberBetsort;
        this.dataSource.sort = this.memberInHallsort;
        this.InGamedataSource.sort = this.memberInGameSort;
    }

    getCurrentLanguage() {
        this.languageService.language$.subscribe(lang => this.currentLanguage = lang.lang);
    }

    useLanguage(language: string) {
        this.languageService.setLang(language);
    }

    ngOnDestroy() {
        clearInterval(this.interval);
        clearInterval(this.intervalInGame);
        clearInterval(this.intervalInHall);
        clearInterval(this.intervalMemberBet);
        clearInterval(this.intervalBet);
        clearInterval(this.intervalRemote);
        clearInterval(this.intervalMember);
        clearInterval(this.intervalAgent);
    }

    clearInterval() {
        clearInterval(this.intervalInGame);
        clearInterval(this.intervalInHall);
        clearInterval(this.intervalMemberBet);
    }

    toggleMinimize(e) {
        this.sidebarMinimized = e;
    }

    /**
     * logout - member logout
     */
    logout(): void {
        // if (confirm(this.translateService.instant('system.logoutConfirm'))) {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
        // }
    }

    /**
     * 取用戶名
     */
    getRealName() {
        const id = this.authenticationService.getUserId();
        this.authenticationService.getUserName(id).subscribe(() => {
            this.real_name = this.authenticationService.getLoggedInUserName();
        });
    }

    getAllowGameStatus(AllowGameStatus: number) {
        switch (AllowGameStatus) {
            case 0:
                return this.translateService.instant('game.gameIsDeactivated');
            case 1:
                return this.translateService.instant('game.gameIsActivated');
        }
    }

    getReceiptType(loginUserRole) {
        return new EnumValueToEnumKeyPipe().transform(loginUserRole, UserRole);
    }

    getTranslatedReceiptType(type: number) {
        return this.enumTranslateServ.getTranslatedReceiptType(type);
    }

    /**
     * 取得用戶資料
     */
    getUser() {
        // clearInterval(this.interval);
        this.userService.getUser(this.loginUserID).subscribe(
            res => {
                const data = res.data;
                const userLoginStr = this.authenticationService.getUserLogin();
                this.deviceLoginAt = this.authenticationService.getDeviceLoginAt();
                this.loginUser = data;
                this.agentID = data.id;
                this.identity = data.identity;
                this.getUserRoles();
                this.getAgentsOnline(this.identity, this.agentID);
                localStorage.setItem('loginUserIdentity', String(this.loginUser.identity));

                if (this.identity === UserRole.dealers) {
                    return this.router.navigate(['/dealers/operation']);
                }

                if (this.identity === UserRole.admin || this.identity === UserRole.management) {
                    this.getMembers();
                    this.navigateToUser(true);
                } else {
                    this.getChildAgentByUserID(this.loginUserID);
                    const userLogin = userLoginStr ? JSON.parse(userLoginStr) : null;
                    // if (userLogin && userLogin.is_first_change_password || this.loginUser.clone_id) {
                    this.navigateToUser(false);
                    // }
                }

                if (this.identity === UserRole.agent) {
                    this.turnOffMemberAutoBetByAgentID(this.agentID);
                }

                if (this.identity !== 1) {
                    this.displayedColumns.splice(2, 1);
                }

                // 取本尊餘額
                if (this.loginUser.clone_id) {
                    this.getCloneWallet(this.loginUser.clone_id);
                }

                this.identityNumber = Number(localStorage.getItem('loginUserIdentity'));

                this.rolesCheck();
                this.remoteLoginCheck();
            }
        );
    }

    getCloneWallet(cloneID: number) {
        this.userService.getUser(cloneID).subscribe(r => {
            const user: User = r.data;
            this.loginUser.wallet = user.wallet;
        });
    }

    navigateToUser(isAdmin: boolean) {
        if (this.router.url.includes('appadmin')) {
            if (isAdmin) {
                return this.router.navigate(['/users']);
            }
            return this.router.navigate(['/system/report-record-agent']);
        }
    }

    /**取遊玩中會員 */
    getMembers() {
        this.memberService.getMemberList().subscribe(r => this.filterMemberOnlineNum(r.data));
        if (!this.intervalMember) {
            this.intervalMember = setInterval(() => this.getMembers(), ONLINE_MIN * 1000);
        }
    }

    getChildAgentByUserID(id: number) {
        this.agentService.getAgentChildsIdByAgentsId(id).subscribe(r => this.getChildMembersByParentIDs(r.data));
        if (!this.intervalMember) {
            this.intervalMember = setInterval(() => this.getChildAgentByUserID(id), ONLINE_MIN * 1000);
        }
    }

    getChildMembersByParentIDs(ids: Array<number>) {
        ids = ids ? ids : [];
        const req = ids.map(id => this.memberService.getMemberByAgent(id));
        req.push(this.memberService.getMemberByAgent(this.loginUserID));
        forkJoin(req).pipe(map(res => res.map(r => r.data)))
            .subscribe(r => {
                const members: Member[] = [];
                r.forEach(e => members.push(...e));
                this.filterMemberOnlineNum(members);
            });
    }

    filterMemberOnlineNum(members: Member[]) {
        this.memberOnlineNum = members.filter(m => m.agent_id !== defaultAgentID && m.is_play_game).length;
    }

    /**
     * 取得會員資料 - 最高權限
     */
    getMemberList() {
        this.memberService.getMemberList().subscribe(
            res => {
                const data = res.data;
                const memberIsLogin = data.map(e => e.is_login);
                this.memberIsLogin = 0;
                memberIsLogin.forEach(e => {
                    this.memberIsLogin = this.memberIsLogin + e;
                });

                const memberIsPlayGame = data.map(e => e.is_play_game);
                this.memberIsPlayGame = 0;
                memberIsPlayGame.forEach(e => {
                    this.memberIsPlayGame = this.memberIsPlayGame + e;
                });
            }
        );
    }

    /**
     * 取得會員資料 by Agent ID
     */
    getMemberByAgent(agentID) {
        this.memberService.getMemberByAgent(agentID).subscribe(
            res => {
                const data = res.data;

                const memberIsLogin = data.map(e => e.is_login);
                this.memberIsLogin = 0;
                memberIsLogin.forEach(e => {
                    this.memberIsLogin = this.memberIsLogin + e;
                });

                const memberIsPlayGame = data.map(e => e.is_play_game);
                this.memberIsPlayGame = 0;
                memberIsPlayGame.forEach(e => {
                    this.memberIsPlayGame = this.memberIsPlayGame + e;
                });
            }
        );
    }

    /**
     * getUserRoles - 取得用戶角色
     */
    getUserRoles() {
        const roles = this.authenticationService.getUserRoles();
        switch (roles) {
            case Roles.VALUE_ADMIN:
                this.userRolesName = Roles.NAME_ADMIN;
                break;
            case Roles.VALUE_MANAGEMENT:
                this.userRolesName = Roles.NAME_MANAGEMENT;
                break;
            case Roles.VALUE_TOP_AGENT:
                this.userRolesName = Roles.NAME_TOP_AGENT;
                break;
            case Roles.VALUE_AGENT:
                this.userRolesName = Roles.NAME_AGENT;
                break;
            default:
                return '';
        }
    }

    /**
     * 權限檢查 - 置換sidebar-nav
     */
    rolesCheck() {
        this.translateService.onLangChange.pipe(throttleTime(500)).subscribe(() => {
            this.navItems = this.navItemService.getNavItemsByManageIDs(this.loginUser);
        });
        this.navItems = this.navItemService.getNavItemsByManageIDs(this.loginUser);
    }

    // 顯示遊玩會員
    openTimelyBetModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, { class: 'modal-xl' });
    }

    // 顯示登入後台的代理人數
    openLogginedAgent(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template, { class: 'modal-xl' });
    }

    // 顯示會員在大廳的人數
    openModalInHall(template: TemplateRef<any>) {
        if (this.identity === UserRole.admin || this.identity === UserRole.management) {
            this.intervalInHall = setInterval(() => {
                this.memberService.getMemberList().subscribe(
                    res => {
                        this.dataOfHall = true;
                        const data = res.data as Member[];
                        this.dataSource.data = data.filter(e => e.is_login === 1);
                        // console.log(this.modalOfmemberOnline);
                    }
                );
            }, 2000);
            this.modalRef = this.modalService.show(template, { class: 'modal-xl', backdrop: 'static' });
        } else {
            this.intervalInHall = setInterval(() => {
                this.memberService.getMemberByAgent(this.agentID).subscribe(
                    res => {
                        this.dataOfHall = true;
                        const data = res.data as Member[];
                        this.dataSource.data = data.filter(e => e.is_login === 1);
                    }
                );
            }, 2000);
            this.modalRef = this.modalService.show(template, { class: 'modal-xl', backdrop: 'static' });
        }
    }

    // 顯示會員在遊戲中的人數
    openModalInGame(template: TemplateRef<any>) {
        if (this.identity === UserRole.admin || this.identity === UserRole.management) {
            this.intervalInGame = setInterval(() => {
                this.memberService.getMemberList().subscribe(
                    res => {
                        this.dataOfGame = true;
                        const data = res.data as Member[];
                        this.InGamedataSource.data = data.filter(e => e.is_play_game === 1);
                        // console.log(this.InGamedataSource.data);
                    }
                );
            }, 2000);
            this.modalRef = this.modalService.show(template, { class: 'modal-xl', backdrop: 'static' });
        } else {
            this.intervalInGame = setInterval(() => {
                this.memberService.getMemberByAgent(this.agentID).subscribe(
                    res => {
                        this.dataOfGame = true;
                        const data = res.data as Member[];
                        this.InGamedataSource.data = data.filter(e => e.is_play_game === 1);
                    }
                );
            }, 2000);
            this.modalRef = this.modalService.show(template, { class: 'modal-xl', backdrop: 'static' });
        }
    }



    /**
   * 取得遊戲最後一筆資料 - 預設1(骰寶)
   */
    getPlayGameLastRowByGameTypesId(gameTypesId) {
        this.playGameService.getPlayGameLastRowByGameTypesId(gameTypesId).subscribe(
            res => {
                const data = res.data as PlayGame;
                this.lastGameData = data;
                this.lastGameTypesId = data.game_types_id;
                this.lastGameNumber = data.game_number;

                if (!this.lastGameCreatedAt) {
                    this.lastGameCreatedAt = formatDate(data.created_at, 'yyyy-MM-dd HH:mm:ss', 'en-US');
                }

                // console.log(this.lastGameData);
                this.getBetRecordByItemTypeAndGameNumber(this.lastGameTypesId, this.lastGameNumber, this.lastGameCreatedAt);
            },
            err => alert(this.translateService.instant('game.gameIsDeactivated'))
        );
    }

    /**
   * 取得及時押注紀錄資料 - 預設遊戲類型1(骰寶)
   */
    getBetRecordByItemTypeAndGameNumber(gameTypesId, lastGameNumber, lastGameCreatedAt) {
        let req = this.betRecordService.getBetRecordByItemTypeAndGameNumber(gameTypesId, lastGameNumber, lastGameCreatedAt);

        if (this.identity !== UserRole.admin) {

            if (!this.memberID) {
                return;
            }
            req = this.betRecordService.getMemberBetRecordByAgentID(gameTypesId, lastGameNumber, this.memberInfo.agent_id, this.memberID, lastGameCreatedAt);
        }

        req.subscribe(
            res => {
                const data = res.data;
                this.data = data;
                this.timelyBetData = this.data.reverse();
            });

    }

    // 監聽會員押注紀錄
    openModal(template: TemplateRef<any>, element) {
        setTimeout(() => {
            this.memberInfo = element;
        }, 2000);
        this.dataOfBet = true;
        this.memberID = element.id;
        this.memberAllowPlay = element.is_allow_play;


        this.intervalMemberBet = setInterval(() => {
            const memberTimelyBet = this.timelyBetData.filter(e => e.members_id === this.memberID);
            this.memberBetDataSource.data = memberTimelyBet;
        }, 2000);
        this.modalRefMemberBet = this.modalService.show(template, { class: 'modal-xl', backdrop: 'static' });
    }

    // 踢出全部會員
    kickAllMembers() {
        const data = {
            game_types_id: 1,
            type: EventEnum.EventkickAllMember,
            value: 'EventEnum.EventkickAllMember'
        } as any;
        this.gameEventService.addGRPCEvent(data).subscribe(
            () => {
                alert(this.translateService.instant('game.kickUserSuccess'));
            },
            err => {
                alert(this.translateService.instant('common.updateFailed'));
            }
        );
    }

    // 允許全部會員遊玩
    AllowMembersPlayGame() {
        const data = {
            game_types_id: 1,
            type: EventEnum.EventAllowMembersPlay,
            value: 'EventEnum.EventAllowMembersPlay'
        } as any;
        this.gameEventService.addGRPCEvent(data).subscribe(
            () => {
                alert(this.translateService.instant('game.resumePlay'));
            },
            err => {
                alert(this.translateService.instant('common.updateFailed'));
            }
        );
    }

    // 禁止會員遊玩按鈕
    allowGameToggle(id, is_allow_play) {

        const data = {
            id: id,
            is_allow_play: is_allow_play ? 0 : 1,
        };

        if (this.identity === UserRole.admin) {
            this.memberService.updateMemberIsAllowPlay(data).subscribe(
                () => {
                    alert(this.translateService.instant('common.updateSuccess'));
                    this.getMemberByAgent(this.loginUserID);
                },
                err => alert(this.translateService.instant('common.updateFailed'))
            );
        } else {
            alert(this.translateService.instant('common.noAuthority'));
        }
    }

    changeMemberStatus(member: Member) {
        const turnedOnMsg = this.translateService.instant('common.turnedOn');
        const turnedOffMsg = this.translateService.instant('common.turnedOff');
        const data = {
            id: member.id,
            status: member.status ? 0 : 1
        };

        this.memberService.updateMemberStatus(data).subscribe(
            () => {
                member.status = data.status;
                let msg = this.translateService.instant('manageAgent.accountStatus');
                msg += data.status ? turnedOnMsg : turnedOffMsg;
                alert(msg);
            },
            err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
        );
    }

    changeMemberAllowGame(member: Member) {
        const turnedOnMsg = this.translateService.instant('common.turnedOn');
        const turnedOffMsg = this.translateService.instant('common.turnedOff');
        const data = {
            id: member.id,
            is_allow_play: member.is_allow_play ? 0 : 1,
        };

        this.memberService.updateMemberIsAllowPlay(data).subscribe(
            () => {
                member.is_allow_play = data.is_allow_play;
                let msg = this.translateService.instant('manageAgent.memberStatus');
                msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
                alert(msg);
            },
            err => alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`)
        );
    }

    // 更改按鈕顏色
    changeBtnColor(status) {
        switch (status) {
            case 0:
                return 'btn-success';
            case 1:
                return 'btn-danger';
        }
    }

    /**
    * 取得遊戲列表
    */
    getGameTypeList() {
        this.gameTypeService.getGameTypes().subscribe(
            res => {
                const data = res.data;
                this.GameType = data;
            }
        );
    }

    /**
    * 取得遊戲中文名稱
    */
    getGameTypeName(game_types_id: number): string {
        const gameName = this.GameType.find(e => e.id === game_types_id);
        if (gameName) { return this.enumTranslateServ.getTranslatedGameName(gameName.en_name); }
        return this.translateService.instant('common.noData');
    }

    /**
    * 取得遊戲項目列表
    */
    getItemTypeList() {
        this.itemTypesService.getItemTypesList().subscribe(
            res => {
                const data = res.data;
                this.ItemTypes = data;
                // console.log(data);
            }
        );
    }

    /**
    * 取得遊戲項目中文名稱
    */
    getItemTypeName(item_types_id: number): string {
        const itemTypeName = this.ItemTypes.find(e => e.id === item_types_id);
        if (itemTypeName) { return this.enumTranslateServ.getTranslatedBetItem(itemTypeName.en_name); }
        return this.translateService.instant('common.noData');
    }

    /**
    * 轉換 - 取得押注動作名稱
    */
    getGameActionTypeByID(id: number) {
        return this.enumTranslateServ.getTranslatedAction(id);
    }

    /**
    * 會員即時押注列表踢人按鈕
    */
    kickMember() {
        const data = {
            id: this.memberID,
            is_allow_play: 0,
        };

        if (this.memberAllowPlay === 1) {
            this.memberService.updateMemberIsAllowPlay(data).subscribe(
                () => alert(this.translateService.instant('game.kickUserSuccess')),
                () => alert(this.translateService.instant('common.updateFailed'))
            );
        } else {
            alert(this.translateService.instant('game.userAlreadyKicked'));
        }
    }

    get Status() {
        return Status;
    }

    get UserRole() {
        return UserRole;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    setMemberOnlineNum(num: number) {
        this.memberOnlineNum = num;
    }

    setAgentOnlineNum(num: number) {
        this.agentOnlineNum = num;
    }

    getAgentsOnline(identity: number, parentID: number) {
        let req = this.userService.getUsers();

        if (identity !== UserRole.admin) {
            req = this.userService.getUserByParentID(parentID);
        }

        req.subscribe(r => {
            const users: User[] = r.data;
            const userOnline = users.filter(e => e.is_hello && (e.identity === UserRole.agent || e.identity === UserRole.topAgent));
            this.agentOnlineNum = userOnline.length;
        });

        if (!this.intervalAgent) {
            this.intervalAgent = setInterval(() => this.getAgentsOnline(identity, parentID), ONLINE_MIN * 1000);
        }
    }

    remoteLoginCheck() {
        this.intervalRemote = setInterval(() => {
            this.userService.userSayHello(this.loginUserID).subscribe(r => {
                const isLastHelloTimeIdle = this.isLastHelloTimeIdle();
                const user: User = r.data;
                if (this.identity === UserRole.agent || this.identity === UserRole.topAgent) {
                    if (user.last_login_at !== this.deviceLoginAt) {
                        console.log(this.translateService.instant('remoteLogined'));

                        alert(this.translateService.instant('remoteLogined'));
                        clearInterval(this.intervalRemote);
                        this.authenticationService.logoutUser(false);
                    }
                }

                if (isLastHelloTimeIdle && this.identity !== UserRole.admin) {
                    this.authenticationService.logout();
                    this.router.navigate(['/login']);
                    window.location.reload();
                }
            });
        }, 5000);
    }

    checkSameUrl() {
        setTimeout(() => {
            if (this.sameUrlNavigation) {
                window.location.reload();
            }
        }, 300);
    }

    updateHelloTime() {
        const now = String(new Date().getTime());
        localStorage.setItem('lastHelloTime', now);
    }

    isLastHelloTimeIdle() {
        const now = new Date().getTime();
        const lastHelloTime = localStorage.getItem('lastHelloTime');
        this.updateHelloTime();
        if (lastHelloTime) {
            const lastTime = Number(lastHelloTime);
            const diffMin = (now - lastTime) / 1000 / 60;
            return diffMin >= IDLE_MIN;
        }
        return false;
    }

    // 自動關閉底下會員百家自動下注狀態
    turnOffMemberAutoBetByAgentID(id: number) {
        this.agentService.getAgentChildsIdByAgentsId(id).pipe(switchMap(r => {
            let ids: number[] = r.data ? r.data : [];
            ids.push(id);
            let reqs = ids.map(agentID => this.memberService.getMemberByAgent(agentID));
            return forkJoin(reqs);
        })).subscribe(r => {
            let members: Member[] = [];
            r.forEach(e => members.push(...e.data));
            members = members.filter(e => e.auto_bet);
            let reqs = members.map(e => this.memberService.updateMemberAutoBet(e.id, id, Status.InActive, e.auto_bet_result));
            forkJoin(reqs).subscribe();
        });
    }
}
