import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { switchMap } from 'rxjs/operators';
import { defaultAgentID } from '../../common/global';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { BetRecord } from '../../models/bet-record';
import { ItemType } from '../../models/item-type';
import { PlayGame } from '../../models/play-game';
import { BetRecordService } from '../../services/bet-record.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { PlayGameService } from '../../services/play-game.service';
import { BaccaratPanelComponent } from '../baccarat-panel/baccarat-panel.component';
import { DragonTigerPanelComponent } from '../dragon-tiger-panel/dragon-tiger-panel.component';

const SINGLE_BET_NUMBER = 17;
const POSITIVE_INTEGER = /^\d+$/;

@Component({
  selector: 'app-calc-item-record-panel',
  templateUrl: './calc-item-record-panel.component.html',
  styleUrls: ['./calc-item-record-panel.component.css']
})
export class CalcItemRecordPanelComponent implements OnInit {
  @Input() gameTypesID: number;
  @Input() gameNumber: number;

  @Input() set memberBetRecords(data: BetRecord[]) {
    if (data) {
      this.betRecords = data;
      this.patchBetRecordsToPanel();
    }
  }
  @ViewChild('baccaratPanel') baccaratPanel: BaccaratPanelComponent;
  @ViewChild('dragonTigerPanel') dragonTigerPanel: DragonTigerPanelComponent;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<BetRecord>();
  displayedColumns: string[] = ['account', 'item_types_id', 'amount', 'created_at'];

  myForm: FormGroup;
  betRecords: BetRecord[];
  currentGameTypeID: number;

  constructor(
    public bsModalRef: BsModalRef,
    private enumTranslateServ: EnumTranslateService,
    private betRecordServ: BetRecordService,
    private playGameServ: PlayGameService,
    private fb: FormBuilder
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    if (this.gameTypesID && this.gameNumber) {
      this.getBetRecordsByGameNumber(this.gameTypesID, this.gameNumber);
    }
  }

  getBetRecordsByGameNumber(gameTypeID: number, gameNumber: number) {
    this.playGameServ.getPlayGameByGameNumber(gameTypeID, gameNumber).pipe(switchMap(r => {
      const playGame: PlayGame = r.data;
      this.currentGameTypeID = playGame.game_types_id;
      return this.betRecordServ.getBetRecordByItemTypeAndGameNumber(playGame.game_types_id, gameNumber, playGame.created_at);
    })).subscribe(r => {
      const records: BetRecord[] = r.data;
      this.betRecords = records.filter(e => e.member.agent_id !== defaultAgentID);
      switch (this.currentGameTypeID) {
        case GameTypeEnum.GameTypeSicBo:
          this.patchBetRecordsToPanel();
          break;
        case GameTypeEnum.GameTypeBaccarat:
          this.baccaratPanel.patchBetRecords(this.betRecords);
          break;
        case GameTypeEnum.GameTypeDragonTiger:
          this.dragonTigerPanel.patchBetRecords(this.betRecords);
          break;
      }
    });

  }

  patchBetRecordsToPanel() {
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const ctrl = this.myForm.get(`${i + 1}`).get('amount');
      ctrl.setValue(this.reduceBetRecords(this.betRecords, i + 1));
    }

    this.baccaratPanel && this.baccaratPanel.patchBetRecords(this.betRecords);
    this.dragonTigerPanel && this.dragonTigerPanel.patchBetRecords(this.betRecords);
  }

  reduceBetRecords(betRecords: BetRecord[], itemTypeID: number) {
    const records = betRecords.filter(e => e.action === GameActionTypeEnum.Bet && e.item_types_id === itemTypeID);
    if (records.length) {
      return records.map(e => e.amount).reduce((a, b) => a + b);
    }
    return 0;
  }

  showBetRecordsByItmeTypeID(itemTypeID: number) {
    this.dataSource.data = this.betRecords.filter(e => e.action === GameActionTypeEnum.Bet && e.item_types_id === itemTypeID);
  }

  /**
   * 表單建構
   */
  groupForm() {
    this.myForm = this.fb.group([]);
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      this.myForm.addControl(`${i + 1}`, this.createSingleBetLimit(i + 1));
    }
  }

  createSingleBetLimit(id: number) {
    return this.fb.group({
      id: [id],
      total: [null, [Validators.pattern(POSITIVE_INTEGER)]],
      amount: [null, Validators.pattern(POSITIVE_INTEGER)]
    });
  }

  getValue(name: string, ctrlName: string) {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.value;
  }

  /**
   * 類型翻譯
   */
  getItemTypeName(itemTypeID: number) {
    return this.enumTranslateServ.getTranslatedBetItem(ItemType.getNameByID(itemTypeID));
  }

  getGameActionTypeName(actionID: number) {
    return this.enumTranslateServ.getTranslatedAction(actionID);
  }


  get GameTypeEnum() { return GameTypeEnum }
}
