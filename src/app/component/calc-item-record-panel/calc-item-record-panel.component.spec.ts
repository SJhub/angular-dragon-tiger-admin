import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcItemRecordPanelComponent } from './calc-item-record-panel.component';

describe('CalcItemRecordPanelComponent', () => {
  let component: CalcItemRecordPanelComponent;
  let fixture: ComponentFixture<CalcItemRecordPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcItemRecordPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcItemRecordPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
