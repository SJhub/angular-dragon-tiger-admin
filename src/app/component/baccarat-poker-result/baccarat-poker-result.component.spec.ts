import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaccaratPokerResultComponent } from './baccarat-poker-result.component';

describe('BaccaratPokerResultComponent', () => {
  let component: BaccaratPokerResultComponent;
  let fixture: ComponentFixture<BaccaratPokerResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaccaratPokerResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaccaratPokerResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
