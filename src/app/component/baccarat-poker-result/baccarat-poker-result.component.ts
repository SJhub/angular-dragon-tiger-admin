import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { baccaratDrawResultStr, baccaratPokerResultStr, calcPoint, numToStr } from '../../common/global';
import { Poker, PokerResult } from '../../models/baccarat';

@Component({
  selector: 'app-baccarat-poker-result',
  templateUrl: './baccarat-poker-result.component.html',
  styleUrls: ['./baccarat-poker-result.component.css']
})
export class BaccaratPokerResultComponent implements OnInit {
  @Input() set drawResult(str: string) {
    str && this.setValue(str);
  }
  @Input() aiDetect = false;
  @Input() className = '';
  @Input() showItemType = false;

  invalidGame = false;

  itemTypeStr: string;

  pokerResultStr: {
    banker: string;
    player: string;
  };

  pokerResult: PokerResult;

  constructor(private translateServ: TranslateService) { }

  ngOnInit(): void {
  }

  setValue(str: string) {
    this.invalidGame = str === '000';
    if (!this.invalidGame) {
      try {
        this.pokerResult = JSON.parse(str);
        this.itemTypeStr = baccaratDrawResultStr(this.pokerResult, this.translateServ, true);
      } catch (error) {
        console.log(error);
      }
    }
  }

  calcPoint(pokers: Poker[]) {
    return calcPoint(pokers)
  }

  numToStr(num: number) {
    return numToStr(num);
  }
}
