import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { UserRole } from '../../enum/user-role.enum';
import { GameType } from '../../models/game-type';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { MemberService } from '../../services/member.service';

interface SingleBetLimit {
  id: number;
  amount: number;
}

interface ReturnObject {
  single_bet_limits: string;
  single_bet_total_amounts: string;
}

const SINGLE_BET_RANGE = [
  {
    start: 1,
    end: 17
  },
  {
    start: 24,
    end: 28
  },
  {
    start: 35,
    end: 37
  }
];
const POSITIVE_INTEGER = /^\d+$/;

@Component({
  selector: 'app-single-bet-panel',
  templateUrl: './single-bet-panel.component.html',
  styleUrls: ['./single-bet-panel.component.css']
})
export class SingleBetPanelComponent implements OnInit {
  @Input()
  set self(self: User) {
    if (self) {
      this.user = self;
      this.patchOwnSingleBetLimit(self.single_bet_limits, 'amount');
      // this.patchOwnSingleBetLimit(self.single_bet_total_amounts, 'total');
    }
  }

  @Input()
  set parent(parent: User) {
    if (parent && parent.identity !== UserRole.admin) {
      this.patchParentSingleBetLimit(parent.single_bet_limits, 'amount');
      // this.patchParentSingleBetLimit(parent.single_bet_total_amounts, 'total');

      // 新增時代入上層設定
      if (this.isCreating) {
        this.patchOwnSingleBetLimit(parent.single_bet_limits, 'amount');
        // this.patchOwnSingleBetLimit(parent.single_bet_total_amounts, 'total');
      }

      if (this.isMember) {
        // this.setSingleBetLimitByParentID(parent.id, parent);
      } else {
        // this.setSingleBetTotalByParentID(parent.id, parent.single_bet_total_amounts);
      }
    }
  }

  @Input() isCreating: boolean = false;
  @Input() isMember: boolean = false;
  @Input() readonly: boolean = false;

  private user: User;
  gameTypeID = GameTypeEnum.GameTypeDragonTiger;
  gameTypes: GameType[];
  myForm: FormGroup;
  betTotalAmountValid = true;

  constructor(
    private fb: FormBuilder,
    private agentServ: AgentService,
    private memberServ: MemberService,
    private gameTypeServ: GameTypeService,
    private enumTranslateServ: EnumTranslateService
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
  }


  /**
   * 取下層代理已開總額
   */
  setSingleBetTotalByParentID(id: number, str: string) {
    this.agentServ.getAgentSingleBetTotalByParentID(id).subscribe(r => {
      try {
        let myBetTotal: SingleBetLimit[];

        const defaultBetTotal = JSON.parse(str) as SingleBetLimit[];
        const childBetTotal: Object = r.data;

        defaultBetTotal.map(e => e.amount = Number(e.amount - childBetTotal[e.id]));

        if (this.user && !this.isCreating) {
          myBetTotal = JSON.parse(this.user.single_bet_total_amounts);
          defaultBetTotal.map(e => e.amount += myBetTotal.find(m => m.id === e.id).amount);
        }

        this.betTotalAmountValid = !defaultBetTotal.some(e => e.amount <= 0);

        if (this.betTotalAmountValid) {
          this.patchParentSingleBetLimit(JSON.stringify(defaultBetTotal), 'total');
        }


      } catch (error) {
        console.log(error);
      }
    });
  }

  /**
   * 取下層會員已開總額
   */
  setSingleBetLimitByParentID(agentID: number, parent: User) {
    this.memberServ.getMemberSingleBetTotalByAgentID(agentID).subscribe(r => {
      try {
        const defaultBetLimits = JSON.parse(parent.single_bet_limits) as SingleBetLimit[];
        const agentBetTotal = JSON.parse(parent.single_bet_total_amounts) as SingleBetLimit[];
        let myBetLimit: SingleBetLimit[];
        let childLimitTotal: number = r.data;

        if (this.user) {
          myBetLimit = JSON.parse(this.user.single_bet_limits);
          const myLimitTotal = myBetLimit.map(e => e.amount).reduce((a, b) => a + b);
          childLimitTotal -= myLimitTotal;
        }

        defaultBetLimits.map(e => {
          const coda = agentBetTotal.find(a => a.id === e.id).amount - childLimitTotal;
          e.amount = !e.amount && childLimitTotal ? coda : coda < e.amount ? coda : e.amount;
          if (coda <= 0) this.betTotalAmountValid = false;
        });

        // this.betTotalAmountValid = !defaultBetLimits.some(e => e.amount <= 0);

        if (this.betTotalAmountValid) {
          this.patchParentSingleBetLimit(JSON.stringify(defaultBetLimits), 'amount');
        }
      } catch (error) {
        console.log(error);
      }

    });
  }


  getFormValid() {
    this.markFormGroupTouched(this.myForm);
    return this.myForm.valid && this.betTotalAmountValid;
  }


  /**
   * 取表單物件
   */
  getFormValue(): ReturnObject {
    const formValue = this.myForm.value;
    const single_bet_limits = JSON.stringify(this.singleBetToArray(formValue, 'amount'));
    const single_bet_total_amounts = JSON.stringify(this.singleBetToArray(formValue, 'total'));
    return { single_bet_limits, single_bet_total_amounts };
  }


  singleBetToArray(singleBet: any, colName: string) {
    const values = Object.values(singleBet) as SingleBetLimit[];
    const arr = values.map(e => {
      const a = { id: e.id };
      a[colName] = Number(e[colName]);
      return a;
    });
    return arr;
  }








  /**
   * 填入自己單項總額or單注限額
   */
  patchOwnSingleBetLimit(str: string, col: string) {

    if (str && str.length) {
      try {
        const arr = JSON.parse(str) as SingleBetLimit[];
        const obj = {};
        arr.forEach(e => {
          const c = {};
          c[col] = Number(e[col]);
          obj[e.id] = c;
        });
        this.myForm.patchValue(obj);
      } catch (error) {
        console.log(error);
      }
    }
  }

  /**
   * 限制上層單項總額or單注限額
   */
  patchParentSingleBetLimit(str: string, col: string) {
    if (str && str.length) {
      try {
        const arr = JSON.parse(str) as SingleBetLimit[];
        arr.forEach(e => {
          if (!e.amount) return;
          const g = this.myForm.controls[String(e.id)] as FormGroup;
          if (g) {
            const ctrl = g.get(col);
            const validators = [Validators.pattern(POSITIVE_INTEGER), Validators.max(e.amount), Validators.min(1), Validators.required];

            if (col === 'total' && !this.isMember) {
              //   g.setValidators([this.valueOverCheck]);
              //   validators.push(Validators.required);
            }

            ctrl.setValidators(validators);
            ctrl.updateValueAndValidity();
            g.updateValueAndValidity();
          }
        });
      } catch (error) {
        console.log(error);
      }
    }
  }

  /**
    * check max amount
    */
  getValue(name: string, ctrlName: string) {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.value;
  }

  /**
    * check max amount
    */
  hasError(name: string, ctrlName: string, errorName: string = 'max') {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.hasError(errorName);
  }


  /**
   * check max amount
   */
  maxValue(name: string, ctrlName: string) {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.errors?.max?.max;
  }


  /**
   * control is invalid check
   */

  isInvalid(name: string, ctrlName: string) {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.errors && ctrl.touched;
  }

  /**
   * 表單建構
   */
  groupForm() {
    this.myForm = this.fb.group([]);
    SINGLE_BET_RANGE.forEach(range => {
      for (let index = range.start; index <= range.end; index++) {
        this.myForm.addControl(`${index}`, this.createSingleBetLimit(index));
      }
    });
  }

  createSingleBetLimit(id: number) {
    return this.fb.group({
      id: [id],
      total: [0, [Validators.pattern(POSITIVE_INTEGER)]],
      amount: [0, Validators.pattern(POSITIVE_INTEGER)]
    });
  }

  valueOverCheck(form: AbstractControl) {
    const amount = form.get('amount');
    const total = form.get('total');
    const errors = amount.errors;
    if (amount.value && total.value) {
      if (Number(amount.value) > Number(total.value)) {
        if (!errors) {
          amount.setErrors({ exceed: true });
        }
        return null;
      }
      errors?.exceed && (delete errors.exceed);
      amount.setErrors(!errors ? null : Object.keys(errors).length ? errors : null);
      return null;
    }
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  applyAll(amount: string, total: string) {
    if (POSITIVE_INTEGER.test(amount) || POSITIVE_INTEGER.test(total)) {
      const range = SINGLE_BET_RANGE[this.gameTypeID - 1];
      for (let index = range.start; index <= range.end; index++) {
        const control = this.myForm.get(String(index));
        const amountCtrl = control.get('amount');
        const totalCtrl = control.get('total');
        amountCtrl.setValue(amount);
        totalCtrl.setValue(total);
      }
    }
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  get GameTypeEnum() { return GameTypeEnum }
}
