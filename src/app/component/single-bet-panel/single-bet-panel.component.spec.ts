import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleBetPanelComponent } from './single-bet-panel.component';

describe('SingleBetPanelComponent', () => {
  let component: SingleBetPanelComponent;
  let fixture: ComponentFixture<SingleBetPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleBetPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleBetPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
