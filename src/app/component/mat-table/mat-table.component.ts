import { GameType } from './../../models/game-type';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BetRecord } from '../../models/bet-record';
import { ItemTypes } from '../../models/item-types';
import { GameResultType } from '../../models/game-result-type';
import { CalcItemResultRecord } from '../../models/calc-item-result-record';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { TranslateService } from '@ngx-translate/core';
import { EnumTranslateService } from '../../services/enum-translate.service';

interface keysAndValue {
  key: string;
  amount_total: any;
  game_result_amount_total: any;
  item_types_ids: any;
  item_type: any;
  data: BetRecord[];
}
@Component({
  selector: 'app-mat-table',
  templateUrl: './mat-table.component.html',
  styleUrls: ['./mat-table.component.css']
})
export class MatTableComponent implements OnInit {

  @Input() GameType: GameType[];
  // @Input() BetRecords: BetRecord[];
  @Input() ItemTypes: ItemTypes[];
  @Input() CalcItemResultRecord: CalcItemResultRecord[];
  @Input() dataOfResultAmountTotal: any;
  @Input() CalcItemResultRecordByDate: any;


  @ViewChild('paginatorOfBetRecord', { static: true }) paginatorOfBetRecord: MatPaginator;
  @ViewChild('sortByBetRecord', { static: true }) sortByBetRecord: MatSort;
  displayedColumnsOfBetRecord: string[] = ['key', 'amount_total', 'game_result_amount_total', 'item_types_id', 'item_type'];
  dataSourceOfBetRecord = new MatTableDataSource<BetRecord>();

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private translateService: TranslateService,
    private calcItemResultRecordService: CalcItemResultRecordService,
  ) { }

  ngOnInit(): void {
    this.dataSourceOfBetRecord.paginator = this.paginatorOfBetRecord;
    this.dataSourceOfBetRecord.sort = this.sortByBetRecord;
    this.dataSourceOfBetRecord.data = this.dataOfResultAmountTotal;

    const keyOfData = [];


    for (const [key, value] of Object.entries(this.dataOfResultAmountTotal)) {
      const val = value as BetRecord[];
      let amountTotal = 0;
      let amountResultTotal = 0;

      const amountMap = val.map(e => e.amount);
      amountMap.forEach(e => {
        amountTotal = amountTotal + e;
      });

      const amountResult = val.map(e => e.game_result_amount);
      amountResult.forEach(e => {
        amountResultTotal = amountResultTotal + e;
      });

      const data: keysAndValue = {
        key: key,
        amount_total: amountTotal,
        game_result_amount_total: amountResultTotal,
        item_types_ids: val.map(e => this.getItemTypesIdChtName(e.item_types_id)).filter(function (element, index, arr) {
          return arr.indexOf(element) === index;
        }),
        item_type: this.CalcItemResultRecordByDate.filter(e => e.game_number === Number(key))
          .map(e => this.getItemTypeChtName(JSON.parse(e.item_types_ids))),
        data: val
      };
      keyOfData.push(data);
    }
    this.dataSourceOfBetRecord.data = keyOfData;

    // console.log(this.dataSourceOfBetRecord.data);
  }

  checkValueByKeys() {
    if (Object.keys(this.dataOfResultAmountTotal).length > 0) {
      return false;
    }
    return true;
  }

  /**
   * 取得開獎結果 By startDate and endDate
   */
  getCalcItemResultRecordByTypeBetweenDate(id: number, startDate: string, endDate: string) {
    this.calcItemResultRecordService.getCalcItemResultRecordByTypeBetweenDate(id, startDate, endDate).subscribe(
      res => {
        const data = res.data;
        const item_types_id_map = data.map(e => e.item_types_id);
      }
    );
  }

  /**
   * 取得遊戲中文名稱
   */
  getGameChtName(game_types_id: number): string {
    const chtName = this.GameType.find(e => e.id === game_types_id);
    if (chtName) { return chtName.cht_name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得遊戲項目名稱
   */
  // getItemTypeChtName(item_types_id: number): string {
  //   const chtName = this.ItemTypes.find(e => e.id === item_types_id);
  //   if (chtName) { return chtName.cht_name; }
  //   return this.translateService.instant('common.noData');
  // }

  /**
   * 取得遊戲項目名稱
   */
  getItemTypesIdChtName(item_types_ids: any): string {
    const Name = this.ItemTypes.find(e => e.id === item_types_ids);
    if (Name) { return this.getTranslatedBetItem(Name.en_name); }
    return this.translateService.instant('common.noData');
  }

  getTranslatedBetItem(name: string) {
    return this.enumTranslateServ.getTranslatedBetItem(name);
  }

  /**
   * 取得遊戲開獎結果名稱
   */
  getItemTypeChtName(item_type: any): Array<string> {
    item_type = item_type.map(item => {
      const Name = this.ItemTypes.find(e => e.id === item);
      if (Name) { return this.getTranslatedBetItem(Name.en_name); }
      return this.translateService.instant('common.noData');
    });
    return item_type;
  }

  getItemTypeIdByGameNumber(game_number: number): any {
    const itemTypeId = this.CalcItemResultRecord.find(e => e.game_number === game_number);
    if (itemTypeId) { return itemTypeId.item_types_ids; }
    return this.translateService.instant('common.noData');
  }

  getGameResultTypeByID(id: number) {
    return GameResultType.getNameByID(id);
  }

}
