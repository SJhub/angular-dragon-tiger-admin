import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { addIndexToArray } from '../../common/global';
import { BetRecord } from '../../models/bet-record';
import { Member } from '../../models/member.model';

@Component({
  selector: 'app-donate-records',
  templateUrl: './donate-records.component.html',
  styleUrls: ['./donate-records.component.css']
})
export class DonateRecordsComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['index', 'game_number', 'amount', 'created_at'];
  dataSource = new MatTableDataSource<BetRecord>();

  @Input() member: Member;
  @Input() donateRecords: BetRecord[];

  constructor(
    public bsModalRef: BsModalRef
  ) {
  }

  ngOnInit(): void {
    this.dataSource.data = addIndexToArray(this.donateRecords);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
