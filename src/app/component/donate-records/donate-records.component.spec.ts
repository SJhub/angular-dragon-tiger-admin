import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateRecordsComponent } from './donate-records.component';

describe('DonateRecordsComponent', () => {
  let component: DonateRecordsComponent;
  let fixture: ComponentFixture<DonateRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
