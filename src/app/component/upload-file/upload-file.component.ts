import { Component, Output, EventEmitter, Input } from '@angular/core';
import { UploadFileService } from '../../services/upload-file.service';
import { ImgFullPathPipe } from '../../pipe/img-full-path.pipe';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent {
  @Output() fileName: EventEmitter<any> = new EventEmitter();
  @Input() files: any = [];

  constructor(private uploadFileService: UploadFileService) { }


  uploadFile(event) {
    for (let index = 0; index < event.length; index++) {
      const element = event[index];
      let file: File = element;

      this.uploadFileService.uploadFile(file).subscribe(
        res => {
          this.fileName.emit(res.data);
        },
        err => {
          alert(`上傳失敗: ${err.error.message}`);
        }
      );

    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1)
  }

  getFullPath(name) {
    return new ImgFullPathPipe().transform(name);
  }
}
