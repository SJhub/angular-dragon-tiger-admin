import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { UploadFileService } from '../../services/upload-file.service';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.css']
})
export class ImageCropperComponent implements OnInit {
  @Output() avatarUrl: EventEmitter<any> = new EventEmitter();
  @Input() aspectRatio: string; 
  @Input() roundCropper: string;
  @Input() containWithinAspectRatio: string;
  @Input() maintainAspectRatio: string = 'true';
  

  constructor(private uploadFileService: UploadFileService) { }

  ngOnInit() {
  }

  imageChangedEvent: any = '';
  croppedImage: any = '';
  fileName: string = '';

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.fileName = event.target.files[0].name;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], { type: mimeString });


  }
  base64toFile(base64) {
    let blob = this.dataURItoBlob(base64);
    let file = new File([blob], this.fileName);
    return file;
  }
  uploadImg() {
    let file = this.base64toFile(this.croppedImage);
    this.uploadFileService.uploadFile(file).subscribe(
      res => {
        this.avatarUrl.emit(res.data);
      },
      err => {
        alert(`上傳失敗: ${err.error.message}`);
      }
    );
  }
}
