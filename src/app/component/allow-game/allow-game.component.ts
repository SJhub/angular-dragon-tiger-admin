import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import { markFormGroupAsTouched, isFormCtrlInvalid } from '../../common/form';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { EnumTranslateService } from '../../services/enum-translate.service';

const GAME_IDS = [GameTypeEnum.GameTypeBaccarat];
const FORM_CONTROL_NAME = 'allow_game';

@Component({
  selector: 'app-allow-game',
  templateUrl: './allow-game.component.html',
  styleUrls: ['./allow-game.component.css']
})
export class AllowGameComponent implements OnInit {
  form: FormGroup;

  constructor(
    private enumTranslateServ: EnumTranslateService,
    private fb: FormBuilder
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
  }

  getValue() {
    let data: any[] = this.getFormArray().getRawValue();
    data.map(e => e['status'] = Number(e['status']));
    return Object.assign([], data);
  }

  setValue(str: string) {
    try {
      let arr: AllowGame[] = JSON.parse(str);
      arr = arr.filter(e => GAME_IDS.includes(e.game_types_id));
      this.form.get(FORM_CONTROL_NAME).patchValue(arr);
    } catch (error) {
      console.log(error);
    }
  }

  setParentValue(str: string) {
    try {
      const arr: AllowGame[] = JSON.parse(str);
      arr.forEach(e => {
        if (!e.status) {
          let index = GAME_IDS.indexOf(e.game_types_id);
          let ctrl = this.getFormArray().get(String(index));
          ctrl.disable();
          ctrl.patchValue({ status: e.status });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  isFormValid() {
    markFormGroupAsTouched(this.form);
    return this.form.valid;
  }

  groupForm() {
    this.form = this.fb.group({
      allow_game: this.makeFormArray()
    });
  }

  makeFormArray() {
    let arr = this.fb.array([]);
    GAME_IDS.forEach(id => {
      let group = this.fb.group({
        game_types_id: [id],
        status: [Status.Active]
      });
      if (id === GameTypeEnum.GameTypeFootBall) {
        group.get('status').disable();
        group.get('status').setValue(Status.InActive);
      }
      arr.push(group);
    });
    return arr;
  }

  getFormControls() {
    return this.form.get(FORM_CONTROL_NAME)['controls'];
  }

  getFormArray() {
    return this.form.get(FORM_CONTROL_NAME) as FormArray;
  }

  getFormArryCtrl(index: number, name: string) {
    const formArray = this.getFormArray();
    return formArray.controls[index].get(name);
  }

  isFormCtrlInvalid(ctrl: AbstractControl) { return isFormCtrlInvalid(ctrl) }

  getTranslatedGameTypeByID(id: number) {
    return this.enumTranslateServ.getTranslatedGameTypeByID(id);
  }

  get FORM_CONTROL_NAME() { return FORM_CONTROL_NAME }
}

interface AllowGame {
  game_types_id: number;
  status: number;
}