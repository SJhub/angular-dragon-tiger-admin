import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllowGameComponent } from './allow-game.component';

describe('AllowGameComponent', () => {
  let component: AllowGameComponent;
  let fixture: ComponentFixture<AllowGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllowGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllowGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
