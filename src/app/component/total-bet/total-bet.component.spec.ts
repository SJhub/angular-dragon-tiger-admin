import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalBetComponent } from './total-bet.component';

describe('TotalBetComponent', () => {
  let component: TotalBetComponent;
  let fixture: ComponentFixture<TotalBetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalBetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalBetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
