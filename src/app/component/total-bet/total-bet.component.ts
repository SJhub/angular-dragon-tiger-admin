import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { BetRecord } from '../../models/bet-record';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { PlayGameService } from '../../services/play-game.service';
import { BetRecordService } from '../../services/bet-record.service';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { PlayGame } from '../../models/play-game';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { UserRole } from '../../enum/user-role.enum';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { defaultAgentID } from '../../common/global';
import { GameType } from '../../models/game-type';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { BaccaratPanelComponent } from '../baccarat-panel/baccarat-panel.component';
import { Status } from '../../enum/status.enum';

const SINGLE_BET_NUMBER = 17;
const POSITIVE_INTEGER = /^\d+$/;
const INTERVAL = 1000;

@Component({
  selector: 'app-total-bet',
  templateUrl: './total-bet.component.html',
  styleUrls: ['./total-bet.component.css']
})
export class TotalBetComponent implements OnInit, OnDestroy {
  @ViewChild('baccaratPanel') baccaratPanel: BaccaratPanelComponent;
  @Input() set members(val: Member[]) {
    if (val?.length && !this.lastGameInterval) {
      this.getLastGamePlay();
      this._members = val;
    } else if (!val?.length) {
      this.clearInterval();
    }
  }
  get members() { return this._members }

  @Input() gameTypeID: number;

  @Input() set gameStatus(status: number) {
    status === Status.InActive && this.patchBetRecordsToPanel([]);
  };

  @Input() user: User;

  @Input() trackMember: Member;

  @Output() cancelTrack: EventEmitter<boolean> = new EventEmitter();
  @Output() betRecords: EventEmitter<BetRecord[]> = new EventEmitter();
  @Output() betMembers: EventEmitter<Member[]> = new EventEmitter();


  myForm: FormGroup;
  lastGamePlay: PlayGame;
  lastGameInterval: any;
  _members: Member[];

  constructor(
    private betRecordServ: BetRecordService,
    private playGameServ: PlayGameService,
    private fb: FormBuilder
  ) {
    this.groupForm();
  }

  ngOnDestroy(): void {
    this.clearInterval();
  }

  ngOnInit(): void {
  }

  clearInterval() {
    clearInterval(this.lastGameInterval);
    this.lastGameInterval = null;
    this.clearPanel();
  }

  // 取最新局
  getLastGamePlay() {
    this.lastGameInterval = setInterval(() => {
      if (this.gameTypeID) {
        this.playGameServ.getPlayGameLastRowByGameTypesId(this.gameTypeID).subscribe(r => {
          this.lastGamePlay = r.data;
          this.getTimelyBetRecord();
        });
      }
    }, INTERVAL);
  }

  /**
   * 取得所有會員時押注紀錄資料
   */
  getTimelyBetRecord() {
    if (this.lastGamePlay && this.user) {
      if (this.user.identity === UserRole.admin) {
        return this.betRecordServ.getBetRecordByItemTypeAndGameNumber(this.gameTypeID, this.lastGamePlay.game_number, this.lastGamePlay.created_at)
          .subscribe(r => this.patchBetRecordsToPanel(r.data));
      }
      this.getTimelyBetRecordByMembers(this.members, this.lastGamePlay);
    }
  }

  getTimelyBetRecordByMembers(members: Member[], gamePlay: PlayGame) {
    const req = members.map(m => this.betRecordServ.getMemberBetRecordByAgentID(
      this.gameTypeID,
      gamePlay.game_number,
      m.agent_id,
      m.id,
      gamePlay.created_at
    ));

    forkJoin(req).pipe(map(res => res.map(r => r.data))).subscribe(r => {
      const betRecords = [];
      r.forEach(e => betRecords.push(...e));
      this.patchBetRecordsToPanel(betRecords);
    });
  }

  /**
   * 表單建構
   */
  groupForm() {
    this.myForm = this.fb.group([]);
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      this.myForm.addControl(`${i + 1}`, this.createSingleBetLimit(i + 1));
    }
  }

  createSingleBetLimit(id: number) {
    return this.fb.group({
      id: [id],
      total: [0, [Validators.pattern(POSITIVE_INTEGER)]],
      amount: [0, Validators.pattern(POSITIVE_INTEGER)]
    });
  }

  getValue(name: string, ctrlName: string) {
    const ctrl = this.myForm.get(name).get(ctrlName);
    return ctrl.value;
  }

  patchBetRecordsToPanel(betRecords: BetRecord[]) {
    betRecords = betRecords.filter(e => e.action === GameActionTypeEnum.Bet);
    betRecords = betRecords.filter(e => e.member.agent_id !== defaultAgentID);
    this.betRecords.emit(betRecords);
    this.betMembers.emit(this.getBetMembers(betRecords));

    if (this.trackMember) {
      betRecords = betRecords.filter(e => e.members_id === this.trackMember.id);
    }

    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const ctrl = this.myForm.get(`${i + 1}`).get('amount');
      ctrl.setValue(this.reduceBetRecords(betRecords, i + 1));
    }

    this.baccaratPanel && this.baccaratPanel.patchBetRecords(betRecords);
  }

  reduceBetRecords(betRecords: BetRecord[], itemTypeID: number) {
    const records = betRecords.filter(e => e.action === GameActionTypeEnum.Bet && e.item_types_id === itemTypeID);
    if (records.length) {
      return records.map(e => e.amount).reduce((a, b) => a + b);
    }
    return 0;
  }

  clearPanel() {
    this.trackMember = null;
    this.lastGamePlay = null;
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const ctrl = this.myForm.get(`${i + 1}`).get('amount');
      ctrl.setValue('0');
    }
  }

  cancelTrackMember() {
    this.cancelTrack.emit(true);
  }

  getBetMembers(data: BetRecord[]) {
    let members: Member[] = [];
    data.forEach(e => {
      // if (!members.some(m => m.id === e.members_id)) members.push(e.member);
      members.push(e.member);
    })
    return members;
  }

  get GameTypeEnum() { return GameTypeEnum }
}
