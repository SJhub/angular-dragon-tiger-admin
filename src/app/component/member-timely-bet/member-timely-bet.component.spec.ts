import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberTimelyBetComponent } from './member-timely-bet.component';

describe('MemberTimelyBetComponent', () => {
  let component: MemberTimelyBetComponent;
  let fixture: ComponentFixture<MemberTimelyBetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberTimelyBetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberTimelyBetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
