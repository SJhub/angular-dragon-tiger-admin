import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { BetRecord } from '../../models/bet-record';
import { GameActionType } from '../../models/game-action-type';
import { GameType } from '../../models/game-type';
import { ItemType } from '../../models/item-type';
import { Member } from '../../models/member.model';
import { PlayGame } from '../../models/play-game';
import { User } from '../../models/user';
import { BetRecordService } from '../../services/bet-record.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { PlayGameService } from '../../services/play-game.service';

const INTERVAL = 3000;
@Component({
  selector: 'app-member-timely-bet',
  templateUrl: './member-timely-bet.component.html',
  styleUrls: ['./member-timely-bet.component.css']
})
export class MemberTimelyBetComponent implements OnInit, OnDestroy {
  @Input() member: Member;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<User>();

  displayedColumns: string[] = ['game_types_id', 'game_number', 'item_types_id', 'action', 'amount', 'created_at'];

  currentTotalDonate = 0;
  lastGamePlay: PlayGame;
  gameTypes: GameType[];
  gameTypeID = GameTypeEnum.GameTypeSicBo;


  lastGameInterval: any;
  timelyBetInterval: any;
  constructor(
    private betRecordServ: BetRecordService,
    private playGameServ: PlayGameService,
    private gameTypeServ: GameTypeService,
    private enumTranslateServ: EnumTranslateService,
    public bsModalRef: BsModalRef
  ) { }

  ngOnDestroy(): void {
    clearInterval(this.lastGameInterval);
    clearInterval(this.timelyBetInterval);
  }

  ngOnInit(): void {
    if (this.member) {
      this.getBetReocrdsByMemberID(this.member);
      this.getLastGamePlay();
      this.getGameTypes();
    }
  }

  getBetReocrdsByMemberID(member: Member) {
    this.timelyBetInterval = setInterval(() => {
      if (this.lastGamePlay && member) {
        this.betRecordServ.getMemberBetRecordByAgentID(
          this.gameTypeID,
          this.lastGamePlay.game_number,
          member.agent_id,
          member.id,
          this.lastGamePlay.created_at
        ).subscribe(r => {
          this.dataSource.data = r.data;
          this.currentTotalDonate = this.calcTotalDonate(r.data);
        });
      }
    }, INTERVAL);
  }

  calcTotalDonate(betRecords: BetRecord[]) {
    let totalDonate = 0;
    betRecords.filter(e => e.action === GameActionTypeEnum.Donate)
      .forEach(e => totalDonate += e.amount);
    return totalDonate;
  }

  getLastGamePlay() {
    this.lastGameInterval = setInterval(() => {
      this.playGameServ.getPlayGameLastRowByGameTypesId(this.gameTypeID).subscribe(r => this.lastGamePlay = r.data);
    }, INTERVAL);
  }

  getGameTypes() {
    this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
  }

  getGameTypeName(gameTypeID: number) {
    const gameType = this.gameTypes.find(e => e.id === gameTypeID);
    return this.enumTranslateServ.getTranslatedGameName(gameType.en_name);
  }

  getItemTypeName(itemTypeID: number) {
    return this.enumTranslateServ.getTranslatedBetItem(ItemType.getNameByID(itemTypeID));
  }

  getGameActionTypeName(actionID: number) {
    return this.enumTranslateServ.getTranslatedAction(actionID);
  }
}
