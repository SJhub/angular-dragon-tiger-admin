import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelyBetModalComponent } from './timely-bet-modal.component';

describe('TimelyBetModalComponent', () => {
  let component: TimelyBetModalComponent;
  let fixture: ComponentFixture<TimelyBetModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelyBetModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelyBetModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
