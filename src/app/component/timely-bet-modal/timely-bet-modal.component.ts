import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { defaultAgentID } from '../../common/global';
import { Status } from '../../enum/status.enum';
import { UserRole } from '../../enum/user-role.enum';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { MemberService } from '../../services/member.service';
import { UserService } from '../../services/user.service';
import { MemberTimelyBetComponent } from '../member-timely-bet/member-timely-bet.component';

const DEFAULT_AGENT_ID = defaultAgentID;
const INTERVAL = 3000;
@Component({
  selector: 'app-timely-bet-modal',
  templateUrl: './timely-bet-modal.component.html',
  styleUrls: ['./timely-bet-modal.component.css']
})
export class TimelyBetModalComponent implements OnInit, OnDestroy {
  @Input() userID: number;
  @Output() memberNum: EventEmitter<number> = new EventEmitter();

  @ViewChild('sortOfAgent', { static: true }) sortOfAgent: MatSort;
  @ViewChild('sortOfMember', { static: true }) sortOfMember: MatSort;
  @ViewChild('paginatorOfAgent', { static: true }) paginatorOfAgent: MatPaginator;
  @ViewChild('paginatorOfMember', { static: true }) paginatorOfMember: MatPaginator;
  dataSourceOfAgent = new MatTableDataSource<User>();
  dataSourceOfMember = new MatTableDataSource<Member>();
  displayedColumnsOfAgent: string[] = [
    'account',
    'ip',
    'updated_at',
    'modal'
  ];

  displayedColumnsOfMember: string[] = [
    'account',
    'ip',
    'updated_at',
    'is_allow_play',
    'modal'
  ];

  user: User;
  loginUserID: number;
  playingMemberInterval: any;


  constructor(
    private translateServ: TranslateService,
    private agentServ: AgentService,
    private userServ: UserService,
    private memberServ: MemberService,
    private modalService: BsModalService,
  ) { }

  ngOnDestroy(): void {
    clearInterval(this.playingMemberInterval);
  }

  ngOnInit(): void {
    this.dataSourceOfMember.filterPredicate = (data, filter) => data.account.includes(filter) || data.nickname.includes(filter);
    this.dataSourceOfMember.sort = this.sortOfMember;
    this.dataSourceOfMember.paginator = this.paginatorOfMember;
    if (this.userID) {
      this.getUser(this.userID);
    }
  }

  getUser(id: number) {
    this.userServ.getUser(id).subscribe(r => {
      this.user = r.data;
      this.startGetMembers();
    });
  }

  startGetMembers() {
    this.fetchMember();
    this.playingMemberInterval = setInterval(() => {
      this.fetchMember();
    }, INTERVAL)
  }

  fetchMember() {
    if (this.user.identity === UserRole.admin) {
      return this.getMembers();
    }
    this.getChildAgentByUserID(this.user.id);
  }

  getMembers() {
    this.memberServ.getMemberList().subscribe(r => this.setMember(r.data));
  }

  getChildAgentByUserID(id: number) {
    this.agentServ.getAgentChildsIdByAgentsId(id).subscribe(r => this.getChildMembersByParentIDs(r.data));
  }

  getChildMembersByParentIDs(ids: Array<number>) {
    ids = ids ? ids : [];
    const req = ids.map(id => this.memberServ.getMemberByAgent(id));
    req.push(this.memberServ.getMemberByAgent(this.user.id));
    forkJoin(req).pipe(map(res => res.map(r => r.data)))
      .subscribe(r => {
        const members: Member[] = [];
        r.forEach(e => members.push(...e));
        this.setMember(members);
      });
  }

  setMember(members: Member[]) {
    const filteredMembers = members.filter(m => m.agent_id !== DEFAULT_AGENT_ID && m.is_play_game);
    this.dataSourceOfMember.data = filteredMembers;
    this.memberNum.emit(filteredMembers.length);
  }

  changeMemberAllowGame(member: Member) {
    const turnedOnMsg = this.translateServ.instant('common.turnedOn');
    const turnedOffMsg = this.translateServ.instant('common.turnedOff');
    const data = {
      id: member.id,
      is_allow_play: member.is_allow_play ? 0 : 1,
    };

    this.memberServ.updateMemberIsAllowPlay(data).subscribe(
      () => {
        member.is_allow_play = data.is_allow_play;
        let msg = this.translateServ.instant('manageAgent.memberStatus');
        msg += data.is_allow_play ? turnedOnMsg : turnedOffMsg;
        alert(msg);
      },
      err => alert(`${this.translateServ.instant('common.updateFailed')}: ${err.error.message}`)
    );
  }

  checkAllowChange(member: Member) {
    if (this.user.identity !== UserRole.admin) {
      return member.agent_id === this.user.id;
    }
    return true;
  }

  openMemberTimelyBet(member: Member) {
    this.modalService.show(MemberTimelyBetComponent, { initialState: { member }, class: 'modal-xl' });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceOfMember.filter = filterValue.trim().toLowerCase();
  }

  get Status() { return Status }

}
