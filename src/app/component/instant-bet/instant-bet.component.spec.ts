import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantBetComponent } from './instant-bet.component';

describe('InstantBetComponent', () => {
  let component: InstantBetComponent;
  let fixture: ComponentFixture<InstantBetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantBetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantBetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
