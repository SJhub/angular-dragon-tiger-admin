import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserRole } from '../../enum/user-role.enum';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-agent-online',
  templateUrl: './agent-online.component.html',
  styleUrls: ['./agent-online.component.css']
})
export class AgentOnlineComponent implements OnInit {
  @Output() agentNum: EventEmitter<number> = new EventEmitter();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<User>();

  displayedColumns: string[] = [
    'account',
    'ip',
    'updated_at'
  ];

  constructor(
    private userServ: UserService,
    private authServ: AuthenticationService
  ) { }

  ngOnInit(): void {
    const identity = Number(this.authServ.getUserIdentity());
    const parentID = Number(this.authServ.getUserId());
    this.getAgentsOnline(identity, parentID);
  }

  getAgentsOnline(identity: number, parentID: number) {
    let req = this.userServ.getUsers();

    if (identity !== UserRole.admin) {
      req = this.userServ.getUserByParentID(parentID);
    }

    req.subscribe(r => {
      const users: User[] = r.data;
      this.dataSource.data = users.filter(e => e.is_hello && (e.identity === UserRole.agent || e.identity === UserRole.topAgent));
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.agentNum.emit(this.dataSource.data.length);
    });
  }
}
