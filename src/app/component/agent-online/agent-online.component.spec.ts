import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentOnlineComponent } from './agent-online.component';

describe('AgentOnlineComponent', () => {
  let component: AgentOnlineComponent;
  let fixture: ComponentFixture<AgentOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
