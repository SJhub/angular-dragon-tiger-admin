import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTesterFormComponent } from './game-tester-form.component';

describe('GameTesterFormComponent', () => {
  let component: GameTesterFormComponent;
  let fixture: ComponentFixture<GameTesterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameTesterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTesterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
