import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GameTester } from '../../models/game-tester';
import { MemberService } from '../../services/member.service';

@Component({
  selector: 'app-game-tester-form',
  templateUrl: './game-tester-form.component.html',
  styleUrls: ['./game-tester-form.component.css']
})
export class GameTesterFormComponent implements OnInit {

  myForm: FormGroup;

  constructor(
    private translateService: TranslateService,
    private memberServ: MemberService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder
  ) { this.groupForm(); }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.getGameTester(id);
  }

  getGameTester(id: number) {
    if (id) {
      this.memberServ.getGameTester(id).subscribe(r => {
        this.myForm.patchValue(r.data);
      });
    }
  }

  editGameTester(body: GameTester) {
    this.memberServ.editGameTester(body).subscribe(() => {
      alert(this.translateService.instant('common.editSuccess'));
      this.location.back();
    },
      err => alert(this.translateService.instant('common.editFailed')))
  }

  groupForm() {
    this.myForm = this.fb.group({
      id: [null],
      account: [null],
      name: [null, Validators.required],
      gender: [null],
      nickname: [null],
      password: [null, Validators.minLength(6)],
      re_password: [null],
      status: []
    });
  }

  submitForm(val: any) {
    val.nickname = val.name;
    val.status = Number(val.status);
    if (this.myForm.valid) {
      if (val.password === val.re_password) {
        return this.editGameTester(val);
      }
    }
    this.markFormGroupTouched(this.myForm);
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  cancel() {
    this.location.back();
  }
}
