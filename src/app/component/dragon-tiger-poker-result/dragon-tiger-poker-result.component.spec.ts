import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragonTigerPokerResultComponent } from './dragon-tiger-poker-result.component';

describe('DragonTigerPokerResultComponent', () => {
  let component: DragonTigerPokerResultComponent;
  let fixture: ComponentFixture<DragonTigerPokerResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragonTigerPokerResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragonTigerPokerResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
