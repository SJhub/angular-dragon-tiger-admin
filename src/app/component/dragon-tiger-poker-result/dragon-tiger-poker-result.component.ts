import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { baccaratDrawResultStr, calcPoint, dragonResultToPokerResult, dragonTigerDrawResultStr, numToStr } from '../../common/global';
import { PokerResult, Poker } from '../../models/baccarat';

@Component({
  selector: 'app-dragon-tiger-poker-result',
  templateUrl: './dragon-tiger-poker-result.component.html',
  styleUrls: ['./dragon-tiger-poker-result.component.css']
})
export class DragonTigerPokerResultComponent implements OnInit {
  @Input() set drawResult(str: string) {
    str && this.setValue(str);
  }
  @Input() aiDetect = false;
  @Input() className = '';
  @Input() showItemType = false;

  invalidGame = false;

  itemTypeStr: string;

  pokerResultStr: {
    banker: string;
    player: string;
  };

  pokerResult: PokerResult;

  constructor(private translateServ: TranslateService) { }

  ngOnInit(): void {
  }

  setValue(str: string) {
    this.invalidGame = str === '000';
    if (!this.invalidGame) {
      try {
        this.pokerResult = dragonResultToPokerResult(str);
        this.itemTypeStr = dragonTigerDrawResultStr(this.pokerResult, true);
      } catch (error) {
        console.log(error);
      }
    }
  }

  calcPoint(pokers: Poker[]) {
    return calcPoint(pokers)
  }

  numToStr(num: number) {
    return numToStr(num);
  }
}
