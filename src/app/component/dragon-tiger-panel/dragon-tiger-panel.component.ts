import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { ItemTypeEnum } from '../../enum/item-type.enum';
import { BetRecord } from '../../models/bet-record';

const ItemTypes = [
  ItemTypeEnum.ItemTypeDragon,
  ItemTypeEnum.ItemTypeDragonTigerTie,
  ItemTypeEnum.ItemTypeTiger,
]

@Component({
  selector: 'app-dragon-tiger-panel',
  templateUrl: './dragon-tiger-panel.component.html',
  styleUrls: ['./dragon-tiger-panel.component.css']
})
export class DragonTigerPanelComponent implements OnInit {
  @Output() onItemTypeClick: EventEmitter<number> = new EventEmitter();
  form: FormGroup

  constructor(private fb: FormBuilder) {
    this.groupForm();
  }

  ngOnInit(): void {
  }

  patchValueToPanel(ids: number[]) {
    const grouped = ids.reduce((r, a) => {
      r[a] = r[a] || [];
      r[a].push(a);
      return r;
    }, Object.create(null));
    ItemTypes.forEach(type => {
      const ctrl = this.form.get(`${type}`).get('amount');
      let times = 0;
      if (grouped[type]) {
        times = grouped[type].length;
      }
      ctrl.setValue(times);
    })
  }

  patchBetRecords(betRecords: BetRecord[]) {
    ItemTypes.forEach(type => {
      const ctrl = this.form.get(`${type}`).get('amount');
      const records = betRecords.filter(e => e.action === GameActionTypeEnum.Bet && e.item_types_id === type);
      let amount = 0;
      if (records.length) {
        amount = records.map(e => e.amount).reduce((a, b) => a + b);
      }
      ctrl.setValue(amount);
    })
  }

  groupForm() {
    this.form = this.fb.group([]);
    const start = 24;
    ItemTypes.forEach(type => {
      this.form.addControl(`${type}`, this.createSingleBox(type));
    })
  }

  createSingleBox(id: number) {
    return this.fb.group({
      id: [id],
      amount: [0]
    });
  }

  getCtrlByType(type: number) {
    return this.form.get(String(type));
  }

  getValue(name: number, ctrlName: string) {
    const ctrl = this.form.get(String(name)).get(ctrlName);
    return ctrl.value;
  }

  onItemClick(type: number) {
    this.onItemTypeClick.emit(Number(type));
  }

  get ItemType() { return ItemTypeEnum }
}
