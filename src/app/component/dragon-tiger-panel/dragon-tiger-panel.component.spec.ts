import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragonTigerPanelComponent } from './dragon-tiger-panel.component';

describe('DragonTigerPanelComponent', () => {
  let component: DragonTigerPanelComponent;
  let fixture: ComponentFixture<DragonTigerPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragonTigerPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragonTigerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
