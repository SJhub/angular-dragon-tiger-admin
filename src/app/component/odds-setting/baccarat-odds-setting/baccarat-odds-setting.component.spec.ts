import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaccaratOddsSettingComponent } from './baccarat-odds-setting.component';

describe('BaccaratOddsSettingComponent', () => {
  let component: BaccaratOddsSettingComponent;
  let fixture: ComponentFixture<BaccaratOddsSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaccaratOddsSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaccaratOddsSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
