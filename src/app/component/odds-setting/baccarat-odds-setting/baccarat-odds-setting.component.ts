import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ItemTypes } from '../../../models/item-types';

const SINGLE_BET_NUMBER = 7;
@Component({
  selector: 'app-baccarat-odds-setting',
  templateUrl: './baccarat-odds-setting.component.html',
  styleUrls: ['./baccarat-odds-setting.component.css']
})
export class BaccaratOddsSettingComponent implements OnInit {
  form: FormGroup

  constructor(private fb: FormBuilder) {
    this.groupForm();
  }

  ngOnInit(): void {
  }

  isValid() {
    this.markFormGroupTouched(this.form);
    return this.form.valid;
  }

  patchValueToPanel(itemTypes: ItemTypes[]) {
    const obj = {};
    itemTypes.forEach(e => obj[e.id] = e.odds);
    this.form.patchValue(obj);
  }

  groupForm() {
    this.form = this.fb.group([]);
    const start = 24;
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const index = start + i;
      this.form.addControl(`${index}`, new FormControl(0, [Validators.required]));
    }
  }

  isFormControlInvalid(i: any) {
    const ctrl = this.form.get(String(i));
    return ctrl.errors && ctrl.touched;
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }
}
