import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { GameType } from '../../models/game-type';
import { ItemTypes } from '../../models/item-types';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { ItemTypesService } from '../../services/item-types.service';
import { BaccaratOddsSettingComponent } from './baccarat-odds-setting/baccarat-odds-setting.component';

@Component({
  selector: 'app-odds-setting',
  templateUrl: './odds-setting.component.html',
  styleUrls: ['./odds-setting.component.css']
})
export class OddsSettingComponent implements OnInit {
  @ViewChild('baccaratOdds') baccaratOdds: BaccaratOddsSettingComponent;

  myForm: FormGroup;

  gameTypes: GameType[];

  currentType: number;

  constructor(
    private translateService: TranslateService,
    private itemTypeServ: ItemTypesService,
    private gameTypeServ: GameTypeService,
    private enumTranslateServ: EnumTranslateService,
    private fb: FormBuilder
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
    this.getGameTypes();
    this.getItemTypes();
  }

  getGameTypes() {
    this.gameTypeServ.getGameTypes().subscribe(r => {
      this.gameTypes = r.data;
      this.currentType = GameTypeEnum.GameTypeSicBo;
    });
  }

  getItemTypes() {
    this.itemTypeServ.getItemTypesList().subscribe(r => this.patchItemTypes(r.data));
  }

  patchItemTypes(itemTypes: ItemTypes[]) {
    const obj = {};
    itemTypes.forEach(e => obj[e.id] = e.odds);
    this.myForm.patchValue(obj);
    this.baccaratOdds.patchValueToPanel(itemTypes);
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  groupForm() {
    this.myForm = this.fb.group([]);
    for (let i = 1; i <= 17; i++) {
      this.myForm.addControl(`${i}`, new FormControl(null, [Validators.required]));
    }
  }

  submitForm(val: any) {
    switch (Number(this.currentType)) {
      case GameTypeEnum.GameTypeSicBo:
        if (this.myForm.valid) {
          return this.updateItemTypes(val);
        }
        return this.markFormGroupTouched(this.myForm);

      case GameTypeEnum.GameTypeBaccarat:
        if (this.baccaratOdds.isValid()) {
          return this.updateItemTypes(this.baccaratOdds.form.value);
        }
        break;
    }
  }

  updateItemTypes(obj: object) {
    const array = [];
    Object.keys(obj).forEach(e => array.push({ id: Number(e), odds: Number(obj[e]) }));
    this.itemTypeServ.updateItemTypesOdds(JSON.stringify(array)).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      () => alert(this.translateService.instant('common.updateFailed'))
    );
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  getSingleBetControlValue(i: string) {
    return this.myForm.get(String(i)).value;
  }

  isFormControlInvalid(i: any) {
    const ctrl = this.myForm.get(String(i));
    return ctrl.errors && ctrl.touched;
  }

  get GameType() { return GameTypeEnum }
}
