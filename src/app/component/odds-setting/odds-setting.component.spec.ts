import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OddsSettingComponent } from './odds-setting.component';

describe('OddsSettingComponent', () => {
  let component: OddsSettingComponent;
  let fixture: ComponentFixture<OddsSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OddsSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OddsSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
