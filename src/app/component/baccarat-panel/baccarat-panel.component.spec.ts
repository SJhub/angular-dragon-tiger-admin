import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaccaratPanelComponent } from './baccarat-panel.component';

describe('BaccaratPanelComponent', () => {
  let component: BaccaratPanelComponent;
  let fixture: ComponentFixture<BaccaratPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaccaratPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaccaratPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
