import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { BetRecord } from '../../models/bet-record';

const SINGLE_BET_NUMBER = 7;
const ITEM_TYPE_START_ID = 24;

@Component({
  selector: 'app-baccarat-panel',
  templateUrl: './baccarat-panel.component.html',
  styleUrls: ['./baccarat-panel.component.css']
})
export class BaccaratPanelComponent implements OnInit {
  @Output() onItemTypeClick: EventEmitter<number> = new EventEmitter();
  form: FormGroup

  constructor(private fb: FormBuilder) {
    this.groupForm();
  }

  ngOnInit(): void {
  }

  patchValueToPanel(ids: number[]) {
    const grouped = ids.reduce((r, a) => {
      r[a] = r[a] || [];
      r[a].push(a);
      return r;
    }, Object.create(null));
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const index = i + ITEM_TYPE_START_ID;
      const ctrl = this.form.get(`${index}`).get('amount');
      let times = 0;
      if (grouped[index]) {
        times = grouped[index].length;
      }
      ctrl.setValue(times);
    }
  }

  patchBetRecords(betRecords: BetRecord[]) {
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const index = i + ITEM_TYPE_START_ID;
      const ctrl = this.form.get(`${index}`).get('amount');
      const records = betRecords.filter(e => e.action === GameActionTypeEnum.Bet && e.item_types_id === index);
      let amount = 0;
      if (records.length) {
        amount = records.map(e => e.amount).reduce((a, b) => a + b);
      }
      ctrl.setValue(amount);
    }
  }

  groupForm() {
    this.form = this.fb.group([]);
    const start = 24;
    for (let i = 0; i < SINGLE_BET_NUMBER; i++) {
      const index = start + i;
      this.form.addControl(`${index}`, this.createSingleBox(index));
    }
  }

  createSingleBox(id: number) {
    return this.fb.group({
      id: [id],
      amount: [0]
    });
  }

  getValue(name: string, ctrlName: string) {
    const ctrl = this.form.get(name).get(ctrlName);
    return ctrl.value;
  }

  onItemClick(num: number) {
    this.onItemTypeClick.emit(num);
  }
}
