import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragonTigerCroupierComponent } from './dragon-tiger-croupier.component';

describe('DragonTigerCroupierComponent', () => {
  let component: DragonTigerCroupierComponent;
  let fixture: ComponentFixture<DragonTigerCroupierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragonTigerCroupierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragonTigerCroupierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
