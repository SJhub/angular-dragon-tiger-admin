import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { baccaratDrawResultStr, dragonTigerDrawResultStr } from '../../common/global';
import { PokerResult, Poker } from '../../models/baccarat';

const POKER_SUITS = ['spades', 'heart', 'diamond', 'club'];
const POKER_NUMS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];

@Component({
  selector: 'app-dragon-tiger-croupier',
  templateUrl: './dragon-tiger-croupier.component.html',
  styleUrls: ['./dragon-tiger-croupier.component.css']
})
export class DragonTigerCroupierComponent implements OnInit {
  form: FormGroup

  constructor(
    private fb: FormBuilder,
    private translateServ: TranslateService
  ) {
    this.groupForm();
  }

  ngOnInit(): void {
  }

  isFormValid() {
    this.markFormGroupTouched(this.form);
    return this.form.valid;
  }

  getFormValue() {
    const res: PokerResult = JSON.parse(JSON.stringify(this.form.value));
    const mapFunc = (e: Poker) => e.number = Number(e.number) + 1;
    res.banker.map(mapFunc);
    res.player.map(mapFunc);

    const dragon = this.getTransformResult(res.player[0]);
    const tiger = this.getTransformResult(res.banker[0]);
    return dragon + tiger;
  }

  setFormValue(value: string, detect = false) {
    if (value === '000') {
      return this.resetForm();
    }

    try {
      const res: PokerResult = JSON.parse(value);
      res.banker.map(e => e.number -= 1);
      res.player.map(e => e.number -= 1);
      this.form.patchValue(res);
    }
    catch (error) {
      console.log(error);
    }
  }

  groupForm() {
    this.form = this.fb.group({
      banker: this.fb.array([]),
      player: this.fb.array([]),
    });

    for (let i = 0; i < 1; i++) {
      const array1 = this.form.get('banker') as FormArray;
      const array2 = this.form.get('player') as FormArray;
      array1.push(this.createSingleBox());
      array2.push(this.createSingleBox());
    }
  }

  createSingleBox() {
    return this.fb.group({
      suit: [POKER_SUITS[0]],
      number: [0]
    });
  }

  resetForm() {
    const res: PokerResult = JSON.parse(JSON.stringify(this.form.value));
    const mapFunc = (e: Poker) => {
      e.suit = POKER_SUITS[0];
      e.number = 0;
    };
    res.banker.map(mapFunc);
    res.player.map(mapFunc);
    this.form.patchValue(res);
  }

  isInvalid(formCtrl: AbstractControl, index: number) {
    const ctrl = formCtrl.get(String(index)).get('number');
    return ctrl.errors && ctrl.touched;
  }

  getValue(formCtrl: AbstractControl, index: number, ctrlName: string) {
    const ctrl = formCtrl.get(String(index)).get(ctrlName);
    return ctrl.value;
  }

  getSuitClass(formCtrl: AbstractControl, index: number, classIndex: number) {
    const ctrl = formCtrl.get(String(index)).get('suit');
    const suit = POKER_SUITS[classIndex];
    const active = suit == ctrl.value ? 'active' : '';
    return `${suit} ${active}`;
    // return POKER_SUITS[this.getValue(formCtrl, index, 'suit')];
  }

  onSuitClick(formCtrl: AbstractControl, index: number, classIndex: number) {
    const ctrl = formCtrl.get(String(index)).get('suit');
    ctrl.setValue(POKER_SUITS[classIndex]);
  }

  onPickerChanged(formCtrl: AbstractControl, index: number, add = true) {
    const ctrl = formCtrl.get(String(index)).get('suit');
    let n = Number(ctrl.value);
    if (add) n += 1;
    else n -= 1;

    const maxIndex = POKER_SUITS.length - 1;
    if (n > maxIndex) n = 0;
    if (n < 0) n = maxIndex;

    ctrl.setValue(n);
  }

  private markFormGroupTouched(form: FormGroup) {
    Object.values(form.controls).forEach(control => {
      control.markAsTouched();

      if ((control as any).controls) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  getTransformResult(data: Poker) {
    let suit = String(data.suit).split('')[0];
    let num: any = 0;

    switch (data.number) {
      case 1:
        num = 'A';
        break;
      case 10:
        num = 'T';
        break;
      case 11:
        num = 'J';
        break;
      case 12:
        num = 'Q';
        break;
      case 13:
        num = 'K';
        break;
      default:
        num = Number(data.number);
        break;
    }
    return `${num}${suit}`;
  }

  getItemTypeStr() {
    if (this.form.valid) {
      const res: PokerResult = JSON.parse(JSON.stringify(this.form.value));
      return dragonTigerDrawResultStr(res);
    }
  }

  get POKER_SUITS() { return POKER_SUITS }

  get POKER_NUMS() { return POKER_NUMS }
}
