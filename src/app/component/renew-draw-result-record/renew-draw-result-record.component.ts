import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { GameType } from '../../models/game-type';
import { RenewRecord } from '../../models/renew-record';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { RenewDrawResultRecordsService } from '../../services/renew-draw-result-records.service';

@Component({
  selector: 'app-renew-draw-result-record',
  templateUrl: './renew-draw-result-record.component.html',
  styleUrls: ['./renew-draw-result-record.component.css']
})
export class RenewDrawResultRecordComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSource = new MatTableDataSource<RenewRecord>();

  displayedColumns: string[] = [
    // 'users_id',
    // 'game_types_id',
    'game_number',
    'old_draw_result',
    'new_draw_result',
    'updated_at',
  ];

  renewRecords: RenewRecord[];
  gameTypes: GameType[];
  currentType: number;

  constructor(
    private renewRecordServ: RenewDrawResultRecordsService,
    private gameTypeServ: GameTypeService,
    private enumTranslateServ: EnumTranslateService
  ) { }


  ngOnInit(): void {
    this.gameTypeServ.getGameTypes().subscribe(r => {
      this.gameTypes = r.data;
      this.currentType = GameTypeEnum.GameTypeDragonTiger;
      this.getRenewRecords();
    });
  }

  getRenewRecords() {
    this.renewRecordServ.getRenewDrawResultRecords().subscribe(r => {
      this.renewRecords = r.data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.onGameTypeChanged(this.currentType);
    });
  }

  onGameTypeChanged(value: string | number) {
    this.dataSource.data = this.renewRecords.filter(e => e.game_types_id === Number(value));
  }


  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  get GameTypeEnum() { return GameTypeEnum }
}
