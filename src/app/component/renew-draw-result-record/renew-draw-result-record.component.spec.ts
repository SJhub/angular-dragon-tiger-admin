import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewDrawResultRecordComponent } from './renew-draw-result-record.component';

describe('RenewDrawResultRecordComponent', () => {
  let component: RenewDrawResultRecordComponent;
  let fixture: ComponentFixture<RenewDrawResultRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewDrawResultRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewDrawResultRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
