import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PlayGame } from '../../models/play-game';

@Component({
  selector: 'app-ai-result',
  templateUrl: './ai-result.component.html',
  styleUrls: ['./ai-result.component.css']
})
export class AiResultComponent implements OnInit {

  playGame: PlayGame;
  picName: string;

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit(): void {
    if (this.playGame) {
      this.picName = `screenshot_baccarat_${this.playGame.game_number}.jpg`;
    }
  }



}
