import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WebcamInitError, WebcamImage, WebcamUtil } from 'ngx-webcam';
import { Subject, Observable } from 'rxjs';
import { baccaratPicCatchToPokerResult } from '../../common/global';
import { PokerResult } from '../../models/baccarat';
import { UploadFileService } from '../../services/upload-file.service';

@Component({
  selector: 'app-ai-webcam',
  templateUrl: './ai-webcam.component.html',
  styleUrls: ['./ai-webcam.component.css']
})
export class AiWebcamComponent implements OnInit {
  @Output() onPicCatch: EventEmitter<PokerResult | string> = new EventEmitter();
  @Input() external = false;

  gameNumber: string;
  deviceLen = 0;
  // toggle webcam on/off
  public showWebcam = false;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {
    width: { ideal: 1920 },
    height: { ideal: 1080 }
  };
  public errors: WebcamInitError[] = [];

  // latest snapshot
  public webcamImage: WebcamImage = null;

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

  constructor(private uploadServ: UploadFileService) { }

  public ngOnInit(): void {
    this.showWebcam = this.external;
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
        this.deviceLen = mediaDevices.length;
      });
  }

  public triggerSnapshot(gameNumber: string): void {
    if (gameNumber) {
      this.gameNumber = gameNumber;
      this.trigger.next();
    }
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
    const file = this.uploadServ.base64toFile(this.webcamImage.imageAsDataUrl, this.gameNumber);
    // const res = { "banker": ["club 10", "club 3", "diamond 5"], "player": ["diamond Q", "spade 9", "heart J"] };
    // this.onPicCatch.emit(baccaratPicCatchToPokerResult(JSON.stringify(res)));
    // return;
    this.uploadServ.uploadSingleFile(file).subscribe(
      r => {
        console.log(r);
        this.onPicCatch.emit(baccaratPicCatchToPokerResult(JSON.stringify(r)));
      },
      err => this.onPicCatch.emit('fail'));
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }
}
