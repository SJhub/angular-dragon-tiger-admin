import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiWebcamComponent } from './ai-webcam.component';

describe('AiWebcamComponent', () => {
  let component: AiWebcamComponent;
  let fixture: ComponentFixture<AiWebcamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AiWebcamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiWebcamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
