import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberBetRecordComponent } from './member-bet-record.component';

describe('MemberBetRecordComponent', () => {
  let component: MemberBetRecordComponent;
  let fixture: ComponentFixture<MemberBetRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberBetRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberBetRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
