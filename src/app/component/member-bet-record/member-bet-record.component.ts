import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { concat, forkJoin, Observable, Subscriber, Subscription } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { baccaratDrawResultStr, getDrawResultToBetItem } from '../../common/global';
import { getLastCalcDay } from '../../common/report';
import { BetRecordResultEnum } from '../../enum/bet-record-result.enum';
import { BetRecordStatusEnum } from '../../enum/bet-record-status.enum';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { ItemTypeEnum } from '../../enum/item-type.enum';
import { UserRole } from '../../enum/user-role.enum';
import { BetRecord } from '../../models/bet-record';
import { CalcItemResultRecord } from '../../models/calc-item-result-record';
import { GameType } from '../../models/game-type';
import { ItemType } from '../../models/item-type';
import { Member } from '../../models/member.model';
import { PlayGame } from '../../models/play-game';
import { AuthenticationService } from '../../services/auth.service';
import { BetRecordService } from '../../services/bet-record.service';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameTypeService } from '../../services/game-type.service';
import { PlayGameService } from '../../services/play-game.service';

interface GameResult {
  game_types_id: number;
  game_number: number;
  draw_result: number;
  item_types_ids: Array<string>;
  total_donate: number;
  game_result_amount: number;
  self_profit: number;
  wash_code_commission: number;
  bet_records: BetRecord[];
  status: number;
  calc: number;
  created_at: string;
}

@Component({
  selector: 'app-member-bet-record',
  templateUrl: './member-bet-record.component.html',
  styleUrls: ['./member-bet-record.component.css']
})
export class MemberBetRecordComponent implements OnInit, OnDestroy {
  @ViewChild('sortOfGame', { static: true }) sortOfGame: MatSort;
  @ViewChild('sortOfBet', { static: true }) sortOfBet: MatSort;

  @Input() gameTypeID: number;
  @Input() member: Member;
  @Input() closingDate: string;
  @Input() set betRecord(obj: Object) {
    if (obj) {
      this._betRecord = obj;
      this.getGameNumberByObj(obj);
    }
  }

  @Output() close = new EventEmitter();

  get betRecord() { return this._betRecord }
  _betRecord: Object;

  dataSourceOfGame = new MatTableDataSource<GameResult>();
  dataSourceOfBet = new MatTableDataSource<BetRecord>();

  checkedGameResult = [];

  displayedColumnsOfGame: string[] = [
    'created_at',
    'game_number',
    'game_result_amount',
    // 'draw_result',
    // 'item_type',
    // 'total_donate',
    // 'self_profit',
    // 'wash_code_commission',
    'view',
    // 'checkBox'
  ];
  displayedColumnsOfBet: string[] = [
    // 'game_types_id',
    'item_types_id',
    // 'action',
    'amount',
    'game_result_amount',
    // 'created_at'
  ];

  gameTypes: GameType[];

  gameResult: GameResult;

  fetched = false;

  loginIdentity: number;

  target: HTMLElement;

  subscription: Subscription;

  constructor(
    private betRecordServ: BetRecordService,
    private gameTypeServ: GameTypeService,
    private enumTranslateServ: EnumTranslateService,
    private playGameServ: PlayGameService,
    private calcRecordServ: CalcItemResultRecordService,
    private translateService: TranslateService,
    private authServ: AuthenticationService
  ) {
    this.loginIdentity = Number(this.authServ.getUserIdentity());
    if (this.loginIdentity !== UserRole.admin) {
      this.displayedColumnsOfGame = this.displayedColumnsOfGame.filter(e => e != 'checkBox');
    }
  }
  ngOnDestroy(): void {
    this.subscription && this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.dataSourceOfGame.sort = this.sortOfGame;
    this.dataSourceOfBet.sort = this.sortOfBet;
    this.getGameTypes();
  }

  getGameNumberByObj(obj: Object) {
    let gameNumbers = [];
    console.log(obj);

    Object.keys(obj).forEach(game_number => gameNumbers.push({ game_number }));
    gameNumbers = gameNumbers.reverse();

    const reqGames = gameNumbers.map(e => this.playGameServ.getPlayGameByGameNumber(this.gameTypeID, e.game_number));
    const reqCalcs = gameNumbers.map(e => this.calcRecordServ.getCalcItemResultRecordByGameTypeAndNumber(this.gameTypeID, e.game_number));
    const reqBets = gameNumbers.map(e => this.betRecordServ.getBetRecordByGameTypeAndNumber(this.gameTypeID, e.game_number, this.member.id));


    this.dataSourceOfGame.data = [];
    this.subscription && this.subscription.unsubscribe();

    let reqs = gameNumbers.map(e => {
      return forkJoin([
        this.playGameServ.getPlayGameByGameNumber(this.gameTypeID, e.game_number),
        this.calcRecordServ.getCalcItemResultRecordByGameTypeAndNumber(this.gameTypeID, e.game_number),
        this.betRecordServ.getBetRecordByGameTypeAndNumber(this.gameTypeID, e.game_number, this.member.id)
      ]).pipe(tap(r => {
        const playGame: PlayGame = r[0].data;
        const calcItems: CalcItemResultRecord[] = r[1].data;
        const betRecords = this.groupBetRecords(r[2].data);
        const lastCalcItem = calcItems.pop();
        const status = betRecords.find(r => r.status === BetRecordStatusEnum.Unusual) ? BetRecordStatusEnum.Unusual : BetRecordStatusEnum.Finish;

        if (playGame && lastCalcItem) {
          const mapCalcItem = this.mapItemTypeName(playGame, lastCalcItem);
          this.dataSourceOfGame.data.push({
            ...playGame,
            ...mapCalcItem,
            ...this.calcGameResult(betRecords, playGame.game_number),
            bet_records: betRecords,
            status,
            calc: Number(this.isDateHasCalc(playGame.created_at))
          });
          this.dataSourceOfGame.data = this.dataSourceOfGame.data.slice();
        }
      }))
    })

    this.subscription = concat(...reqs).subscribe(() => this.fetched = true);

    return;

    let playGames: PlayGame[] = null;
    let calcItems: CalcItemResultRecord[] = null;


    forkJoin(reqGames).pipe(
      map(res => res.map(r => r.data)),
      switchMap(r => {
        playGames = r;
        return forkJoin(reqCalcs).pipe(map(res => res.map(r => r.data)));
      }),
      switchMap(r => {
        const tmp = [];
        // 取最後每局開獎項目最後一筆
        r.forEach((e: CalcItemResultRecord[]) => tmp.push(e[e.length - 1]));
        calcItems = tmp;
        return forkJoin(reqBets).pipe(map(res => res.map(r => r.data)));
      })
    ).subscribe(r => {
      const gameResult: GameResult[] = [];
      r.forEach((e: BetRecord[], i: number) => {
        const betRecords = this.groupBetRecords(e);
        const gameNumber = gameNumbers[i].game_number;
        const playGame: PlayGame = playGames.find(g => g.game_number == gameNumber);
        const status = e.find(r => r.status === BetRecordStatusEnum.Unusual) ? BetRecordStatusEnum.Unusual : BetRecordStatusEnum.Finish;
        let calcItem: CalcItemResultRecord = calcItems.find(c => c?.game_number == gameNumber);

        if (playGame && calcItem) {
          calcItem = this.mapItemTypeName(playGame, calcItem);
          gameResult.push({
            ...playGame,
            ...calcItem,
            ...this.calcGameResult(betRecords, gameNumber),
            bet_records: betRecords,
            status,
            calc: Number(this.isDateHasCalc(playGame.created_at))
          });
        }
      });




      this.dataSourceOfGame.data = gameResult;
      console.log(gameResult);
      this.fetched = true;
    });
  }

  isDateHasCalc(date: string) {
    const gameCreatedTime = new Date(date).getTime();
    const lastCalcTime = new Date(getLastCalcDay());
    lastCalcTime.setDate(lastCalcTime.getDate() + 1);
    lastCalcTime.setHours(12, 0, 0);
    return gameCreatedTime <= lastCalcTime.getTime();
  }

  getGameTypes() {
    this.gameTypeServ.getGameTypes().subscribe(r => this.gameTypes = r.data);
  }

  getGameTypeName(gameTypeID: number) {
    const gameType = this.gameTypes.find(e => e.id === gameTypeID);
    return this.enumTranslateServ.getTranslatedGameName(gameType.en_name);
  }

  getItemTypeName(itemTypeID: number) {
    if (itemTypeID === -1) {
      return this.translateService.instant('searching.donate');
    }
    if (itemTypeID === -2) {
      return this.translateService.instant('manageAgent.form.washCode');
    }
    return this.enumTranslateServ.getTranslatedBetItem(ItemType.getNameByID(itemTypeID));
  }

  getGameActionTypeName(actionID: number) {
    return this.enumTranslateServ.getTranslatedAction(actionID);
  }

  showBetReocrds(e: GameResult, event: any) {
    console.log(event);
    this.target = event.target;

    let totalDonateAmount = 0;
    e.bet_records.forEach(item => {
      if (item.action === GameActionTypeEnum.Donate) {
        totalDonateAmount += item.amount;
      }
    });

    const totalDonate = {
      item_types_id: -1,
      amount: totalDonateAmount,
      game_result_amount: 0 - totalDonateAmount
    } as BetRecord;

    const washCodeCommission = {
      item_types_id: -2,
      amount: null,
      game_result_amount: e.wash_code_commission
    } as BetRecord;

    const betRecords = e.bet_records.filter(item => item.action === GameActionTypeEnum.Bet);

    if (totalDonate.amount) {
      betRecords.push(totalDonate);
    }

    if (washCodeCommission.game_result_amount) {
      betRecords.push(washCodeCommission);
    }

    this.gameResult = e;
    this.dataSourceOfBet.data = betRecords;
    console.log(betRecords);

  }

  clearBetRecords() {
    this.dataSourceOfBet.data = [];
    setTimeout(() => this.target.scrollIntoView({ block: "center" }), 100);
  }

  mapItemTypeName(playGame: PlayGame, calcRecord: CalcItemResultRecord) {

    if (playGame.draw_result === '000' || !calcRecord.item_types_ids.length) {
      return calcRecord.item_types_ids = this.translateService.instant('invalidDrawResult');
    }

    switch (playGame.game_types_id) {
      case GameTypeEnum.GameTypeSicBo:
        calcRecord.item_types_ids = getDrawResultToBetItem(playGame.draw_result).map(e => this.getItemTypeName(e));
        break;
      case GameTypeEnum.GameTypeBaccarat:
        calcRecord.item_types_ids = baccaratDrawResultStr(JSON.parse(playGame.draw_result), this.translateService, true);
    }

    return calcRecord;
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }

  calcGameResult(betRecord: BetRecord[], gameNumber: number) {
    let totalDonate = 0;
    let activeTotalAmount = 0;
    let gameResultAmount = 0;
    let washCodeCommissionPercent = 0;
    let washCodeCommission = 0;
    let selfProfit = 0;
    betRecord.forEach(e => {
      if (e.result !== BetRecordResultEnum.Invalid) {
        switch (e.action) {
          case GameActionTypeEnum.Bet:
            washCodeCommissionPercent = e.commission_percent;
            gameResultAmount += e.game_result_amount;
            activeTotalAmount += e.amount;
            break;
          case GameActionTypeEnum.Donate:
            totalDonate += e.amount;
            break;
        }
      }
    });

    // 洗碼佣金
    washCodeCommission = activeTotalAmount * washCodeCommissionPercent / 100;

    // 本身獲利
    selfProfit = gameResultAmount + washCodeCommission - totalDonate;

    const gameResult = {
      game_number: gameNumber,
      total_donate: totalDonate,
      game_result_amount: gameResultAmount + washCodeCommission,
      self_profit: selfProfit,
      wash_code_commission: washCodeCommission
    } as GameResult;

    return gameResult;
  }

  groupBetRecords(betRecords: BetRecord[]) {
    const donate = betRecords.filter(e => e.action === GameActionTypeEnum.Donate);
    // group by item_types_id
    const result = betRecords.filter(e => e.action === GameActionTypeEnum.Bet).reduce((r, a) => {
      r[a.item_types_id] = r[a.item_types_id] || [];
      r[a.item_types_id].push(a);
      return r;
    }, Object.create(null));

    // group object to array
    let tmp: BetRecord[] = [];
    Object.values(result).forEach((records: BetRecord[]) => {
      const record = records.pop();
      records.forEach(r => {
        record.amount += r.amount;
        record.game_result_amount += r.game_result_amount;
      });
      tmp.push(record);
    });

    // sort pic first
    const sortTmp: BetRecord[] = []

    tmp = tmp.sort((a, b) => a.item_types_id - b.item_types_id);

    tmp.forEach(e => {
      if (e.item_types_id >= ItemTypeEnum.ItemTypeSpecificDoubles1 && e.item_types_id <= ItemTypeEnum.ItemTypeSingleDice6) {
        sortTmp.unshift(e);
      } else {
        sortTmp.push(e);
      }
    });
    sortTmp.push(...donate);
    return sortTmp;
  }

  getBetPics(itemTypeID: number) {

    switch (itemTypeID) {
      case ItemTypeEnum.ItemTypeSpecificDoubles1:
        return ['img_dice_one.png', 'img_dice_one.png'];
      case ItemTypeEnum.ItemTypeSpecificDoubles2:
        return ['icon_full_screen (3).png', 'icon_full_screen (3).png'];
      case ItemTypeEnum.ItemTypeSpecificDoubles3:
        return ['img_dice_three.png', 'img_dice_three.png'];
      case ItemTypeEnum.ItemTypeSpecificDoubles4:
        return ['img_dice_four.png', 'img_dice_four.png'];
      case ItemTypeEnum.ItemTypeSpecificDoubles5:
        return ['icon_full_screen (2).png', 'icon_full_screen (2).png'];
      case ItemTypeEnum.ItemTypeSpecificDoubles6:
        return ['img_dice_six.png', 'img_dice_six.png'];
      case ItemTypeEnum.ItemTypeSingleDice1:
        return ['img_dice_one.png'];
      case ItemTypeEnum.ItemTypeSingleDice2:
        return ['icon_full_screen (3).png'];
      case ItemTypeEnum.ItemTypeSingleDice3:
        return ['img_dice_three.png'];
      case ItemTypeEnum.ItemTypeSingleDice4:
        return ['img_dice_four.png'];
      case ItemTypeEnum.ItemTypeSingleDice5:
        return ['icon_full_screen (2).png'];
      case ItemTypeEnum.ItemTypeSingleDice6:
        return ['img_dice_six.png'];
      default:
        return null;
    }
  }

  onGameNumberChecked(gameResult: GameResult, e: any) {
    const checked = e.target.checked;
    if (checked) {
      this.checkedGameResult.push(gameResult.game_number);
    } else {
      const index = this.checkedGameResult.findIndex(e => e === gameResult.game_number);
      this.checkedGameResult.splice(index, 1);
    }
    console.log(this.checkedGameResult);

  }

  updateGameResultStatus() {
    if (this.checkedGameResult.length) {
      if (confirm(this.translateService.instant('gameUnusualConfirm'))) {
        const apis = this.checkedGameResult.map(e => this.betRecordServ.setMembersToUnusual(this.gameTypeID, Number(e), this.member.id))
        forkJoin(apis).subscribe(
          () => {
            this.checkedGameResult = [];
            this.getGameTypes();
            this.getGameNumberByObj(this.betRecord);
            alert(this.translateService.instant('common.updateSuccess'));
          }, err => alert(this.translateService.instant('common.updateFailed'))
        );
      }
    }
  }

  onClose() {
    this.close.emit(true);
    this.subscription && this.subscription.unsubscribe();
  }

  get PATH() { return './assets/img/brand/' }

  get BetRecordStatus() { return BetRecordStatusEnum }

  get UserRole() { return UserRole }

  get GameTypeEnum() { return GameTypeEnum }
}
