import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { addIndexToArray } from '../../common/global';
import { ProclamationTypesEnum } from '../../enum/proclamation-type.enum';
import { Status } from '../../enum/status.enum';
import { Proclamation } from '../../models/proclamation';
import { ProclamationService } from '../../services/proclamation.service';

@Component({
  selector: 'app-marquee',
  templateUrl: './marquee.component.html',
  styleUrls: ['./marquee.component.css']
})
export class MarqueeComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['index', 'title', 'status', 'function'];
  dataSource = new MatTableDataSource<Proclamation>();

  isFetched = false;
  isAdding = false;

  constructor(
    private translateService: TranslateService,
    private proclamationService: ProclamationService,
  ) { }

  ngOnInit(): void {
    this.getProclamations();
  }

  getProclamations() {
    this.proclamationService.getProclamations().subscribe(r => {
      const proclamations: Proclamation[] = r.data;
      this.dataSource.data = addIndexToArray(proclamations.filter(e => e.type === ProclamationTypesEnum.ProclamationTypeGame));
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isFetched = true;
    });
  }

  addProclamation(title: string) {
    title = title.trim();
    if (title) {
      const proclamation = {
        title,
        status: Status.Active,
        type: ProclamationTypesEnum.ProclamationTypeGame
      } as Proclamation;
      this.proclamationService.addProclamation(proclamation).subscribe(
        () => {
          alert(this.translateService.instant('common.addSuccess'));
          this.getProclamations();
        },
        () => alert(this.translateService.instant('common.addFailed'))
      );
    }
  }

  deleteProclamation(id: number) {
    if (confirm(this.translateService.instant('common.wannaDelete'))) {
      this.proclamationService.deleteProclamation(id).subscribe(
        () => {
          alert(this.translateService.instant('common.deleteSuccess'));
          this.getProclamations();
        },
        err => {
          alert(`${this.translateService.instant('common.deleteFailed')}: ${err.error.message}`);
        });
    }
  }

  onEdit(e: Proclamation) {
    e.edit = !e.edit;
    if (!e.edit) {
      if (e.title) {
        this.proclamationService.updateProclamation(e).subscribe(
          () => alert(this.translateService.instant('common.updateSuccess')),
          err => {
            alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`);
          }
        );
      }
    }
  }

  toggleStatus(e: Proclamation) {
    e.status = Number(!e.status);
    this.proclamationService.updateProclamation(e).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      err => {
        e.status = Number(!e.status);
        alert(`${this.translateService.instant('common.updateFailed')}: ${err.error.message}`);
      }
    );
  }

  get Status() { return Status }
}
