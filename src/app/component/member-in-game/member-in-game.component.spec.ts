import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberInGameComponent } from './member-in-game.component';

describe('MemberInGameComponent', () => {
  let component: MemberInGameComponent;
  let fixture: ComponentFixture<MemberInGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberInGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberInGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
