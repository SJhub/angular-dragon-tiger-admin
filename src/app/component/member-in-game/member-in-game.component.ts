import { formatDate } from '@angular/common';
import { Component, Input, OnInit, TemplateRef, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { addIndexToArray, defaultAgentID } from '../../common/global';
import { Status } from '../../enum/status.enum';
import { BetRecord } from '../../models/bet-record';
import { GameActionType } from '../../models/game-action-type';
import { GameType } from '../../models/game-type';
import { ItemTypes } from '../../models/item-types';
import { Member } from '../../models/member.model';
import { PlayGame } from '../../models/play-game';
import { BetRecordService } from '../../services/bet-record.service';
import { MemberService } from '../../services/member.service';
import { PlayGameService } from '../../services/play-game.service';

const DEFAULT_AGENT_ID = defaultAgentID;
@Component({
  selector: 'app-member-in-game',
  templateUrl: './member-in-game.component.html',
  styleUrls: ['./member-in-game.component.css']
})
export class MemberInGameComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns: string[] = ['game_number', 'game_types_id', 'item_types_id', 'action', 'amount', 'members_id'
    , 'members_account', 'created_at'];
  dataSource = new MatTableDataSource<BetRecord>();

  memberDisplayedColumns: string[] = ['index', 'account', 'is_allow_play'];
  memberPlayDataSource = new MatTableDataSource<Member>();

  memberBetDisplayedColumns: string[] = ['game_types_id', 'game_number', 'item_types_id', 'action', 'amount', 'created_at'];
  memberBetDataSource = new MatTableDataSource<BetRecord>();

  @Output() memberEvent = new EventEmitter<Member>();
  @Input() GameType: GameType[];
  @Input() ItemTypes: ItemTypes[];
  @Input() Member: Member[]; // 取得即時押注資料
  @Input() set gameTypesID(gameTypesID: any) {
    if (gameTypesID) {
      this._gameTypesID = gameTypesID;
      clearInterval(this.intervalBet);
      this.intervalBet = setInterval(() => {
        this.getPlayGameLastRowByGameTypesId(Number(this._gameTypesID));
        this.getMembersListByMemberIsPlayGame();
      }, 2000);
    }
  }

  @Input() data: any;

  modalRef: BsModalRef;

  lastGameData: PlayGame;
  lastGameTypesId: number;
  lastGameNumber: number;
  lastGameCreatedAt: string;
  intervalBet: any;
  intervalMemberBet: any;
  memberID: number;
  nickName: string;
  memberInfo: Member;
  memberName: string;
  memberAllowPlay: number;
  _gameTypesID: number;

  constructor(
    private translateService: TranslateService,
    private modalService: BsModalService,
    private memberService: MemberService,
    private playGameService: PlayGameService,
    private betRecordService: BetRecordService,
  ) {
  }

  ngOnDestroy() {
    clearInterval(this.intervalBet);
    clearInterval(this.intervalMemberBet);
  }

  ngOnInit(): void {

  }

  /**
   * 取得遊戲最後一筆資料
   */
  getPlayGameLastRowByGameTypesId(gameTypesId) {
    this.playGameService.getPlayGameLastRowByGameTypesId(gameTypesId).subscribe(
      res => {
        const data = res.data as PlayGame;
        this.lastGameData = data;
        this.lastGameTypesId = data.game_types_id;
        this.lastGameNumber = data.game_number;

        if (!this.lastGameCreatedAt) {
          this.lastGameCreatedAt = formatDate(data.created_at, 'yyyy-MM-dd HH:mm:ss', 'en-US');
        }

        // console.log(this.lastGameNumber);
        this.getBetRecordByItemTypeAndGameNumber(this._gameTypesID, this.lastGameNumber, this.lastGameCreatedAt);
      },
      err => {
        alert(this.translateService.instant('gameIsNotToMarket'));
        clearInterval(this.intervalBet);
      }
    );
  }

  /**
   * 取得及時押注紀錄資料
   */
  getBetRecordByItemTypeAndGameNumber(gameTypesId, lastGameNumber, lastGameCreatedAt) {
    this.betRecordService.getBetRecordByItemTypeAndGameNumber(gameTypesId, lastGameNumber, lastGameCreatedAt)
      .subscribe(
        res => {
          const data = res.data;
          this.data = data;

          let formatCreatedAt: any;
          const createdAt = data.map(e => e.created_at).pop();

          if (!data.length) {
            return;
          }

          if (!createdAt.length) {
            return;
          }

          formatCreatedAt = formatDate(createdAt, 'yyyy-MM-dd HH:mm:ss', 'en-US');

          if (!formatCreatedAt) {
            return;
          }

          this.dataSource.data = this.data.reverse();

          // console.log(this.dataSource.data);
        },
      );
  }

  /**
   * 取得遊戲中文名稱
   */
  getGameTypeName(game_types_id: number): string {
    const gameName = this.GameType.find(e => e.id === game_types_id);
    if (gameName) { return gameName.cht_name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得遊戲項目中文名稱
   */
  getItemTypeName(item_types_id: number): string {
    const itemTypeName = this.ItemTypes.find(e => e.id === item_types_id);
    if (itemTypeName) { return itemTypeName.cht_name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 轉換 - 取得押注動作名稱
   */
  getGameActionTypeByID(id: number) {
    return GameActionType.getNameByID(id);
  }

  /**
   * 取得會員姓名
   */
  getMemberName(members_id: number): string {
    const memberName = this.Member.find(e => e.id === members_id);
    if (memberName) { return memberName.name; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得會員帳號
   */
  getMemberAccount(members_id: number): string {
    const memberAccount = this.Member.find(e => e.id === members_id);
    if (memberAccount) { return memberAccount.account; }
    return this.translateService.instant('common.noData');
  }

  /**
   * 取得在遊戲中的會員
   */
  getMembersListByMemberIsPlayGame() {
    this.memberService.getMemberList().subscribe(
      res => {
        const data: Member[] = res.data;
        this.Member = data;
        this.memberPlayDataSource.data = addIndexToArray(data.filter(e => e.is_play_game === 1 && e.agent_id !== DEFAULT_AGENT_ID));
        this.memberPlayDataSource.paginator = this.paginator;
        this.memberPlayDataSource.sort = this.sort;
        // console.log(this.memberPlayDataSource.data);
      }
    );
  }

  /**
   * 按鈕切換 - 修改遊戲狀態
   */
  allowGameToggle(id, is_allow_play) {

    const data = {
      id: id,
      is_allow_play: is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        this.getMembersListByMemberIsPlayGame();
        alert(this.translateService.instant('common.updateSuccess'));
      },
      err => (this.translateService.instant('common.updateFailed'))
    );
  }

  /**
   * 會員即時押注列表踢人按鈕
   */
  kickMember() {
    const data = {
      id: this.memberID,
      is_allow_play: this.memberAllowPlay ? 0 : 1,
    };

    if (this.memberAllowPlay === 1) {
      this.memberService.updateMemberIsAllowPlay(data).subscribe(
        () => {
          this.getMembersListByMemberIsPlayGame();
          alert(this.translateService.instant('game.kickUserSuccess'));
        },
        err => (this.translateService.instant('common.operationFailed'))
      );
    } else {
      alert(this.translateService.instant('game.userAlreadyKicked'));
    }
  }

  /**
   * bootstrap modal - 查看會員押注紀錄
   */
  openModal(template: TemplateRef<any>, memberInfo: Member) {
    setTimeout(() => {
      this.memberInfo = memberInfo;
    }, 2000);
    this.memberBetDataSource.data = [];
    this.memberID = memberInfo.id;
    this.nickName = memberInfo.nickname;
    this.memberName = memberInfo.name;
    this.memberAllowPlay = memberInfo.is_allow_play;

    this.intervalMemberBet = setInterval(() => {
      const memberTimelyBet = this.dataSource.data.filter(e => e.members_id === this.memberID);
      this.memberBetDataSource.data = memberTimelyBet;
      // console.log(this.memberBetDataSource.data);
    }, 2000);

    this.modalRef = this.modalService.show(template, { class: 'modal-xl' });
    console.log(memberInfo);
  }

  /**
   * title提示遊戲狀態
   */
  getAllowGameStatus(AllowGameStatus: number) {
    switch (AllowGameStatus) {
      case 0:
        return this.translateService.instant('game.gameIsDeactivated');
      case 1:
        return this.translateService.instant('game.gameIsActivated');
    }
  }

  /**
   * [ngClass] 切換顏色
   */
  changeBtnColor(is_allow_play) {
    switch (is_allow_play) {
      case 0:
        return 'btn-success';
      case 1:
        return 'btn-danger';
    }
  }

  /**
   * 發送選擇的會員
   */
  emitMember(member: Member) {
    this.memberEvent.emit(member);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.memberPlayDataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * 取得狀態名稱
   */
  get Status() { return Status; }

}
