import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { markFormGroupAsTouched, isFormCtrlInvalid } from '../../common/form';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { CommissionSetting } from '../../models/commission-setting';

const GAME_IDS = [GameTypeEnum.GameTypeDouDizhu, GameTypeEnum.GameTypeCow, GameTypeEnum.GameTypePushTongZi];
const FORM_CONTROL_NAME = 'commission';
const MAX_VALUE = 97;

@Component({
  selector: 'app-commission-setting',
  templateUrl: './commission-setting.component.html',
  styleUrls: ['./commission-setting.component.css']
})
export class CommissionSettingComponent implements OnInit {
  @Input() id: string;
  @Input() readonly = false;
  range = [];
  form: FormGroup;

  parentValue = MAX_VALUE;

  currentValue: number;

  constructor(
    private translateServ: TranslateService,
    private fb: FormBuilder
  ) {
    this.groupForm();
    this.makeRange();
  }

  ngOnInit(): void {
  }

  getValue(percent_cash = 0) {
    let value = Number(this.form.get(FORM_CONTROL_NAME).value);
    let arr: CommissionSetting[] = [];
    GAME_IDS.forEach(e => {
      let data: CommissionSetting = {
        game_types_id: e,
        commission_percent: value,
        stored_value_card_percent: 0,
        commission_percent_cash: percent_cash
      }
      arr.push(data);
    });
    return arr;
  }

  setValue(str: string) {
    try {
      let arr: CommissionSetting[] = JSON.parse(str);
      let val = arr[0].commission_percent
      this.form.get(FORM_CONTROL_NAME).setValue(val);
      this.currentValue = arr[0].commission_percent_cash;
    } catch (error) {
      console.log('no commission settings');
    }
  }

  setParentValue(str: string, setValue = false) {
    try {
      let arr: CommissionSetting[] = JSON.parse(str);
      this.parentValue = arr[0].commission_percent;
      setValue && this.form.get('commission').setValue(this.parentValue);
    } catch (error) {
      console.log('no commission settings');
    }
  }

  isFormValid() {
    let value = Number(this.form.get(FORM_CONTROL_NAME).value);
    if (value > this.parentValue) {
      alert(this.translateServ.instant('manageAgent.playAgainstCommission') + '不得高於上層代理');
      return false;
    }
    markFormGroupAsTouched(this.form);
    return this.form.valid;
  }

  groupForm() {
    this.form = this.fb.group({
      commission: [MAX_VALUE, [Validators.required, Validators.min(0), Validators.max(MAX_VALUE)]]
    });
  }

  makeRange() {
    let max = 100;
    let step = 5
    for (let index = 1; index <= (max / step); index++) {
      let val = index * step;
      if (val > MAX_VALUE) val = MAX_VALUE;
      this.range.push(val);
    }
  }

  isFormCtrlInvalid(ctrl: AbstractControl) { return isFormCtrlInvalid(ctrl) }

  get MAX_VALUE() { return MAX_VALUE }
}
