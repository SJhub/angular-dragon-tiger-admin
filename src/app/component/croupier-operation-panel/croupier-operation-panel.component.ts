import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, timer } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import { getDrawResultToBetItem } from '../../common/global';
import { commonEnum } from '../../enum/common.enum';
import { EventEnum } from '../../enum/event.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { Status } from '../../enum/status.enum';
import { PokerResult } from '../../models/baccarat';
import { GameActionType } from '../../models/game-action-type';
import { GameType } from '../../models/game-type';
import { GRPCEvent } from '../../models/grpc-event';
import { ItemTypes } from '../../models/item-types';
import { Member } from '../../models/member.model';
import { PlayGame } from '../../models/play-game';
import { SystemSetting } from '../../models/system-setting';
import { User } from '../../models/user';
import { GameTypeRenewComponent } from '../../pages/timely-bet/game-type-renew/game-type-renew.component';
import { AuthenticationService } from '../../services/auth.service';
import { CalcItemResultRecordService } from '../../services/calc-item-result-record.service';
import { EnumTranslateService } from '../../services/enum-translate.service';
import { GameEventService } from '../../services/game-event.service';
import { GameTypeService } from '../../services/game-type.service';
import { ItemTypesService } from '../../services/item-types.service';
import { LanguageService } from '../../services/language.service';
import { LivesRestartService } from '../../services/lives-restart.service';
import { MemberService } from '../../services/member.service';
import { PlayGameService } from '../../services/play-game.service';
import { SystemService } from '../../services/system.service';
import { UserService } from '../../services/user.service';
import { AiWebcamComponent } from '../ai-webcam/ai-webcam.component';
import { BaccaratCroupierComponent } from '../baccarat-croupier/baccarat-croupier.component';
import { DragonTigerCroupierComponent } from '../dragon-tiger-croupier/dragon-tiger-croupier.component';

const DEFAULT_REDIRECT_URL = 'https://www.google.com/';
const synth = window.speechSynthesis;
@Component({
  selector: 'app-croupier-operation-panel',
  templateUrl: './croupier-operation-panel.component.html',
  styleUrls: ['./croupier-operation-panel.component.css']
})
export class CroupierOperationPanelComponent implements OnInit, OnDestroy {
  @ViewChild('baccaratCroupier') baccaratCroupier: BaccaratCroupierComponent;
  @ViewChild('dragonTigerCroupier') dragonTigerCroupier: DragonTigerCroupierComponent;
  @ViewChild('webcam') webcam: AiWebcamComponent;
  @ViewChild('renew') renew: GameTypeRenewComponent;

  myForm: FormGroup;
  GameType: GameType[];
  ItemTypes: ItemTypes[];
  Member: Member[];
  gameModeType: number;
  gameTypesID: any;
  selectedGame: GameType;
  systemSetting: SystemSetting;
  countDownTimer: Observable<number>;
  currentCountTime: number;
  cancelCountDown = false;
  isGameOpening = false;
  selectedMember: Member;

  currentLanguage: string;
  systemSettings: SystemSetting[];
  gameTypes: GameType[];
  _gameTypesID: any;
  gameMode: any;
  memberCnt: any;
  playMemberPlus: any;
  timeToMarket: any;
  playGameFirstData: PlayGame[];
  selectedGameNumber: number;
  selectGameResult: any;
  game_Cht_Name: string;
  game_En_Name: string;
  selectGameId: number;
  gameIntruction: string;
  gameTypeStatus: number = Status.Active;
  gameTypeChtName: string;
  gameTypeEnName: string;
  gameTypeRestStatus: number;
  gameTypeBreakStatus: number;
  gameTypeRestMin: number;
  currentGameNumber: number;

  // 直播機剩餘秒數
  liveRemainingTime1: Observable<string>;
  liveRemainingTime2: Observable<string>;

  // 大小單雙
  itemTypeStr: string;

  // AI辨識
  useAI = false;
  hasDetected = false;
  detecting = false;
  detectFailed = false;

  // 開局狀態
  isNewGame = false;

  // 維護時間
  maintenanceInterval: any;
  isMaintenanceTime = false;

  constructor(
    private liveRestartServ: LivesRestartService,
    private calcItemResultRecordServ: CalcItemResultRecordService,
    private enumTranslateServ: EnumTranslateService,
    private languageService: LanguageService,
    private authService: AuthenticationService,
    private userService: UserService,
    private translateService: TranslateService,
    private gameTypeService: GameTypeService,
    private playGameService: PlayGameService,
    private systemService: SystemService,
    private router: Router,
    private fb: FormBuilder,
    private gameEventService: GameEventService,
    private memberService: MemberService,
    private itemTypesService: ItemTypesService,
  ) {
    this.groupForm();
    this.maintenanceInterval = setInterval(() => this.checkIsMaintenanceTime(), 1000);
  }

  ngOnDestroy(): void {
    clearInterval(this.maintenanceInterval);
  }


  ngOnInit(): void {
    // this.gameTypeOnChange(1);

    this.gemeType();
    this.getCurrentLanguage();
    this.getItemTypeList();
    this.getLiveRemainingTime();
    this.subscribeDrawResult();
  }

  getLiveRemainingTime() {
    this.liveRemainingTime1 = this.liveRestartServ.getRemainingTimeByGameLiveNumber(1);
    this.liveRemainingTime2 = this.liveRestartServ.getRemainingTimeByGameLiveNumber(2);
  }

  getUser() {
    const userID = Number(this.authService.getUserId());
    if (userID) {
      this.userService.getUser(userID).subscribe(res => {
        const user: User = res.data;
        let operation = [];
        try {
          operation = JSON.parse(user.operation_items);
        } catch (error) {
          console.log(error);
        }

        if (operation.length) {
          const gameID = operation.pop();
          this.setGame(Number(gameID));
        }
      })
    }
  }

  setGame(manageID: number) {
    let game: GameType = null;

    game = this.GameType.find(type => type.id === manageID);
    if (game) {
      this.gameTypesID = game.id;
      this.selectedGame = game;
      this.getSystemSetting(this.gameTypesID);
      this.getGameType(this.gameTypesID);
    }
  }

  getSystemSetting(id: number) {
    this.systemService.getSystemSetting(id).subscribe(
      res => {
        const data = res.data;
        this.systemSetting = data;

        this.myForm.patchValue({
          wash_code_amount: data.wash_code_amount,
          draw_seconds: data.draw_seconds,
        });

        // console.log(data);
      }
    );
  }

  groupForm() {
    this.myForm = this.fb.group({
      'draw_result': [null, Validators.pattern(/^[0-6]{1,3}$/)],
      'draw_seconds': [null],
    });
  }

  /**
   * gameTypeOnChange - when select opetion change
   * @param gameTypesID
   */
  gemeType() {
    this.gameTypeService.getGameTypes().subscribe(
      res => {
        this.GameType = res.data;
        this.getUser();
        // console.log(this.GameType);
      }
    );
  }

  checkGameIsMarket(): boolean {
    if (this.gameTypesID > GameTypeEnum.GameTypeDragonTiger) {
      return false;
    }

    return true;
  }

  newGame(val: any) {
    this.cancelCountDown = true;

    if (this.gameModeType === 1) {
      alert(this.translateService.instant('currentModeIsAuto'));
      return;
    }

    if (!this.checkGameIsMarket()) {
      alert(this.translateService.instant('gameIsNotToMarket'));
      return;
    }

    if (this.myForm.valid) {
      this.isGameOpening = true;
      this.countDownTimer = null;

      const playGame = {
        game_types_id: Number(this.gameTypesID),
        countdown: Number(val.draw_seconds)
      } as PlayGame;

      this.playGameService.addPlayGame(playGame)
        .subscribe(
          r => {
            this.isNewGame = true;
            this.currentGameNumber = r.data;
            this.isGameOpening = false;
            this.hasDetected = false;
            this.startCountDown(2300);
            this.baccaratCroupier.resetForm();
            this.dragonTigerCroupier.resetForm();
          },
          err => {
            this.isGameOpening = false;
            this.hasDetected = true;
            alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`);
          }
        );
    }
  }

  /**
   * 顯示開獎倒數秒數
   */
  startCountDown(delay: number) {
    const draw_seconds = this.myForm.get('draw_seconds').value;
    this.currentCountTime = draw_seconds;
    this.countDownTimer = timer(delay, 1000).pipe(take(draw_seconds), map(s => draw_seconds - s - 1), tap(s => this.currentCountTime = s));
  }

  /**
   * invalidDrawResult - 此局無效
   * @param val
   */
  invalidDrawResult() {
    this.cancelCountDown = false;

    if (this.gameModeType === 1) {
      alert(this.translateService.instant('currentModeIsAuto'));
      return;
    }

    if (!this.checkGameIsMarket()) {
      alert(this.translateService.instant('gameIsNotToMarket'));
      return;
    }

    if (this.myForm.valid) {
      this.baccaratCroupier.resetForm();
      this.dragonTigerCroupier.resetForm();
      this.systemService.updateDrawResult(this.gameTypesID, commonEnum.INVALID_RESULT)
        .subscribe(
          () => alert(this.translateService.instant('common.submitSuccess')),
          err => alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`)
        );
      this.isNewGame = false;
    }
  }

  /**
  * updateDrawResult - 更新開獎結果
  * @param val
  */
  updateDrawResult(val: any) {
    let drawResult: any;
    if (this.gameModeType === 1) {
      return alert(this.translateService.instant('currentModeIsAuto'));
    }

    if (!this.checkGameIsMarket()) {
      return alert(this.translateService.instant('gameIsNotToMarket'));
    }

    switch (this.gameTypesID) {
      case GameTypeEnum.GameTypeDragonTiger:
        if (!this.dragonTigerCroupier.isFormValid) return;
        drawResult = this.dragonTigerCroupier.getFormValue();
        break;
      case GameTypeEnum.GameTypeBaccarat:
        if (!this.baccaratCroupier.isFormValid) return;
        drawResult = JSON.stringify(this.baccaratCroupier.getFormValue());
      default:
        break;
    }

    this.cancelCountDown = false;
    this.systemService.updateDrawResult(this.gameTypesID, drawResult)
      .subscribe(() => {
        this.isNewGame = false;
        alert(this.translateService.instant('common.submitSuccess'));
        if (this.gameTypesID === GameTypeEnum.GameTypeBaccarat) {
          this.speechSynthesis(this.BaccaratItemTypeStr);
        }
        this.myForm.get('draw_result').setValue('');
      },
        err => alert(`${this.translateService.instant('common.operationFailed')}: ${err.error.message}`));
  }

  gameTypeOnChange(gameType) {
    this.gameTypesID = gameType;
    const findObj = this.findSystemSettingByGameTypesID(this.gameTypesID);
    this.getGameType(this.gameTypesID);

    if (findObj) {
      this.systemSetting = findObj;
      console.log(this.systemSetting);
    }
  }

  checkNumber(e: any, formValue: string) {
    const key = e.key
    const regxSingle = /[0-6]/;
    const regxResult = /^[1-6]{1,3}$|^0{1,3}$/;


    if (!regxSingle.test(key)) {
      e.preventDefault();
    }

    if (formValue && !regxResult.test(formValue + key)) {
      e.preventDefault();
    }
  }

  logout() {
    // if (confirm(this.translateService.instant('system.logoutConfirm'))) {
    this.authService.logout();
    this.router.navigate(['/login']);
    // }
  }

  useLanguage(language: string) {
    this.languageService.setLang(language);
  }

  getCurrentLanguage() {
    this.languageService.language$.subscribe(lang => this.currentLanguage = lang.lang);
  }

  getTranslatedGameName(name: string) {
    return this.enumTranslateServ.getTranslatedGameName(name);
  }

  subscribeDrawResult() {
    this.myForm.get('draw_result').valueChanges.subscribe(val => {
      let str = '';
      if (val) {
        const ids = getDrawResultToBetItem(val);
        const total = this.getDrawResultTotal(val);
        str = total + ',' + this.getItemTypeNameByIDs(ids);
      }
      this.itemTypeStr = str;
    });
  }

  getDrawResultTotal(str: string) {
    return str.split('').map(e => Number(e)).reduce((a, b) => a + b);
  }

  getItemTypeNameByIDs(ids: Array<number>) {
    const str = ids.map(item => {
      const itemTypesName = this.ItemTypes.find(e => e.id === item);
      if (itemTypesName) {
        return this.enumTranslateServ.getTranslatedBetItem(itemTypesName.en_name);
      }
      return this.translateService.instant('common.noData');
    });
    return str.join(',');
  }

  // 踢出全部會員
  kickAllMembers() {
    const data = {
      game_types_id: 1,
      type: EventEnum.EventkickAllMember,
      value: 'EventEnum.EventkickAllMember'
    } as any;
    this.gameEventService.addGRPCEvent(data).subscribe(
      () => {
        alert('踢出成功');
      },
      err => {
        alert('更新失敗');
      }
    );
  }

  // 允許全部會員遊玩
  AllowMembersPlayGame() {
    const data = {
      game_types_id: 1,
      type: EventEnum.EventAllowMembersPlay,
      value: 'EventEnum.EventAllowMembersPlay'
    } as any;
    this.gameEventService.addGRPCEvent(data).subscribe(
      () => {
        alert('恢復遊玩');
      },
      err => {
        alert('更新失敗');
      }
    );
  }

  // 禁止會員遊玩按鈕
  allowGameToggle(id, is_allow_play) {

    const data = {
      id: id,
      is_allow_play: is_allow_play ? 0 : 1,
    };

    this.memberService.updateMemberIsAllowPlay(data).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
      },
      err => alert(this.translateService.instant('common.updateFailed'))
    );
  }



  // 押注狀況

  getSystemSettings() {
    this.systemService.getSystemSettings()
      .subscribe(
        res => {
          this.systemSettings = res.data;
          this.gameTypeOnChange(1);
        },
        err => console.log(err)
      );
  }

  /**
   * 取得遊戲中文名稱
   */
  getGameTypeName(game_types_id: number): string {
    const gameName = this.GameType.find(e => e.id === game_types_id);
    if (gameName) { return gameName.cht_name; }
    return this.translateService.instant('common.noData');;
  }

  /**
   * 取得遊戲項目列表
   */
  getItemTypeList() {
    this.itemTypesService.getItemTypesList().subscribe(
      res => {
        const data = res.data;
        this.ItemTypes = data;
        // console.log(data);
      }
    );
  }

  /**
   * 取得遊戲項目中文名稱
   */
  getItemTypeName(item_types_id: number): string {
    const itemTypeName = this.ItemTypes.find(e => e.id === item_types_id);
    if (itemTypeName) { return itemTypeName.cht_name; }
    return this.translateService.instant('common.noData');;
  }

  /**
   * 取得會員列表
   */
  getMemberList() {
    this.memberService.getMemberList().subscribe(
      res => {
        this.Member = res.data;
        // console.log(this.Member);
      }
    );
  }

  /**
   * 取得會員姓名
   */
  getMemberName(members_id: number): string {
    const memberName = this.Member.find(e => e.id === members_id);
    if (memberName) { return memberName.name; }
    return this.translateService.instant('common.noData');;
  }

  /**
   * 取得會員帳號
   */
  getMemberAccount(members_id: number): string {
    const memberAccount = this.Member.find(e => e.id === members_id);
    if (memberAccount) { return memberAccount.account; }
    return this.translateService.instant('common.noData');;
  }

  getGameActionTypeByID(id: number) {
    return GameActionType.getNameByID(id);
  }

  /**
  * 依照遊戲局號來篩選當局資料
  */
  selectGameNumber(gameNumber) {
    this.selectedGameNumber = gameNumber;
    const gameResult = this.playGameFirstData.filter(e => e.game_number === Number(gameNumber));
    this.selectGameResult = gameResult[0].draw_result;

    // console.log(this.selectedGameNumber);
  }

  /**
   * find system setting by game types id
   * @param id
   */
  findSystemSettingByGameTypesID(id: number) {
    if (id <= 0) {
      return;
    }

    if (this.systemSettings.length <= 0) {
      return;
    }
    const findObj = this.systemSettings.find(
      it => it.game_types_id === Number(id)
    );

    if (findObj === undefined) {
      return;
    }
    return findObj;
  }

  /**
     * 取得該遊戲人數
     */
  getMemberCnt() {

    if (!this.checkGameIsMarket()) {
      alert('該遊戲尚未開放!');
      return;
    }

    this.gameTypeService.getGameType(this.gameTypesID)
      .subscribe(
        res => this.memberCnt = res.data['member_count'],
        err => alert('取得失敗!')
      );
  }

  /**
   * 取得gameType
   * @param mode
   */
  getGameType(gameTypeId) {
    this.gameTypeService.getGameType(gameTypeId).subscribe(
      res => {
        const data: GameType = res.data;
        this.gameMode = data.mode;
        this.gameTypeStatus = data.status;
        this.gameTypeChtName = data.cht_name;
        this.gameTypeEnName = data.en_name;
        this.gameTypeRestStatus = data.rest_status;
        this.gameTypeBreakStatus = data.break_status;
        this.gameTypeRestMin = data.rest_min;
        if (!this.gameTypeStatus) {
          alert('緊急狀態已啟動無法操作');
          this.authService.logoutUser(false);
        }

      }
    );
  }

  selectGameType(gameTypeValue) {
    this.selectGameId = gameTypeValue;
    this.gameTypeService.getGameType(gameTypeValue).subscribe(
      res => {
        const data = res.data;
        this.game_Cht_Name = data.cht_name;
        this.game_En_Name = data.en_name;
        this.gameIntruction = data.instruction;
        this.playMemberPlus = data.play_member_plus;
        this.timeToMarket = data.time_to_market;

        this.gameModeType = data.mode;
      }
    );
  }

  updateGameTypeStatus(event) {
    const status = Number(event);

    this.gameTypeService.updateGameTypeStatus(this.gameTypesID, status).subscribe(
      () => {
        if (status === Status.Active) {
          alert(this.translateService.instant('common.updateSuccess'));
          this.gameTypeStatus = Status.Active;
        }
      },
      () => {
        if (status === Status.Active) {
          alert(this.translateService.instant('common.updateFailed'));
        }
      }
    );

    if (status === Status.InActive) {
      this.addEventStopLivePush();
      if (this.isNewGame) this.invalidDrawResult();
    }
  }

  updateGametypeTime(val) {
    val.id = this.gameTypesID;
    val.time_to_market = this.timeToMarket;
    val.cht_name = this.gameTypeChtName;
    val.en_name = this.gameTypeEnName;

    this.gameTypeService.updateGameType(val).subscribe(
      () => {
        alert(this.translateService.instant('common.updateSuccess'));
      },
      () => {
        alert(this.translateService.instant('common.updateFailed'));
      }
    );
  }

  updateRestGameStatus(status: boolean) {
    const now = new Date();
    const restMin = this.gameTypeRestMin;
    now.setMinutes(now.getMinutes() + Number(restMin));
    const timeToRest = status ? formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en-US') : null;
    this.gameTypeService.updateRestGameStatus(this.gameTypesID, Number(status), timeToRest).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      err => alert(this.translateService.instant('common.updateFailed'))
    );

  }

  updateBreakGameStatus(status: boolean) {
    const now = new Date();
    const restMin = this.gameTypeRestMin;
    now.setMinutes(now.getMinutes() + Number(restMin));
    const timeToRest = status ? formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'en-US') : null;
    this.gameTypeService.updateBreakGameStatus(this.gameTypesID, Number(status), timeToRest).subscribe(
      () => alert(this.translateService.instant('common.updateSuccess')),
      err => alert(this.translateService.instant('common.updateFailed'))
    );

  }

  /**
   * 選擇會員事件
   */
  onMemberSelected(member: Member) {
    this.selectedMember = member;
  }


  /**
   * 發送直播停止推流事件
   */
  addEventStopLivePush() {
    const event: GRPCEvent = {
      game_types_id: GameTypeEnum.GameTypeDragonTiger,
      type: EventEnum.EventStopLivePush,
      value: String(GameTypeEnum.GameTypeDragonTiger)
    };
    this.gameEventService.addGRPCEvent(event).subscribe(
      () => this.systemRedirect(),
      () => this.systemRedirect()
    );
  }

  systemRedirect() {
    this.authService.logout();
    let redirectUrl = DEFAULT_REDIRECT_URL;

    if (this.systemSetting && this.systemSetting.redirect_url) {
      redirectUrl = this.systemSetting.redirect_url;
    }

    window.location.href = redirectUrl;
  }

  /**
   * 重置開獎次數
   */
  resetGameTimes() {
    if (confirm('確定重置開獎次數?')) {
      this.calcItemResultRecordServ.clearCalcItemResultRecordsStatus().subscribe(
        () => alert(this.translateService.instant('common.updateSuccess')),
        () => alert(this.translateService.instant('common.updateFailed'))
      );
    }
  }

  /**
   * 重啟直播機
   */
  restartLive(num: number) {
    if (this.liveRestartServ.checkRestartTimeIsValid(num)) {
      const userID = Number(this.authService.getUserId());
      // 本地測試
      // this.liveRestartServ.logRestartTime(num);
      // this.getLiveRemainingTime();
      // return;
      return this.liveRestartServ.restartByGameLiveNumber(num, userID).subscribe(
        () => {
          this.getLiveRemainingTime();
          alert(this.translateService.instant('common.updateSuccess'));
        },
        () => {
          alert(this.translateService.instant('common.updateFailed'));
        }
      );
    };
    return alert(this.translateService.instant('restartingStream'));
  }

  // AI辨識
  takeSnapShot() {
    if (this.currentGameNumber) {
      this.baccaratCroupier.resetForm();
      this.webcam.triggerSnapshot(String(this.currentGameNumber));
      this.detecting = true;
    }
  }

  onPicCatch(result: PokerResult & string) {
    this.detecting = false;
    this.hasDetected = true;
    if (result == "fail") {
      return alert('辨識失敗服務器回傳錯誤');
    }
    this.baccaratCroupier.setFormValue(JSON.stringify(result), true);
    this.detectFailed = this.isDetectFailed(result);
    !this.detectFailed && this.speechSynthesis(this.BaccaratItemTypeStr);
  }

  speechSynthesis(text: string) {
    const utterThis = new SpeechSynthesisUtterance(text)
    utterThis.rate = 1;
    utterThis.pitch = 1;
    const voices = synth.getVoices();
    let voiceIndex = voices.findIndex(e => e.name === 'Google 國語（臺灣）');
    if (voiceIndex !== -1) {
      utterThis.voice = voices[voiceIndex];
    } else {
      utterThis.lang = 'zh-TW';
    }
    synth.speak(utterThis);
  }

  isDetectFailed(result: PokerResult) {
    const less = result.banker.length < 2 || result.player.length < 2;
    const over = result.banker.length > 3 || result.player.length > 3;
    if (less || over) {
      this.speechSynthesis('辨識失敗');
      return true;
    }
    return false;
  }

  toggleWebcam() {
    this.webcam.toggleWebcam();
    this.hasDetected = false;
    this.detectFailed = false;
  }

  onTabChanged(event: any) {
    if (event.index == 1 && this.renew.loaded) this.renew.refresh();
  }

  checkIsMaintenanceTime() {
    const now = new Date();
    const t1 = new Date();
    const t2 = new Date();
    t1.setHours(23, 50, 0);
    t2.setHours(0, 10, 0);
    this.isMaintenanceTime = (now.getTime() >= t1.getTime() || now.getTime() <= t2.getTime());
  }

  get BaccaratItemTypeStr() { return this.baccaratCroupier?.getItemTypeStr() }

  get DragonTigerItemTypeStr() { return this.dragonTigerCroupier?.getItemTypeStr() }

  get GameTypeEnum() { return GameTypeEnum }
}
