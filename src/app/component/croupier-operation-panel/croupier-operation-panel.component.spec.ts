import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CroupierOperationPanelComponent } from './croupier-operation-panel.component';

describe('CroupierOperationPanelComponent', () => {
  let component: CroupierOperationPanelComponent;
  let fixture: ComponentFixture<CroupierOperationPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CroupierOperationPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CroupierOperationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
