import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaccaratCroupierComponent } from './baccarat-croupier.component';

describe('BaccaratCroupierComponent', () => {
  let component: BaccaratCroupierComponent;
  let fixture: ComponentFixture<BaccaratCroupierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaccaratCroupierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaccaratCroupierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
