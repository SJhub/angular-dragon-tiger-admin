import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByDateRangeMemberComponent } from './report-by-date-range-member.component';

describe('ReportByDateRangeMemberComponent', () => {
  let component: ReportByDateRangeMemberComponent;
  let fixture: ComponentFixture<ReportByDateRangeMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportByDateRangeMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByDateRangeMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
