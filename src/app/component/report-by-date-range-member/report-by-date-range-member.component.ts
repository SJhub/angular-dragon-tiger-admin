import { formatDate } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { getFormatedDateStr } from '../../common/common';
import { checkTodayIsBeforeDays, getTimeRangeByDate, RECORDS_KEEP_DAYS } from '../../common/date';
import { isDateRangeIncludedToday } from '../../common/report';
import { GameActionTypeEnum } from '../../enum/game-action-type.enum';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { MemberDailyReport } from '../../models/member-daily-report';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/auth.service';
import { BetRecordService } from '../../services/bet-record.service';
import { MemberService } from '../../services/member.service';
import { MemberBetRecordComponent } from '../member-bet-record/member-bet-record.component';

@Component({
  selector: 'app-report-by-date-range-member',
  templateUrl: './report-by-date-range-member.component.html',
  styleUrls: ['./report-by-date-range-member.component.css']
})
export class ReportByDateRangeMemberComponent implements OnInit {

  @ViewChild('memberBetRecord') memberBetRecord: MemberBetRecordComponent;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input() startDate: string;
  @Input() endDate: string;
  @Input() gameTypeID = GameTypeEnum.GameTypeDragonTiger;
  @Input() member: Member;
  @Input() records: MemberDailyReport[];


  dataSource = new MatTableDataSource<MemberDailyReport>();

  displayedColumns: string[] = [
    'closing_date',
    'total_amount',
    'number_of_game',
    'game_result_amount',
    'wash_code_commission',
    'settlement_amount',
    'detail'
  ];

  userID: number;

  betRecord: Object;
  closingDate: string;

  constructor(
    public bsModalRef: BsModalRef,
    private authServ: AuthenticationService,
    private memberServ: MemberService,
    private betRecordServ: BetRecordService
  ) {
    this.userID = Number(this.authServ.getUserId());
  }

  ngOnInit(): void {
    this.getMemberRecords();
  }

  getMemberRecords() {

    this.setDataSource(this.records.filter(e => e.members_id === this.member.id));
  }

  setDataSource(data: MemberDailyReport[]) {
    this.dataSource.data = data;
    this.dataSource.sort = this.sort;
  }

  getBody() {
    return {
      game_types_id: this.gameTypeID,
      agents_id: this.userID,
      members_id: this.member.id,
      start_date: this.startDate,
      end_date: this.endDate,
    }
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }

  openMemberBetRecord(member: Member) {
    const data: any = this.getBody();

    let closingDate = member.closing_date;
    let tmp = new Date(closingDate);
    tmp.setDate(tmp.getDate() + 1);
    let st = getFormatedDateStr(closingDate, 'yyyy-MM-dd');
    let et = getFormatedDateStr(tmp, 'yyyy-MM-dd');
    const range = getTimeRangeByDate(st);

    let start = new Date(st);
    // 星期日以星期日中午12點結束
    if (start.getDay() == 0) {
      et = st;
    }

    // 星期一以星期日中午12點開始
    if (start.getDay() == 1) {
      start.setDate(start.getDate() - 1);
      st = getFormatedDateStr(start, 'yyyy-MM-dd');
    }

    this.closingDate = closingDate;
    data.start_date = `${st} ${range.st}`;
    data.end_date = `${et} ${range.ed}`;
    data.action = GameActionTypeEnum.Donate;

    forkJoin([this.betRecordServ.getResultTotalAmountByDateAndMember(data), this.betRecordServ.getTotalAmountByDateAndMember(data)])
      .pipe(map(res => res.map(r => r.data)))
      .subscribe(r => {
        this.betRecord = { ...r[0], ...r[1] };
        console.log(this.betRecord);

      });
  }

  formatDate(str: string) {
    return formatDate(str, 'yyyy-MM-dd', 'en-US');
  }

  checkTodayIsBeforeDays(target: string) { return checkTodayIsBeforeDays(target, RECORDS_KEEP_DAYS) }
}
