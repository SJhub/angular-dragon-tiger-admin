import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedSearchingComponent } from './advanced-searching.component';

describe('AdvancedSearchingComponent', () => {
  let component: AdvancedSearchingComponent;
  let fixture: ComponentFixture<AdvancedSearchingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedSearchingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedSearchingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
