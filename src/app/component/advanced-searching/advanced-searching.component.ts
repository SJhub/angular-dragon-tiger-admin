import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabGroup } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { defaultAgentID } from '../../common/global';
import { UserRole } from '../../enum/user-role.enum';
import { Member } from '../../models/member.model';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';
import { AuthenticationService } from '../../services/auth.service';
import { MemberService } from '../../services/member.service';
import { UserService } from '../../services/user.service';

const DEFAULT_AGENT_ID = defaultAgentID;

@Component({
  selector: 'app-advanced-searching',
  templateUrl: './advanced-searching.component.html',
  styleUrls: ['./advanced-searching.component.css']
})
export class AdvancedSearchingComponent implements OnInit {
  @Input() set filterValue(val: string) {
    val = val.trim();
    if (val) {
      this.tabGroupAgent.realignInkBar();
      this.tabGroupMember.realignInkBar();
      this.dataSourceOfAgent.filter = val;
      this.dataSourceOfMember.filter = val;
    }
  }

  @Input() startDate: string;
  @Input() endDate: string;
  @Output() isTopAgent: EventEmitter<boolean> = new EventEmitter();

  @ViewChild('tabGroupAgent') tabGroupAgent: MatTabGroup;
  @ViewChild('tabGroupMember') tabGroupMember: MatTabGroup;
  @ViewChild('sortOfAgent', { static: true }) sortOfAgent: MatSort;
  @ViewChild('sortOfMember', { static: true }) sortOfMember: MatSort;
  @ViewChild('paginatorOfAgent', { static: true }) paginatorOfAgent: MatPaginator;
  @ViewChild('paginatorOfMember', { static: true }) paginatorOfMember: MatPaginator;
  dataSourceOfAgent = new MatTableDataSource<User>();
  dataSourceOfMember = new MatTableDataSource<Member>();
  displayedColumnsOfAgent: string[] = ['parent', 'account', 'view'];
  displayedColumnsOfMember: string[] = ['parent', 'account', 'view'];
  userID: number;
  userIdentity: number;
  constructor(
    private router: Router,
    private agentServ: AgentService,
    private userServ: UserService,
    private memberServ: MemberService,
    private authServ: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.userID = Number(this.authServ.getUserId());
    this.userIdentity = Number(this.authServ.getUserIdentity());
    this.dataSourceOfAgent.sort = this.sortOfAgent;
    this.dataSourceOfAgent.paginator = this.paginatorOfAgent;
    this.dataSourceOfMember.sort = this.sortOfMember;
    this.dataSourceOfMember.paginator = this.paginatorOfMember;

    this.dataSourceOfAgent.filterPredicate = (data, filter) => data.name === filter || data.real_name === filter;
    this.dataSourceOfMember.filterPredicate = (data, filter) => data.account === filter || data.nickname === filter;


    if (this.userIdentity === UserRole.admin) {
      this.getAgents();
      return this.getMembers();
    }
    this.getAgentChildsIdByAgentsId(this.userID);
  }

  getAgents() {
    this.userServ.getUsers().subscribe(r => {
      const users: User[] = r.data;
      const agents = users.filter(r => (r.identity === UserRole.agent || r.identity === UserRole.topAgent) && r.id !== DEFAULT_AGENT_ID && !r.clone_id);
      this.dataSourceOfAgent.data = agents;
    });
  }

  getMembers() {
    this.memberServ.getMemberList().subscribe(r => {
      const data: Member[] = r.data;
      const members = data.filter(r => r.agent_id !== DEFAULT_AGENT_ID);
      this.dataSourceOfMember.data = members;
    });
  }

  getAgentChildsIdByAgentsId(id: number) {
    this.agentServ.getAgentChildsIdByAgentsId(id).subscribe(r => {
      const childIDs: Array<number> = r.data ? r.data : [];
      const userReqs = childIDs.map(e => this.userServ.getUser(e));
      const memberReqs = childIDs.map(e => this.memberServ.getMemberByAgent(e));
      memberReqs.push(this.memberServ.getMemberByAgent(this.userID));

      forkJoin(userReqs).pipe(map(res => res.map(r => r.data))).subscribe(r => {
        this.dataSourceOfAgent.data = r;
      });

      forkJoin(memberReqs).pipe(map(res => res.map(r => r.data))).subscribe(r => {
        const members = [];
        r.forEach(e => members.push(...e));
        this.dataSourceOfMember.data = members;
      });
    });
  }

  getAgentPath(user: User, editPage = false) {
    let path = `top-agent-edit/${user.id}/${user.parent_id}`;

    if (!editPage) {
      if ((user.identity === UserRole.topAgent || user.parent_id === this.userID) && this.startDate) {
        return this.isTopAgent.emit(true);
      }
    }

    if (this.startDate && !editPage) {
      path = `system/report-record-member-list/${user.parent_id}`;
      path += `/${this.startDate}/${this.endDate}`;
    }

    this.router.navigate([path]);
  }

  getMemberPath(member: Member, editPage = false) {
    let path = `member-edit/${member.id}`;

    if (!editPage) {
      if (member.agent_id === this.userID && this.startDate) {
        return this.isTopAgent.emit(true);
      }
    }

    if (this.startDate && !editPage) {
      path = `system/report-record-member-list/${member.agent_id}`;
      path += `/${this.startDate}/${this.endDate}`;
    }

    this.router.navigate([path]);
  }

  getParent(element: User & Member) {
    let parent: User = null;
    if (element.parent_id) {
      parent = this.dataSourceOfAgent.data.find(e => e.id === element.parent_id);
    }
    if (element.agent_id) {
      parent = this.dataSourceOfAgent.data.find(e => e.id === element.agent_id);
    }

    return parent ? `${parent.name}/${parent.real_name}` : '';
  }
}
