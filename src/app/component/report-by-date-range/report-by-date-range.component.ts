import { formatDate } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RecordsCalcSettlementRecords } from '../../common/report';
import { GameTypeEnum } from '../../enum/game-type.enum';
import { SettlementRecord } from '../../models/settlement-record';
import { User } from '../../models/user';
import { AgentService } from '../../services/agent.service';

@Component({
  selector: 'app-report-by-date-range',
  templateUrl: './report-by-date-range.component.html',
  styleUrls: ['./report-by-date-range.component.css']
})
export class ReportByDateRangeComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @Input() user: User;
  @Input() startDate: string;
  @Input() endDate: string;
  @Input() settlementRecords: SettlementRecord[];
  @Input() realTimeRecords: {
    agent_daily_report: User[],
    member_daily_report: User[]
  };

  dataSource = new MatTableDataSource<User>();

  displayedColumns: string[] = [
    'closing_date',
    'total_amount',
    'active_total_amount',
    'number_of_game',
    'game_result_amount',
    'wash_code_commission_percent',
    'wash_code_commission',
    'revenue_share',
    'total_donate',
    'settlement_amount',
    'settlement_amount_from_child',
  ];

  constructor(
    private agentServ: AgentService,
    public bsModalRef: BsModalRef
  ) {

  }

  ngOnInit(): void {
    this.getAgentRecords();
  }

  getAgentRecords() {
    if (this.isSearchingToday()) {
      const data = this.realTimeRecords.agent_daily_report.filter(e => e.agents_id === this.user.id && e.is_top);
      return this.setDataSource(data);
    }
    const body = this.getBody();
    this.agentServ.getAgentDailyReportsSearchByTypeAndAgentBetweenDate(body).subscribe(r => {
      this.setDataSource(r.data);
    })
  }

  calcSettlementRecords(records: User[], settlementRecords: SettlementRecord[]) {
    return RecordsCalcSettlementRecords(records, settlementRecords, this.startDate, this.endDate);
  }

  setDataSource(data: User[]) {
    data = data.filter(e => e.is_top);
    this.dataSource.data = this.calcSettlementRecords(data, this.settlementRecords);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getBody() {
    return {
      game_types_id: GameTypeEnum.GameTypeSicBo,
      agents_id: this.user.id,
      end_date: this.endDate,
      start_date: this.startDate,
    }
  }

  isSearchingToday() {
    const today = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
    return today === this.startDate && today === this.endDate;
  }

  changeColor(val: number) {
    if (val > 0) {
      return 'text-success';
    }
    if (val < 0) {
      return 'text-danger';
    }
  }
}
