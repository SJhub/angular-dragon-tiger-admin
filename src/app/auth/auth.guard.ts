import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivateChild } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { UserRole } from '../enum/user-role.enum';
import { UserLogin } from '../models/user-login';
import { AuthenticationService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private router: Router,
    private translateService: TranslateService,
    private auth: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const userLoginStr = this.auth.getUserLogin();


    if (!this.auth.isUserLoggedIn() || !userLoginStr) {
      this.router.navigate(['/login']);
      return false;
    }

    const userLogin: UserLogin = JSON.parse(userLoginStr);
    if ((userLogin.identity === UserRole.agent || userLogin.identity === UserRole.topAgent) && !this.auth.getUserCloneId()) {

      // if (!userLogin.is_first_change_password && state.url !== "/my-account") {
      //   this.router.navigate(['/my-account']);
      //   alert('首次登入需修改密碼');
      //   return false;
      // }
    }

    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {

    let auth = localStorage.getItem('operationitemsid');
    let authManageRoute = childRoute.data.auth_manageID;
    const identity = localStorage.getItem('loginUserIdentity');


    if (auth === 'null' || auth === null) {
      return this.canActivate(childRoute, state);
    }

    if (identity !== '1' && identity !== '2') {
      return this.canActivate(childRoute, state);
    }

    auth = JSON.parse(auth);
    authManageRoute = String(authManageRoute);

    if (childRoute.component) {
      if (!auth.includes(authManageRoute)) {
        alert('無權訪問');
        return false;
      }
    }
    return this.canActivate(childRoute, state);
  }
}
