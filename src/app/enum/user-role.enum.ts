export enum UserRole {
    '最高權限' = 1,
    '管理者',
    '荷官',
    '超級代理',
    '代理',
    '直播主',
    '會員',
    '遊戲測試',
    '工作人員',
    '管理者(展示)',

    'admin' = 1,
    'management',
    'dealers',
    'topAgent',
    'agent',
    'streamer',
    'member',
    'gametester',
    'staff',
    'managementDemo'
}
