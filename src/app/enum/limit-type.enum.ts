export enum LimitTypeEnum {
  // LimitTypeNone -
  LimitTypeNone,
  // LimitTypeMaximum -
  LimitTypeMaximum,
  // LimitTypeMinimum -
  LimitTypeMinimum,
  // LimitTypeTotalMaximum -
  LimitTypeTotalMaximum,
}
