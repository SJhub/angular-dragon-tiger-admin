export enum GameResultTypeEnum {
    // Loss -
    Loss,
    // Win -
    Win,
    // NoLottery -
    NoLottery,
}
