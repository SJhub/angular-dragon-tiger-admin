export enum Type {
    Login = '登入',
    Logout = '登出',
    Online = '線上',
    Offline = '離線',
    PushNotify = '推播',
    Cancel = '取消',
}