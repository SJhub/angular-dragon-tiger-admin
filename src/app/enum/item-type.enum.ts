export enum ItemTypeEnum {
  // ItemTypeNone -
  ItemTypeNone,
  // ItemTypeBig -
  ItemTypeBig,
  // ItemTypeSmall -
  ItemTypeSmall,
  // ItemTypeOdd -
  ItemTypeOdd,
  // ItemTypeEven -
  ItemTypeEven,
  // ItemTypeSpecificDoubles1 -
  ItemTypeSpecificDoubles1,
  // ItemTypeSpecificDoubles2 -
  ItemTypeSpecificDoubles2,
  // ItemTypeSpecificDoubles3 -
  ItemTypeSpecificDoubles3,
  // ItemTypeSpecificDoubles4 -
  ItemTypeSpecificDoubles4,
  // ItemTypeSpecificDoubles5 -
  ItemTypeSpecificDoubles5,
  // ItemTypeSpecificDoubles6 -
  ItemTypeSpecificDoubles6,
  // ItemTypeSingleDice1 -
  ItemTypeSingleDice1,
  // ItemTypeSingleDice2 -
  ItemTypeSingleDice2,
  // ItemTypeSingleDice3 -
  ItemTypeSingleDice3,
  // ItemTypeSingleDice4 -
  ItemTypeSingleDice4,
  // ItemTypeSingleDice5 -
  ItemTypeSingleDice5,
  // ItemTypeSingleDice6 -
  ItemTypeSingleDice6,
  // ItemTypeAnyTriple -
  ItemTypeAnyTriple,
  // ItemTypeSpecificTriples1 -
  ItemTypeSpecificTriples1,
  // ItemTypeSpecificTriples2 -
  ItemTypeSpecificTriples2,
  // ItemTypeSpecificTriples3 -
  ItemTypeSpecificTriples3,
  // ItemTypeSpecificTriples4 -
  ItemTypeSpecificTriples4,
  // ItemTypeSpecificTriples5 -
  ItemTypeSpecificTriples5,
  // ItemTypeSpecificTriples6 -
  ItemTypeSpecificTriples6,

  // ItemTypePlayerWin -
  ItemTypePlayerWin,
  // ItemTypeBankerWin -
  ItemTypeBankerWin,
  // ItemTypeTie -
  ItemTypeTie,
  // ItemTypePlayerPair -
  ItemTypePlayerPair,
  // ItemTypeBankerPair -
  ItemTypeBankerPair,
  // ItemTypeBaccaBig -
  ItemTypeBaccaBig,
  // ItemTypeBaccaSmall -
  ItemTypeBaccaSmall,
  // ItemTypePlayerOdd -
  ItemTypePlayerOdd,
  // ItemTypePlayerEven -
  ItemTypePlayerEven,
  // ItemTypeBankerOdd -
  ItemTypeBankerOdd,
  // ItemTypeBankerven -
  ItemTypeBankerven,

  ItemTypeDragon,         // 龍
  ItemTypeTiger,          // 虎
  ItemTypeDragonTigerTie, // 和
  ItemTypeDragonOdd,      // 龍單
  ItemTypeDragonEven,     // 龍雙
  ItemTypeTigerOdd,       // 虎單
  ItemTypeTigerEven,      // 虎雙
  ItemTypeDragonBlack,    // 龍黑
  ItemTypeDragonRed,      // 龍紅
  ItemTypeTigerBlack,     // 虎黑
  ItemTypeTigerRed,       // 虎紅
}
