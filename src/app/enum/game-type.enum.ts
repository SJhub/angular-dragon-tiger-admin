export enum GameTypeEnum {
  // GameTypeNone -
  GameTypeNone,
  // GameTypeSicBo -
  GameTypeSicBo,
  // GameTypeBaccarat -
  GameTypeBaccarat,
  // GameTypeRoulette -
  GameTypeRoulette,
  // GameTypeDouDizhu -
  GameTypeDouDizhu,
  // GameTypeCow -
  GameTypeCow,
  // GameTypePushTongZi -
  GameTypePushTongZi,
  // GameTypeThirteen -
  GameTypeThirteen,
  // GameTypeFootBall -
  GameTypeFootBall,
  // GameTypeMahjong -
  GameTypeMahjong,
  // GameTypeMLB -
  GameTypeMLB,
  // GameTypeFingerGuessing -
  GameTypeFingerGuessing,
  // GameTypeDouDizhu2P -
  GameTypeDouDizhu2P,
  // GameTypeBasketball -
  GameTypeBasketball,
  // GameTypeCPBL-
  GameTypeCPBL,
  // GameTypeSuperLotto -
  GameTypeSuperLotto,
  // GameTypeDragonTiger
  GameTypeDragonTiger,
  // GameTypeBattleGames -
  GameTypeBattleGames = 99,
}
