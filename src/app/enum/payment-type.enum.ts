export enum PaymentType {
    Credit = 1,
    ConvenienceStore = 2,
    Transfer = 3
}