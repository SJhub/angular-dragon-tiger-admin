export enum AgentAuthority {
    OperationItemNone = 0,

    // 帳戶管理
    OperationItemUser,

    // 押注紀錄
    OperationItemBetRecordList,
    OperationItemBetRecordSetting,

    // 打賞紀錄
    OperationItemDonateList,
    OperationItemDonateSetting,

    // 遊戲紀錄
    OperationItemcalcItemResultList,
    OperationItemcalcItemResultSetting,

    // 進階查詢
    OperationItemSearchResultList,
    OperationItemSearchResultSetting,

    // 報表紀錄
    OperationItemReportRecordList,
    OperationItemReportRecordSetting,

    // 荷官操作
    OperationItemCroupierOperation,

    // 即時下注
    OperationItemDrawSettings,

    // 單注設置
    OperationItemSingleItems,

    // 項目設置
    OperationItemGameSettingsList,

    // 籌碼設置
    OperationItemBetAmounts,

    // 其他設置
    OperationItemOtherSettings,

    // 機器人設置
    OperationItemBotSettings,

    // 留言過濾
    OperationItemMessageBlacklists,

    // 即時公告
    OperationItemProclamations,

    // 一般公告
    OperationItemNormalProclamations,

    // 站內公告
    OperationItemInsiteProclamations,

    // 訊息編輯
    OperationItemProclamationsSetting,

    // 帳戶權限
    OperationItemAuthority,

    // 遊戲 - 骰寶
    SicBo,

    // 遊戲 - 百家樂
    Baccarat,

    // 遊戲 - 輪盤
    Roulette,

    // 遊戲 - 龍虎榜
    DragonTiger,

    // 遊戲設置
    OperationItemTimelyBet,

    // 廣告公告
    OperationItemAdProclamationsSetting,

    // 註冊會員
    OperationItemRegistrationMembers,

    // 打賞帳號
    OperationItemBotAccounts,

    // 虛擬人數
    OperationItemVirtualOnlineMembers,

    // 代理分身
    OperationItemCloneAgents,

    // 開獎修正記錄
    OperationItemRenewDrawResultRecord,

    // 報表校正
    OperationItemReCalcReport,

    // 開獎統計
    OperationItemLotteryStatistics,

    // 新增代理
    OperationItemMyAgents,
    // 新增會員
    OperationItemMyMembers,
    // 子帳號
    OperationItemMyClones,

    // 其他設置
    OperationItemVipRecords,

    // 線上付款
    OperationItemOnlinePayment,
}
