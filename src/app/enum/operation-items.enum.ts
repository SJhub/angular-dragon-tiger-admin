export enum OperationItems {
	// OperationItemsNone -
	OperationItemsNone = 0,
	// OperationItemsAgentList -
	OperationItemsAgentList,
	// OperationItemsAgentSetting -
	OperationItemsAgentSetting,
	// OperationItemsMemberList -
	OperationItemsMemberList,
	// OperationItemsMemberSetting -
	OperationItemsMemberSetting,
	// OperationItemsNewList -
	OperationItemsNewList,
	// OperationItemsNewSetting -
	OperationItemsNewSetting,
	// OperationItemsSystemLogList -
	OperationItemsSystemLogList,
	// OperationItemsSystemLogSetting -
	OperationItemsSystemLogSetting,
	// OperationItemsUserList -
	OperationItemsUserList,
	// OperationItemsUserSetting -
	OperationItemsUserSetting
}