export enum ProductTypeIcon {
    '3c.svg' = 1,
    'beauty.svg',
    'beauty_hand_cream.svg',
    'child.svg',
    'chips.svg',
    'cloth.svg',
    'delivery.svg',
    'furniture-and-household.svg',
    'hotel1.svg',
    'online.svg',
    'pet.svg',
    'spoon_fork.svg',
    'sports.svg',
    'thumb up.svg',
    'ticket.svg',
    'tv.svg'
}
