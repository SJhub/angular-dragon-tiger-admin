export enum ProclamationTypesEnum {
    // ProclamationTypeGeneral - 一般
    ProclamationTypeGeneral,
    // ProclamationTypeGame - 遊戲
    ProclamationTypeGame,
    // ProclamationTypeInSite - 站內
    ProclamationTypeInSite,
    // ProclamationTypeAd - 廣告
    ProclamationTypeAd
}
