export enum ChangeLogType {
    Update,
    Clear,
}

export enum ChangeLogColumn {
    PokerCredit = "poker_credit",
    PokerWallet = "poker_wallet",
    OtherCredit = "baccarat_credit",
    OtherWallet = "baccarat_wallet",
    SportCredit = "world_football_credit",
    SportWallet = "world_football_wallet",
    BaccaratWinTotalAmountLimit = "baccarat_win_total_amount_limit",
    BaccaratGameResultAmount = "baccarat_game_result_amount",
    CntOfSpecialCard = "cnt_of_special_card",
    AutoBet = "auto_bet",
    AutoBetResult = "auto_bet_result",
    WithdrawWallet = "withdraw_wallet",
    IsAllowPlay = "is_allow_play"
}

export enum ChangeLogColumnText {
    "poker_wallet" = "對戰遊戲",
    "baccarat_wallet" = "百家樂",
    "poker_credit" = "對戰遊戲",
    "baccarat_credit" = "百家樂",
    "baccarat_win_total_amount_limit" = "百家樂",
    "baccarat_game_result_amount" = "百家樂",
    "world_football_credit" = "體育賽事",
    "world_football_wallet" = "體育賽事",
    "is_allow_play" = "功能設定"
}

export enum ChangeLogColumnTypeText {
    "poker_wallet" = "剩餘餘額",
    "baccarat_wallet" = "剩餘餘額",
    "world_football_wallet" = "剩餘餘額",
    "poker_credit" = "信用額度",
    "baccarat_credit" = "信用額度",
    "world_football_credit" = "信用額度",
    "cnt_of_special_card" = "特殊牌型",
    "auto_bet" = "自動下注",
    "auto_bet_result" = "輸贏金額",
    "withdraw_wallet" = "獲利錢包",
    "is_allow_play" = "下注狀態"
}