export enum commonEnum {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  MALE_NAME = '男',
  FEMALE_NAME = '女',
  GENDER = '性別',
  BIRTHDAY = '生日',
  MUGSHOT = '大頭照',
  ACTICE = '啟用',
  INACTICE = '未啟用',
  PAYMENT = '付款方式',
  LICENSE_TYPE = '駕照類型',
  REVIEW_STATUS = '審核狀態',
  LATITUDE = '緯度',
  LONGITUDE = '經度',
  CREATED_TIME = '建立時間',
  UPDATED_TIME = '更新時間',
  COMPLATED_TIME = '完成時間',
  CANCELLED_TIME = '取消時間',
  ARRIVED_TIME = '抵達時間',
  TOOK_TIME = '接單時間',
  PAYMENT_TIME = '付款時間',
  YES = '1',
  NO = '0',
  YES_CHTNAME = '是',
  NO_CHTNAME = '否',
  CAR_CHTNAME = '汽車',
  MOTO_CHTNAME = '機車',
  BOTH_CHTNAME = '兩者都有',
  MEMBER = '會員',
  AUDIENCE = '受眾',
  AUDIENCE_MEMBER = '1',
  AUDIENCE_DRIVER = '2',
  AUDIENCE_BOTH = '3',
  LICENSE_PLATE_NUMBER = '車牌號碼',
  CAR_LICENSE_NUMBER = '汽車車牌號碼',
  MOTO_LICENSE_NUMBER = '機車車牌號碼',
  CAR_TYPE = '車輛類型',
  REMARK = '備註',
  ID = '編號',
  START_ADDRESS = '出發地址',
  END_ADDRESS = '到達地址',
  RATING = '評分',
  CONTACT_PERSON = '連絡人姓名',
  CONTACT_PHONE = '連絡人電話',
  PAY_AMOUNT = '付款金額',
  ORDER_NUMBER = '訂單號碼',
  FROM = '起點',
  END = '終點',
  CASH_NUMBER = '1',
  CREDIT_NUMBER = '2',
  CASH_NAME = '現金',
  CREDIT_NAME = '信用卡',
  NON_NAME = '無',
  ORDER_STATUS = '訂單狀態',
  INVALID_RESULT = '000',
  PROCLAMATION_TYPE_GENERAL = '0',
  PROCLAMATION_TYPE_GAME = '1',
}
