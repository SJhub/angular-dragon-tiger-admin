export enum GameActionTypeEnum {
    // Bet -
    Bet,
    // CancelBet -
    CancelBet,
    // Donate -
    Donate,
}
