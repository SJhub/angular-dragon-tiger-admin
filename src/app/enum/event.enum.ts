export enum EventEnum {
    // EventNewBoardMessage - 新留言訊息
    EventNewBoardMessage = 0,
    // EventNewDrawResult - 新開獎結果
    EventNewDrawResult,
    // EventNewGame - 開新局
    EventNewGame,
    // EventThisGameInvalid - 該局無效事件
    EventThisGameInvalid,
    // EventGameSecondsCountdown - 遊戲秒數倒數事件
    EventGameSecondsCountdown,
    // EventKickMember - 踢會員事件
    EventKickMember,
    // EventLivePush - 直播推流事件
    EventLivePush,
    // EventStopLivePush - 直播停止推流事件
    EventStopLivePush,
    // EventEntryAutoMode - 進入自動模式事件
    EventEntryAutoMode,
    // EventEntryManualMode - 進入手動模式事件
    EventEntryManualMode,
    // EventAllowMemberPlay - 允許會員進行遊戲
    EventAllowMemberPlay,
    // EventNotAllowMemberPlay - 不允許會員進行遊戲
    EventNotAllowMemberPlay,
    // EventRenewDrawResult - 更正開獎結果
    EventRenewDrawResult,
    // EventImmediateProclamation - 即時公告事件
    EventImmediateProclamation,
    // EventMemberLoginFromAnotherDevice - 會員從其他裝置登入
    EventMemberLoginFromAnotherDevice,
    // EventkickAllMember - 踢所有會員事件
    EventkickAllMember,
    // EventKickSomeMember - 踢除一些會員事件
    EventKickSomeMember,
    // EventAllowMembersPlay - 允許會員遊玩
    EventAllowMembersPlay,

}
