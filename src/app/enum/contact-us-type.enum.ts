export enum ContactUsType {
    CustomerService = 1,
    Cooperation,
    Complain
}

export enum ContactUsTypeText {
    '客服' = 1,
    '合作',
    '申訴'
}