export enum BetRecordStatusEnum {
    // Process - 進行中
    Process,
    // Finish - 完成
    Finish,
    // Unusual - 異常
    Unusual,
    // Deleted - 刪除
    Deleted,
    // Voided - 作廢
    Voided
}
