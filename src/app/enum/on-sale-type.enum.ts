export enum OnSaleType {
    ComboSet = 1,          //套餐組合
    FullDiscount = 2       //滿件優惠
}
