export enum newsEnum {
  TITLE = 'Title 名稱',
  PIC_PATH = '圖片位置',
  BODY = '內容',
  DEFAULT_NEWS_PIC = 'https://o.aolcdn.com/hss/storage/midas/9c4e8a638879a25c32623e07c8ba0dd6/202565057/OGB-INSIDER-BLOGS-GoogleLogox2-Animated.gif',
}
