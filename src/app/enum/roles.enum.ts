export enum Roles {
    ROLE_ADMIN = 1,
    ROLE_MANAGEMENT = 2,
    ROLE_STORE = 3,

    VALUE_ADMIN = `["ROLE_ADMIN","ROLE_MANAGEMENT","ROLE_TOP_AGENT","ROLE_AGENT","ROLE_MEMBER","ROLE_NORMAL"]`,
    VALUE_MANAGEMENT = `["ROLE_MANAGEMENT","ROLE_TOP_AGENT","ROLE_AGENT","ROLE_MEMBER","ROLE_NORMAL"]`,
    VALUE_TOP_AGENT = `["ROLE_TOP_AGENT","ROLE_AGENT","ROLE_MEMBER","ROLE_NORMAL"]`,
    VALUE_AGENT = `["ROLE_AGENT","ROLE_MEMBER","ROLE_NORMAL"]`,

    NAME_ADMIN = '管理者',
    NAME_MANAGEMENT = '一般商家',
    NAME_TOP_AGENT = '超級代理',
    NAME_AGENT = '一般代理',
}
