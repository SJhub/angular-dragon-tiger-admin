import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'imgFullPath'
})
export class ImgFullPathPipe implements PipeTransform {

  transform(value: any, picCatch = false): any {
    if (value == 'default') return 'assets/img/avatars/admin.png';

    let dir = localStorage.getItem('serverinfoPath');
    let host = localStorage.getItem('serverinfoLocalhost');
    let head = 'http';
    if (environment.fileServer == 'host') {
      //線上圖片位址
      host = localStorage.getItem('serverinfoHost');
      head = 'https';
    }

    if (picCatch) dir = 'upload';

    return `${head}://${host}/${dir}/${value}`;
  }
}
