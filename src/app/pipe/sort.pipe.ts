import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: string): string {
    if (value) {
      const split = value.split('');
      const sorted = split.sort((a, b) => Number(a) - Number(b));
      return sorted.join('');
    }
    return value;
  }

}
