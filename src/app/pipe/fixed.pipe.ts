import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fixed'
})
export class FixedPipe implements PipeTransform {

  transform(value: any) {
    return Number(value) ? parseFloat(Number(value).toFixed(2)) : 0;
  }

}
