export interface BotMembers {
    id: number;
    name: string;
    auto_donate_settings_id: number;
    game_types_id: number;
    edit: boolean;
}
