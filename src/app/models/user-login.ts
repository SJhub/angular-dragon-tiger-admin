export interface UserLogin {
    identity: number;
    is_first_change_password: number;
    last_login_at: string;
}
