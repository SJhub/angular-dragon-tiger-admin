export interface FileServerInfo {
    id: number;
    fileServerUrl: string;
    picPath: string;
    uploadUrlLocal: string;
}