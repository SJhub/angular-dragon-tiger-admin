import { User } from "./user";

export interface ChangeLog {
    id: number;
    operator_id: number;
    operator_role: number;
    operation_type: number;
    role: number;
    role_id: number;
    column_name: string;
    old_value: string;
    new_value: string;
    current_wallet: string;
    created_at: string;
    user: User;
    settlement_when_setting_log: SettlementWhenSettingLogs;

    is_result?: boolean;
}

export interface SettlementWhenSettingLogs {
    id: number;
    change_log_id: number;
    role: number;
    role_id: number;
    amount: number;
    status: number;
}