export interface SettlementRecord {
    id: number;
    agent_daily_reports_id: number;
    from_agents_id: number;
    to_agents_id: number;
    settlement_amount_to_parent: number;
    settlement_amount_from_child: number;
    own_profit: number;
    revenue_share: number;
    closing_date: string;
    game_types_id: number;
    created_at: string;
    updated_at: string;
}
