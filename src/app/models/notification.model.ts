export interface Notification {
    id: number;
    title: string;
    body: string;
    img: string;
    tokenGroupId: number;
}