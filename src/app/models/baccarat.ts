export interface Poker {
    suit: number | string;
    number: number;
}

export interface PokerResult {
    banker_detect: Poker[];
    player_detect: Poker[];
    banker: Poker[];
    player: Poker[];
    use_detect?: number;
}