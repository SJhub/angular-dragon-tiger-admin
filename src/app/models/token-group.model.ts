export interface TokenGroup {
    id: number;
    groupName: string;
    memberName: string;
    listType: number;
    memberList: string;
}