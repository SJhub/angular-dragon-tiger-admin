export interface ContactUs {
    id: number;
    types: number;
    line_id: string;
    line_url: string;
    wechat_id: string;
    wechat_url: string;
    status: number;
}
