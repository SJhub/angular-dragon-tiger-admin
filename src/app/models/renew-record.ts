export interface RenewRecord {
    users_id: number;
    game_types_id: number;
    game_number: number;
    old_draw_result: string;
    new_draw_result: string;
    updated_at: string;
}
