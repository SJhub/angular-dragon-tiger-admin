export interface Bank {
    id: number,
    bank_name: string,
    account_name: string,
    account_number: string,
}
