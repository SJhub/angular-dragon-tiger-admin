export interface PlayGame {
    id: number;
    game_types_id: number;
    game_number: number;
    countdown: number;
    draw_result: string;
    mode: number;
    created_at: string;
    game_result_amount?: number;
    total_amount?: number;
}
