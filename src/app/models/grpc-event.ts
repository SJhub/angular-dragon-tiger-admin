export interface GRPCEvent {
    game_types_id: number;
    type: number;
    value: string;
}
