export interface Banner {
    id: number,
    pic: string,
    url: string
}
