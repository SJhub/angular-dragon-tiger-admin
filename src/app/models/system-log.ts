import { SystemLogMessage } from './ststem-log-message';

export class System {
    created_at: string;
    id: number;
    ip: string;
    message: string;
    updated_at: string;
}