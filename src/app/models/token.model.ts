export interface Token {
    id: number;
    tokenType: string;
    memberName: string;
    token: string;
}