export interface CommissionSetting {
    game_types_id: number;
    commission_percent: number;
    stored_value_card_percent: number;
    commission_percent_cash: number;
}
