export interface SingleItems {
    id: number;
    item_types_id: any;
    agents_id: any;
    parents_id: number;
    amount: any;
    status: number;
    created_at: string;
    updated_at: string;
}
