export interface UpdateRebateTime {
    start_time: string;
    end_time: string;
    week: number;
}
