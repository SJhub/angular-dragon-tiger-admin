export class Member {
  id: number;
  account: string;
  name: string;
  agent_id: number;
  gender: string;
  birthday: string;
  mugshot: string;
  roles: string;
  status: number;
  created_at: string;
  updated_at: string;
}
