export interface MenuDetail {
    id: number;
    title: string;
    type: number;
    content: string;
    translate: string;
    pic: string;
}