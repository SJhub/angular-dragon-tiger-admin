import { GameResultTypeEnum } from '../enum/game-result-type.enum';

export class GameResultType {
    static getNameByID(type: number) {
        switch (type) {
            case GameResultTypeEnum.Loss:
                return '輸';
            case GameResultTypeEnum.Win:
                return '贏';
            case GameResultTypeEnum.NoLottery:
                return '無彩票';
            default:
                return '無';
        }
    }
}
