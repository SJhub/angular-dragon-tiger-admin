export interface MessageBlacklist {
    id: number;
    message: string;
    created_at: string;
    updated_at: string;
}
