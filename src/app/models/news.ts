export interface News {
    id: number,
    title: string,
    pic: string,
    description: string,
    type: number,
    body: string,
    link: string,
    status: number,
    audience: number;
    is_push_notify: number;
}
