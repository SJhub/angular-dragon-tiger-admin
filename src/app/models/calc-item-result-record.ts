import { PlayGame } from './play-game';

export interface CalcItemResultRecord {
    id: number;
    game_types_id: number;
    item_types_ids: any;
    game_number: number;
    count: number;
    is_renew: number;
    play_game?: PlayGame;
    use_detect?: number;
}
