import { AgentAuthority } from '../enum/agent-authority.enum';

export class Authority {
    static authorityManagement(managementID: number) {
        managementID = Number(managementID);
        // 帳戶
        if (managementID === AgentAuthority.OperationItemUser) { return 'users'; }

        // 紀錄
        if (managementID === AgentAuthority.OperationItemBetRecordList) { return 'system/bet-record-list'; }
        if (managementID === AgentAuthority.OperationItemBetRecordSetting) { return 'system/bet-records'; }

        if (managementID === AgentAuthority.OperationItemDonateList) { return 'system/bet-record-list'; }
        if (managementID === AgentAuthority.OperationItemDonateSetting) { return 'system/bet-records'; }

        if (managementID === AgentAuthority.OperationItemcalcItemResultList) { return 'system/bet-record-list'; }
        if (managementID === AgentAuthority.OperationItemcalcItemResultSetting) { return 'system/bet-records'; }

        if (managementID === AgentAuthority.OperationItemSearchResultList) { return 'system/bet-record-list'; }
        if (managementID === AgentAuthority.OperationItemSearchResultSetting) { return 'system/bet-records'; }

        if (managementID === AgentAuthority.OperationItemReportRecordList) { return 'system/report-record-list'; }
        if (managementID === AgentAuthority.OperationItemReportRecordSetting) { return 'system/bet-records'; }

        // 設定
        if (managementID === AgentAuthority.OperationItemCroupierOperation) { return 'system/croupier-operation'; }

        if (managementID === AgentAuthority.OperationItemDrawSettings) { return 'system/draw-settings'; }

        if (managementID === AgentAuthority.OperationItemSingleItems) { return 'system/single-items'; }

        if (managementID === AgentAuthority.OperationItemGameSettingsList) { return 'system/game-settings-list'; }

        if (managementID === AgentAuthority.OperationItemBetAmounts) { return 'system/bet-amounts'; }

        if (managementID === AgentAuthority.OperationItemOtherSettings) { return 'system/other-settings'; }

        if (managementID === AgentAuthority.OperationItemMessageBlacklists) { return 'system/message-blacklists'; }

        // 公告
        if (managementID === AgentAuthority.OperationItemProclamations) { return 'system/proclamations'; }

        if (managementID === AgentAuthority.OperationItemNormalProclamations) { return 'system/normal-proclamations'; }

        if (managementID === AgentAuthority.OperationItemInsiteProclamations) { return 'system/insite-proclamations'; }

        if (managementID === AgentAuthority.OperationItemProclamationsSetting) { return 'system/proclamations'; }

        // 帳戶權限
        if (managementID === AgentAuthority.OperationItemAuthority) { return 'system/agent-authority'; }

        return 'my-account';
    }
}
