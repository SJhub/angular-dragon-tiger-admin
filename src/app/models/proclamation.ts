export interface Proclamation {
    id: number;
    title: string;
    pic: string;
    type: number;
    body: string;
    status: number;
    created_at: string;
    updated_at: string;
    message?: any;
    edit?: boolean;
}
