export interface GameSetting {
    id: number;
    game_type: number;
    item_type: number;
    limit_type: number;
    amount: number;
}