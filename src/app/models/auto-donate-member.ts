export interface AutoDonateMember {
    id: number;
    name: string;
    game_types_id: number;
    edit: boolean;
}
