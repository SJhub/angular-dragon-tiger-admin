import { LimitTypeEnum } from '../enum/limit-type.enum';

export class LimitType {
    static getNameByID(type: number) {
        switch (type) {
            case LimitTypeEnum.LimitTypeMaximum:
                return '最大值';
            case LimitTypeEnum.LimitTypeMinimum:
                return '最小值';
            case LimitTypeEnum.LimitTypeTotalMaximum:
                return '單項最大值';
            default:
                return '無';
        }
    }
}
