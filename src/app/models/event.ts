export interface Event {
    id: number;
    type: number;
    value: string;
    status: number;
    created_at: string;
    updated_at: string;
}
