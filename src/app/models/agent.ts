export class Agent {
  id: number;
  name: string;
  real_name: string;
  status: number;
  credit: number;
  wallet: number;
  rebate: number;
  parent_id: number;
  created_at: string;
  updated_at: string;
  password: string;
  re_password: string;
}
