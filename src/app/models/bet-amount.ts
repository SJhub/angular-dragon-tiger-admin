export interface Betamount {
    id: number;
    game_types_id: number;
    amount: number;
    type: number;
    is_auto_bet: number;
    status: number;
    created_at: string;
    updated_at: string;
}
