import { OperationItems } from '../enum/operation-items.enum';

export class RouteManagement {
    static routeManagement(routeid: number) {

        //member
        if (routeid === OperationItems.OperationItemsMemberList) return 'members';
        if (routeid === OperationItems.OperationItemsMemberSetting) return 'member-edit/:id';

        //new
        if (routeid === OperationItems.OperationItemsNewList) return 'news';
        if (routeid === OperationItems.OperationItemsNewSetting) return 'news-edit/:id';

        //system
        if (routeid === OperationItems.OperationItemsSystemLogList) return 'system-log';

        //user
        if (routeid === OperationItems.OperationItemsUserList) return 'users';
        if (routeid === OperationItems.OperationItemsUserSetting) return 'user-add';

        //appadmin, my-account
        if (routeid === OperationItems.OperationItemsNone) return 'appadmin';
        if (routeid === OperationItems.OperationItemsNone) return 'my-account';

        return 'my-account';
    }
}