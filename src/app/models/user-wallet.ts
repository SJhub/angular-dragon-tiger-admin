export interface UserWallet {
    id: number;
    users_id: number;
    sic_bo_credit: number;
    sic_bo_wallet: number;
    poker_credit: number;
    baccarat_credit: number;
    world_football_credit: number;
    poker_wallet: number;
    baccarat_wallet: number;
    dragon_tiger_credit: number;
    dragon_tiger_wallet: number;
    world_football_wallet: number;
    world_football_rebate: number;
    world_football_revenue_share: number;
    withdraw_wallet: number;
    last_deposit_at: string;
    types: number;
    status: number;
}
