import { Member } from "./member.model";

export interface MemberDailyReport {
    id: number;
    members_id: number;
    member: Member;
    agents_id: number;
    account: string;
    game_result_amount: number;
    total_amount: number;
    active_total_amount: number;
    total_donate: number;
    total_stored_value_card: number;
    wash_code_commission_percent: number;
    wash_code_commission: number;
    number_of_game: number;
    settlement_amount: number;
    total_rebate: number;
    closing_date: string;
    is_settlement: number;
    settlement_type: number;
    game_types_id: number;
    rebate_percent: number;
    appeal_refund_amount: number;
    status: number;
    created_at: string;

    show_header?: boolean;
}
