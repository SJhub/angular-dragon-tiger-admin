import { User } from "./user";

export interface AgentDailyReport {
    id: number;
    agents_id: number;
    parent_id: number;
    name: string;
    account: string;
    total_amount: number;
    active_total_amount: number;
    total_donate: number;
    total_stored_value_card: number;
    platform_profit: number;
    own_profit: number;
    rebate_percent: number;
    rebate_amount: number;
    wash_code_commission_percent: number;
    wash_code_commission: number;
    settlement_wash_code_commission: number;
    revenue_share: number;
    number_of_game: number;
    game_result_amount: number;
    settlement_amount: number;
    member_settlement_amount: number;
    member_total_rebate: number;
    member_total_appeal_refund_amount: number;
    closing_date: string;
    game_types_id: number;
    is_settlement: number;
    is_top: number;
    settlement_type: number;
    commission_percent: number;
    commission: number;
    stored_value_card_percent: number;
    stored_value_card_commission: number;
    total_member_platform_commission: number;
    status: number;


    user?: User;
    show_header?: boolean;
}
