import { Member } from "./member.model";

export interface BetRecord {
    id: number;
    game_types_id: number;
    item_types_id: number;
    game_number: number;
    action: number;
    amount: number;
    members_id: number;
    result: number;
    draw_result: string;
    status: number;
    created_at: string;
    updated_at: string;
    key?: any;
    game_result_amount: any;
    commission_percent: number;
    member: Member;
}
