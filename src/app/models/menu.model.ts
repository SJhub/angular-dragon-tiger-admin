export interface Menu {
    id: number;
    category: number;
    nameCht: string;
    translate: string;
    menuDetailIds: string;
}