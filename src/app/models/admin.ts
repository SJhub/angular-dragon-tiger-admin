export interface Admin {
    id: number,
    name: string,
    password: string,
    real_name: string,
    roles: string,
    identity: number,
    management: any,
    status: number,
    created_at: string,
    updated_at: string,
    credit: any;
    wallet: any;
}
