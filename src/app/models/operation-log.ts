import { User } from "./user";

export interface OperationLog {
    device_type: number;
    users_id: number;
    identity: number;
    action_type: number;
    value: any;
    user?: User;
}
