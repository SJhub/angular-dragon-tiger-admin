export interface BoardMessage {
    id: number;
    game_types_id: number;
    board_number: number;
    message: string;
    members_id: number;
    created_at: string;
}
