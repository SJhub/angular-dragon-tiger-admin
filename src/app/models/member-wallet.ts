export interface MemberWallet {
    id: number;
    members_id: number;
    sic_bo_credit: number;
    sic_bo_wallet: number;
    poker_credit: number;
    baccarat_credit: number;
    poker_wallet: number;
    baccarat_wallet: number;
    dragon_tiger_credit: number;
    dragon_tiger_wallet: number;
    world_football_credit: number;
    world_football_wallet: number;
    world_football_rebate: number;
    world_football_revenue_share: number;
    baccarat_win_total_amount_limit: number;
    baccarat_game_result_amount: number;
    dragon_tiger_win_total_amount_limit: number;
    dragon_tiger_game_result_amount: number;
    wallet_settlement_type_baccarat: number;
    wallet_settlement_type_poker: number;
    wallet_settlement_type_world_football: number;

    status: number;
}
