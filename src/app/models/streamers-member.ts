export interface StreamersMember {
    id: number;
    account: string;
    name: string;
    nickname: string;
    password: string;
    parent_id: number;
}
