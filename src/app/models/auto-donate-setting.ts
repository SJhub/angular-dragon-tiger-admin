export interface AutoDonateSetting {
    id: number;
    game_types_id: number;
    start_time: string;
    end_time: string;
    interval_sec: number;
    count_of_member : number;
    status: number;
}
