import { GameTypeEnum } from '../enum/game-type.enum';

export class GameType {

    id: number;
    cht_name: string;
    en_name: string;
    member_count: number;
    mode: number;
    instruction: string;
    play_member_plus: number;
    time_to_open: string;
    time_to_close: string;
    time_to_rest: string;
    time_to_open_week: string;
    rest_min: number;
    rest_status: number;
    break_status: number;
    status: number;
    show_status: number;

    static getNameByID(type: number) {
        switch (type) {
            case GameTypeEnum.GameTypeSicBo:
                return '骰寶';
            case GameTypeEnum.GameTypeBaccarat:
                return '百家樂';
            case GameTypeEnum.GameTypeRoulette:
                return '輪盤';
            default:
                return '無';
        }
    }
}
