import { ItemTypeEnum } from '../enum/item-type.enum';

export class ItemType {
    static getNameByID(type: number) {
        switch (type) {
            case ItemTypeEnum.ItemTypeBig:
                return 'Big';
            case ItemTypeEnum.ItemTypeSmall:
                return 'Small';
            case ItemTypeEnum.ItemTypeOdd:
                return 'Odd';
            case ItemTypeEnum.ItemTypeEven:
                return 'Even';
            case ItemTypeEnum.ItemTypeSpecificDoubles1:
                return 'SpecificDoubles1';
            case ItemTypeEnum.ItemTypeSpecificDoubles2:
                return 'SpecificDoubles2';
            case ItemTypeEnum.ItemTypeSpecificDoubles3:
                return 'SpecificDoubles3';
            case ItemTypeEnum.ItemTypeSpecificDoubles4:
                return 'SpecificDoubles4';
            case ItemTypeEnum.ItemTypeSpecificDoubles5:
                return 'SpecificDoubles5';
            case ItemTypeEnum.ItemTypeSpecificDoubles6:
                return 'SpecificDoubles6';
            case ItemTypeEnum.ItemTypeSingleDice1:
                return 'SingleDice1';
            case ItemTypeEnum.ItemTypeSingleDice2:
                return 'SingleDice2';
            case ItemTypeEnum.ItemTypeSingleDice3:
                return 'SingleDice3';
            case ItemTypeEnum.ItemTypeSingleDice4:
                return 'SingleDice4';
            case ItemTypeEnum.ItemTypeSingleDice5:
                return 'SingleDice5';
            case ItemTypeEnum.ItemTypeSingleDice6:
                return 'SingleDice6';
            case ItemTypeEnum.ItemTypeSpecificTriples1:
                return 'SpecificTriples1';
            case ItemTypeEnum.ItemTypeSpecificTriples2:
                return 'SpecificTriples2';
            case ItemTypeEnum.ItemTypeSpecificTriples3:
                return 'SpecificTriples3';
            case ItemTypeEnum.ItemTypeSpecificTriples4:
                return 'SpecificTriples4';
            case ItemTypeEnum.ItemTypeSpecificTriples5:
                return 'SpecificTriples5';
            case ItemTypeEnum.ItemTypeSpecificTriples6:
                return 'SpecificTriples6';
            case ItemTypeEnum.ItemTypeAnyTriple:
                return 'AnyTriple';
            case ItemTypeEnum.ItemTypePlayerWin:
                return 'PlayerShort';
            case ItemTypeEnum.ItemTypeBankerWin:
                return 'BankerShort';
            case ItemTypeEnum.ItemTypeTie:
                return 'Tie';
            case ItemTypeEnum.ItemTypePlayerPair:
                return 'PlayerPair';
            case ItemTypeEnum.ItemTypeBankerPair:
                return 'BankerPair';
            case ItemTypeEnum.ItemTypeDragon:
                return 'Dragon';
            case ItemTypeEnum.ItemTypeDragonTigerTie:
                return 'DragonTigerTie';
            case ItemTypeEnum.ItemTypeTiger:
                return 'Tiger';
            default:
                return 'None';
        }
    }
}
