export interface ItemTypes {
    id: number;
    cht_name: string;
    en_name: string;
    odds: number;
}
