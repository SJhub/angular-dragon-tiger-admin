export class FileServerInfo {
  id: number;
  host: string;
  localhost: string;
  pic_path: string;
  created_at: string;
  updated_at: string;
}
