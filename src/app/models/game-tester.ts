export interface GameTester {
    id: number,
    account: string,
    name: string,
    nickname: string,
    gender: string,
    password: string
}
