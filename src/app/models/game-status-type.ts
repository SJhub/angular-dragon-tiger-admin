import { BetRecordStatusEnum } from '../enum/bet-record-status.enum';

export class GameStatusType {
    static getName(type: number) {
        switch (type) {
            case BetRecordStatusEnum.Process:
                return '進行中';
            case BetRecordStatusEnum.Finish:
                return '正常';
            case BetRecordStatusEnum.Unusual:
                return '異常';
            case BetRecordStatusEnum.Deleted:
                return '刪除';
            case BetRecordStatusEnum.Voided:
                return '作廢';
            default:
                return '無';
        }
    }
}
