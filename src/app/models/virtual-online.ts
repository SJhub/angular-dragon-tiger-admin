export interface VirtualOnline {
    id: number;
    game_types_id: number;
    start_time: string;
    end_time: string;
    number_of_member: number;
    status: number;
}
