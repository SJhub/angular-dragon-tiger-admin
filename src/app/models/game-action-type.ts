import { GameActionTypeEnum } from '../enum/game-action-type.enum';

export class GameActionType {
    static getNameByID(type: number) {
        switch (type) {
            case GameActionTypeEnum.Bet:
                return '押注';
            case GameActionTypeEnum.CancelBet:
                return '取消押注';
            case GameActionTypeEnum.Donate:
                return '打賞';
            default:
                return '無';
        }
    }
}
