import { AgentDailyReport } from "../models/agent-daily-report";
import { MemberDailyReport } from "../models/member-daily-report";
import { groupByKey } from "./common";

export function groupAgentDailyReport(data: AgentDailyReport[]) {
    let group = groupByKey(data, 'agents_id');
    let res: AgentDailyReport[] = [];
    Object.values(group).forEach((e: AgentDailyReport[]) => res.push(grandTotalAgentDailyReport(e)));
    return res;
}

export function groupMemberDailyReport(data: MemberDailyReport[]) {
    let group = groupByKey(data, 'members_id');
    let res: MemberDailyReport[] = [];
    Object.values(group).forEach((e: MemberDailyReport[]) => res.push(grandTotalMemberDailyReport(e)));
    return res;
}

// 代理日報表加總
export function grandTotalAgentDailyReport(data: AgentDailyReport[]): AgentDailyReport {
    let res = {
        // 總投注
        total_amount: 0,
        // 有效投注
        active_total_amount: 0,
        // 局數
        number_of_game: 0,
        // 輸贏結果
        member_settlement_amount: 0,
        // 會員輸贏結果
        game_result_amount: 0,
        // 佔成%
        revenue_share: 0,
        // 洗碼佣金
        wash_code_commission: 0,
        // 洗碼%
        wash_code_commission_percent: 0,
        // 打賞金額
        total_donate: 0,


        // 平台抽佣
        total_member_platform_commission: 0,
        // 平台退佣%
        commission_percent: 0,
        // 退佣獲利
        commission: 0,
        // 申訴結果
        member_total_appeal_refund_amount: 0,

        // 交收金額
        settlement_amount: 0,

    } as AgentDailyReport;

    data.forEach(e => {
        // 累加項目
        res.total_amount += e.total_amount;
        res.active_total_amount += e.active_total_amount;
        res.number_of_game += e.number_of_game;
        res.member_settlement_amount += e.member_settlement_amount;
        res.game_result_amount += e.game_result_amount;
        res.wash_code_commission += e.wash_code_commission;
        res.total_member_platform_commission += e.total_member_platform_commission;
        res.commission += e.commission;
        res.member_total_appeal_refund_amount += e.member_total_appeal_refund_amount;
        res.total_donate += e.total_donate;
        res.settlement_amount += e.settlement_amount;

        // 固定項目
        res.revenue_share = e.revenue_share;
        res.wash_code_commission_percent = e.wash_code_commission_percent;
        res.commission_percent = e.commission_percent;
        res.account = e.account;
        res.user = e.user;
    });
    return res;
}

// 會員日報表加總
export function grandTotalMemberDailyReport(data: MemberDailyReport[]): MemberDailyReport {
    let res = {
        // 總投注
        total_amount: 0,
        // 有效投注
        active_total_amount: 0,
        // 局數
        number_of_game: 0,
        // 輸贏結果
        game_result_amount: 0,
        // 洗碼佣金
        wash_code_commission: 0,
        // 洗碼%
        wash_code_commission_percent: 0,
        // 對戰退佣
        total_rebate: 0,
        // 申訴結果
        appeal_refund_amount: 0,
        // 打賞金額
        total_donate: 0,
        // 交收金額
        settlement_amount: 0,

    } as MemberDailyReport;

    data.forEach(e => {
        // 累加項目
        res.total_amount += e.total_amount;
        res.active_total_amount += e.active_total_amount;
        res.number_of_game += e.number_of_game;
        res.game_result_amount += e.game_result_amount;
        res.wash_code_commission += e.wash_code_commission;
        res.appeal_refund_amount += e.appeal_refund_amount;
        res.total_rebate += e.total_rebate;
        res.total_donate += e.total_donate;
        res.settlement_amount += e.settlement_amount;

        // 固定項目
        res.wash_code_commission_percent = e.wash_code_commission_percent;
        res.member = e.member
    });
    return res;
}