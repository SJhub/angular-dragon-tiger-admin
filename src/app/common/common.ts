import { formatDate } from "@angular/common";

export const MAX_WALLET_AMOUNT = 1000000000;
export const AGENT_ACCOUNT_LEN = 6; // 代理帳號長度
export const MEMBER_ACCOUNT_LEN = 8;// 會員帳號長度

export const ROBOT_ACCOUNT_RANGE = { min: 65000000, max: 75000000 }; // 機器人帳號區間

export const BANK_INFO_CHANGE_MAX_HOURS = 72; // 銀行資料修改72小時後才可提現

export const COMPANY_CASH = {
  revenue_share_cash: 0,
  wash_code_commission_percent_cash: 1,
  commission_percent_cash: 100,
}


export function getFormatedDateStr(value: string | number | Date, format: string) {
  if (value) {
    return formatDate(value, format, 'en-US');
  }
}

export function groupByKey(arr: Array<any>, key: string) {
  return arr.reduce(function (r: any, a: any) {
    r[a[key]] = r[a[key]] || [];
    r[a[key]].push(a);
    return r;
  }, Object.create(null));
}

export function getRandom(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export function genCode(len = 6) {
  return Math.random().toString(10).substr(2, len);
}

export function genAccounts(len: number, num: number, isMember = false) {
  let arr = [];
  while (arr.length < num) {
    let code = genCode(len);

    // 檢查帳號是否在機器人區間
    if (isMember && Number(code) >= ROBOT_ACCOUNT_RANGE.min && Number(code) <= ROBOT_ACCOUNT_RANGE.max) continue;

    // 檢查帳號是否重複
    if (arr.includes(code)) continue;

    arr.push(code);
  }
  return arr;
}


export function copyToClipboard(text: string) {
  let selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '0';
  selBox.style.top = '0';
  selBox.style.opacity = '0';
  selBox.value = text;
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  document.body.removeChild(selBox);
  alert('已複製');
}

export function checkCellphone(val: string) {
  let regexp = new RegExp('^09[0-9]{8}$');
  return regexp.test(val);
}

export function checkUpdateWashCodeTime(str: string) {
  try {
    let time: UpdateWashCodeTime = JSON.parse(str)[0];
    let now = new Date();
    let tmp = new Date();
    if (now.getDay() != time.week) return false;
    let startArr = time.start_time.split(':').map(e => Number(e));
    tmp.setHours(startArr[0], startArr[1]);
    if (now.getTime() < tmp.getTime()) return false;
    let endArr = time.end_time.split(':').map(e => Number(e));
    tmp.setHours(endArr[0], endArr[1]);
    if (now.getTime() > tmp.getTime()) return false;
    return true;
  } catch (error) {
    return false;
  }
}

// 補0
export function pad(str: string, max: number) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

export function changeColor(val: number) {
  if (val > 0) {
    return 'text-success';
  }
  if (val < 0) {
    return 'text-danger';
  }
}

interface UpdateWashCodeTime {
  week: number;
  start_time: string;
  end_time: string;
}