import { TranslateService } from "@ngx-translate/core";
import { AgentAuthority } from "../enum/agent-authority.enum";
import { AutoBetItemType } from "../enum/auto-bet-item-type.enum";
import { ItemTypeEnum } from "../enum/item-type.enum";
import { Poker, PokerResult } from "../models/baccarat";

export const API_TIMEOUT = 15000;// api回應超時設定(ms)

export const defaultAgentID = 99;

export const defaultAgentOperation = [
    AgentAuthority.OperationItemDonateList,
    // AgentAuthority.OperationItemcalcItemResultList,
    // AgentAuthority.OperationItemReportRecordList,
    // AgentAuthority.OperationItemDrawSettings,
];

export function addIndexToArray(arr: Array<any>) {
    let i = 0;
    arr.forEach(e => e.index = i += 1);
    return arr;
}

export function getDrawResultToBetItem(draw: string) {
    const arrDraw = draw.split('');
    const total = arrDraw.map(e => Number(e)).reduce((a, b) => a + b);
    const betItem = [];

    if (findReapetTimes(arrDraw) === 3) {
        betItem.push(ItemTypeEnum.ItemTypeAnyTriple);
    } else {
        // 小
        if (total >= 4 && total <= 10) {
            betItem.push(ItemTypeEnum.ItemTypeSmall);
        }
        // 大
        if (total >= 11 && total <= 17) {
            betItem.push(ItemTypeEnum.ItemTypeBig);
        }

        if (total % 2 === 0) {
            betItem.push(ItemTypeEnum.ItemTypeEven);
        } else {
            betItem.push(ItemTypeEnum.ItemTypeOdd);
        }
    }
    return betItem;
}

export function calcPoint(pokers: Poker[], count = 0) {
    count = count ? count : pokers.length;
    let point = 0;

    for (let index = 0; index < count; index++) {
        const poker = pokers[index];
        if (poker.number < 10) point += poker.number;
    }

    point %= 10;
    return point;
}

export function baccaratDrawResultStr(res: PokerResult, translateServ: TranslateService, color = false) {
    let bankerPoint = calcPoint(res.banker);
    let playerPoint = calcPoint(res.player);

    const point = `${translateServ.instant('poker.point')},`;
    const bankerWin = translateServ.instant('bet.BankerWin');
    const playerWin = translateServ.instant('bet.PlayerWin');
    const tie = translateServ.instant('bet.Tie');
    const bankerPair = `,${translateServ.instant('bet.BankerPair')}`;
    const playerPair = `,${translateServ.instant('bet.PlayerPair')}`;
    let resStr = '';
    let pairStr = '';
    let pointStr = '';

    if (res.banker[0].number === res.banker[1].number) {
        pairStr += bankerPair;
    }

    if (res.player[0].number === res.player[1].number) {
        pairStr += playerPair;
    }

    if (bankerPoint == playerPoint) {
        resStr = tie;
        if (color) resStr = `<b class="green">${resStr}</b>`;
        pointStr = bankerPoint + point;
    }

    if (bankerPoint > playerPoint) {
        resStr = bankerWin;
        if (color) resStr = `<b class="red">${resStr}</b>`;
        pointStr = bankerPoint + point;
    }

    if (bankerPoint < playerPoint) {
        resStr = playerWin;
        if (color) resStr = `<b class="blue">${resStr}</b>`;
        pointStr = playerPoint + point;
    }

    const itemStr = `${pointStr}${resStr}${pairStr}`;
    const drawStr = baccaratDrawCalc(res);
    return drawStr && !color ? drawStr : itemStr;
}

export function baccaratDrawCalc(res: PokerResult) {
    let str = '';

    // 檢查牌數
    if (res.banker.length < 2 || res.player.length < 2) return str;

    // 莊家前兩張合計點數
    const bankerBP = calcPoint(res.banker, 2);
    const playerBP = calcPoint(res.player, 2);

    // 例牌
    const natural = 8;
    if (bankerBP >= natural || playerBP >= natural) return str;

    // 是否需要補牌狀態
    let playerDrawFlag = false;
    let bankerDrawFlag = false;

    if (res.player.length === 2) {
        // 閒家補牌最低點數
        const playerDraw = 5;
        if (calcPoint(res.player, 2) <= playerDraw) playerDrawFlag = true;
    }

    if (res.banker.length === 2) {
        // 莊家補牌最低點數
        const bankerDraw = 2;
        if (bankerBP <= bankerDraw) bankerDrawFlag = true;

        let drawArr = [];

        // 閒家已補第3張牌
        if (res.player.length === 3) {
            // 閒家補牌點數
            let playerDrawNum = res.player[2].number;
            if (playerDrawNum >= 10) playerDrawNum = 0;

            switch (bankerBP) {
                case 3:
                    bankerDrawFlag = playerDrawNum !== 8;
                    break;
                case 4:
                    drawArr = [1, 8, 9, 0];
                    bankerDrawFlag = !drawArr.includes(playerDrawNum);
                    break;
                case 5:
                    drawArr = [1, 2, 3, 8, 9, 0];
                    bankerDrawFlag = !drawArr.includes(playerDrawNum);
                    break;
                case 6:
                    drawArr = [6, 7];
                    bankerDrawFlag = drawArr.includes(playerDrawNum);
                    break;
            }
        } else {
            if (!playerDrawFlag) {
                drawArr = [6, 7];
                bankerDrawFlag = !drawArr.includes(bankerBP);
            }
        }
    }

    const tmp = [];
    if (playerDrawFlag) tmp.push('閒家');
    if (bankerDrawFlag) tmp.push('莊家');
    str = tmp.length ? tmp.join(',') + '補牌' : '';

    return str;
}

export function baccaratPokerResultStr(res: string, serv: TranslateService) {
    try {
        const pokerRes: PokerResult = JSON.parse(res);
        const mapFunc = (e: Poker) => serv.instant(`poker.${e.suit}`) + e.number;
        const bankerPokers: string[] = pokerRes.banker.map(mapFunc);
        const playrPokers: string[] = pokerRes.player.map(mapFunc);
        const bankerStr = `${bankerPokers.join(',')}`
        const playerStr = `${playrPokers.join(',')}`
        return { banker: bankerStr, player: playerStr };
    } catch (error) {
        console.log(error);
    }
}

export function baccaratPicCatchToPokerResult(picCatch: string) {
    try {
        const res: PicCatch = JSON.parse(picCatch);
        const result: PokerResult = {
            banker_detect: res.banker.map(e => strToPoker(e)),
            player_detect: res.player.map(e => strToPoker(e)),
            banker: res.banker.map(e => strToPoker(e)),
            player: res.player.map(e => strToPoker(e)),
        }
        return result;
    } catch (error) {
        console.log(error);
    }
}

export function getDrawResultDetect(str: string) {
    try {
        const tmp: PokerResult = JSON.parse(str);
        return tmp.use_detect;
    } catch (error) {
        console.log(error);
        return 0;
    }
}

export function isDrawResultDifferent(str: string) {
    try {
        const tmp: PokerResult = JSON.parse(str);
        const banker = tmp.banker;
        const player = tmp.player;
        const bankerDetect = tmp.banker_detect;
        const playerDetect = tmp.player_detect;

        // 未使用辨識
        if (!tmp.use_detect) return false;

        // 判斷長度
        if (
            banker.length !== bankerDetect.length ||
            player.length !== playerDetect.length
        ) {
            return true;
        }

        const cmpFun = (p1: Poker, p2: Poker) => {
            if (p1.suit !== p2.suit || p1.number !== p2.number) {
                return true;
            }
        }

        for (let index = 0; index < banker.length; index++) {
            const p1 = banker[index];
            const p2 = bankerDetect[index];
            if (cmpFun(p1, p2)) return true;
        }

        for (let index = 0; index < player.length; index++) {
            const p1 = player[index];
            const p2 = playerDetect[index];
            if (cmpFun(p1, p2)) return true;
        }

    } catch (error) {
        return 0;
    }
}

function strToPoker(str: string): Poker {
    const arr = str.split(' ');
    let suit = arr[0];
    let number = Number(arr[1]);
    if (!number) {
        switch (arr[1]) {
            case "A":
                number = 1;
                break;
            case "J":
                number = 11;
                break;
            case "Q":
                number = 12;
                break;
            case "K":
                number = 13;
                break;
        }
    }
    return { suit, number };
}

export function numToStr(num: number) {
    let str = String(num);
    switch (num) {
        case 1:
            str = 'A';
            break;
        case 11:
            str = 'J';
            break;
        case 12:
            str = 'Q';
            break;
        case 13:
            str = 'K';
            break;
    }
    return str;
}

export function getAutoBetTypeByArr(arr: number[]) {
    let tmp = 0;
    arr.forEach(e => tmp += e);
    return tmp;
}

export function getArrByAutoBetType(type: number) {
    switch (type) {
        case 1:
            return [AutoBetItemType.DragonTiger];
        default:
            return [type];
    }
}

export function dragonTigerDrawResultStr(res: PokerResult, color = false) {
    let tigerPoint = res.banker[0].number;
    let dragonPoint = res.player[0].number;

    let resStr = '';

    if (tigerPoint == dragonPoint) {
        resStr = '和局';
        if (color) resStr = `<b class="green">${resStr}</b>`;
    }

    if (tigerPoint > dragonPoint) {
        resStr = '虎贏';
        if (color) resStr = `<b class="red">${resStr}</b>`;
    }

    if (dragonPoint > tigerPoint) {
        resStr = '龍贏';
        if (color) resStr = `<b class="blue">${resStr}</b>`;
    }

    return resStr;
}

export function dragonResultToPokerResult(str: string) {
    const splits = str.split('');
    const dragonNum = splits.length == 4 ? strToNum(splits[0]) : 0;
    const tigerNum = splits.length == 4 ? strToNum(splits[2]) : 0;
    const dragonSuit = splits.length == 4 ? strToSuit(splits[1]) : 'spades';
    const tigerSuit = splits.length == 4 ? strToSuit(splits[3]) : 'spades';
    const res = {
        banker: [{ number: tigerNum, suit: tigerSuit }],
        player: [{ number: dragonNum, suit: dragonSuit }]
    } as PokerResult;
    return res;
}

function strToNum(str: string) {
    let number = Number(str);
    switch (str) {
        case "A":
            number = 1;
            break;
        case "T":
            number = 10;
            break;
        case "J":
            number = 11;
            break;
        case "Q":
            number = 12;
            break;
        case "K":
            number = 13;
            break;
    }
    return number;
}

function strToSuit(str: string) {
    let suit = 'spades';
    switch (str) {
        case "c":
            suit = 'clubs';
            break;
        case "d":
            suit = 'diamonds';
            break;
        case "h":
            suit = 'hearts';
            break;
        case "s":
            suit = 'spades';
            break;
    }
    return suit;
}


function findReapetTimes(arr: Array<string>) {
    let times = 0;
    if (arr && arr.length) {
        const first = arr[0];
        arr.forEach(e => {
            if (first === e) {
                times += 1;
            }
        });
    }
    return times;
}

interface PicCatch {
    banker: string[];
    player: string[];
}