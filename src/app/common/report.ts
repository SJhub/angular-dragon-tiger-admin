import { formatDate } from '@angular/common';
import { from, zip, of } from 'rxjs';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { UserRole } from '../enum/user-role.enum';
import { Member } from '../models/member.model';
import { SettlementRecord } from '../models/settlement-record';
import { User } from '../models/user';
import { getToday } from './date';

// agents
export function UsersJoinRecordWithSettlementCalc(
  records: User[],
  users: User[],
  settlementRecords: SettlementRecord[],
  start: string,
  end: string
) {
  const settlementRecordsByDateRange = getSettlementRecordsByDateRange(settlementRecords, start, end);
  const firstEveryGroupRecords = groupSettlementRecords(settlementRecordsByDateRange);

  users.map(u => {
    u = setDefaultValue(u);

    // join recors
    const userRecords = getRecordsByUserID(u.id, records);
    u = mapRecordsToUser(u, userRecords);

    // calc settlement from child
    const userSettlementRecords = getSettlementRecordsByUserID(u.id, settlementRecordsByDateRange);
    const latestSettlementRecords = getLatestDateOfRecords(userSettlementRecords);
    const groupFirstRecords = firstEveryGroupRecords.filter(r => r.from_agents_id === u.id);

    u = mapSettlementRecordsToUser(u, latestSettlementRecords);
    u = mapSettlementRecordsToUser(u, groupFirstRecords, true);
  });

  return users.filter(e => e.total_amount);
}

export function RecordsCalcSettlementRecords(
  records: User[],
  settlementRecords: SettlementRecord[],
  start: string,
  end: string
) {
  const settlementRecordsByDateRange = getSettlementRecordsByDateRange(settlementRecords, start, end);
  const firstEveryGroupRecords = groupSettlementRecords(settlementRecordsByDateRange);
  records.map(r => {
    const groupFirstRecords = firstEveryGroupRecords.filter(g => g.from_agents_id === r.agents_id && g.closing_date === r.closing_date);
    const settlements = getSettlementRecordsByUserID(r.agents_id, settlementRecordsByDateRange).filter(s => s.closing_date === r.closing_date);

    r.settlement_amount_from_child = 0;
    r = mapSettlementRecordsToUser(r, settlements);
    r = mapSettlementRecordsToUser(r, groupFirstRecords, true);
  });
  return records;
}

export function CalcOwnRecord(self: User, agentRecords: User[], memberRecords: Member[]) {
  self = setDefaultValue(self);

  agentRecords.forEach(r => {
    self.total_amount += r.total_amount;
    self.active_total_amount += r.active_total_amount;
    self.number_of_game += r.number_of_game;
    self.game_result_amount += r.game_result_amount;
    self.total_donate += r.total_donate;
    self.settlement_amount += r.settlement_amount;
    self.wash_code_commission += r.wash_code_commission;
    self.member_settlement_amount += r.member_settlement_amount;
    self.own_profit += r.own_profit;
  });

  memberRecords.forEach(r => {
    self.total_amount += r.total_amount;
    self.active_total_amount += r.active_total_amount;
    self.number_of_game += r.number_of_game;
    self.game_result_amount += r.game_result_amount;
    self.total_donate += r.total_donate;
    self.settlement_amount += r.settlement_amount;
  });
  return calcAgentSettlementAmount(self);
}

export function AdminOwnRecord(self: User, records: User[]) {
  self = setDefaultValue(self);

  records.forEach(r => {
    self.total_amount += r.total_amount;
    self.active_total_amount += r.active_total_amount;
    self.number_of_game += r.number_of_game;
    self.game_result_amount += r.game_result_amount;
    self.total_donate += r.total_donate;
    self.settlement_amount += r.settlement_amount;
    self.wash_code_commission += r.wash_code_commission;
    self.member_settlement_amount += r.member_settlement_amount;
    self.own_profit += r.own_profit;
  });

  if (self.identity === UserRole.admin) {
    self.settlement_amount = 0;
    self.member_settlement_amount = 0;
    self.own_profit = 0;
  }

  return self;
}

// member
export function MemberJoinRecords(records: Member[], members: Member[]) {
  const latestRecords = getLatestMemberRecords(records);
  members.map(m => {
    m = setDefaultValueMember(m);

    const latestRecord = latestRecords.find(r => r.members_id === m.id);
    const memberRecords = records.filter(r => r.members_id === m.id);

    if (latestRecord) {
      m.wash_code_commission_percent = latestRecord.wash_code_commission_percent;
      memberRecords.forEach(r => {
        m.total_amount += r.total_amount;
        m.active_total_amount += r.active_total_amount;
        m.number_of_game += r.number_of_game;
        m.game_result_amount += r.game_result_amount;
        m.wash_code_commission += r.wash_code_commission;
        m.total_donate += r.total_donate;
        m.settlement_amount += r.settlement_amount;
      });
    }
  });
  return members.filter(e => e.total_amount);
}

export function CalcOwnProfitByID(id: number, records: SettlementRecord[], start: string, end: string) {
  const recordsByDateRange = getSettlementRecordsByDateRange(records, start, end);
  const firstEveryGroupRecords = groupSettlementRecords(recordsByDateRange);
  const firstRecords = firstEveryGroupRecords.filter(f => f.from_agents_id === id);
  let selfRecords = recordsByDateRange.filter(r => r.to_agents_id === id);
  selfRecords = selfRecords.concat(firstRecords);

  if (selfRecords.length) {
    return selfRecords.map(r => r.own_profit).reduce((a, b) => a + b);
  }
  return 0;
}

// 取報表最後結算日
export function getLastCalcDay() {
  const now = new Date();
  now.setDate(now.getDate() - 1);
  if (now.getHours() < 12) {
    now.setDate(now.getDate() - 1);
  }
  return formatDate(now, 'yyyy-MM-dd', 'en-US');
}

// 是週第一天且未結算
export function isWeekFirstDayBeforeCalc() {
  const today = new Date();
  const dayOfWeek = today.getDay();
  const nowDate = formatDate(today, 'yyyy-MM-dd', 'en-US');
  const weekStart = formatDate(today.setDate(today.getDate() - dayOfWeek), 'yyyy-MM-dd', 'en-US');
  return nowDate === weekStart && today.getHours() < 12;
}

// 是月第一天且未結算
export function isMonthFirstDayBeforeCalc() {
  const today = new Date();
  const year = today.getFullYear();
  const month = today.getMonth();
  const nowDate = formatDate(today, 'yyyy-MM-dd', 'en-US');
  const monthStart = formatDate(new Date(year, month, 1), 'yyyy-MM-dd', 'en-US');
  return nowDate === monthStart && today.getHours() < 12;
}

function getLatestMemberRecords(records: Member[]): Member[] {
  return groupMemberRecords(records);
}

// 檢查日期區間是否包含今日
export function isDateRangeIncludedToday(startDate: string, endDate: string) {
  const now = new Date(getToday().st).getTime();
  const t1 = new Date(startDate);
  const t2 = new Date(endDate);
  t1.setHours(0, 0, 0);
  t2.setHours(0, 0, 0);
  t2.setDate(t2.getDate() + 1);
  return now >= t1.getTime() && now <= t2.getTime();
}


function mapRecordsToUser(user: User, records: User[]) {
  if (records.length) {
    const latestRecord = records.sort((a, b) => a.id - b.id)[records.length - 1];
    user.closing_date = latestRecord.closing_date;
    user.revenue_share = latestRecord.revenue_share;
    user.wash_code_commission_percent = latestRecord.wash_code_commission_percent;

    records.forEach(r => {
      user.total_amount += r.total_amount;
      user.active_total_amount += r.active_total_amount;
      user.number_of_game += r.number_of_game;
      user.game_result_amount += r.game_result_amount;
      user.total_donate += r.total_donate;
      user.wash_code_commission += r.wash_code_commission;
      user.settlement_amount += r.settlement_amount;
      user.member_settlement_amount += r.member_settlement_amount;
      user.own_profit += r.own_profit;
    });
    user.wash_code_commission = Number(user.wash_code_commission.toFixed(2));
  }
  return user;
}

function mapSettlementRecordsToUser(user: User, records: SettlementRecord[], isFirst = false) {
  if (records.length) {
    if (isFirst) {
      user.settlement_amount_from_child = records.map(r => r.settlement_amount_from_child).reduce((a, b) => a + b);
    } else {
      user.settlement_amount_from_child = records.map(r => r.settlement_amount_to_parent).reduce((a, b) => a + b);
    }
  }
  return user;
}


function getRecordsByUserID(id: number, records: User[]) {
  return records.filter(r => r.agents_id === id);
}

function getSettlementRecordsByUserID(id: number, records: SettlementRecord[]) {
  return records.filter(r => r.to_agents_id === id);
}

function getLatestDateOfRecords(records: SettlementRecord[]) {
  const sortedRecords = records.sort((a, b) => new Date(a.closing_date).getTime() - new Date(b.closing_date).getTime());
  const latestRecord = sortedRecords[sortedRecords.length - 1];
  return records.filter(r => r.closing_date === latestRecord.closing_date);
}

function setDefaultValue(user: User) {
  user.agents_id = user.id;
  user.number_of_game = 0;
  user.total_amount = 0;
  user.active_total_amount = 0;
  user.number_of_game = 0;
  user.game_result_amount = 0;
  user.wash_code_commission = 0;
  user.total_donate = 0;
  user.settlement_amount = 0;
  user.settlement_amount_from_child = 0;
  user.member_settlement_amount = 0;
  user.own_profit = 0;
  return user;
}

function setDefaultValueMember(member: Member) {
  member.members_id = member.id;
  member.total_amount = 0;
  member.active_total_amount = 0;
  member.number_of_game = 0;
  member.game_result_amount = 0;
  member.wash_code_commission_percent = 0;
  member.wash_code_commission = 0;
  member.total_donate = 0;
  member.settlement_amount = 0;
  return member;
}

function getSettlementRecordsByDateRange(records: SettlementRecord[], start: string, end: string) {
  return records.filter(r => {
    const startDate = new Date(start).getTime();
    const endDate = new Date(end).getTime();
    const recordCD = new Date(r.closing_date).getTime();
    return recordCD >= startDate && recordCD <= endDate;
  });
}

function groupSettlementRecords(records: SettlementRecord[]) {
  const firstEveryGroupRecords: SettlementRecord[] = [];

  from(records)
    .pipe(
      groupBy(
        record => record.agent_daily_reports_id,
        r => r
      ),
      mergeMap(group => zip(of(group.key), group.pipe(toArray())))
    )
    .subscribe(g => {
      const firstRecord = g[1].sort((a, b) => a.id - b.id)[0];
      firstEveryGroupRecords.push(firstRecord);
    });
  return firstEveryGroupRecords;
}

function groupMemberRecords(records: Member[]) {
  const latestRecords = [];
  from(records)
    .pipe(
      groupBy(
        record => record.members_id,
        r => r
      ),
      mergeMap(group => zip(of(group.key), group.pipe(toArray())))
    )
    .subscribe(g => {
      const lastRecord = g[1].sort((a, b) => b.id - a.id)[0];
      latestRecords.push(lastRecord);
    });
  return latestRecords;
}

function calcAgentSettlementAmount(record: User) {
  // 總洗碼佣金 = 有效投注金額(洗碼量) * (洗碼佣金% / 100)
  const totalWC = record.active_total_amount * record.wash_code_commission_percent / 100

  // 自身洗碼佣金 = 總洗碼佣金 * (100% - 占成數 / 100)
  const ownWC = totalWC * ((100 - record.revenue_share) / 100);

  // 自身輸贏佔成 = 總輸贏佔成 * (100% - 占成數 / 100)
  const ownResult = record.game_result_amount * ((100 - record.revenue_share) / 100);

  // 會員總交收 = 會員(輸/贏) - 打賞金額
  const totalResultAmount = record.game_result_amount - record.total_donate;

  // 上級交收 = 自身輸贏佔成 + 自身洗碼佣金 - 打賞金額
  const settlementAmount = ownResult + ownWC - record.total_donate;

  record.wash_code_commission = ownWC;
  record.settlement_amount = settlementAmount;


  return record;
}