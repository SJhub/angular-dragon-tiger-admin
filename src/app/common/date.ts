import { getFormatedDateStr } from "./common";

// 報表換日時間
export const DATE_LINE_HOUR = 0;

// 明細紀錄保留1天
export const RECORDS_KEEP_DAYS = 1;

// 取今日
export function getToday(format = 'yyyy-MM-dd') {
    let st = new Date();
    let ed = new Date();
    st.setHours(0, 0, 0);
    ed.setHours(0, 0, 0);
    ed.setDate(ed.getDate() + 1);


    // 星期日中午12點後自動+1天
    let hour = new Date().getHours();
    if (st.getDay() == 0 && hour >= 12) {
        st.setDate(st.getDate() + 1);
        ed.setDate(st.getDate() + 1);
    }

    let stStr = getFormatedDateStr(st, format);
    let edStr = getFormatedDateStr(ed, format);
    return { st: stStr, ed: edStr };
}

// 取昨日
export function getYesterday(format = 'yyyy-MM-dd') {
    let today = getToday();
    let st = new Date(today.st);
    let ed = new Date(today.ed);
    st.setHours(0, 0, 0);
    ed.setHours(0, 0, 0);
    st.setDate(st.getDate() - 1);
    ed.setDate(ed.getDate() - 1);


    let stStr = getFormatedDateStr(st, format);
    let edStr = getFormatedDateStr(ed, format);
    return { st: stStr, ed: edStr };
}

// 取本週
export function getThisWeek(format = 'yyyy-MM-dd') {
    var curr = new Date(); // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6

    let ed = new Date(new Date().setDate(last + 1));
    let st = new Date(new Date().setDate(first + 1));


    // 檢查是否為星期日且為中午12點前
    let hour = new Date().getHours();
    if (new Date().getDay() == 0 && hour < 12) {
        st.setDate(st.getDate() - 7);
        ed.setDate(ed.getDate() - 7);
    }

    let stStr = getFormatedDateStr(st, format);
    let edStr = getFormatedDateStr(ed, format);
    return { st: stStr, ed: edStr };
}

// 取上週
export function getLastWeek(format = 'yyyy-MM-dd') {
    var curr = new Date(); // get current date
    var first = curr.getDate() - curr.getDay() - 7; // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6

    let st = new Date(new Date().setDate(first + 1));
    let ed = new Date(new Date().setDate(last + 1));

    // 檢查是否為星期日且為中午12點前
    let hour = new Date().getHours();
    if (new Date().getDay() == 0 && hour < 12) {
        st.setDate(st.getDate() - 7);
        ed.setDate(ed.getDate() - 7);
    }

    let stStr = getFormatedDateStr(st, format);
    let edStr = getFormatedDateStr(ed, format);
    return { st: stStr, ed: edStr };
}

// 取本月
export function getThisMonth(dateLine = 0, format = 'yyyy-MM-dd') {
    let date = new Date();
    let month = date.getMonth();
    let st = new Date(date.getFullYear(), month, 1);
    let ed = new Date(date.getFullYear(), month + 1, 0);

    // 檢查是否為本月開始日且跨日(預設0點)
    let hour = new Date().getHours();
    let isStartDay = getFormatedDateStr(st, format) == getFormatedDateStr(new Date(), format);
    if (isStartDay && hour < dateLine) {
        month -= 1;
    }

    st = new Date(date.getFullYear(), month, 1);
    ed = new Date(date.getFullYear(), month + 1, 0);

    let stStr = getFormatedDateStr(st, format);
    let edStr = getFormatedDateStr(ed, format);
    return { st: stStr, ed: edStr };
}

// 取上月
export function getLastMonth(dateLine = 0, format = 'yyyy-MM-dd') {
    let date = new Date();
    let month = date.getMonth();
    let st = new Date(date.getFullYear(), month - 1, 1);
    let ed = new Date(date.getFullYear(), month, 0);

    // 檢查是否為本月開始日且跨日(預設0點)
    let hour = new Date().getHours();
    let isStartDay = getFormatedDateStr(getThisMonth().st, format) == getFormatedDateStr(new Date(), format);
    if (isStartDay && hour < dateLine) {
        month -= 1;
    }

    st = new Date(date.getFullYear(), month - 1, 1);
    ed = new Date(date.getFullYear(), month, 0);


    let stStr = getFormatedDateStr(st, format);
    let edStr = getFormatedDateStr(ed, format);
    return { st: stStr, ed: edStr };
}

// 檢查日期是否超過指定天數
export function checkTodayIsBeforeDays(target: string, days: number) {
    let today = getToday().st;
    let before = new Date(today);
    let tmp = new Date(target);

    tmp.setHours(0, 0, 0);
    before.setHours(0, 0, 0);
    before.setDate(before.getDate() - days);

    return tmp.getTime() >= before.getTime();
}

// 以日期取時間區間
export function getTimeRangeByDate(target: string) {
    let date = new Date(target);
    let stStr = '00:00:00';
    let edStr = '00:00:00';

    switch (date.getDay()) {
        // 星期日
        case 0:
            if (date.getHours() < 12) {
                edStr = '12:00:00';
            } else {
                stStr = '12:00:00';
            }
            break;

        // 星期一
        case 1:
            stStr = '12:00:00';
            break;
    }

    return { st: stStr, ed: edStr };
}

export enum DateSearchType {
    Today,
    Yesterday,
    ThisWeek,
    LastWeek,
    ThisMonth,
    LastMonth,
}