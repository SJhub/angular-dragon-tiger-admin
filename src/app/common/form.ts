import { AbstractControl, FormGroup } from '@angular/forms';

/**
 * 將所有formControl標記為已碰觸
 * @param formGroup 
 */
export function markFormGroupAsTouched(formGroup: FormGroup) {
  Object.values(formGroup.controls).forEach(control => {
    control.markAsTouched();
    if ((control as any).controls) {
      markFormGroupAsTouched(control as FormGroup);
    }
  });
}

/**
 * 檢查formControl是否觸碰且未通過驗證
 * @param ctrl 
 */
export function isFormCtrlInvalid(ctrl: AbstractControl) {
  return ctrl?.touched && ctrl?.errors;
}

/**
 * formControl是否有errorCode
 * @param ctrl 
 * @param errorCode 
 */
export function isFormCtrlHasError(ctrl: AbstractControl, errorCode: string) {
  return ctrl?.touched && ctrl?.hasError(errorCode);
}

/**
 * 取得formControl驗證失敗描述
 * @param ctrl 
 */
export function getFormCtrlErrDesc(ctrl: AbstractControl) {
  const errors = ctrl?.errors;
  if (errors) {
    const keys = Object.keys(errors);
    const k = keys[0];
    const value = errors[k];
    switch (k) {
      case 'required':
        return '必填';
      case 'max':
        return `最大值${value.max}`;
      case 'min':
        return `最小值${value.min}`;
      case 'maxlength':
        return `最多${value.requiredLength}個字`;
      case 'minlength':
        return `最少${value.requiredLength}個字`;
      default:
        return value;
    }
  }
}