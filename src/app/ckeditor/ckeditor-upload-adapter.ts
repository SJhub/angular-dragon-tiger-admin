import { environment } from '../../environments/environment';
import { ImgFullPathPipe } from '../pipe/img-full-path.pipe';

export class CkeditorUploadAdapter {
    loader;  // your adapter communicates to CKEditor through this
    constructor(loader) {
        this.loader = loader;
        console.log('Upload Adapter Constructor', this.loader);
    }


    upload() {
        return this.loader.file
            .then(uploadedFile => {
                return new Promise((resolve, reject) => {
                    const data = new FormData();
                    let fileName = `${Math.floor(Date.now() / 100)}-${uploadedFile.name}`;
                    data.append('file', uploadedFile, fileName);
                    const axios = require('axios').default;
                    // let picStoregeURL = 'http://localhost:3000/public/uploads';
                    axios({
                        url: `${environment.apiUrl}/api/v1/upload`,
                        method: 'post',
                        data,
                        headers: {
                            'Content-Type': 'multipart/form-data;'
                        },
                        withCredentials: false
                    })
                        .then(response => {
                            console.log(response);
                            if (response.data.code == 200) {
                                resolve({ default: this.getFullPath(response.data.data) });
                            } else {
                                reject(response.data.message);
                            }
                        })
                        .catch(response => {
                            reject('Upload failed');
                        });
                });
            });
    }

    abort() {
        console.log('UploadAdapter abort');
    }

    getFullPath(name) {
        return new ImgFullPathPipe().transform(name);
    }
}
