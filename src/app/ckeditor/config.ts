import { environment } from '../../environments/environment';

export const options: Object = {
    filebrowserUploadUrl: `${environment.apiUrl}/api/v1/editor-upload`,
    uploadUrl: `${environment.apiUrl}/api/v1/editor-upload`,
    filebrowserUploadMethod: 'post',
    format_tags: 'p;h1;h2;h3;pre;div',
    allowedContent: true,

}