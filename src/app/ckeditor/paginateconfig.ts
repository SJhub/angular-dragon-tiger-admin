import { PaginationInstance } from 'ngx-pagination';

export class PaginateConfig {
    config: PaginationInstance = {
        id: 'custom',
        itemsPerPage: 10,
        currentPage: 1,
    };
    constructor(id?: string, itemsPerPage?: number) {
        this.config.id = id || this.config.id;
        this.config.itemsPerPage = itemsPerPage || this.config.itemsPerPage;
    }
};
