import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelyBetItemComponent } from './timely-bet-item.component';

describe('TimelyBetItemComponent', () => {
  let component: TimelyBetItemComponent;
  let fixture: ComponentFixture<TimelyBetItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelyBetItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelyBetItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
