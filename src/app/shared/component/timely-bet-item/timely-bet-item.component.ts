import { Component, Input, OnInit } from '@angular/core';
import { ItemTypeEnum } from '../../../enum/item-type.enum';
import { BetRecord } from '../../../models/bet-record';

@Component({
  selector: 'app-timely-bet-item',
  templateUrl: './timely-bet-item.component.html',
  styleUrls: ['./timely-bet-item.component.scss']
})
export class TimelyBetItemComponent implements OnInit {
  @Input() set records(data: BetRecord[]) {
    if (data) {
      this.patchValue(data);
    }
  }

  dragonAmount = 0;
  tigerAmount = 0;
  tieAmount = 0;

  constructor() { }

  ngOnInit(): void {
  }

  patchValue(data: BetRecord[]) {
    this.dragonAmount = this.getTotalAmountByType(data, ItemTypeEnum.ItemTypeDragon);
    this.tigerAmount = this.getTotalAmountByType(data, ItemTypeEnum.ItemTypeTiger);
    this.tieAmount = this.getTotalAmountByType(data, ItemTypeEnum.ItemTypeDragonTigerTie);
  }

  getTotalAmountByType(data: BetRecord[], type: number) {
    let tmp = 0;
    let r = data.filter(e => e.item_types_id === type);
    r.forEach(e => tmp += e.amount);
    return tmp;
  }
}
