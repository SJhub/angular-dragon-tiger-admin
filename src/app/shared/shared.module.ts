import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadFileComponent } from '../component/upload-file/upload-file.component';
import { ImgFullPathPipe } from '../pipe/img-full-path.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageCropperComponent } from '../component/image-cropper/image-cropper.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EnumToArrayPipe } from '../pipe/enum-to-array.pipe';
import { EnumValueToEnumKeyPipe } from '../pipe/enum-value-to-enum-key.pipe';
import { CKEditorModule } from 'ckeditor4-angular';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { RecaptchaModule } from 'ng-recaptcha';
import { MatTabsModule } from '@angular/material/tabs';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FixedPipe } from '../pipe/fixed.pipe';
import { AdvancedSearchingComponent } from '../component/advanced-searching/advanced-searching.component';
import { TranslateModule } from '@ngx-translate/core';
import { SortPipe } from '../pipe/sort.pipe';
import { CalcItemRecordPanelComponent } from '../component/calc-item-record-panel/calc-item-record-panel.component';
import { DebounceClickDirective } from '../directiv/debounce-click.directive';
import { BaccaratPanelComponent } from '../component/baccarat-panel/baccarat-panel.component';
import { WebcamModule } from 'ngx-webcam';
import { AiWebcamComponent } from '../component/ai-webcam/ai-webcam.component';
import { RouterModule } from '@angular/router';
import { AllowGameComponent } from '../component/allow-game/allow-game.component';
import { CommissionSettingComponent } from '../component/commission-setting/commission-setting.component';
import { ConfirmModalComponent } from '../component/confirm-modal/confirm-modal.component';
import { AbsPipe } from '../pipe/abs.pipe';
import { SpinnerComponent } from './component/spinner/spinner.component';
import { TimelyBetItemComponent } from './component/timely-bet-item/timely-bet-item.component';
import { DragonTigerPanelComponent } from '../component/dragon-tiger-panel/dragon-tiger-panel.component';

@NgModule({
  declarations: [
    UploadFileComponent,
    ImageCropperComponent,
    ImgFullPathPipe,
    EnumToArrayPipe,
    EnumValueToEnumKeyPipe,
    FixedPipe,
    SortPipe,
    AbsPipe,
    AdvancedSearchingComponent,
    CalcItemRecordPanelComponent,
    DebounceClickDirective,
    BaccaratPanelComponent,
    DragonTigerPanelComponent,
    AiWebcamComponent,
    AllowGameComponent,
    CommissionSettingComponent,
    ConfirmModalComponent,
    SpinnerComponent,
    TimelyBetItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ImageCropperModule,
    RecaptchaModule,
    AngularSvgIconModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    TranslateModule,
    WebcamModule,
  ],
  exports: [
    UploadFileComponent,
    ImageCropperComponent,
    ImgFullPathPipe,
    EnumToArrayPipe,
    EnumValueToEnumKeyPipe,
    FixedPipe,
    SortPipe,
    AbsPipe,
    NgxPaginationModule,
    FormsModule,
    CollapseModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    ImageCropperModule,
    CKEditorModule,
    MatDatepickerModule,
    MatRadioModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    RecaptchaModule,
    MatTabsModule,
    AngularSvgIconModule,
    AdvancedSearchingComponent,
    TranslateModule,
    CalcItemRecordPanelComponent,
    DebounceClickDirective,
    BaccaratPanelComponent,
    DragonTigerPanelComponent,
    AiWebcamComponent,
    AllowGameComponent,
    CommissionSettingComponent,
    SpinnerComponent,
    TimelyBetItemComponent
  ],
  entryComponents: [
    ConfirmModalComponent,
  ],
})
export class SharedModule { }
