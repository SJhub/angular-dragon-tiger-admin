/**
 * @fileoverview gRPC-Web generated client stub for pb
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.pb = require('./game_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.GameGreeterClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pb.GameGreeterPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.LoginRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_MemberLogin = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/MemberLogin',
  grpc.web.MethodType.UNARY,
  proto.pb.LoginRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.LoginRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_MemberLogin = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.LoginRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.memberLogin =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/MemberLogin',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_MemberLogin,
      callback);
};


/**
 * @param {!proto.pb.LoginRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.memberLogin =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/MemberLogin',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_MemberLogin);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.LogoutRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_MemberLogout = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/MemberLogout',
  grpc.web.MethodType.UNARY,
  proto.pb.LogoutRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.LogoutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.LogoutRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_MemberLogout = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.LogoutRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.LogoutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.memberLogout =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/MemberLogout',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_MemberLogout,
      callback);
};


/**
 * @param {!proto.pb.LogoutRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.memberLogout =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/MemberLogout',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_MemberLogout);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.MemberRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_GetMember = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetMember',
  grpc.web.MethodType.UNARY,
  proto.pb.MemberRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.MemberRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.MemberRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_GetMember = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.MemberRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.MemberRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getMember =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/GetMember',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetMember,
      callback);
};


/**
 * @param {!proto.pb.MemberRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.getMember =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/GetMember',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetMember);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.MessageRequest,
 *   !proto.pb.MessageReply>}
 */
const methodDescriptor_GameGreeter_AddMessage = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/AddMessage',
  grpc.web.MethodType.UNARY,
  proto.pb.MessageRequest,
  proto.pb.MessageReply,
  /**
   * @param {!proto.pb.MessageRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.MessageReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.MessageRequest,
 *   !proto.pb.MessageReply>}
 */
const methodInfo_GameGreeter_AddMessage = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.MessageReply,
  /**
   * @param {!proto.pb.MessageRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.MessageReply.deserializeBinary
);


/**
 * @param {!proto.pb.MessageRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.MessageReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.MessageReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.addMessage =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/AddMessage',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_AddMessage,
      callback);
};


/**
 * @param {!proto.pb.MessageRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.MessageReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.addMessage =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/AddMessage',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_AddMessage);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.MessageRequest,
 *   !proto.pb.MessageReply>}
 */
const methodDescriptor_GameGreeter_GetLastMessage = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetLastMessage',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.pb.MessageRequest,
  proto.pb.MessageReply,
  /**
   * @param {!proto.pb.MessageRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.MessageReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.MessageRequest,
 *   !proto.pb.MessageReply>}
 */
const methodInfo_GameGreeter_GetLastMessage = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.MessageReply,
  /**
   * @param {!proto.pb.MessageRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.MessageReply.deserializeBinary
);


/**
 * @param {!proto.pb.MessageRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.pb.MessageReply>}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getLastMessage =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/pb.GameGreeter/GetLastMessage',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetLastMessage);
};


/**
 * @param {!proto.pb.MessageRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.pb.MessageReply>}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterPromiseClient.prototype.getLastMessage =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/pb.GameGreeter/GetLastMessage',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetLastMessage);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GameTypeRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_GetGameTypes = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetGameTypes',
  grpc.web.MethodType.UNARY,
  proto.pb.GameTypeRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.GameTypeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GameTypeRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_GetGameTypes = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.GameTypeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.GameTypeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getGameTypes =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/GetGameTypes',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetGameTypes,
      callback);
};


/**
 * @param {!proto.pb.GameTypeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.getGameTypes =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/GetGameTypes',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetGameTypes);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.ItemTypeRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_GetItemTypes = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetItemTypes',
  grpc.web.MethodType.UNARY,
  proto.pb.ItemTypeRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.ItemTypeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.ItemTypeRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_GetItemTypes = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.ItemTypeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.ItemTypeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getItemTypes =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/GetItemTypes',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetItemTypes,
      callback);
};


/**
 * @param {!proto.pb.ItemTypeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.getItemTypes =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/GetItemTypes',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetItemTypes);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.SystemSettingRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_GetSystemSettings = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetSystemSettings',
  grpc.web.MethodType.UNARY,
  proto.pb.SystemSettingRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.SystemSettingRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.SystemSettingRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_GetSystemSettings = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.SystemSettingRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.SystemSettingRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getSystemSettings =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/GetSystemSettings',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetSystemSettings,
      callback);
};


/**
 * @param {!proto.pb.SystemSettingRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.getSystemSettings =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/GetSystemSettings',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetSystemSettings);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.EventRequest,
 *   !proto.pb.EventReply>}
 */
const methodDescriptor_GameGreeter_GetLastEvent = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetLastEvent',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.pb.EventRequest,
  proto.pb.EventReply,
  /**
   * @param {!proto.pb.EventRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.EventReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.EventRequest,
 *   !proto.pb.EventReply>}
 */
const methodInfo_GameGreeter_GetLastEvent = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.EventReply,
  /**
   * @param {!proto.pb.EventRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.EventReply.deserializeBinary
);


/**
 * @param {!proto.pb.EventRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.pb.EventReply>}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getLastEvent =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/pb.GameGreeter/GetLastEvent',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetLastEvent);
};


/**
 * @param {!proto.pb.EventRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.pb.EventReply>}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterPromiseClient.prototype.getLastEvent =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/pb.GameGreeter/GetLastEvent',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetLastEvent);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.GameTypeRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_GetSecondsLeftOfGameType = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetSecondsLeftOfGameType',
  grpc.web.MethodType.UNARY,
  proto.pb.GameTypeRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.GameTypeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.GameTypeRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_GetSecondsLeftOfGameType = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.GameTypeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.GameTypeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getSecondsLeftOfGameType =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/GetSecondsLeftOfGameType',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetSecondsLeftOfGameType,
      callback);
};


/**
 * @param {!proto.pb.GameTypeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.getSecondsLeftOfGameType =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/GetSecondsLeftOfGameType',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetSecondsLeftOfGameType);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.ActionRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_Action = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/Action',
  grpc.web.MethodType.UNARY,
  proto.pb.ActionRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.ActionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.ActionRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_Action = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.ActionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.ActionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.action =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/Action',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_Action,
      callback);
};


/**
 * @param {!proto.pb.ActionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.action =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/Action',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_Action);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pb.BetRecordRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodDescriptor_GameGreeter_GetBetRecords = new grpc.web.MethodDescriptor(
  '/pb.GameGreeter/GetBetRecords',
  grpc.web.MethodType.UNARY,
  proto.pb.BetRecordRequest,
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.BetRecordRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pb.BetRecordRequest,
 *   !proto.pb.GeneralReply>}
 */
const methodInfo_GameGreeter_GetBetRecords = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pb.GeneralReply,
  /**
   * @param {!proto.pb.BetRecordRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pb.GeneralReply.deserializeBinary
);


/**
 * @param {!proto.pb.BetRecordRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pb.GeneralReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pb.GeneralReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pb.GameGreeterClient.prototype.getBetRecords =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pb.GameGreeter/GetBetRecords',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetBetRecords,
      callback);
};


/**
 * @param {!proto.pb.BetRecordRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pb.GeneralReply>}
 *     Promise that resolves to the response
 */
proto.pb.GameGreeterPromiseClient.prototype.getBetRecords =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pb.GameGreeter/GetBetRecords',
      request,
      metadata || {},
      methodDescriptor_GameGreeter_GetBetRecords);
};


module.exports = proto.pb;

