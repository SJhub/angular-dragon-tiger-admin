#!/bin/bash


rm -rf ./pb/ts/*

protoc -I . game.proto --js_out=import_style=commonjs:./pb/ts --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/ts

exit 0
